<?php
defined('BASEPATH') or exit('No direct script access allowed');


$route['default_controller'] = 'main/Main_controller/admin_login_panel';
$route['admin'] = 'main/Main_controller/admin_login_panel';
$route['somobay-office'] = 'main/Main_controller/somobay_office_link_page';
$route['link_page'] = 'main/Main_controller/link_page';
$route['dashboard'] = 'main/Main_controller/dashboard';
$route['addMember'] = 'main/Main_controller/addMember';
$route['memberAccounts'] = 'main/Main_controller/memberAccounts';
$route['takaUttolonChahida'] = 'main/Main_controller/takaUttolonChahida';
$route['sonchoyAmanothUttolon'] = 'main/Main_controller/sonchoyAmanothUttolon';
$route['memberList'] = 'main/Main_controller/memberList';
$route['memberListView'] = 'main/Main_controller/memberListView';
$route['memberListPending'] = 'main/Main_controller/memberListPending';
$route['memberListApproved'] = 'main/Main_controller/memberListApproved';
$route['c_68'] = 'main/Main_controller/c_68';
$route['c_68_list'] = 'main/Main_controller/c_68_list';
$route['c_32'] = 'main/Main_controller/c_32_p';
$route['memberListView_accountant'] = 'main/Main_controller/memberListView_accountant';
$route['memberListView_general'] = 'main/Main_controller/memberListView_general';
$route['amanoth_add'] = 'main/Main_controller/amanoth_add';
$route['sodorsho_nitimala_add'] = 'main/Main_controller/sodorsho_nitimala_add';
$route['akalinAmanoth_add'] = 'main/Main_controller/akalinAmanoth_add';
$route['mashikAmanoth_add'] = 'main/Main_controller/mashikAmanoth_add';
$route['amanoth_list'] = 'main/Main_controller/amanoth_list';
$route['amanoth_view'] = 'main/Main_controller/amanoth_view';
$route['mashikAmanoth_list'] = 'main/Main_controller/mashikAmanoth_list';
$route['mashikAmanoth_view'] = 'main/Main_controller/mashikAmanoth_view';
$route['akalinAmanoth_list'] = 'main/Main_controller/akalinAmanoth_list';
$route['akalinAmanoth_view'] = 'main/Main_controller/akalinAmanoth_view';
$route['beniyog_add'] = 'main/Main_controller/beniyog_add';
$route['beniyog_list'] = 'main/Main_controller/beniyog_list';
$route['beniyog_view'] = 'main/Main_controller/beniyog_view';
$route['besses_beniyog_add'] = 'main/Main_controller/besses_beniyog_add';
$route['besses_beniyog_list'] = 'main/Main_controller/besses_beniyog_list';
$route['besses_beniyog_view'] = 'main/Main_controller/besses_beniyog_view';
$route['biniyog_kotiyan'] = 'main/Main_controller/biniyog_kotiyan';
$route['biniyog_kotiyan_search'] = 'main/Main_controller/biniyog_kotiyan_search';
$route['biniyog_paper_return'] = 'main/Main_controller/biniyog_paper_return';
$route['biniyog_grohonkarir_chahida'] = 'main/Main_controller/biniyog_grohonkarir_chahida';
$route['ponno_kroy_bikry_chukti'] = 'main/Main_controller/ponno_kroy_bikry_chukti';
$route['sodorsho_protahar'] = 'main/Main_controller/sodorsho_protahar';
$route['biniyog_paper_return_list'] = 'main/Main_controller/biniyog_paper_return_list';
$route['biniyog_paper_return_view'] = 'main/Main_controller/biniyog_paper_return_view';
$route['biniyog_grohonkarir_chahida_list'] = 'main/Main_controller/biniyog_grohonkarir_chahida_list';
$route['biniyog_grohonkarir_chahida_view'] = 'main/Main_controller/biniyog_grohonkarir_chahida_view';
$route['ponno_kroy_bikry_chukti_list'] = 'main/Main_controller/ponno_kroy_bikry_chukti_list';
$route['ponno_kroy_bikry_chukti_view'] = 'main/Main_controller/ponno_kroy_bikry_chukti_view';
$route['sodorsho_protahar_list'] = 'main/Main_controller/sodorsho_protahar_list';
$route['sodorsho_protahar_view'] = 'main/Main_controller/sodorsho_protahar_view';
$route['code_8'] = 'main/Main_controller/code_8';
$route['code_9'] = 'main/Main_controller/code_9';
$route['code_10'] = 'main/Main_controller/code_10';
$route['c_27'] = 'main/Main_controller/c_27';
$route['c_28'] = 'main/Main_controller/c_28';
$route['c_29'] = 'main/Main_controller/c_29';
$route['c_153'] = 'main/Main_controller/c_153';
$route['c_31'] = 'main/Main_controller/c_31';
$route['c_30'] = 'main/Main_controller/c_30';
$route['c_79'] = 'main/Main_controller/c_79';
$route['c_42'] = 'main/Main_controller/c_42';
$route['c_80'] = 'main/Main_controller/c_80';
$route['c_81'] = 'main/Main_controller/c_81';
$route['c_179'] = 'main/Main_controller/c_179';
$route['c_180'] = 'main/Main_controller/c_180';
$route['c_182'] = 'main/Main_controller/c_182';
$route['c_183'] = 'main/Main_controller/c_183';
$route['c_184'] = 'main/Main_controller/c_184';
$route['c_185'] = 'main/Main_controller/c_185';
$route['c_181'] = 'main/Main_controller/c_181';
$route['code_23'] = 'main/Main_controller/code_23';
$route['c_144'] = 'main/Main_controller/c_144';
$route['c_136'] = 'main/Main_controller/c_136';
$route['code_61'] = 'main/Main_controller/code_61';
$route['c_46'] = 'main/Main_controller/c_46';
$route['c_47'] = 'main/Main_controller/c_47';
$route['c_38'] = 'main/Main_controller/c_38';
$route['c_178'] = 'main/Main_controller/c_178';
$route['code_16'] = 'main/Main_controller/code_16';
$route['c_147'] = 'main/Main_controller/c_147';
$route['c_146'] = 'main/Main_controller/c_146';
$route['c_145'] = 'main/Main_controller/c_145';
$route['c_22'] = 'main/Main_controller/c_22';
$route['c_48'] = 'main/Main_controller/c_48';
$route['code_49'] = 'main/Main_controller/code_49';
$route['c_39'] = 'main/Main_controller/c_39';
$route['c_13'] = 'main/Main_controller/c_13';
$route['c_33'] = 'main/Main_controller/c_33';

//service start
$route['code_192'] = 'main/Main_controller/code_192';
$route['code_90'] = 'main/Main_controller/code_90';
$route['code_95'] = 'main/Main_controller/code_95';
$route['c_151'] = 'main/Main_controller/c_151';
$route['c_69'] = 'main/Main_controller/c_69';
$route['c_186'] = 'main/Main_controller/c_186';
$route['c_158'] = 'main/Main_controller/c_158';
$route['c_76'] = 'main/Main_controller/c_76';
$route['c_162'] = 'main/Main_controller/c_162';
$route['code_63'] = 'main/Main_controller/code_63';

//service end


//General start

//member start
$route['general-dashboard'] = 'general/General_controller/generalDashboard';
$route['code-194'] = 'general/General_controller/code_194';
$route['code-01'] = 'general/General_controller/code_1';
$route['code-05'] = 'general/General_controller/code_5';
$route['c-32'] = 'general/General_controller/c_032';
$route['code-195'] = 'general/General_controller/code_195';
$route['code_195_list'] = 'general/General_controller/code_195_list';
//member end
//accounts start
$route['code_08'] = 'general/General_controller/code_08';
$route['code_09'] = 'general/General_controller/code_09';
$route['code-10'] = 'general/General_controller/code_010';
$route['c-167'] = 'general/General_controller/c_167';
$route['c-168'] = 'general/General_controller/c_168';
$route['c-187'] = 'general/General_controller/c_187';
$route['c-188'] = 'general/General_controller/c_188';
$route['c-170'] = 'general/General_controller/c_170';
$route['c-171'] = 'general/General_controller/c_171';
$route['c-189'] = 'general/General_controller/c_189';
$route['c-190'] = 'general/General_controller/c_190';
$route['c-191'] = 'general/General_controller/c_191';
$route['c-169'] = 'general/General_controller/c_169';

//accounts end

//detail list start
$route['c-181'] = 'general/General_controller/c_181';
$route['c-182'] = 'general/General_controller/c_182';
$route['c-183'] = 'general/General_controller/c_183';
$route['c-184'] = 'general/General_controller/c_184';
//detail list end


//investment start
$route['code-196'] = 'general/General_controller/code_196';
$route['c-46'] = 'general/General_controller/c_46';
$route['c-38'] = 'general/General_controller/c_38';
$route['c-39'] = 'general/General_controller/c_39';
$route['code-16'] = 'general/General_controller/code_16';
$route['code-165'] = 'general/General_controller/code_165';
$route['c-166'] = 'general/General_controller/c_166';
$route['code-159'] = 'general/General_controller/code_159';
$route['c-70'] = 'general/General_controller/c_70';
$route['c-48'] = 'general/General_controller/c_48';
$route['c-47'] = 'general/General_controller/c_47';
$route['code-15'] = 'general/General_controller/code_15';
$route['code-21'] = 'general/General_controller/code_21';
$route['c-147'] = 'general/General_controller/c_147';
$route['c-146'] = 'general/General_controller/c_146';
$route['c-145'] = 'general/General_controller/c_145';
$route['code-49'] = 'general/General_controller/code_49';
$route['c-136'] = 'general/General_controller/c_136';

//investment end
//General end
//Central start
//Member start
$route['central-dashboard'] = 'central/Central_controller/centralDashboard';
$route['c-c-301'] = 'central/Central_controller/c_c_301';
$route['c-c-302'] = 'central/Central_controller/c_c_302';
$route['c-c-303'] = 'central/Central_controller/c_c_303';
$route['c-c-304'] = 'central/Central_controller/c_c_304';
$route['c-c-305'] = 'central/Central_controller/c_c_305';
$route['c-c-306'] = 'central/Central_controller/c_c_306';
$route['c-c-307'] = 'central/Central_controller/c_c_307';
$route['c-c-308'] = 'central/Central_controller/c_c_308';
//Member end

//Accounts starts
$route['c-code-142'] = 'central/Central_controller/c_code_142';
$route['c-code-143'] = 'central/Central_controller/c_code_143';
$route['c-c-172'] = 'central/Central_controller/c_c_172';
$route['c-c-312'] = 'central/Central_controller/c_c_312';
$route['c-c-173'] = 'central/Central_controller/c_c_173';
$route['c-c-313'] = 'central/Central_controller/c_c_313';
$route['c-c-176'] = 'central/Central_controller/c_c_176';
$route['c-c-177'] = 'central/Central_controller/c_c_177';
$route['c-c-314'] = 'central/Central_controller/c_c_314';
$route['c-c-315'] = 'central/Central_controller/c_c_315';
$route['c-c-316'] = 'central/Central_controller/c_c_316';
$route['c-c-77'] = 'central/Central_controller/c_c_77';
$route['c-c-175'] = 'central/Central_controller/c_c_175';


//Accounts end

//Detail List start

$route['c-c-182'] = 'central/Central_controller/c_c_182';
$route['c-c-183'] = 'central/Central_controller/c_c_183';
$route['c-c-184'] = 'central/Central_controller/c_c_184';
$route['c-c-181'] = 'central/Central_controller/c_c_181';

//Detail List end

//inversement start

$route['c-c-309'] = 'central/Central_controller/c_c_309';
$route['c-c-310'] = 'central/Central_controller/c_c_310';
$route['c-code-16'] = 'central/Central_controller/c_code_16';
$route['c-code-15'] = 'central/Central_controller/c_code_15';



//inversement end


//Central end




//admin panel end
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
