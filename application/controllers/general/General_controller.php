<?php


class General_controller extends CI_Controller
{
	public function generalDashboard()
	{
		$data['abc'] = '';

		$data['content'] = $this->load->view('general/dashboard', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('general/main_layout', $data);
		}

//		elseif ($user_type == "Accountant") {
//
//
//			$this->load->view('general/main_layout_accountant', $data);
//		} elseif ($user_type == "General") {
//			$this->load->view('general/main_layout_general', $data);
//		}

	}

//member start
	public function code_1()
	{
		$data['abc'] = '';

		$data['content'] = $this->load->view('general/code_1', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('general/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('general/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('general/main_layout_general', $data);
		}

	}

	public function code_5()
	{
		$data['abc'] = '';

		$data['content'] = $this->load->view('general/code_5', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('general/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('general/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('general/main_layout_general', $data);
		}

	}

	public function code_195()
	{
		$data['abc'] = '';

		$data['content'] = $this->load->view('general/code_195', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('general/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('general/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('general/main_layout_general', $data);
		}
	}

	public function code_195_list()
	{
		$data['abc'] = '';

		$data['content'] = $this->load->view('general/code_195_list', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('general/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('general/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('general/main_layout_general', $data);
		}
	}

	public function code_194()
	{
		$data['abc'] = '';

		$data['content'] = $this->load->view('general/code_194', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('general/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('general/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('general/main_layout_general', $data);
		}
	}

	public function c_032()
	{
		$data['abc'] = '';

		$data['content'] = $this->load->view('general/c_32', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('general/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('general/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('general/main_layout_general', $data);
		}
	}

	//member end


//accounts start
	public function code_08()
	{
		$data['abc'] = '';

		$data['content'] = $this->load->view('general/code_8', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('general/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('general/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('general/main_layout_general', $data);
		}
	}

	public function code_09()
	{
		$data['abc'] = '';

		$data['content'] = $this->load->view('general/code_9', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('general/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('general/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('general/main_layout_general', $data);
		}
	}

	public function code_010()
	{
		$data['abc'] = '';

		$data['content'] = $this->load->view('general/code_10', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('general/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('general/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('general/main_layout_general', $data);
		}
	}

	public function c_167()
	{
		$data['abc'] = '';

		$data['content'] = $this->load->view('general/c_167', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('general/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('general/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('general/main_layout_general', $data);
		}
	}

	public function c_187()
	{
		$data['abc'] = '';

		$data['content'] = $this->load->view('general/c_187', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('general/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('general/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('general/main_layout_general', $data);
		}
	}

	public function c_168()
	{
		$data['abc'] = '';

		$data['content'] = $this->load->view('general/c_168', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('general/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('general/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('general/main_layout_general', $data);
		}
	}

	public function c_188()
	{
		$data['abc'] = '';

		$data['content'] = $this->load->view('general/c_188', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('general/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('general/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('general/main_layout_general', $data);
		}
	}

	public function c_170()
	{
		$data['abc'] = '';

		$data['content'] = $this->load->view('general/c_170', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('general/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('general/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('general/main_layout_general', $data);
		}
	}

	public function c_171()
	{
		$data['abc'] = '';

		$data['content'] = $this->load->view('general/c_171', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('general/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('general/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('general/main_layout_general', $data);
		}
	}

	public function c_189()
	{
		$data['abc'] = '';

		$data['content'] = $this->load->view('general/c_189', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('general/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('general/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('general/main_layout_general', $data);
		}
	}

	public function c_190()
	{
		$data['abc'] = '';

		$data['content'] = $this->load->view('general/c_190', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('general/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('general/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('general/main_layout_general', $data);
		}
	}

	public function c_191()
	{
		$data['abc'] = '';

		$data['content'] = $this->load->view('general/c_191', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('general/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('general/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('general/main_layout_general', $data);
		}
	}

	public function c_169()
	{
		$data['abc'] = '';

		$data['content'] = $this->load->view('general/c_169', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('general/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('general/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('general/main_layout_general', $data);
		}
	}


//accounts end
//detail list start

	public function c_181()
	{
		$data['abc'] = '';

		$data['content'] = $this->load->view('general/c_181', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('general/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('general/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('general/main_layout_general', $data);
		}
	}

	public function c_182()
	{
		$data['abc'] = '';

		$data['content'] = $this->load->view('general/c_182', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('general/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('general/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('general/main_layout_general', $data);
		}
	}

	public function c_183()
	{
		$data['abc'] = '';

		$data['content'] = $this->load->view('general/c_183', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('general/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('general/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('general/main_layout_general', $data);
		}
	}

	public function c_184()
	{
		$data['abc'] = '';

		$data['content'] = $this->load->view('general/c_184', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('general/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('general/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('general/main_layout_general', $data);
		}
	}




//detail list end


//investment start
	public function code_196()
	{
		$data['abc'] = '';

		$data['content'] = $this->load->view('general/code_196', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('general/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('general/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('general/main_layout_general', $data);
		}
	}

	public function c_46()
	{
		$data['abc'] = '';

		$data['content'] = $this->load->view('general/c_46', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('general/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('general/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('general/main_layout_general', $data);
		}
	}

	public function c_38()
	{
		$data['abc'] = '';

		$data['content'] = $this->load->view('general/c_38', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('general/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('general/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('general/main_layout_general', $data);
		}
	}

	public function c_39()
	{
		$data['abc'] = '';

		$data['content'] = $this->load->view('general/c_39', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('general/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('general/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('general/main_layout_general', $data);
		}
	}

	public function code_16()
	{
		$data['abc'] = '';

		$data['content'] = $this->load->view('general/code_16', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('general/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('general/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('general/main_layout_general', $data);
		}
	}

	public function code_165()
	{
		$data['abc'] = '';

		$data['content'] = $this->load->view('general/code_165', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('general/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('general/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('general/main_layout_general', $data);
		}
	}

	public function c_166()
	{
		$data['abc'] = '';

		$data['content'] = $this->load->view('general/c_166', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('general/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('general/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('general/main_layout_general', $data);
		}
	}

	public function code_159()
	{
		$data['abc'] = '';

		$data['content'] = $this->load->view('general/code_159', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('general/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('general/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('general/main_layout_general', $data);
		}
	}

	public function c_70()
	{
		$data['abc'] = '';

		$data['content'] = $this->load->view('general/c_70', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('general/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('general/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('general/main_layout_general', $data);
		}
	}

	public function c_48()
	{
		$data['abc'] = '';

		$data['content'] = $this->load->view('general/c_48', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('general/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('general/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('general/main_layout_general', $data);
		}
	}

	public function c_47()
	{
		$data['abc'] = '';

		$data['content'] = $this->load->view('general/c_47', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('general/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('general/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('general/main_layout_general', $data);
		}
	}

	public function code_15()
	{
		$data['abc'] = '';

		$data['content'] = $this->load->view('general/code_15', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('general/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('general/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('general/main_layout_general', $data);
		}
	}

	public function code_21()
	{
		$data['abc'] = '';

		$data['content'] = $this->load->view('general/code_21', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('general/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('general/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('general/main_layout_general', $data);
		}
	}

	public function c_147()
	{
		$data['abc'] = '';

		$data['content'] = $this->load->view('general/c_147', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('general/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('general/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('general/main_layout_general', $data);
		}
	}

	public function c_146()
	{
		$data['abc'] = '';

		$data['content'] = $this->load->view('general/c_146', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('general/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('general/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('general/main_layout_general', $data);
		}
	}

	public function c_145()
	{
		$data['abc'] = '';

		$data['content'] = $this->load->view('general/c_145', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('general/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('general/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('general/main_layout_general', $data);
		}
	}

	public function code_49()
	{
		$data['abc'] = '';

		$data['content'] = $this->load->view('general/code_49', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('general/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('general/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('general/main_layout_general', $data);
		}
	}

//investment end

//Others start


	public function c_136()
	{
		$data['abc'] = '';

		$data['content'] = $this->load->view('general/c_136', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('general/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('general/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('general/main_layout_general', $data);
		}
	}




//Others end

}
