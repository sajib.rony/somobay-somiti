<?php


class Main_controller extends CI_Controller
{

	public function admin_login_panel()
	{
		$this->load->view('modified_system_admin_login');
	}

	public function admin_login_check()
	{
		$check = $this->input->post('SystemAdminListID');
		$this->session->set_userdata('user', $check);
		redirect('somobay-office', 'location');


	}

	public function somobay_office_link_page()
	{
		$data['content'] = $this->load->view('somobay_office_link_page');
	}

	public function link_page()
	{
		$data['content'] = $this->load->view('p_g_c_link_page');
	}

	public function dashboard()
	{
		$data['abc'] = '';

		$data['content'] = $this->load->view('main/dashboard', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('main/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('main/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('main/main_layout_general', $data);
		}

	}

	public function addMember()
	{
		$data['abc'] = '';
		$data['content'] = $this->load->view('main/add_member', $data, TRUE);
		$this->load->view('main/main_layout', $data);
	}

	public function memberList()
	{
		$data['abc'] = '';
		$data['content'] = $this->load->view('main/member_list', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('main/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('main/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('main/main_layout_general', $data);
		}
	}

	public function memberListView()
	{
		$data['abc'] = '';
		$data['content'] = $this->load->view('main/memberListView', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('main/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('main/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('main/main_layout_general', $data);
		}
	}

	public function memberListView_accountant()
	{
		$data['abc'] = '';
		$data['content'] = $this->load->view('main/memberListView_accountant', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('main/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('main/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('main/main_layout_general', $data);
		}
	}

	public function memberListView_general()
	{
		$data['abc'] = '';
		$data['content'] = $this->load->view('main/memberListView_general', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('main/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('main/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('main/main_layout_general', $data);
		}
	}

	public function memberListPending()
	{
		$data['abc'] = '';
		$data['content'] = $this->load->view('main/memberListPending', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('main/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('main/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('main/main_layout_general', $data);
		}
	}

	public function memberListApproved()
	{
		$data['abc'] = '';
		$data['content'] = $this->load->view('main/memberListApproved', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('main/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('main/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('main/main_layout_general', $data);
		}
	}

	public function amanoth_add()
	{
		$data['abc'] = '';
		$data['content'] = $this->load->view('main/amanoth_add', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('main/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('main/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('main/main_layout_general', $data);
		}
	}

	public function amanoth_list()
	{
		$data['abc'] = '';
		$data['content'] = $this->load->view('main/amanoth_list', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('main/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('main/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('main/main_layout_general', $data);
		}
	}


	public function amanoth_view()
	{
		$data['abc'] = '';
		$data['content'] = $this->load->view('main/amanoth_view', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('main/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('main/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('main/main_layout_general', $data);
		}
	}

	public function akalinAmanoth_add()
	{
		$data['abc'] = '';
		$data['content'] = $this->load->view('main/akalinAmanoth_add', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('main/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('main/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('main/main_layout_general', $data);
		}
	}

	public function akalinAmanoth_list()
	{
		$data['abc'] = '';
		$data['content'] = $this->load->view('main/akalinAmanoth_list', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('main/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('main/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('main/main_layout_general', $data);
		}
	}

	public function akalinAmanoth_view()
	{
		$data['abc'] = '';
		$data['content'] = $this->load->view('main/akalinAmanoth_view', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('main/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('main/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('main/main_layout_general', $data);
		}
	}

	public function mashikAmanoth_add()
	{
		$data['abc'] = '';
		$data['content'] = $this->load->view('main/mashikAmanoth_add', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('main/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('main/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('main/main_layout_general', $data);
		}
	}

	public function mashikAmanoth_list()
	{
		$data['abc'] = '';
		$data['content'] = $this->load->view('main/mashikAmanoth_list', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('main/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('main/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('main/main_layout_general', $data);
		}
	}

	public function mashikAmanoth_view()
	{
		$data['abc'] = '';
		$data['content'] = $this->load->view('main/mashikAmanoth_view', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('main/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('main/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('main/main_layout_general', $data);
		}
	}

	public function beniyog_add()
	{
		$data['abc'] = '';
		$data['content'] = $this->load->view('main/beniyog_add', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('main/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('main/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('main/main_layout_general', $data);
		}
	}

	public function beniyog_list()
	{
		$data['abc'] = '';
		$data['content'] = $this->load->view('main/beniyog_list', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('main/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('main/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('main/main_layout_general', $data);
		}
	}

	public function beniyog_view()
	{
		$data['abc'] = '';
		$data['content'] = $this->load->view('main/beniyog_view', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('main/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('main/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('main/main_layout_general', $data);
		}
	}

	public function besses_beniyog_add()
	{
		$data['abc'] = '';
		$data['content'] = $this->load->view('main/besses_beniyog_add', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('main/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('main/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('main/main_layout_general', $data);
		}
	}

	public function besses_beniyog_list()
	{
		$data['abc'] = '';
		$data['content'] = $this->load->view('main/besses_beniyog_list', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('main/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('main/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('main/main_layout_general', $data);
		}
	}

	public function besses_beniyog_view()
	{
		$data['abc'] = '';
		$data['content'] = $this->load->view('main/besses_beniyog_view', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('main/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('main/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('main/main_layout_general', $data);
		}
	}

	public function biniyog_kotiyan()
	{
		$data['abc'] = '';
		$data['content'] = $this->load->view('main/biniyog_kotiyan', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('main/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('main/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('main/main_layout_general', $data);
		}
	}

	public function biniyog_kotiyan_search()
	{
		$data['abc'] = '';
		$data['content'] = $this->load->view('main/biniyog_kotiyan_search', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('main/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('main/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('main/main_layout_general', $data);
		}
	}

	public function biniyog_paper_return()
	{
		$data['abc'] = '';
		$data['content'] = $this->load->view('main/biniyog_paper_return', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('main/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('main/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('main/main_layout_general', $data);
		}
	}

	public function biniyog_grohonkarir_chahida()
	{
		$data['abc'] = '';
		$data['content'] = $this->load->view('main/biniyog_grohorkarir_chahida', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('main/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('main/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('main/main_layout_general', $data);
		}
	}

	public function ponno_kroy_bikry_chukti()
	{
		$data['abc'] = '';
		$data['content'] = $this->load->view('main/ponno_kroy_bikry_chukti', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('main/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('main/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('main/main_layout_general', $data);
		}
	}

	public function sodorsho_protahar()
	{
		$data['abc'] = '';
		$data['content'] = $this->load->view('main/sodorsho_protahar', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('main/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('main/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('main/main_layout_general', $data);
		}
	}

	public function biniyog_paper_return_list()
	{
		$data['abc'] = '';
		$data['content'] = $this->load->view('main/biniyog_paper_return_list', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('main/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('main/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('main/main_layout_general', $data);
		}
	}

	public function biniyog_paper_return_view()
	{
		$data['abc'] = '';
		$data['content'] = $this->load->view('main/biniyog_paper_return_view', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('main/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('main/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('main/main_layout_general', $data);
		}
	}

	public function biniyog_grohonkarir_chahida_list()
	{
		$data['abc'] = '';
		$data['content'] = $this->load->view('main/biniyog_grohonkarir_chahida_list', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('main/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('main/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('main/main_layout_general', $data);
		}
	}

	public function biniyog_grohonkarir_chahida_view()
	{
		$data['abc'] = '';
		$data['content'] = $this->load->view('main/biniyog_grohonkarir_chahida_view', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('main/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('main/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('main/main_layout_general', $data);
		}
	}

	public function ponno_kroy_bikry_chukti_list()
	{
		$data['abc'] = '';
		$data['content'] = $this->load->view('main/ponno_kroy_bikry_chukti_list', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('main/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('main/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('main/main_layout_general', $data);
		}
	}

	public function ponno_kroy_bikry_chukti_view()
	{
		$data['abc'] = '';
		$data['content'] = $this->load->view('main/ponno_kroy_bikry_chukti_view', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('main/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('main/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('main/main_layout_general', $data);
		}
	}

	public function sodorsho_protahar_list()
	{
		$data['abc'] = '';
		$data['content'] = $this->load->view('main/sodorsho_protahar_list', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('main/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('main/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('main/main_layout_general', $data);
		}
	}

	public function sodorsho_protahar_view()
	{
		$data['abc'] = '';
		$data['content'] = $this->load->view('main/sodorsho_protahar_view', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('main/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('main/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('main/main_layout_general', $data);
		}
	}

	public function sodorsho_nitimala_add()
	{
		$data['abc'] = '';
		$data['content'] = $this->load->view('main/sodorsho_nitimala_add', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('main/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('main/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('main/main_layout_general', $data);
		}
	}

	public function memberAccounts()
	{
		$data['abc'] = '';
		$data['content'] = $this->load->view('main/memberAccounts', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('main/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('main/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('main/main_layout_general', $data);
		}
	}

	public function takaUttolonChahida()
	{
		$data['abc'] = '';
		$data['content'] = $this->load->view('main/takaUttolonChahida', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('main/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('main/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('main/main_layout_general', $data);
		}
	}

	public function sonchoyAmanothUttolon()
	{
		$data['abc'] = '';
		$data['content'] = $this->load->view('main/sonchoyAmanothUttolon', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('main/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('main/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('main/main_layout_general', $data);
		}
	}

	public function code_8()
	{
		$data['abc'] = '';
		$data['content'] = $this->load->view('main/code_8', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('main/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('main/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('main/main_layout_general', $data);
		}
	}

	public function code_9()
	{
		$data['abc'] = '';
		$data['content'] = $this->load->view('main/code_9', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('main/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('main/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('main/main_layout_general', $data);
		}
	}

	public function code_10()
	{
		$data['abc'] = '';
		$data['content'] = $this->load->view('main/code_10', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('main/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('main/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('main/main_layout_general', $data);
		}
	}

	public function c_27()
	{
		$data['abc'] = '';
		$data['content'] = $this->load->view('main/c_27', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('main/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('main/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('main/main_layout_general', $data);
		}
	}

	public function c_28()
	{
		$data['abc'] = '';
		$data['content'] = $this->load->view('main/c_28', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('main/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('main/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('main/main_layout_general', $data);
		}
	}

	public function c_29()
	{
		$data['abc'] = '';
		$data['content'] = $this->load->view('main/c_29', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('main/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('main/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('main/main_layout_general', $data);
		}
	}

	public function c_30()
	{
		$data['abc'] = '';
		$data['content'] = $this->load->view('main/c_30', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('main/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('main/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('main/main_layout_general', $data);
		}
	}

	public function c_31()
	{
		$data['abc'] = '';
		$data['content'] = $this->load->view('main/c_31', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('main/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('main/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('main/main_layout_general', $data);
		}
	}


	public function c_79()
	{
		$data['abc'] = '';
		$data['content'] = $this->load->view('main/c_79', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('main/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('main/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('main/main_layout_general', $data);
		}
	}

	public function c_42()
	{
		$data['abc'] = '';
		$data['content'] = $this->load->view('main/c_42', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('main/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('main/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('main/main_layout_general', $data);
		}
	}

	public function c_80()
	{
		$data['abc'] = '';
		$data['content'] = $this->load->view('main/c_80', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('main/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('main/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('main/main_layout_general', $data);
		}
	}

	public function c_81()
	{
		$data['abc'] = '';
		$data['content'] = $this->load->view('main/c_81', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('main/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('main/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('main/main_layout_general', $data);
		}
	}

	public function c_182()
	{
		$data['abc'] = '';
		$data['content'] = $this->load->view('main/c_182', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('main/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('main/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('main/main_layout_general', $data);
		}
	}

	public function c_183()
	{
		$data['abc'] = '';
		$data['content'] = $this->load->view('main/c_183', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('main/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('main/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('main/main_layout_general', $data);
		}
	}

	public function c_184()
	{
		$data['abc'] = '';
		$data['content'] = $this->load->view('main/c_184', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('main/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('main/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('main/main_layout_general', $data);
		}
	}

	public function c_185()
	{
		$data['abc'] = '';
		$data['content'] = $this->load->view('main/c_185', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('main/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('main/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('main/main_layout_general', $data);
		}
	}

	public function c_181()
	{
		$data['abc'] = '';
		$data['content'] = $this->load->view('main/c_181', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('main/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('main/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('main/main_layout_general', $data);
		}
	}

	public function code_23()
	{
		$data['abc'] = '';
		$data['content'] = $this->load->view('main/code_23', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('main/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('main/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('main/main_layout_general', $data);
		}
	}

	public function c_144()
	{
		$data['abc'] = '';
		$data['content'] = $this->load->view('main/c_144', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('main/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('main/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('main/main_layout_general', $data);
		}
	}

	public function c_136()
	{
		$data['abc'] = '';
		$data['content'] = $this->load->view('main/c_136', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('main/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('main/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('main/main_layout_general', $data);
		}
	}

	public function code_61()
	{
		$data['abc'] = '';
		$data['content'] = $this->load->view('main/code_61', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('main/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('main/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('main/main_layout_general', $data);
		}
	}

	public function c_153()
	{
		$data['abc'] = '';
		$data['content'] = $this->load->view('main/c_153', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('main/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('main/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('main/main_layout_general', $data);
		}
	}

	public function c_68()
	{
		$data['abc'] = '';
		$data['content'] = $this->load->view('main/c_68', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('main/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('main/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('main/main_layout_general', $data);
		}
	}

	public function c_68_list()
	{
		$data['abc'] = '';
		$data['content'] = $this->load->view('main/c_68_list', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('main/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('main/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('main/main_layout_general', $data);
		}
	}

	public function c_32_p()
	{
		$data['abc'] = '';
		$data['content'] = $this->load->view('main/c_32_p', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('main/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('main/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('main/main_layout_general', $data);
		}
	}

	public function c_46()
	{
		$data['abc'] = '';
		$data['content'] = $this->load->view('main/c_46', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('main/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('main/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('main/main_layout_general', $data);
		}
	}

	public function c_38()
	{
		$data['abc'] = '';
		$data['content'] = $this->load->view('main/c_38', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('main/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('main/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('main/main_layout_general', $data);
		}
	}

	public function c_178()
	{
		$data['abc'] = '';
		$data['content'] = $this->load->view('main/c_178', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('main/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('main/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('main/main_layout_general', $data);
		}
	}

	public function code_16()
	{
		$data['abc'] = '';
		$data['content'] = $this->load->view('main/code_16', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('main/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('main/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('main/main_layout_general', $data);
		}
	}

	public function c_147()
	{
		$data['abc'] = '';
		$data['content'] = $this->load->view('main/c_147', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('main/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('main/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('main/main_layout_general', $data);
		}
	}

	public function c_146()
	{
		$data['abc'] = '';
		$data['content'] = $this->load->view('main/c_146', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('main/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('main/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('main/main_layout_general', $data);
		}
	}

	public function c_145()
	{
		$data['abc'] = '';
		$data['content'] = $this->load->view('main/c_145', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('main/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('main/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('main/main_layout_general', $data);
		}
	}

	public function c_22()
	{
		$data['abc'] = '';
		$data['content'] = $this->load->view('main/c_22', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('main/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('main/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('main/main_layout_general', $data);
		}
	}

	public function c_48()
	{
		$data['abc'] = '';
		$data['content'] = $this->load->view('main/c_48', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('main/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('main/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('main/main_layout_general', $data);
		}
	}

	public function code_49()
	{
		$data['abc'] = '';
		$data['content'] = $this->load->view('main/code_49', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('main/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('main/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('main/main_layout_general', $data);
		}
	}

	public function c_180()
	{
		$data['abc'] = '';
		$data['content'] = $this->load->view('main/c_180', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('main/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('main/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('main/main_layout_general', $data);
		}
	}

	public function c_179()
	{
		$data['abc'] = '';
		$data['content'] = $this->load->view('main/c_179', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('main/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('main/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('main/main_layout_general', $data);
		}
	}

	public function c_39()
	{
		$data['abc'] = '';
		$data['content'] = $this->load->view('main/c_39', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('main/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('main/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('main/main_layout_general', $data);
		}
	}

	public function c_47()
	{
		$data['abc'] = '';
		$data['content'] = $this->load->view('main/c_47', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('main/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('main/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('main/main_layout_general', $data);
		}
	}

	public function c_13()
	{
		$data['abc'] = '';
		$data['content'] = $this->load->view('main/c_13', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('main/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('main/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('main/main_layout_general', $data);
		}
	}

	public function c_33()
	{
		$data['abc'] = '';
		$data['content'] = $this->load->view('main/c_33', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('main/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('main/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('main/main_layout_general', $data);
		}
	}

//	service start


	public function code_192()
	{
		$data['abc'] = '';
		$data['content'] = $this->load->view('main/code_192', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('main/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('main/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('main/main_layout_general', $data);
		}
	}

	public function code_90()
	{
		$data['abc'] = '';
		$data['content'] = $this->load->view('main/code_90', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('main/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('main/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('main/main_layout_general', $data);
		}
	}

	public function code_95()
	{
		$data['abc'] = '';
		$data['content'] = $this->load->view('main/code_95', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('main/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('main/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('main/main_layout_general', $data);
		}
	}

	public function c_151()
	{
		$data['abc'] = '';
		$data['content'] = $this->load->view('main/c_151', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('main/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('main/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('main/main_layout_general', $data);
		}
	}

	public function c_69()
	{
		$data['abc'] = '';
		$data['content'] = $this->load->view('main/c_69', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('main/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('main/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('main/main_layout_general', $data);
		}
	}

	public function c_186()
	{
		$data['abc'] = '';
		$data['content'] = $this->load->view('main/c_186', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('main/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('main/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('main/main_layout_general', $data);
		}
	}

	public function c_158()
	{
		$data['abc'] = '';
		$data['content'] = $this->load->view('main/c_158', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('main/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('main/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('main/main_layout_general', $data);
		}
	}

	public function c_76()
	{
		$data['abc'] = '';
		$data['content'] = $this->load->view('main/c_76', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('main/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('main/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('main/main_layout_general', $data);
		}
	}

	public function c_162()
	{
		$data['abc'] = '';
		$data['content'] = $this->load->view('main/c_162', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('main/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('main/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('main/main_layout_general', $data);
		}
	}

	public function code_63()
	{
		$data['abc'] = '';
		$data['content'] = $this->load->view('main/code_63', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('main/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('main/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('main/main_layout_general', $data);
		}
	}

	public function test_test()
	{
		$data['abc'] = '';
		$data['content'] = $this->load->view('main/test_test', $data, TRUE);
		$user_type = $this->session->userdata('user');
		if ($user_type == "Operator") {
			$this->load->view('main/main_layout', $data);
		} elseif ($user_type == "Accountant") {
			$this->load->view('main/main_layout_accountant', $data);
		} elseif ($user_type == "General") {
			$this->load->view('main/main_layout_general', $data);
		}
	}
//	service end

}
