<?php defined('BASEPATH') OR exit('No direct script access allowed');

    function user_notification_number() {
    	$CI = & get_instance();
    	$CI->load->model('M_notification_helper');
    	$where_data = array(
    			'user_id' => $CI->session->userdata('UserID'),
    		);
    	return $CI->M_notification_helper->total_notification_user($where_data);
    }

    function acountant_notification_number() {
    	$AN = & get_instance();
    	$AN->load->model('M_notification_helper');
    	$where_data = array(
    			'state' => 0
    		);
    	return $AN->M_notification_helper->total_notification_user($where_data);
    }
