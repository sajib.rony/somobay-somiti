<style>
	p {
		font-family: Roboto;
		font-size: medium;
		color: black;
		text-align: justify;
	}

	h4 {
		font-family: Roboto;

		color: black;
	}

	.div-padding1 {

		padding-top: 10px;
		padding-right: 30px;
		padding-bottom: 10px;
		padding-left: 30px;
	}

	.div-padding2 {

		padding-top: 0px;
		padding-right: 30px;
		padding-bottom: 10px;
		padding-left: 30px;
	}

	.input-field1 {
		width: 50%;
		padding: 5px 20px;
		margin: 8px 0;
		display: inline-block;
		border: 1px solid #ccc;
		border-radius: 4px;
		box-sizing: border-box;
	}

	.text_color_th {
		color: white;
	}
</style>


<div class="" style="background-color:#4A235A">
	<div class="row">
		<div class="col-md-12">
			<center><h3 style="color:white"> শিক্ষিত বেকার কেন্দ্রীয় সঞ্চয় ও ঋণদান সমবায় সমিতি লিঃ </h3></center>
			<center><h3 style="color:white">কর্মসংস্থান ও দারিদ্র বিমোচন প্রকল্প</h3></center>
			<center><h5 style="color:white">নিবন্ধন নম্বর -১৩/চাঁদ /১২, তারিখ :০৯/০৭/২০১২ </h5></center>
			<center><h6 style="color:white">ঠিকানা: ঠাকুর বাজার ,শাহারাস্তি , চাঁদপুর। </h6></center>
			<br>

			<div class="" style="background-color:white">
				<div class="div-padding1">
					<div class="form-group">
						<div class="row">
							<div class="col-md-6">

							</div>
							<div class="col-md-6 text-right">
								<h5 style="color:black">সি -১৭৫</h5>
							</div>
							<div class="col-md-12 text-center">
								<h2 style="color:black">
									হওলাত গ্রহণ এবং প্রদানের স্থিতি প্রতিবেদন
								</h2>
								</h3>
								<h5 style="color:black; text-align:right">মাসের নাম :<?php echo date('M/yy') ?></h5>
							</div>

						</div>
					</div>
				</div>

				<div class="div-padding2">
					<div class="form-group">
						<div class="row">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table table-bordered">
										<tr style="background:  #dd3333">
											<th class="text-center text_color_th">ক্র নং</th>
											<th class="text-center text_color_th">
												তারিখ
											</th>

											<th class="text-center text_color_th">
												কার নিকট হতে গ্রহণ / কাহাকে প্রদান
											</th>
											<th class="text-center text_color_th">
												গ্রহণ টাকা
											</th>
											<th class="text-center text_color_th">
												প্রদান টাকা
											</th>
											<th class="text-center text_color_th">
												পরিশোধ করার তারিখ
											</th>
										</tr>


										<?php
										$i = '';
										$sum = 0;
										for ($i = 1; $i <= 17; $i++) { ?>
											<tr>
												<td><p style="text-align:center"><?php echo $i; ?></p></td>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
												<td></td>

											</tr>
											<?php
										} ?>
										<tr>
											<th colspan="3">
												<p style="text-align:right;">মোট =</p>
											</th>
											<th>
											</th>
											<th>
											</th>
											<th>
											</th>
										</tr>
									</table>
								</div>

							</div>
						</div>
					</div>
				</div>


				<div class="div-padding2">
					<div class="form-group">
						<div class="row">
							<div class="col-md-12">
								<div class="col-md-3">
									<p style="color:black;">
										<input type="text" name="" value="সভাপতি " class="form-control">
									<hr>
									</p>
									<p style="text-align: center">
										সভাপতি
									</p>
								</div>
								<div class="col-md-3">
									<p style="color:black;">
										<input type="text" name="" value="সম্পাদক" class="form-control">
									<hr>
									</p>
									<p style="text-align: center">
										সম্পাদক
									</p>
								</div>
								<div class="col-md-3">
									<p style="color:black;">
										<input type="text" name="" value="ব্যবস্থাপক" class="form-control">
									<hr>
									</p>
									<p style="text-align: center">
										ব্যবস্থাপক
									</p>
								</div>


								<div class="col-md-3">
									<p style="color:black;">
										<input type="text" name="" value="ক্যাশিয়ার " class="form-control">
									<hr>
									</p>
									<p style="text-align: center">
										ক্যাশিয়ার
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>







