<style>
	p {
		font-family: Roboto;
		font-size: medium;
		color: black;
		text-align: justify;
	}

	h4 {
		font-family: Roboto;

		color: black;
	}

	.div-padding1 {

		padding-top: 10px;
		padding-right: 30px;
		padding-bottom: 10px;
		padding-left: 30px;
	}

	.div-padding2 {

		padding-top: 0px;
		padding-right: 30px;
		padding-bottom: 10px;
		padding-left: 30px;
	}

	.input-field1 {
		width: 30%;
		padding: 3px 20px;
		margin: 8px 0;
		display: inline-block;
		border: 1px solid #ccc;
		border-radius: 4px;
		box-sizing: border-box;
	}

	.text_color_th {
		color: white;
	}
</style>


<div class="" style="background-color:#4A235A">
	<div class="row">
		<div class="col-md-12">
			<center><h3 style="color:white"> শিক্ষিত বেকার কেন্দ্রীয় সঞ্চয় ও ঋণদান সমবায় সমিতি লিঃ </h3></center>
			<center><h3 style="color:white">কর্মসংস্থান ও দারিদ্র বিমোচন প্রকল্প</h3></center>
			<center><h5 style="color:white">নিবন্ধন নম্বর -১৩/চাঁদ /১২, তারিখ :০৯/০৭/২০১২ </h5></center>
			<center><h6 style="color:white">ঠিকানা: ঠাকুর বাজার ,শাহারাস্তি , চাঁদপুর। </h6></center>
			<br>

			<div class="" style="background-color:white">
				<div class="div-padding1">
					<div class="form-group">
						<div class="row">
							<div class="col-md-6">
							</div>
							<div class="col-md-6 text-right">
								<h5 style="color:black">সি -১৭৬</h5>
							</div>
							<div class="col-md-12 text-center">
								<h3 style="color:black">
									মাসিক খাত ভিত্তিক দেনা হিসাব
								</h3>
								<h5 style="color:black; text-align:right">মাসের নাম :<?php echo date('M/yy') ?></h5>

							</div>
						</div>
					</div>
				</div>


				<div class="div-padding2">
					<div class="form-group">
						<div class="row">
							<div class="col-md-12">

								<div class="table-responsive">
									<table class="table table-bordered">


										<tr style="background:  #dd3333">
											<th class="text-center text_color_th" rowspan="2">খাত</th>
											<th class="text-center text_color_th" rowspan="2">
												শেয়ার
											</th>
											<th class="text-center text_color_th" colspan="2">
												সঞ্চয়
											</th>

											<th class="text-center text_color_th" rowspan="2">
												আমানত গ্রহণ
											</th>
											<th class="text-center text_color_th" colspan="2">
												কর্মকর্তা কর্মচারীদের ভবিষ্য তহবিল
											</th>
											<th class="text-center text_color_th" rowspan="2">
												এফ ডি আর
											</th>
											<th class="text-center text_color_th" rowspan="2">
												ঋণ গ্রহণ
											</th>
											<th class="text-center text_color_th" rowspan="2">হওলাদ গ্রহণ
											</th>
											<th class="text-center text_color_th" rowspan="2">সংরক্ষিত তহবিল
											</th>
											<th class="text-center text_color_th" rowspan="2">কু-ঋণ তহবিল
											</th>
											<th class="text-center text_color_th" rowspan="2">সমবায় উন্নয়ন তহবিল
											</th>
											<th class="text-center text_color_th" rowspan="2">অবন্টিত লাভ

											</th>
											<th class="text-center text_color_th" rowspan="2">নীট লাভ /রাজস্ব নিজস্ব

											</th>
											<th class="text-center text_color_th" rowspan="2">প্রভিশন হিসাব
											</th>
											<th class="text-center text_color_th" colspan="2">
												সকল খাতের মোট দেনা
											</th>


										</tr>
										<tr style="background:  #dd3333">


											<th class="text-center text_color_th">
												মূলধন
											</th>

											<th class="text-center text_color_th">
												লাভ
											</th>
											<th class="text-center text_color_th">
												মূলধন
											</th>

											<th class="text-center text_color_th">
												লাভ
											</th>
											<th class="text-center text_color_th">
												মূলধন
											</th>

											<th class="text-center text_color_th">
												লাভ
											</th>


										</tr>

										<tr style="background:  #dd3333">

											<th class="text-center text_color_th">০১
											</th>

											<th class="text-center text_color_th">০২
											</th>
											<th class="text-center text_color_th">০৩
											</th>
											<th class="text-center text_color_th">০৪
											</th>
											<th class="text-center text_color_th">০৫
											</th>
											<th class="text-center text_color_th">০৬
											</th>
											<th class="text-center text_color_th">০৭
											</th>
											<th class="text-center text_color_th">০৮
											</th>
											<th class="text-center text_color_th">০৯
											</th>
											<th class="text-center text_color_th">১০
											</th>
											<th class="text-center text_color_th">১১
											</th>
											<th class="text-center text_color_th">১২
											</th>
											<th class="text-center text_color_th">১৩
											</th>
											<th class="text-center text_color_th">১৪
											</th>
											<th class="text-center text_color_th">১৫
											</th>
											<th class="text-center text_color_th">১৬
											</th>
											<th class="text-center text_color_th">১৭
											</th>
											<th class="text-center text_color_th">১৮
											</th>


										</tr>

										<tr>
											<td style="color:black"><p> জমা</p></td>
											<td style="color:black"><p></p></td>
											<td style="color:black">
												<p></p>
											</td>
											<td style="color:black">
												<p></p>
											</td>
											<td style="color:black">
												<p></p>
											</td>
											<td style="color:black">
												<p></p>
											</td>
											<td style="color:black">
												<p></p>
											</td>
											<td style="color:black">
												<p></p>
											</td>
											<td style="color:black">
												<p></p>
											</td>
											<td style="color:black">
												<p></p>
											</td>
											<td style="color:black">
												<p></p>
											</td>
											<td style="color:black">
												<p></p>
											</td>
											<td style="color:black">
												<p></p>
											</td>
											<td style="color:black">
												<p></p>
											</td>
											<td style="color:black">
												<p></p>
											</td>
											<td style="color:black">
												<p></p>
											</td>
											<td style="color:black"><p></p></td>
											<td style="color:black"><p></p></td>


										</tr>

										<tr>
											<td style="color:black"><p> খরচ</p></td>
											<td style="color:black"><label></label></td>
											<td style="color:black">
												<label> </label>
											</td>
											<td style="color:black">
												<label> </label>
											</td>
											<td style="color:black">
												<label></label>
											</td>
											<td style="color:black">
												<label></label>
											</td>
											<td style="color:black">
												<label></label>
											</td>
											<td style="color:black">
												<label> </label>
											</td>
											<td style="color:black">
												<label></label>
											</td>
											<td style="color:black">
												<label></label>
											</td>
											<td style="color:black">
												<label></label>
											</td>
											<td style="color:black">
												<label></label>
											</td>
											<td style="color:black">
												<label> </label>
											</td>
											<td style="color:black">
												<label></label>
											</td>
											<td style="color:black">
												<label></label>
											</td>
											<td style="color:black">
												<label></label>
											</td>
											<td style="color:black"><label> </label></td>
											<td style="color:black"><label> </label></td>


										</tr>

										<tr>
											<td style="color:black"><p>অবশিষ্ট </p></td>
											<td style="color:black"><label></label></td>
											<td style="color:black">
												<label> </label>
											</td>
											<td style="color:black">
												<label> </label>
											</td>
											<td style="color:black">
												<label></label>
											</td>
											<td style="color:black">
												<label></label>
											</td>
											<td style="color:black">
												<label></label>
											</td>
											<td style="color:black">
												<label> </label>
											</td>
											<td style="color:black">
												<label></label>
											</td>
											<td style="color:black">
												<label></label>
											</td>
											<td style="color:black">
												<label></label>
											</td>
											<td style="color:black">
												<label></label>
											</td>
											<td style="color:black">
												<label> </label>
											</td>
											<td style="color:black">
												<label></label>
											</td>
											<td style="color:black">
												<label></label>
											</td>
											<td style="color:black">
												<label></label>
											</td>
											<td style="color:black"><label> </label></td>
											<td style="color:black"><label> </label></td>


										</tr>

										<tr>
											<td style="color:black"><p>আগত </p></td>
											<td style="color:black"><label></label></td>
											<td style="color:black">
												<label> </label>
											</td>
											<td style="color:black">
												<label> </label>
											</td>
											<td style="color:black">
												<label></label>
											</td>
											<td style="color:black">
												<label></label>
											</td>
											<td style="color:black">
												<label></label>
											</td>
											<td style="color:black">
												<label> </label>
											</td>
											<td style="color:black">
												<label></label>
											</td>
											<td style="color:black">
												<label></label>
											</td>
											<td style="color:black">
												<label></label>
											</td>
											<td style="color:black">
												<label></label>
											</td>
											<td style="color:black">
												<label> </label>
											</td>
											<td style="color:black">
												<label></label>
											</td>
											<td style="color:black">
												<label></label>
											</td>
											<td style="color:black">
												<label></label>
											</td>
											<td style="color:black"><label> </label></td>
											<td style="color:black"><label> </label></td>


										</tr>

										<tr>
											<td style="color:black"><p>সর্বমোট </p></td>
											<td style="color:black"><label></label></td>
											<td style="color:black">
												<label> </label>
											</td>
											<td style="color:black">
												<label> </label>
											</td>
											<td style="color:black">
												<label></label>
											</td>
											<td style="color:black">
												<label></label>
											</td>
											<td style="color:black">
												<label></label>
											</td>
											<td style="color:black">
												<label> </label>
											</td>
											<td style="color:black">
												<label></label>
											</td>
											<td style="color:black">
												<label></label>
											</td>
											<td style="color:black">
												<label></label>
											</td>
											<td style="color:black">
												<label></label>
											</td>
											<td style="color:black">
												<label> </label>
											</td>
											<td style="color:black">
												<label></label>
											</td>
											<td style="color:black">
												<label></label>
											</td>
											<td style="color:black">
												<label></label>
											</td>
											<td style="color:black"><label> </label></td>
											<td style="color:black"><label> </label></td>


										</tr>

										<tr>
											<td style="color:black"><p>খাতের স্থিতি </p></td>
											<td style="color:black"><label></label></td>
											<td style="color:black">
												<label> </label>
											</td>
											<td style="color:black">
												<label> </label>
											</td>
											<td style="color:black">
												<label></label>
											</td>
											<td style="color:black">
												<label></label>
											</td>
											<td style="color:black">
												<label></label>
											</td>
											<td style="color:black">
												<label> </label>
											</td>
											<td style="color:black">
												<label></label>
											</td>
											<td style="color:black">
												<label></label>
											</td>
											<td style="color:black">
												<label></label>
											</td>
											<td style="color:black">
												<label></label>
											</td>
											<td style="color:black">
												<label> </label>
											</td>
											<td style="color:black">
												<label></label>
											</td>
											<td style="color:black">
												<label></label>
											</td>
											<td style="color:black">
												<label></label>
											</td>
											<td style="color:black"><label> </label></td>
											<td style="color:black"><label> </label></td>


										</tr>


									</table>

								</div>
							</div>
						</div>
					</div>
				</div>


				<div class="div-padding2">
					<div class="form-group">
						<div class="row">
							<div class="col-md-12">
								<div class="col-md-4">
									<p style="color:black;">
										<input type="text" name="" value="সভাপতি " class="form-control">
									<hr>
									</p>
									<p style="text-align: center">
										সভাপতি
									</p>
								</div>
								<div class="col-md-4">
									<p style="color:black;">
										<input type="text" name="" value="সম্পাদক" class="form-control">
									<hr>
									</p>
									<p style="text-align: center">
										সম্পাদক
									</p>
								</div>
								<div class="col-md-4">
									<p style="color:black;">
										<input type="text" name="" value="ব্যবস্থাপক" class="form-control">
									<hr>
									</p>
									<p style="text-align: center">
										ব্যবস্থাপক
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>





