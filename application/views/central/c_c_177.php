<style>
	p {
		font-family: Roboto;
		font-size: medium;
		color: black;
		text-align: justify;
	}

	h4 {
		font-family: Roboto;

		color: black;
	}

	.div-padding1 {

		padding-top: 10px;
		padding-right: 30px;
		padding-bottom: 10px;
		padding-left: 30px;
	}

	.div-padding2 {

		padding-top: 0px;
		padding-right: 30px;
		padding-bottom: 10px;
		padding-left: 30px;
	}

	.input-field1 {
		width: 30%;
		padding: 3px 20px;
		margin: 8px 0;
		display: inline-block;
		border: 1px solid #ccc;
		border-radius: 4px;
		box-sizing: border-box;
	}

	.text_color_th {
		color: white;
	}
</style>


<div class="" style="background-color:#4A235A">
	<div class="row">
		<div class="col-md-12">
			<center><h3 style="color:white"> শিক্ষিত বেকার কেন্দ্রীয় সঞ্চয় ও ঋণদান সমবায় সমিতি লিঃ </h3></center>
			<center><h3 style="color:white">কর্মসংস্থান ও দারিদ্র বিমোচন প্রকল্প</h3></center>
			<center><h5 style="color:white">নিবন্ধন নম্বর -১৩/চাঁদ /১২, তারিখ :০৯/০৭/২০১২ </h5></center>
			<center><h6 style="color:white">ঠিকানা: ঠাকুর বাজার ,শাহারাস্তি , চাঁদপুর। </h6></center>
			<br>

			<div class="" style="background-color:white">
				<div class="div-padding1">
					<div class="form-group">
						<div class="row">
							<div class="col-md-6">
							</div>
							<div class="col-md-6 text-right">
								<h5 style="color:black">সি -১৭৭</h5>
							</div>
							<div class="col-md-12 text-center">
								<h3 style="color:black">
									মাসিক খাত ভিত্তিক পাওনা হিসাব
								</h3>
								<h5 style="color:black; text-align:right">মাসের নাম :<?php echo date('M/yy') ?></h5>

							</div>
						</div>
					</div>
				</div>


				<div class="div-padding2">
					<div class="form-group">
						<div class="row">
							<div class="col-md-12">

								<div class="table-responsive">
									<table class="table table-bordered">


										<tr style="background:  #dd3333">
											<th class="text-center text_color_th">খাত</th>
											<th class="text-center text_color_th">ব্যাংক মজুদ
											</th>
											<th class="text-center text_color_th">হস্ত মজুদ
											</th>
											<th class="text-center text_color_th">কর্জ /বিনিয়োগ
											</th>
											<th class="text-center text_color_th">আসবাবপত্র
											</th>
											<th class="text-center text_color_th">হওলাত প্রদান
											</th>
											<th class="text-center text_color_th">শেয়ার মার্কেটে বিনিয়োগ
											</th>
											<th class="text-center text_color_th">
												ষ্টোরের মজুদ মালামাল
											</th>
											<th class="text-center text_color_th">অর্থগ্রহণ পাওনা
											</th>
											<th class="text-center text_color_th">সর্বমোট পাওনা
											</th>
										</tr>
										<tr style="background:#dd3333">

											<th class="text-center text_color_th">০১
											</th>

											<th class="text-center text_color_th">০২
											</th>
											<th class="text-center text_color_th">০৩
											</th>
											<th class="text-center text_color_th">০৪
											</th>
											<th class="text-center text_color_th">০৫
											</th>
											<th class="text-center text_color_th">০৬
											</th>
											<th class="text-center text_color_th">০৭
											</th>
											<th class="text-center text_color_th">০৮
											</th>
											<th class="text-center text_color_th">০৯
											</th>
											<th class="text-center text_color_th">১০
											</th>


										</tr>
										<tr>
											<td><p> খরচ/ব্যয় </p></td>
											<td><label></label></td>
											<td style="color:black">
												<label> </label>
											</td>
											<td style="color:black">
												<label> </label>
											</td>
											<td style="color:black">
												<label></label>
											</td>
											<td style="color:black">
												<label></label>
											</td>
											<td style="color:black">
												<label></label>
											</td>
											<td style="color:black">
												<label> </label>
											</td>
											<td style="color:black">
												<label></label>
											</td>
											<td style="color:black">
												<label></label>
											</td>

										</tr>
										<tr>
											<td><p> জমা/আয়</p></td>
											<td style="color:black"><label></label></td>
											<td style="color:black"><label></label></td>
											<td style="color:black"><label></label></td>
											<td style="color:black">
												<label> </label>
											</td>
											<td style="color:black">
												<label> </label>
											</td>
											<td style="color:black">
												<label></label>
											</td>
											<td style="color:black">
												<label></label>
											</td>
											<td style="color:black">
												<label></label>
											</td>
											<td style="color:black">
												<label> </label>
											</td>

										</tr>

										<tr>
											<td><p>অবশিষ্ট </p></td>
											<td style="color:black"><label></label></td>
											<td style="color:black">
												<label> </label>
											</td>
											<td style="color:black">
												<label> </label>
											</td>
											<td style="color:black">
												<label></label>
											</td>
											<td style="color:black">
												<label></label>
											</td>
											<td style="color:black">
												<label></label>
											</td>
											<td style="color:black">
												<label> </label>
											</td>
											<td style="color:black">
												<label></label>
											</td>
											<td style="color:black">
												<label></label>
											</td>

										</tr>
										<tr>
											<td><p>আগত </p></td>
											<td style="color:black"><label></label></td>
											<td style="color:black">
												<label> </label>
											</td>

											<td style="color:black">
												<label></label>
											</td>
											<td style="color:black">
												<label></label>
											</td>
											<td style="color:black">
												<label> </label>
											</td>
											<td style="color:black">
												<label></label>
											</td>
											<td style="color:black">
												<label></label>
											</td>
											<td style="color:black">
												<label></label>
											</td>
											<td style="color:black">
												<label></label>
											</td>
										</tr>
										<tr>
											<td><p>সর্বমোট </p></td>
											<td style="color:black"><label></label></td>

											<td style="color:black">
												<label></label>
											</td>
											<td style="color:black">
												<label></label>
											</td>
											<td style="color:black">
												<label></label>
											</td>
											<td style="color:black">
												<label> </label>
											</td>
											<td style="color:black">
												<label></label>
											</td>
											<td style="color:black">
												<label></label>
											</td>
											<td style="color:black">
												<label></label>
											</td>
											<td style="color:black">
												<label></label>
											</td>
										</tr>
										<tr>
											<td><p>খাতের স্থিতি </p></td>
											<td style="color:black"><label></label></td>
											<td style="color:black">
												<label> </label>
											</td>

											<td style="color:black">
												<label></label>
											</td>
											<td style="color:black">
												<label></label>
											</td>
											<td style="color:black">
												<label> </label>
											</td>
											<td style="color:black">
												<label></label>
											</td>
											<td style="color:black">
												<label></label>
											</td>
											<td style="color:black">
												<label></label>
											</td>
											<td style="color:black">
												<label></label>
											</td>
										</tr>
										<tr>
											<td><p>ব্যাংকে বাস্তব স্থিতি</p></td>
											<td style="color:black"><label></label></td>
										</tr>


									</table>

								</div>
							</div>
						</div>
					</div>
				</div>


				<div class="div-padding2">
					<div class="form-group">
						<div class="row">
							<div class="col-md-12">
								<div class="col-md-4">
									<p style="color:black;">
										<input type="text" name="" value="সভাপতি " class="form-control">
									<hr>
									</p>
									<p style="text-align: center">
										সভাপতি
									</p>
								</div>
								<div class="col-md-4">
									<p style="color:black;">
										<input type="text" name="" value="সম্পাদক" class="form-control">
									<hr>
									</p>
									<p style="text-align: center">
										সম্পাদক
									</p>
								</div>
								<div class="col-md-4">
									<p style="color:black;">
										<input type="text" name="" value="ব্যবস্থাপক" class="form-control">
									<hr>
									</p>
									<p style="text-align: center">
										ব্যবস্থাপক
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


