<style>
	p {
		font-family: Roboto;
		font-size: medium;
		color: black;
		text-align: justify;
	}

	h4 {
		font-family: Roboto;

		color: black;
	}

	.div-padding1 {

		padding-top: 10px;
		padding-right: 30px;
		padding-bottom: 10px;
		padding-left: 30px;
	}

	.div-padding2 {

		padding-top: 0px;
		padding-right: 30px;
		padding-bottom: 10px;
		padding-left: 30px;
	}

	.input-field1 {
		width: 50%;
		padding: 5px 20px;
		margin: 8px 0;
		display: inline-block;
		border: 1px solid #ccc;
		border-radius: 4px;
		box-sizing: border-box;
	}

	.text_color_th {
		color: white;
	}
</style>


<div class="" style="background-color:#4A235A">
	<div class="row">
		<div class="col-md-12">
			<center><h3 style="color:white"> শিক্ষিত বেকার কেন্দ্রীয় সঞ্চয় ও ঋণদান সমবায় সমিতি লিঃ </h3></center>
			<center><h3 style="color:white">কর্মসংস্থান ও দারিদ্র বিমোচন প্রকল্প</h3></center>
			<center><h5 style="color:white">নিবন্ধন নম্বর -১৩/চাঁদ /১২, তারিখ :০৯/০৭/২০১২ </h5></center>
			<center><h6 style="color:white">ঠিকানা: ঠাকুর বাজার ,শাহারাস্তি , চাঁদপুর। </h6></center>
			<br>

			<div class="" style="background-color:white">
				<div class="div-padding1">
					<div class="form-group">
						<div class="row">
							<div class="col-md-6">

							</div>
							<div class="col-md-6 text-right">
								<h5 style="color:black">সি -১৮১</h5>
							</div>
							<div class="col-md-12 text-center">
								<h2 style="color:black">
									<?php echo date('d/m/yy') ?>তারিখে প্রাপ্ত আসবাবপত্রসহ মজুদ মালামালের তালিকা
								</h2>
							</div>

						</div>
					</div>
				</div>

				<div class="div-padding2">
					<div class="form-group">
						<div class="row">
							<div class="col-md-12">

								<div class="table-responsive">
									<table class="table table-bordered">
										<tr style="background:  #dd3333">
											<th class="text-center text_color_th" rowspan="2">
												ক্র নং
											</th>
											<th class="text-center text_color_th" rowspan="2">
												দ্রব্যের নাম
											</th>


											<th class="text-center text_color_th" colspan="2">
												বহি অনুসারে
											</th>
											<th class="text-center text_color_th" colspan="2">
												প্রাপ্ত মালামালের তালিকা
											</th>

											<th class="text-center text_color_th" colspan="2">
												ঘাটিত /অপচয়
											</th>

											<th class="text-center text_color_th" colspan="2">
												উদৃত্ত
											</th>
											<th class="text-center text_color_th" rowspan="2">
												মন্তব্য
											</th>


										</tr>


										<tr style="background:  #dd3333">
											<th class="text-center text_color_th">
												পরিমান
											</th>
											<th class="text-center text_color_th">
												মূল্য
											</th>
											<th class="text-center text_color_th">
												পরিমান
											</th>
											<th class="text-center text_color_th">
												মূল্য
											</th>
											<th class="text-center text_color_th">পরিমান
											</th>
											<th class="text-center text_color_th">মূল্য
											</th>
											<th class="text-center text_color_th">পরিমান
											</th>
											<th class="text-center text_color_th">মূল্য
											</th>
										</tr>
										<tr>
											<td><p>১ </p></td>
											<td><p>ভিজি চেয়ার গ্রাহক </p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
										</tr>
										<tr>
											<td><p>২ </p></td>
											<td><p>টেবিল বড় /ছোট </p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
										</tr>
										<tr>
											<td><p>৩</p></td>
											<td><p>হাইবেক চেয়ার</p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>

										</tr>
										<tr>
											<td><p>৪</p></td>
											<td><p> কাঠের চেয়ার </p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
										</tr>
										<tr>
											<td><p>৫</p></td>
											<td><p> কাঠের রেক </p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>

										</tr>
										<tr>
											<td><p>৬</p></td>
											<td><p> পাখা</p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>

										</tr>
										<tr>
											<td><p>৭</p></td>
											<td><p> স্টিলের আলমারি</p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>

										</tr>
										<tr>
											<td><p>৮</p></td>
											<td><p> মোবাইল সেট</p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>

										</tr>
										<tr>
											<td><p>৯</p></td>
											<td><p>টেবিল সভাপতি </p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>

										</tr>
										<tr>
											<td><p> ১০ </p></td>
											<td><p>জুইদান চেয়ার সভাপতি </p></td>
										</tr>
										<tr>
											<td><p>১১ </p></td>
											<td><p> কম্পিউটার টেবিল</p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
										</tr>
										<tr>
											<td><p>১২</p></td>
											<td><p> স্টিল রেক </p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>

										</tr>
										<tr>
											<td><p>১৩</p></td>
											<td><p> স্পেশাল সোকেজ</p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
										</tr>
										<tr>
											<td><p> ১৪ </p></td>
											<td><p>ডিজিটাল হাইবেক চেয়ার বড়</p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>

										</tr>
										<tr>
											<td><p>১৫</p></td>
											<td><p> এয়ার কন্ডিশন</p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>

										</tr>
										<tr>
											<td><p>১৬</p></td>
											<td><p> কম্পিউটার </p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>

										</tr>
										<tr>
											<td><p>১৭</p></td>
											<td><p> ফাইল কেবিনেট </p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>

										</tr>
										<tr>
											<td><p>১৮</p></td>
											<td><p> লেজার প্রিন্টার </p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
										</tr>
										<tr>
											<td><p>১৯</p></td>
											<td><p> খাট সভাপতি </p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
										</tr>
										<tr>
											<td><p>২০</p></td>
											<td><p> আই পি এস </p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
										</tr>
										<tr>
											<td><p>২১</p></td>
											<td><p> সাউন্ড সিস্টেম</p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
										</tr>
										<tr>
											<td><p>২২</p></td>
											<td><p> সি সি ক্যামেরা </p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
										</tr>
										<tr>
											<td><p>২৩</p></td>
											<td><p> টেলিভশন </p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
										</tr>
										<tr>
											<td><p>২৪</p></td>
											<td><p> স্টাফ চেয়ার</p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
										</tr>
										<tr>
											<td><p>২৫</p></td>
											<td><p> প্ল্যাস্টিক চেয়ার </p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
										</tr>

										<tr>

											<th colspan="2">
												<p style="text-align: right">	মোট =</p>
											</th>
											<th>
											</th>
											<th>
											</th>
											<th>
											</th>
											<th>
											</th>
											<th>
											</th>
											<th>
											</th>
											<th>
											</th>
											<th>
											</th>
											<th>
											</th>
										</tr>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div style="background-color:white">
				<?php echo "<br>"; ?>
				<div class="form-group">
					<div class="row">
						<div class="col-md-12">
							<?php echo "<br><br><br><br>"; ?>
							<div class="col-md-4">
								<p style="color:black;">
									<input type="text" name="" value="সভাপতি" class="form-control">
								<hr>
								</p>
								<p style="text-align: center">

									সভাপতি
								</p>
							</div>
							<div class="col-md-4">
								<p style="color:black;">
									<input type="text" name="" value="সম্পাদক" class="form-control">
								<hr>
								</p>
								<p style="text-align: center">
									সম্পাদক
								</p>

							</div>
							<div class="col-md-4">
								<p style="color:black;">
									<input type="text" name="" value="ব্যবস্থাপক" class="form-control">
								<hr>
								</p>

								<p style="text-align: center">ব্যবস্থাপক</p>

							</div>

						</div>
					</div>
				</div>
				<?php echo "<br>"; ?>

			</div>
		</div>
		<span><br></span>
	</div>
</div>




