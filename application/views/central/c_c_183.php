<style>
	p {
		font-family: Roboto;
		font-size: medium;
		color: black;
		text-align: justify;
	}

	h4 {
		font-family: Roboto;

		color: black;
	}

	.div-padding1 {

		padding-top: 10px;
		padding-right: 30px;
		padding-bottom: 10px;
		padding-left: 30px;
	}

	.div-padding2 {

		padding-top: 0px;
		padding-right: 30px;
		padding-bottom: 10px;
		padding-left: 30px;
	}

	.input-field1 {
		width: 50%;
		padding: 5px 20px;
		margin: 8px 0;
		display: inline-block;
		border: 1px solid #ccc;
		border-radius: 4px;
		box-sizing: border-box;
	}

	.text_color_th {
		color: white;
	}
</style>


<div class="" style="background-color:#4A235A">
	<div class="row">
		<div class="col-md-12">
			<center><h3 style="color:white"> শিক্ষিত বেকার কেন্দ্রীয় সঞ্চয় ও ঋণদান সমবায় সমিতি লিঃ </h3></center>
			<center><h3 style="color:white">কর্মসংস্থান ও দারিদ্র বিমোচন প্রকল্প</h3></center>
			<center><h5 style="color:white">নিবন্ধন নম্বর -১৩/চাঁদ /১২, তারিখ :০৯/০৭/২০১২ </h5></center>
			<center><h6 style="color:white">ঠিকানা: ঠাকুর বাজার ,শাহারাস্তি , চাঁদপুর। </h6></center>
			<br>

			<div class="" style="background-color:white">
				<div class="div-padding1">
					<div class="form-group">
						<div class="row">
							<div class="col-md-6">

							</div>
							<div class="col-md-6 text-right">
								<h5 style="color:black">সি -১৮৩</h5>
							</div>
							<div class="col-md-12 text-center">
								<h2 style="color:black">
									<?php echo date('d/m/yy') ?> তারিখ পযন্ত ঋণ/বিনিয়োগ হতে মূলধন পাওনা ডিটেল লিস্ট
								</h2>
							</div>

						</div>
					</div>
				</div>
				<div class="div-padding2">
					<div class="form-group">
						<div class="row">
							<div class="col-md-12">

								<div class="table-responsive">
									<table class="table table-bordered">
										<tbody>

										<tr style="background:  #dd3333">
											<th class="text-center text_color_th">ক্র নং</th>
											<th class="text-center text_color_th">
												বিনিয়োগ গ্রহণকারীর নাম
											</th>

											<th class="text-center text_color_th">
												ঠিকানা
											</th>
											<th class="text-center text_color_th">
												এককালীন হিসাব নং
											</th>
											<th class="text-center text_color_th">
												কিস্তিতে হিসাব নং
											</th>
											<th class="text-center text_color_th">
												এককালীন ঋণ /বিনিয়োগের প্রদানের তাং
											</th>
											<th class="text-center text_color_th">
												কিস্তিতে ঋণ /বিনিয়োগের প্রদানের তাং
											</th>

											<th class="text-center text_color_th">
												এককালীন ঋণ /বিনিয়োগের স্থিতি
											</th>
											<th class="text-center text_color_th">
												কিস্তিতে ঋণ /বিনিয়োগের স্থিতি
											</th>
											<th class="text-center text_color_th">
												মোট বিনিয়োগ স্থিতি
											</th>

										</tr>
										</tbody>
										<?php
										$i = '';
										$sum = 0;
										for ($i = 1; $i <= 20; $i++) { ?>
											<tbody>
											<tr>
												<td style="color:black"><label><?php echo $i; ?></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black">
													<label> </label>
												</td>
												<td style="color:black">
													<label> </label>
												</td>
												<td style="color:black">
													<label></label>
												</td>
												<td style="color:black">
													<label></label>
												</td>
												<td style="color:black">
													<label></label>
												</td>

												<td style="color:black">
													<label> </label>
												</td>
												<td style="color:black">
													<label></label>
												</td>

												<td style="color:black">
													<label> </label>
												</td>

											</tr>
											</tbody>


											<?php

										} ?>
										<tbody>
										<tr>

											<th style="color:black" colspan="7">
												<p class="text-right"> মোট =</p>
											</th>
											<th>
											</th>
											<th>
											</th>
											<th>
											</th>

										</tr>
										</tbody>
									</table>
								</div>
							</div>


						</div>


					</div>
				</div>

			</div>


			<div style="background-color:white">

				<?php echo "<br>"; ?>
				<div class="form-group">
					<div class="row">

						<div class="col-md-12">


							<?php echo "<br><br><br><br>"; ?>

							<div class="col-md-4">
								<p style="color:black;">
									<input type="text" name="" value="প্রস্তুতকারী" class="form-control">
								<hr>
								</p>
								<p style="text-align: center">
									প্রস্তুতকারী
								</p>

							</div>

							<div class="col-md-4">


							</div>
							<div class="col-md-4">
								<p style="color:black;">
									<input type="text" name="" value="ব্যবস্থাপক" class="form-control">
								<hr>
								</p>
								<p style="text-align: center">
									ব্যবস্থাপক
								</p>
							</div>

						</div>
					</div>
				</div>
				<?php echo "<br>"; ?>

			</div>
		</div>
		<span><br></span>
	</div>
</div>




