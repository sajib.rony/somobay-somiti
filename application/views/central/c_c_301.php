<style>
	p {
		font-family: Roboto;
		font-size: medium;
		color: black;
		text-align: justify;
	}

	h4 {
		font-family: Roboto;

		color: black;
	}

	.div-padding1 {

		padding-top: 10px;
		padding-right: 30px;
		padding-bottom: 50px;
		padding-left: 80px;
	}

	.div-padding2 {

		padding-top: 20px;
		padding-right: 30px;
		padding-bottom: 50px;
		padding-left: 70px;
	}

	.input-field1 {
		width: 50%;
		padding: 5px 20px;
		margin: 8px 0;
		display: inline-block;
		border: 1px solid #ccc;
		border-radius: 4px;
		box-sizing: border-box;
	}

</style>

<div class="" style="background-color:#4A235A">
	<div class="row">
		<div class="col-md-12">
			<center><h3 style="color:white">শিক্ষিত বেকার কেন্দ্রীয় সঞ্চয় ও ঋণদান সমবায় সমিতি লিঃ</h3></center>
			<center><h3 style="color:white">কর্মসংস্থান ও দারিদ্র বিমোচন প্রকল্প</h3></center>
			<center><h5 style="color:white">নিবন্ধন নম্বর -১৩/চাঁদ /১২, তারিখ :০৯/০৭/২০১২ </h5></center>
			<center><h6 style="color:white">ঠিকানা: ঠাকুর বাজার ,শাহারাস্তি , চাঁদপুর। </h6></center>
			<br>
		</div>
	</div>
</div>
<div class="" style="background-color:white">
	<div class="div-padding1">
		<div class="form-group">
			<div class="row">
				<div class="col-md-6">

				</div>
				<div class="col-md-6 text-right">
					<h5 style="color:black"> সি.সি -৩০১</h5>
				</div>

			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-6">
					<h5 style="color:black">স্মারক নং - <?php echo "কেন্দ্রীয়-" . time() ?></h5>
				</div>
				<div class="col-md-6 text-right">
					<h5 style="color:black">তারিখ :<?php echo date('d/m/yy') ?></h5>
				</div>
				<div class="col-md-12 text-center">
					<h2 style="color:black">সদস্য আবেদন ফরম </h2>
				</div>

			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<p>বরাবর,</p>
					<p>সভাপতি /সম্পাদক,</p>
					<p>শিক্ষিত বেকার কেন্দ্রীয় সঞ্চয় ও ঋণদান সমবায় সমিতি লিঃ</p>
					<p>ওয়ারলেছ বাজার , চাঁদপুর সদর, চাঁদপুর। </p><br>
					<h4>বিষয় : সদস্য পদ অন্তুর্ভুক্তির আবেদন ।</h4><br>
					<p>জনাব,</p>
					<p>বিনীত নিবেদন এই যে , আমি নিন্মে স্বাক্ষরকারী আবেদনকৃত সমিতির পক্ষে ক্ষমতাপ্রাপ্ত হয়ে শিক্ষিত
						বেকার সঞ্চয় ও ঋণদান সমবায় সমিতি লিঃ এর সদস্য অন্তুর্ভুক্ত হতে বিশেষভাবে আগ্রহী। এ মর্মে শিক্ষিত
						বেকার সঞ্চয় ও ঋণদান সমবায় সমিতি লিঃ এর সকল আইন-কানুনসহ সমিতির উপ-আইন এবং সমবায় সমিতির আইন ও
						বিধিমালা মেনে চলার পূর্ণ অঙ্গীকার ব্যক্ত করছি।</p>


				</div>
			</div>
		</div>
	</div>
	<div class="div-padding2">
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-3">
						<p>সমিতির নাম</p>
					</div>
					<div class="col-md-9">
						<input type="text" name="" class="form-control" placeholder="সমিতির নাম">
					</div>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-3">
						<p>সমিতির কার্যালয়ের ঠিকানা</p>
					</div>
					<div class="col-md-9">
						<textarea type="text" name="" class="form-control"
								  placeholder="সমিতির কার্যালয়ের ঠিকানা"></textarea>
					</div>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-3">
						<p>সমিতির নিবন্ধন নম্বর ও তারিখ</p>
					</div>
					<div class="col-md-9">
						<input type="text" name="" class="form-control"
							   placeholder="সমিতির নিবন্ধন নম্বর ও তারিখ">
					</div>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-3">
						<p>সমিতির নিবন্ধন ঠিকানা</p>
					</div>
					<div class="col-md-9">
						<textarea type="text" name="" class="form-control"
								  placeholder="সমিতির নিবন্ধন নম্বর ও তারিখ"></textarea>
					</div>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-3">
						<p>সমিতির অনুমোদিত শেয়ার মূলধন</p>
					</div>
					<div class="col-md-9">
						<input type="text" name="" class="form-control"
							   placeholder="সমিতির অনুমোদিত শেয়ার মূলধন">
					</div>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-3">
						<p>সমিতির পরিশোধিত মূলধন</p>
					</div>
					<div class="col-md-9">
						<input type="text" name="" class="form-control"
							   placeholder="সমিতির অনুমোদিত শেয়ার মূলধন">
					</div>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-3">
						<p>সর্বশেষ অর্থ-বছরের নীট লাভ</p>
					</div>
					<div class="col-md-9">
						<input type="text" name="" class="form-control"
							   placeholder="সর্বশেষ অর্থ-বছরের নীট লাভ">
					</div>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-3">
						<p>সর্বশেষ বার্ষিক সাধারণ সভার তারিখ</p>
					</div>
					<div class="col-md-9">
						<input type="text" name="" class="form-control"
							   placeholder="সর্বশেষ বার্ষিক সাধারণ সভার তারিখ">
					</div>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-3">
						<p>মোবাইল নম্বর</p>
					</div>
					<div class="col-md-9">
						<input type="text" name="" class="form-control"
							   placeholder="মোবাইল নম্বর">
					</div>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-12">
						<p>অতএব, উক্ত বিবেচনা করে। <input type="text" name="" class="input-field1"
														  placeholder=".....................সমবায় সমিতি লিঃ">
							সমবায় সমিতি লিঃ কে শিক্ষিত বেকার কেন্দ্রীয় সঞ্চয়
							ও ঋণদান সমবায় সমিতি লিঃ এর সদস্য পদে
							অন্তুর্ভুক্তির অনুরোধ করছি।</p>
					</div>

				</div>
			</div>
		</div>
	</div>
	<div class="div-padding2">
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-6">
						<h4>সংযক্তি :</h4>
						<p>ক্ষমতাপত্র</p>
						<p>ব্যবস্থাপনা কমিটির সিদ্ধান্তের রেজুলেশন।</p>
						<p>নিবন্ধন সনদ</p>
						<p>ব্যবস্থাপনা কমিটির নির্বাচনে ফলাফল</p>
						<p>সমিতির অনুমোদিত উপ-আইন।</p>
					</div>
					<div class="col-md-6">
						<h4 class="text-center">নিবেদক (সমিতির পক্ষে )</h4><br>
						<div class="form-group">
							<div class="row">
								<div class="col-md-12">
									<div class="col-md-2">
										<p>নাম</p>
									</div>
									<div class="col-md-10">
										<input type="text" name="" class="form-control" placeholder="নাম">
									</div>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-md-12">
									<div class="col-md-2"><p>পদবী</p></div>
									<div class="col-md-10">
										<input type="text" name="" class="form-control" placeholder="পদবী">
									</div>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-md-12">
									<div class="col-md-2"><p>স্বাক্ষর</p></div>
									<div class="col-md-2"><input type="checkbox" name="" class="form-control"></div>
									<div class="col-md-8"></div>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
	<div class="form-group">
		<div class="row">
			<div class="col-md-12">
				<center><br><br>
					<button type="submit" style="background: #dd3333" class="btn btn-danger">Submit</button>
				</center>
				<br>
			</div>
		</div>
	</div>
</div>

