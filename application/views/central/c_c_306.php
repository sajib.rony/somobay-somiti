<style>
	p {
		font-family: Roboto;
		font-size: medium;
		color: black;
		text-align: justify;
	}

	h4 {
		font-family: Roboto;

		color: black;
	}

	.div-padding1 {

		padding-top: 10px;
		padding-right: 30px;
		padding-bottom: 10px;
		padding-left: 30px;
	}

	.div-padding2 {

		padding-top: 20px;
		padding-right: 30px;
		padding-bottom: 10px;
		padding-left: 30px;
	}

	.input-field1 {
		width: 50%;
		padding: 5px 20px;
		margin: 8px 0;
		display: inline-block;
		border: 1px solid #ccc;
		border-radius: 4px;
		box-sizing: border-box;
	}

	.text_color_th {
		color: white;
	}
</style>

<div class="" style="background-color:#4A235A">
	<div class="row">
		<div class="col-md-12">
			<center><h3 style="color:white">শিক্ষিত বেকার কেন্দ্রীয় সঞ্চয় ও ঋণদান সমবায় সমিতি লিঃ</h3></center>
			<center><h3 style="color:white">কর্মসংস্থান ও দারিদ্র বিমোচন প্রকল্প</h3></center>
			<center><h5 style="color:white">নিবন্ধন নম্বর -১৩/চাঁদ /১২, তারিখ :০৯/০৭/২০১২ </h5></center>
			<center><h6 style="color:white">ঠিকানা: ঠাকুর বাজার ,শাহারাস্তি , চাঁদপুর। </h6></center>
			<br>
		</div>
	</div>
</div>
<div class="" style="background-color:white">
	<div class="div-padding1">
		<div class="form-group">
			<div class="row">
				<div class="col-md-6">

				</div>
				<div class="col-md-6 text-right">
					<h5 style="color:black"> সি.সি -৩০৬</h5>
				</div>

			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-6">
					<h5 style="color:black">স্মারক নং - <?php echo "কেন্দ্রীয়-" . time() ?></h5>
				</div>
				<div class="col-md-6 text-right">
					<h5 style="color:black">তারিখ :<?php echo date('d/m/yy') ?></h5>
				</div>
				<div class="col-md-12 text-center">
					<h2 style="color:black">অস্থায়ী আমানত নীতিমালা ও আবেদন ফরম </h2>
				</div>
			</div>
		</div>
		<!--		<div class="form-group">-->
		<!--			<div class="row">-->
		<!--				<div class="col-md-12">-->
		<!--					<p></p>-->
		<!--				</div>-->
		<!--			</div>-->
		<!--		</div>-->
	</div>
	<div class="div-padding2">
		<div class="form-group">
			<div class="row">
				<div class="col-md-12 text-center">
					<h2 style="color:black">পরিচিত </h2>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-4">
						<p>সদস্য সমিতির নাম </p><input type="text" name="" class="form-control"
													   placeholder="সদস্য সমিতির নাম ">
					</div>
					<div class="col-md-4">
						<p>সদস্য নম্বর </p><input type="text" name="" class="form-control" placeholder="সদস্য নম্বর ">
					</div>
					<div class="col-md-4">
						<p>স্থায়ী আমানত জমার তারিখ </p><input type="text" name="" class="form-control"
															  placeholder="স্থায়ী আমানত জমার তারিখ ">
					</div>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-4">
						<p>মেয়াদ </p><input type="text" name="" class="form-control" placeholder="মেয়াদ ">
					</div>
					<div class="col-md-4">
						<p>অস্থায়ী আমানত জমার পরিমান </p><input type="text" name="" class="form-control"
																placeholder="টাকা ">
					</div>
					<div class="col-md-4">
						<p>সমিতির পক্ষে জমাকারীর স্বাক্ষর </p><input type="text" name="" class="form-control"
																	 placeholder="সমিতির পক্ষে জমাকারীর স্বাক্ষর ">
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="div-padding2">
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<div class="table-responsive">
						<table class="table-responsive table table-bordered">
							<tr style="background: #dd3333">
								<th colspan="5" class="text-center text_color_th">ক্ষমতাপ্রাপ্ত ব্যাক্তিবর্গের পরিচিতি ও
									নমুনা স্বাক্ষর
								</th>
							</tr>
							<tr style="background: #dd3333">
								<th class="text-center text_color_th">নং</th>
								<th class="text-center text_color_th">নাম</th>
								<th class="text-center text_color_th">পদবী</th>
								<th class="text-center text_color_th">স্বাক্ষর</th>
								<th class="text-center text_color_th">তারিখ</th>
							</tr>
							<?php for ($i = 1;
									   $i <= 6;
									   $i++) { ?>
								<tr>
									<td><p><?php echo $i; ?></p></td>
									<td><input type="text" name="" class="form-control" placeholder="নাম"></td>
									<td><input type="text" name="" class="form-control" placeholder="পদবী"></td>
									<td><input type="text" name="" class="form-control" placeholder="স্বাক্ষর"></td>
									<td><input type="text" name="" class="form-control" placeholder="তারিখ"></td>
								</tr>
							<?php } ?>
							<tr style="background: #dd3333">
								<th colspan="5" class="text_color_th">
									আমানতের টাকা উত্তোলনকালীন নির্দেশনা
								</th>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="div-padding2">
		<div class="form-group">
			<div class="row">
				<div class="col-md-12 text-center">
					<h2 style="color:black">সমিতির ব্যবস্থাপনা কমিটি কর্তৃক অনুমোদন </h2>
				</div>
			</div>
		</div>

		<div class="form-group">
			<?php echo "<br>" ?>
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-4">
						<p>জমার তারিখ </p>
						<input type="text" class="form-control" placeholder="জমার তারিখ">
					</div>
					<div class="col-md-4">
						<p>জমা খতিয়ানের পৃষ্ঠা নম্বর </p>
						<input type="text" class="form-control" placeholder="জমা খতিয়ানের পৃষ্ঠা নম্বর ">
					</div>
					<div class="col-md-4">
						<p>সদস্য নম্বর</p>
						<input type="text" class="form-control" placeholder="সদস্য নম্বর">
					</div>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-4">
						<p>জমা রশিদ নম্বর</p>
						<input type="text" class="form-control" placeholder="জমা রশিদ নম্বর">
					</div>
					<div class="col-md-4">
						<p>সনদ নম্বর</p>
						<input type="text" class="form-control" placeholder="সনদ নম্বর">
					</div>
					<div class="col-md-4">
						<p>অস্থায়ী আমানতের পরিমান </p>
						<input type="text" class="form-control" placeholder="স্থায়ী আমানতের পরিমান ">
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="div-padding2">
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-9">

					</div>
					<div class="col-md-3">
						<hr>
						<p style="text-align: center">সভাপতি /সম্পাদক</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="form-group">
		<div class="row">
			<div class="col-md-12">
				<center><br><br>
					<button type="submit" style="background: #dd3333" class="btn btn-danger">Submit</button>
				</center>
				<br>
			</div>
		</div>
	</div>
</div>
</div>
