<style>
	p {
		font-family: Roboto;
		font-size: medium;
		color: black;
		text-align: justify;
	}

	h4 {
		font-family: Roboto;

		color: black;
	}

	.div-padding1 {

		padding-top: 10px;
		padding-right: 30px;
		padding-bottom: 10px;
		padding-left: 30px;
	}

	.div-padding2 {

		padding-top: 0px;
		padding-right: 30px;
		padding-bottom: 10px;
		padding-left: 30px;
	}

	.input-field1 {
		width: 30%;
		padding: 5px 20px;
		margin: 8px 0;
		display: inline-block;
		border: 1px solid #ccc;
		border-radius: 4px;
		box-sizing: border-box;
	}

	.text_color_th {
		color: white;
	}
</style>

<div class="" style="background-color:#4A235A">
	<div class="row">
		<div class="col-md-12">
			<center><h3 style="color:white">শিক্ষিত বেকার কেন্দ্রীয় সঞ্চয় ও ঋণদান সমবায় সমিতি লিঃ</h3></center>
			<center><h3 style="color:white">কর্মসংস্থান ও দারিদ্র বিমোচন প্রকল্প</h3></center>
			<center><h5 style="color:white">নিবন্ধন নম্বর -১৩/চাঁদ /১২, তারিখ :০৯/০৭/২০১২ </h5></center>
			<center><h6 style="color:white">ঠিকানা: ঠাকুর বাজার ,শাহারাস্তি , চাঁদপুর। </h6></center>
			<br>
		</div>
	</div>
</div>
<div class="" style="background-color:white">
	<div class="div-padding1">
		<div class="form-group">
			<div class="row">
				<div class="col-md-6">

				</div>
				<div class="col-md-6 text-right">
					<h5 style="color:black"> সি.সি -৩০৭</h5>
				</div>

			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-6">
					<h5 style="color:black">স্মারক নং - <?php echo "কেন্দ্রীয়-" . time() ?></h5>
				</div>
				<div class="col-md-6 text-right">
					<h5 style="color:black">তারিখ :<?php echo date('d/m/yy') ?></h5>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<p>বরাবর,</p>
					<p>সভাপতি /সম্পাদক,</p>
					<p>শিক্ষিত বেকার কেন্দ্রীয় সঞ্চয় ও ঋণদান সমবায় সমিতি লিঃ</p>
					<p>ওয়ারলেছ বাজার , চাঁদপুর সদর, চাঁদপুর। </p><br>
					<h4>বিষয় : সদস্যপদের জমাকৃত  স্থায়ী/অস্থায়ী আমানত উত্তোলনের আবেদন। </h4><br>
					<p>জনাব,</p>

					<p>
						উল্লেখিত বিষয়ের আলোকে আপনাকে জানানো যাচ্ছে যে, আমি নিন্ম স্বাক্ষরকারীর ক্ষমতাপ্রাপ্ত হয়ে।
						<input type="text" name="" class="input-field1"
							   placeholder="....................."> সমবায় সমিতির সদস্য পদের জমাকৃত আমানত উত্তোলনের আবেদন
						করছি। সমিতির
						ব্যবস্থাপনা কমিটির <input type="text" name="" class="input-field1"
												  placeholder="তারিখ"> তারিখে সভার সিদ্ধান্ত মোতাবেক আমাকে ক্ষমতা
						দেয়ায় সমিতির সদস্য পদের জমাকৃত স্থায়ী/অস্থায়ী আমানত উত্তোলনের সমিতির পক্ষে টাকা গ্রহণ করবো।
						আবেদনকৃত স্থায়ী/অস্থায়ী আমানত উত্তোলনের আমানতটি <input type="text" name="" class="input-field1"
																			   placeholder="তারিখ">
						তারিখে
						<input type="text" name="" class="input-field1"
							   placeholder="টাকা"> টাকা , আবেদন ফরম <input type="text" name=""
																		   class="input-field1"
																		   placeholder=".....................">
						এর মাধ্যমে জমা রাখা হয়েছিল।
					</p>
					<p>
						এমতাবস্তায় সমিতির সদস্যপদে নামের জমাকৃত স্থায়ী/অস্থায়ী আমানত উত্তোলনের আবেদনটি মঞ্জুর করতে সবিনয়
						অনুরোধ করছি।
					</p>


				</div>
			</div>
		</div>
	</div>
	<div class="div-padding2">
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<div class="table-responsive">
						<table class="table table-bordered">
							<tr style="background: #dd3333">
								<th colspan="5" class="text-center text_color_th">আবেদনকৃত টাকা উত্তোলনের ক্ষমতাপ্রাপ্ত
									ব্যাক্তিবর্গের পরিচিতি ও নমুনা স্বাক্ষর
								</th>
							</tr>
							<tr style="background: #dd3333">
								<th class="text-center text_color_th">নং</th>
								<th class="text-center text_color_th">নাম</th>
								<th class="text-center text_color_th">পদবী</th>
								<th class="text-center text_color_th">স্বাক্ষর</th>
								<th class="text-center text_color_th">তারিখ</th>
							</tr>
							<?php for ($i = 1;
									   $i <= 6;
									   $i++) { ?>
								<tr>
									<td><p><?php echo $i; ?></p></td>
									<td><input type="text" name="" class="form-control" placeholder="নাম"></td>
									<td><input type="text" name="" class="form-control" placeholder="পদবী"></td>
									<td><input type="text" name="" class="form-control" placeholder="স্বাক্ষর"></td>
									<td><input type="text" name="" class="form-control" placeholder="তারিখ"></td>
								</tr>
							<?php } ?>
							<tr style="background: #dd3333">
								<th colspan="5" class="text_color_th">আমানতের টাকা উত্তোলনকালীন নির্দেশনা
								</th>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="div-padding2">

		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-12">
						<p>সংযুক্তি    :</p>
					</div>
					<div class="col-md-4">
						<p>সদস্য পদ  স্থায়ী/অস্থায়ী আমানত জমা /উত্তোলনের ক্ষমতাপত্র</p>
					</div>
					<div class="col-md-4">
						<input type="text" class="form-control" name="">
					</div>
					<div class="col-md-4">
						<p>কপি </p>
					</div>


				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-4">
						<p>ব্যবস্থাপনা কমিটির সিদ্ধান্তের রেজুলেশন সত্যায়িত </p>
					</div>
					<div class="col-md-4">
						<input type="text" class="form-control" name="">
					</div>
					<div class="col-md-4">
						<p>পাতা </p>
					</div>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-4">
						<p>সদস্য পদের শেয়ার সনদপত্র </p>
					</div>
					<div class="col-md-4">
						<input type="text" class="form-control" name="">
					</div>
					<div class="col-md-4">
						<p>কপি </p>
					</div>
				</div>
			</div>
		</div>
	</div>


	<div class="form-group">
		<div class="row">
			<div class="col-md-12">
				<center><br><br>
					<button type="submit" style="background: #dd3333" class="btn btn-danger">Submit</button>
				</center>
				<br><br>
			</div>
		</div>
	</div>
</div>

