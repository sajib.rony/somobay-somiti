<style>
	p {
		font-family: Roboto;
		font-size: medium;
		color: black;
		text-align: justify;
	}

	h4 {
		font-family: Roboto;

		color: black;
	}

	.div-padding1 {

		padding-top: 10px;
		padding-right: 30px;
		padding-bottom: 50px;
		padding-left: 30px;
	}

	.div-padding2 {

		padding-top: 20px;
		padding-right: 30px;
		padding-bottom: 50px;
		padding-left: 30px;
	}

	.input-field1 {
		width: 30%;
		padding: 5px 20px;
		margin: 8px 0;
		display: inline-block;
		border: 1px solid #ccc;
		border-radius: 4px;
		box-sizing: border-box;
	}

</style>

<div class="" style="background-color:#4A235A">
	<div class="row">
		<div class="col-md-12">
			<center><h3 style="color:white">শিক্ষিত বেকার কেন্দ্রীয় সঞ্চয় ও ঋণদান সমবায় সমিতি লিঃ</h3></center>
			<center><h3 style="color:white">কর্মসংস্থান ও দারিদ্র বিমোচন প্রকল্প</h3></center>
			<center><h5 style="color:white">নিবন্ধন নম্বর -১৩/চাঁদ /১২, তারিখ :০৯/০৭/২০১২ </h5></center>
			<center><h6 style="color:white">ঠিকানা: ঠাকুর বাজার ,শাহারাস্তি , চাঁদপুর। </h6></center>
			<br>
		</div>
	</div>
</div>
<div class="" style="background-color:white">
	<div class="div-padding1">
		<div class="form-group">
			<div class="row">
				<div class="col-md-6">

				</div>
				<div class="col-md-6 text-right">
					<h5 style="color:black"> সি.সি -৩০৮</h5>
				</div>

			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-6">
					<h5 style="color:black">স্মারক নং - <?php echo "কেন্দ্রীয়-" . time() ?></h5>
				</div>
				<div class="col-md-6 text-right">
					<h5 style="color:black">তারিখ :<?php echo date('d/m/yy') ?></h5>
				</div>
				<div class="col-md-12 text-center">
					<h2 style="color:black"> স্থায়ী/অস্থায়ী আমানত জমা/উত্তোলনের ক্ষমতাপত্র </h2>
				</div>

			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<p>
						জনাব <input type="text" name="" class="input-field1"
									placeholder=".....................">, পদবী
						<input type="text" name="" class="input-field1"
							   placeholder="পদবী"> তাকে ব্যবস্থাপনা
						কমিটির <input type="text" name="" class="input-field1"
									  placeholder="তারিখ"> তারিখের সভার সিদ্ধান্ত অনুযায়ী
						<input type="text" name="" class="input-field1"
							   placeholder="................."> সমবায় সমিতি লিঃ পক্ষে ক্ষমতা প্রদান করা হচ্ছে যে,
						শিক্ষিত
						বেকার কেন্দ্রীয় সঞ্চয় ও ঋণদান সমবায় সমিতি লিঃ এর সদস্যপদে জমাকৃত স্থায়ী/অস্থায়ী আমানত জমা এবং
						উত্তোলনের যাবতীয় কার্যাবলীর স্বাক্ষরের মাধ্যমে <input type="text" name="" class="input-field1"
																			  placeholder="তারিখ"> তারিখ
						হতে <input type="text" name="" class="input-field1"
								   placeholder="তারিখ"> পযন্ত দিনের জন্য ক্ষমতা প্রদান করা হলো। তবে উক্ত
						ক্ষমতা যেকোনো
						কারণে উক্ত সময়সীমার মধ্যেও বাতিল করা হতে পারে। সেক্ষেত্রে দ্রুত জানিয়ে দেয়া হবে। </p>


				</div>
			</div>
		</div>
	</div>

	<div class="div-padding2">
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-8">
						<h4>ক্ষমতাপ্রাপ্ত ব্যাক্তির স্বাক্ষর </h4>

						<p>১।</p>
						<p>২।</p>
						<p>৩।</p>
					</div>
					<div class="col-md-4">
						<p style="color:black;">
							<input type="text" name="" value="সভাপতি/সম্পাদক" class="form-control">
						<hr>
						</p>
						<p style="text-align: center">

							সভাপতি/সম্পাদক
						</p>


					</div>

				</div>
			</div>
		</div>
	</div>
	<div class="div-padding2">
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<center><br><br>
						<button type="submit" style="background: #dd3333" class="btn btn-danger">Submit</button>
					</center>
					<br>
				</div>
			</div>
		</div>
	</div>
</div>
