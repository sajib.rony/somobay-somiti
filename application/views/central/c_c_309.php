<style>
	p {
		font-family: Roboto;
		font-size: medium;
		color: black;
		text-align: justify;
	}

	h4 {
		font-family: Roboto;

		color: black;
	}

	.div-padding1 {

		padding-top: 10px;
		padding-right: 30px;
		padding-bottom: 50px;
		padding-left: 30px;
	}

	.div-padding2 {

		padding-top: 20px;
		padding-right: 30px;
		padding-bottom: 50px;
		padding-left: 30px;
	}

	.input-field1 {
		width: 30%;
		padding: 5px 20px;
		margin: 8px 0;
		display: inline-block;
		border: 1px solid #ccc;
		border-radius: 4px;
		box-sizing: border-box;
	}

	.text_color_th {
		color: white;
	}
</style>

<div class="" style="background-color:#4A235A">
	<div class="row">
		<div class="col-md-12">
			<center><h3 style="color:white">শিক্ষিত বেকার কেন্দ্রীয় সঞ্চয় ও ঋণদান সমবায় সমিতি লিঃ</h3></center>
			<center><h3 style="color:white">কর্মসংস্থান ও দারিদ্র বিমোচন প্রকল্প</h3></center>
			<center><h5 style="color:white">নিবন্ধন নম্বর -১৩/চাঁদ /১২, তারিখ :০৯/০৭/২০১২ </h5></center>
			<center><h6 style="color:white">ঠিকানা: ঠাকুর বাজার ,শাহারাস্তি , চাঁদপুর। </h6></center>
			<br>
		</div>
	</div>
</div>
<div class="" style="background-color:white">
	<div class="div-padding1">
		<div class="form-group">
			<div class="row">
				<div class="col-md-6">

				</div>
				<div class="col-md-6 text-right">
					<h5 style="color:black"> সি.সি -৩০৯</h5>
				</div>

			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-6">
					<h5 style="color:black">স্মারক নং - <?php echo "কেন্দ্রীয়-" . time() ?></h5>
				</div>
				<div class="col-md-6 text-right">
					<h5 style="color:black">তারিখ :<?php echo date('d/m/yy') ?></h5>
				</div>

			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<p>বরাবর,</p>
					<p>সভাপতি /সম্পাদক</p>
					<p>ব্যবস্থাপনা কমিটি</p>
					<p>শিক্ষিত বেকার কেন্দ্রীয় সঞ্চয় ও ঋণদান সমবায় সমিতি লিঃ</p><br>
					<h4>বিষয় : বিনিয়োগ /ঋণ পাওয়ার আবেদন ।</h4><br>
					<p>জনাব,</p>
					<p>আমরা ব্যবস্থাপনা কমিটির সিদ্ধান্ত মোতাবেক নিম্ম স্বাক্ষরকারী ক্ষমতাপ্রাপ্ত হয়ে সমিতির বিশেষ
						প্রয়োজনে বিনিয়োগ /ঋণ গ্রহণের উদ্দেশ্যে সমিতির তথ্যাদি নিন্মে পেশ করলাম।
					</p>
				</div>
			</div>
		</div>
	</div>
	<div class="div-padding2">
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-3">
						<p>সমিতির নাম</p>
					</div>
					<div class="col-md-9">
						<input type="text" name="" class="form-control" placeholder="সমিতির নাম">
					</div>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-3">
						<p>সমিতির নিবন্ধন নম্বর ও তারিখ</p>
					</div>
					<div class="col-md-5">
						<input type="text" name="" class="form-control"
							   placeholder="সমিতির নিবন্ধন নম্বর">
					</div>
					<div class="col-md-4">
						<input type="text" name="" class="form-control"
							   placeholder="তারিখ">
					</div>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-3">
						<p>নিবন্ধন ঠিকানা ও মোবাইল নম্বর</p>
					</div>
					<div class="col-md-5">
						<input type="text" name="" class="form-control"
							   placeholder="নিবন্ধন ঠিকানা">
					</div>
					<div class="col-md-4">
						<input type="text" name="" class="form-control"
							   placeholder="মোবাইল নম্বর">
					</div>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-3">
						<p>বিনিয়োগ /ঋণ গ্রহণের পরিমান</p>
					</div>
					<div class="col-md-9">
						<input type="text" name="" class="form-control"
							   placeholder="বিনিয়োগ /ঋণ গ্রহণের পরিমান">
					</div>
				</div>
			</div>
		</div>

		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-3">
						<p>ঋণ গ্রহণের উদ্দেশ্য</p>
					</div>
					<div class="col-md-9">
						<textarea type="text" name="" class="form-control"
								  placeholder="ঋণ গ্রহণের উদ্দেশ্য"></textarea>
					</div>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-3">
						<p>কেন্দ্রীয় সমিতির সদস্য নম্বর ও ভর্তির তারিখ</p>
					</div>
					<div class="col-md-5">
						<input type="text" name="" class="form-control"
							   placeholder="সদস্য নম্বর">
					</div>
					<div class="col-md-4">
						<input type="text" name="" class="form-control"
							   placeholder="ভর্তির তারিখ">
					</div>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-3">
						<p>সদস্য পদে জমা টাকার পরিমান</p>
					</div>
					<div class="col-md-5">
						<p>ক্রয়কৃত শেয়ারের মূল্য স্থিতি</p>
						<input type="text" name="" class="form-control"
							   placeholder="ক্রয়কৃত শেয়ারের মূল্য স্থিতি">
					</div>
					<div class="col-md-4">
						<p>সঞ্চয় জমা স্থিতি</p>
						<input type="text" name="" class="form-control"
							   placeholder="সঞ্চয় জমা স্থিতি">
					</div>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-3">

					</div>
					<div class="col-md-5">
						<p>স্থায়ী আমানত জমা স্থিতি</p>
						<input type="text" name="" class="form-control"
							   placeholder="স্থায়ী আমানত জমা স্থিতি">
					</div>
					<div class="col-md-4">
						<p>অস্থায়ী আমানত জমা স্থিতি</p>
						<input type="text" name="" class="form-control"
							   placeholder="অস্থায়ী আমানত জমা স্থিতি">
					</div>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-3">

					</div>
					<div class="col-md-9">
						<p>সর্বমোট স্থিতি</p>
						<input type="text" name="" class="form-control"
							   placeholder="সর্বমোট স্থিতি">
					</div>

				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-3">
						<p>ঋণের টাকা পরিশোধের মেয়াদ ও পদ্ধতি</p>
					</div>
					<div class="col-md-5">
						<p>মেয়াদ</p>
						<input type="text" name="" class="form-control"
							   placeholder="মাস">
					</div>
					<div class="col-md-4">
						<p> কিস্তি /এককালীন</p>
						<input type="text" name="" class="form-control"
							   placeholder="কিস্তি /এককালীন।">
					</div>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-3">
						<p>সমিতির কার্যকরি মূলধন ও তারল্য জমা</p>
					</div>
					<div class="col-md-5">
						<p>কার্যকরী মূলধন</p>
						<input type="text" name="" class="form-control"
							   placeholder="টাকা">
					</div>
					<div class="col-md-4">
						<p>তারল্য মজুদ</p>
						<input type="text" name="" class="form-control"
							   placeholder="টাকা">
					</div>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-3">
						<p>ব্যবস্থাপনা কমিটির সিদ্ধান্ত গ্রহণের তারিখ</p>
					</div>
					<div class="col-md-9">

						<input type="text" name="" class="form-control"
							   placeholder="তারিখ">
					</div>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-3">

					</div>
					<div class="col-md-5">
						<p>কমিটির সদস্য সংখ্যা</p>
						<input type="text" name="" class="form-control"
							   placeholder="সংখ্যা">
					</div>
					<div class="col-md-4">
						<p>উপস্থিতি সংখ্যা</p>
						<input type="text" name="" class="form-control"
							   placeholder="সংখ্যা">
					</div>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-3">
						<p>বিগত অর্থ বছরে নীট লাভের পরিমান</p>
					</div>
					<div class="col-md-9">
						<input type="text" name="" class="form-control"
							   placeholder="বিগত অর্থ বছরে নীট লাভের পরিমান">
					</div>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-3">
						<p>পূর্বের ঋণ গ্রহণ , পরিশোধ ও পাওনা স্থিতি</p>
					</div>
					<div class="col-md-3">
						<p>সর্বমোট গ্রহণ</p>
						<input type="text" name="" class="form-control"
							   placeholder="সর্বমোট গ্রহণ">
					</div>
					<div class="col-md-3">
						<p>পরিশোধ</p>
						<input type="text" name="" class="form-control"
							   placeholder="পরিশোধ">
					</div>
					<div class="col-md-3">
						<p>পাওনা স্থিতি</p>
						<input type="text" name="" class="form-control"
							   placeholder="পাওনা স্থিতি">
					</div>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-3">
						<p>পূর্বের ঋণ গ্রহণের চুক্তি বাস্তবায়ন তথ্য</p>
					</div>
					<div class="col-md-9">
						<textarea type="text" name="" class="form-control"
								  placeholder="পূর্বের ঋণ গ্রহণের চুক্তি বাস্তবায়ন তথ্য"></textarea>
					</div>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-12">
						<p>বিনিয়োগ বা ঋণ গ্রহণকৃত টাকা পরিশোধের নিশ্চয়তা : আমরা ব্যবস্থাপনা কমিটি এই মর্মে নিশ্চয়তা
							প্রদান করছি যে , বিনিয়োগ বা ঋণের আবেদনকৃত টাকা মঞ্জুর করা হলে সমবায় সমিতির আইন , সমবায়
							সমিতির বিধিমালা, সমিতির উপ-আইন, সমিতির বিনিয়োগ নীতিমালা
							ও ঋণের চুক্তিনামা অনুযায়ী পরিশোধ করবো। বর্তমান ব্যবস্থাপনা কমিটির মেয়াদ শেষে নতুন
							ব্যবস্থাপনা কমিটি গঠন হলেও ঋণের টাকা পরিশোধ করার নিশ্চয়তা দিচ্ছি এবং সমিতির বার্ষিক সাধারণ
							সভায় তা অনুমোদন করবো।
						</p>
					</div>

				</div>
			</div>
		</div>
	</div>
	<div class="div-padding2">
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<div class="table-responsive">
						<table class="table-responsive table table-bordered">
							<tr style="background: #dd3333">
								<th colspan="5" class="text-center text_color_th">আবেদনকারী সমিতির ব্যবস্থাপনা কমিটির
									পক্ষে
									দুই তৃতীয়াংশ অংশের স্বাক্ষর ( কমিটির সদস্য সংখ্যা ৬ জন হলে ৪ জন হবে )
								</th>
							</tr>
							<tr style="background: #dd3333">
								<th class="text-center text_color_th">নং</th>
								<th class="text-center text_color_th">নাম</th>
								<th class="text-center text_color_th">পদবী</th>
								<th class="text-center text_color_th">স্বাক্ষর</th>
								<th class="text-center text_color_th">তারিখ</th>
							</tr>
							<?php for ($i = 1;
									   $i <= 6;
									   $i++) { ?>
								<tr>
									<td><p><?php echo $i; ?></p></td>
									<td><input type="text" name="" class="form-control" placeholder="নাম"></td>
									<td><input type="text" name="" class="form-control" placeholder="পদবী"></td>
									<td><input type="text" name="" class="form-control" placeholder="স্বাক্ষর"></td>
									<td><input type="text" name="" class="form-control" placeholder="তারিখ"></td>
								</tr>
							<?php } ?>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="div-padding2">
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-12">
						<p>বিনিয়োগের ঋণ অনুমোধন : বিনিয়োগ /ঋণ আবেদনকারী সমিতির সকল বিষয় পর্যালোচনা করে এই মর্মে নিশ্চয়তা
							দিচ্ছি যে, আবেদনকৃত ঋণের <input type="text" name="" class="input-field1"
															placeholder="টাকা"> টাকা প্রদান করা হলে আদায়
							নিশ্চিত হবে এবং ঋণের কোন ঝুঁকি হবেনা। ব্যবস্থাপনা কমিটির সাথে যোগাযোগ করে নিশ্চিত হয়েছি ঋণের
							টাকা চুক্তি অনুযায়ী পরিশোধ করবে। তাই <input type="text" name="" class="input-field1"
																		placeholder=".............."> শিক্ষিত বেকার
							সঞ্চয় ও
							ঋণদান সমবায় সমিতিকে <input type="text" name="" class="input-field1"
														placeholder="টাকা"> টাকা ঋণ অনুমোদন /মঞ্জুর করলাম।

						</p>
					</div>

				</div>
			</div>
		</div>
	</div>
	<div class="div-padding2">
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">

					<div class="form-group">
						<div class="row">
							<div class="col-md-12">
								<div class="col-md-1">
									<p>নাম</p>
								</div>
								<div class="col-md-5">
									<input type="text" name="" class="form-control" placeholder="নাম">
								</div>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="row">
							<div class="col-md-12">
								<div class="col-md-1"><p>পদবী</p></div>
								<div class="col-md-5">
									<input type="text" name="" class="form-control" placeholder="পদবী">
								</div>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="row">
							<div class="col-md-12">
								<div class="col-md-1"><p>স্বাক্ষর</p></div>
								<div class="col-md-1"><input type="checkbox" name="" class="form-control"></div>
								<div class="col-md-8"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="form-group">
		<div class="row">
			<div class="col-md-12">
				<center><br><br>
					<button type="submit" style="background: #dd3333" class="btn btn-danger">Submit</button>
				</center>
				<br>
			</div>
		</div>
	</div>
</div>
</div>
