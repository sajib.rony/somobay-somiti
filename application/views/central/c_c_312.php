<style>
	p {
		font-family: Roboto;
		font-size: medium;
		color: black;
		text-align: justify;
	}

	h4 {
		font-family: Roboto;

		color: black;
	}

	.div-padding1 {

		padding-top: 10px;
		padding-right: 30px;
		padding-bottom: 10px;
		padding-left: 30px;
	}

	.div-padding2 {

		padding-top: 0px;
		padding-right: 30px;
		padding-bottom: 10px;
		padding-left: 30px;
	}

	.input-field1 {
		width: 30%;
		padding: 3px 20px;
		margin: 8px 0;
		display: inline-block;
		border: 1px solid #ccc;
		border-radius: 4px;
		box-sizing: border-box;
	}

	.text_color_th {
		color: white;
	}
</style>


<div class="" style="background-color:#4A235A">
	<div class="row">
		<div class="col-md-12">
			<center><h3 style="color:white"> শিক্ষিত বেকার কেন্দ্রীয় সঞ্চয় ও ঋণদান সমবায় সমিতি লিঃ </h3></center>
			<center><h3 style="color:white">কর্মসংস্থান ও দারিদ্র বিমোচন প্রকল্প</h3></center>
			<center><h5 style="color:white">নিবন্ধন নম্বর -১৩/চাঁদ /১২, তারিখ :০৯/০৭/২০১২ </h5></center>
			<center><h6 style="color:white">ঠিকানা: ঠাকুর বাজার ,শাহারাস্তি , চাঁদপুর। </h6></center>
			<br>

			<div class="" style="background-color:white">
				<div class="div-padding1">
					<div class="form-group">
						<div class="row">
							<div class="col-md-6">
							</div>
							<div class="col-md-6 text-right">
								<h5 style="color:black">সি.সি - ৩১২</h5>
							</div>
							<div class="col-md-12 text-center">
								<h3 style="color:black">
									<input type="date" name="" class="input-field1"> হতে <input type="date" name=""
																								class="input-field1">
									তারিখ পযন্ত জমা খরচ হিসাব</h3>
							</div>
						</div>
					</div>
				</div>
				<div class="div-padding2">
					<div class="form-group">
						<div class="row">
							<div class="col-md-12">

								<div class="table-responsive">
									<table class="table table-bordered">
										<tr style="background:  #dd3333">
											<th class="text-center text_color_th" colspan="3"> জমা</th>
											<th class="text-center text_color_th" colspan="3"> খরচ</th>
										</tr>


										<tr style="background:  #dd3333">
											<th class="text-center text_color_th"> নং</th>
											<th class="text-center text_color_th">
												বিবরণ
											</th>

											<th class="text-center text_color_th">মোট টাকা
											</th>
											<th class="text-center text_color_th"> নং</th>
											<th class="text-center text_color_th"> বিবরণ
											</th>
											<th class="text-center text_color_th">মোট টাকা
											</th>
										</tr>
										<?php for ($i = 1; $i <= 30; $i++) { ?>
											<tr>
												<td style="color:black"><label><?php echo $i ?></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label><?php echo $i ?></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
											</tr>
										<?php } ?>

										<tr>

											<td colspan="2" class="text-right" scope="row" style="color:black">
												<label>মোট =</label></td>
											<td></td>
											<td></td>
											<td class="text-right" style="color:black"><label>মোট =</label></td>
											<td></td>
										</tr>

										<tr>
											<td colspan="2" class="text-right" scope="row" style="color:black">
												<label>
													আগত তহবিল =</label>
											</td>
											<td></td>
											<td></td>
											<td class="text-right" style="color:black"><label>
													হস্ত মওজুদ তহবিল =</label>
											</td>
											<td></td>
										</tr>
										<tr>
											<td colspan="2" class="text-right" scope="row" style="color:black">
												<label>সর্বমোট =</label></td>
											<td></td>
											<td></td>
											<td class="text-right" style="color:black"><label>সর্বমোট =</label></td>
											<td></td>
										</tr>


										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="div-padding2">
					<div class="form-group">
						<div class="row">
							<div class="col-md-12">
								<div class="col-md-3">
									<p style="color:black;">
										<input type="text" name="" value="সভাপতি " class="form-control">
									<hr>
									</p>
									<p style="text-align: center">
										সভাপতি
									</p>
								</div>
								<div class="col-md-3">
									<p style="color:black;">
										<input type="text" name="" value="সম্পাদক" class="form-control">
									<hr>
									</p>
									<p style="text-align: center">
										সম্পাদক
									</p>
								</div>
								<div class="col-md-3">
									<p style="color:black;">
										<input type="text" name="" value="ব্যবস্থাপক" class="form-control">
									<hr>
									</p>
									<p style="text-align: center">
										ব্যবস্থাপক
									</p>
								</div>


								<div class="col-md-3">
									<p style="color:black;">
										<input type="text" name="" value="ক্যাশিয়ার " class="form-control">
									<hr>
									</p>
									<p style="text-align: center">
										ক্যাশিয়ার
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="div-padding2">
					<div class="form-group">
						<div class="row">
							<div class="col-md-12">
								<center><br><br>
									<button type="submit" style="background: #dd3333" class="btn btn-danger">Submit
									</button>
								</center>
								<br>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>






