<style>
	p {
		font-family: Roboto;
		font-size: medium;
		color: black;
		text-align: justify;
	}

	h4 {
		font-family: Roboto;

		color: black;
	}

	.div-padding1 {

		padding-top: 10px;
		padding-right: 30px;
		padding-bottom: 10px;
		padding-left: 30px;
	}

	.div-padding2 {

		padding-top: 0px;
		padding-right: 30px;
		padding-bottom: 10px;
		padding-left: 30px;
	}

	.input-field1 {
		width: 30%;
		padding: 3px 20px;
		margin: 8px 0;
		display: inline-block;
		border: 1px solid #ccc;
		border-radius: 4px;
		box-sizing: border-box;
	}

	.text_color_th {
		color: white;
	}
</style>


<div class="" style="background-color:#4A235A">
	<div class="row">
		<div class="col-md-12">
			<center><h3 style="color:white"> শিক্ষিত বেকার কেন্দ্রীয় সঞ্চয় ও ঋণদান সমবায় সমিতি লিঃ </h3></center>
			<center><h3 style="color:white">কর্মসংস্থান ও দারিদ্র বিমোচন প্রকল্প</h3></center>
			<center><h5 style="color:white">নিবন্ধন নম্বর -১৩/চাঁদ /১২, তারিখ :০৯/০৭/২০১২ </h5></center>
			<center><h6 style="color:white">ঠিকানা: ঠাকুর বাজার ,শাহারাস্তি , চাঁদপুর। </h6></center>
			<br>

			<div class="" style="background-color:white">
				<div class="div-padding1">
					<div class="form-group">
						<div class="row">
							<div class="col-md-6">
							</div>
							<div class="col-md-6 text-right">
								<h5 style="color:black">সি.সি -৩১৪</h5>
							</div>
							<div class="col-md-12 text-center">
								<h3 style="color:black">
									<input type="date" name="" class="input-field1"> হতে <input type="date" name=""
																								class="input-field1">
									তারিখ পযন্ত লাভ ক্ষতির হিসাব
								</h3>
								<h5 style="color:black; text-align:right">মাসের নাম :<?php echo date('M/yy') ?></h5>

							</div>
						</div>
					</div>
				</div>


				<div class="div-padding2">
					<div class="form-group">
						<div class="row">
							<div class="col-md-12">

								<div class="table-responsive">
									<table class="table table-bordered">
										<tr style="background:  #dd3333">
											<th class="text-center text_color_th">হিসাব কোড</th>
											<th class="text-center text_color_th">বিবরণ
											</th>
											<th class="text-center text_color_th">নোট নং
											</th>
											<th class="text-center text_color_th">টাকা
											</th>
											<th class="text-center text_color_th">টাকা
											</th>
										</tr>

										<tr>
											<td><p></p></td>
											<td><p></p></td>
											<td rowspan="31"><p></p></td>
											<td rowspan="31"><p></p></td>
											<td rowspan="32"><p></p></td>
										</tr>

										<?php for ($i = 1; $i <= 8; $i++) { ?>
											<tr>
												<td><p></p></td>
												<td><p></p></td>
											</tr>
										<?php } ?>
										<tr>
											<th></th>
											<th><p >মোট লাভ = </p></th>
										</tr>
										<?php for ($i = 1; $i <= 20; $i++) { ?>
											<tr>
												<td><p></p></td>
												<td><p></p></td>
											</tr>
										<?php } ?>


										<tr>
											<th></th>
											<th ><p >মোট ক্ষতি = </p></th>
										</tr>
										<tr style="background:  ">
											<th colspan="4"><p style="text-align:right">নীট লাভ (উদৃত পত্রে
													স্তানান্তর করা হলো )=</p>
											</th>

										</tr>
										<tr>
											<td><p style="text-align:center">২০১০২১৫ </p></td>
											<td><p style="text-align:right"> সংরক্ষিত তহবিল ১৫%</p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
										</tr>
										<tr>
											<td><p  style="text-align:center">২০১০২১৭  </p></td>
											<td><p style="text-align:right"> সমবায় উন্নয়ন তহবিল ৩%</p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
										</tr>
										<tr>
											<td><p style="text-align:center">২০১০২১৬  </p></td>
											<td><p style="text-align:right">কু-ঋণ তহবিল ১০%</p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
										</tr>
										<tr>
											<td><p style="text-align:center">২০১০২৩২  </p></td>
											<td><p style="text-align:right">অবন্টিত লাভ ৭২%</p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
										</tr>
										<tr>
											<td ><p style="text-align:center">২০১০২৩৫ </p></td>
											<th><p style="text-align:right"> নীট লাভ (উদৃত পত্রে স্তানান্তর করা হলো )=</p></th>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
										</tr>

									</table>

								</div>
							</div>
						</div>
					</div>
				</div>


				<div class="div-padding2">
					<div class="form-group">
						<div class="row">
							<div class="col-md-12">
								<div class="col-md-3">
									<p style="color:black;">
										<input type="text" name="" value="সভাপতি " class="form-control">
									<hr>
									</p>
									<p style="text-align: center">
										সভাপতি
									</p>
								</div>
								<div class="col-md-3">
									<p style="color:black;">
										<input type="text" name="" value="সম্পাদক" class="form-control">
									<hr>
									</p>
									<p style="text-align: center">
										সম্পাদক
									</p>
								</div>
								<div class="col-md-3">
									<p style="color:black;">
										<input type="text" name="" value="ব্যবস্থাপক" class="form-control">
									<hr>
									</p>
									<p style="text-align: center">
										ব্যবস্থাপক
									</p>
								</div>
								<div class="col-md-3">
									<p style="color:black;">
										<input type="text" name="" value="ক্যাশিয়ার" class="form-control">
									<hr>
									</p>
									<p style="text-align: center">
										ক্যাশিয়ার
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


