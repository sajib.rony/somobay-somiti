<style>
	p {
		font-family: Roboto;
		font-size: medium;
		color: black;
		text-align: justify;
	}

	h4 {
		font-family: Roboto;

		color: black;
	}

	.div-padding1 {

		padding-top: 10px;
		padding-right: 30px;
		padding-bottom: 10px;
		padding-left: 30px;
	}

	.div-padding2 {

		padding-top: 0px;
		padding-right: 30px;
		padding-bottom: 10px;
		padding-left: 30px;
	}

	.input-field1 {
		width: 50%;
		padding: 5px 20px;
		margin: 8px 0;
		display: inline-block;
		border: 1px solid #ccc;
		border-radius: 4px;
		box-sizing: border-box;
	}

	.text_color_th {
		color: white;
	}
</style>


<div class="" style="background-color:#4A235A">
	<div class="row">
		<div class="col-md-12">
			<center><h3 style="color:white"> শিক্ষিত বেকার কেন্দ্রীয় সঞ্চয় ও ঋণদান সমবায় সমিতি লিঃ </h3></center>
			<center><h3 style="color:white">কর্মসংস্থান ও দারিদ্র বিমোচন প্রকল্প</h3></center>
			<center><h5 style="color:white">নিবন্ধন নম্বর -১৩/চাঁদ /১২, তারিখ :০৯/০৭/২০১২ </h5></center>
			<center><h6 style="color:white">ঠিকানা: ঠাকুর বাজার ,শাহারাস্তি , চাঁদপুর। </h6></center>
			<br>

			<div class="" style="background-color:white">
				<div class="div-padding1">
					<div class="form-group">
						<div class="row">
							<div class="col-md-6">

							</div>
							<div class="col-md-6 text-right">
								<h5 style="color:black">সি -৭৭</h5>
							</div>
							<div class="col-md-12 text-center">
								<h2 style="color:black">
									মাসিক রাজস্ব আয়-ব্যয় হিসাব
								</h2>
							</div>

						</div>
					</div>
				</div>

				<div class="div-padding2">
					<div class="form-group">
						<div class="row">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table table-bordered">
										<tr style="background:  #dd3333">
											<th class="text-center text_color_th" rowspan="2">ক্র নং</th>
											<th class="text-center text_color_th">
												কার্যালয়ের নাম
											</th>

											<th class="text-center text_color_th">
												রাজস্ব আয়
											</th>
											<th class="text-center text_color_th">
												আয়ের স্থিতি
											</th>
											<th class="text-center text_color_th">
												রাজস্ব ব্যয়
											</th>
											<th class="text-center text_color_th">
												ব্যয়ের স্থিতি
											</th>
											<th class="text-center text_color_th">
												আয়-ব্যয়ের অবশিষ্ট (৩-৫)
											</th>
											<th class="text-center text_color_th">
												নীট লাভ
											</th>
										</tr>
										<tr style="background:  #dd3333">
											<th class="text-center text_color_th">১</th>
											<th class="text-center text_color_th"> ২
											</th>
											<th class="text-center text_color_th">৩
											</th>
											<th class="text-center text_color_th">৪
											</th>
											<th class="text-center text_color_th">৫
											</th>
											<th class="text-center text_color_th">৬
											</th>
											<th class="text-center text_color_th">৭
											</th>
										</tr>

										<?php
										$i = '';
										$sum = 0;
										for ($i = 1; $i <= 17; $i++) { ?>
											<tr>
												<td><p style="text-align:center"><?php echo $i; ?></p></td>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
											</tr>
											<?php
										} ?>
										<tr>
											<th colspan="2">
												<p style="text-align:right;">মোট =</p>
											</th>
											<th>
											</th>
											<th>
											</th>
											<th>
											</th>
											<th>
											</th>
											<th>
											</th>
											<th>
											</th>

										</tr>
										<tr>
											<td colspan="2"><p style="text-align:right;">পূর্বের মাসে এ সময়ের স্থিতি</p>
											</td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>

										</tr>
										<tr>
											<td colspan="2"><p style="text-align:right;">পূর্বের মাসে এ সময়ের চেয়ে
													বৃদ্ধি </p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>

										</tr>
										<tr>
											<td colspan="2"><p style="text-align:right;">পূর্বের বছর এ সময়ের স্থিতি</p>
											</td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>

										</tr>

									</table>
								</div>
								<div class="table-responsive">
									<table class="table table-bordered">
										<tr style="background:  #dd3333">

											<th class="text-center text_color_th">
												কার্যালয়ের নাম
											</th>

											<th class="text-center text_color_th">
												রাজস্ব আয়
											</th>
											<th class="text-center text_color_th">
												আয়ের স্থিতি
											</th>
											<th class="text-center text_color_th">
												রাজস্ব ব্যয়
											</th>
											<th class="text-center text_color_th">
												ব্যয়ের স্থিতি
											</th>
											<th class="text-center text_color_th">
												আয়-ব্যয়ের অবশিষ্ট (৩-৫)
											</th>
											<th class="text-center text_color_th">
												নীট লাভ
											</th>
										</tr>
										<tr>
											<td><p>শিক্ষিত বেকার কেন্দ্রীয় সঞ্চয় </p>
											</td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>
											<td><p></p></td>

										</tr>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>


				<div class="div-padding2">
					<div class="form-group">
						<div class="row">
							<div class="col-md-12">
								<div class="col-md-4">
									<p style="color:black;">
										<input type="text" name="" value="সভাপতি " class="form-control">
									<hr>
									</p>
									<p style="text-align: center">
										সভাপতি
									</p>
								</div>
								<div class="col-md-4">

								</div>
								<div class="col-md-4">
									<p style="color:black;">
										<input type="text" name="" value="তথ্য সংগ্রহকারী " class="form-control">
									<hr>
									</p>
									<p style="text-align: center">
										তথ্য সংগ্রহকারী
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>







