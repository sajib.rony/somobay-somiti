<style>
	p {
		font-family: Roboto;
		font-size: medium;
		color: black;
		text-align: justify;
	}

	h4 {
		font-family: Roboto;

		color: black;
	}

	.div-padding1 {

		padding-top: 10px;
		padding-right: 30px;
		padding-bottom: 10px;
		padding-left: 30px;
	}

	.div-padding2 {

		padding-top: 0px;
		padding-right: 30px;
		padding-bottom: 10px;
		padding-left: 30px;
	}

	.input-field1 {
		width: 50%;
		padding: 5px 20px;
		margin: 8px 0;
		display: inline-block;
		border: 1px solid #ccc;
		border-radius: 4px;
		box-sizing: border-box;
	}

	.text_color_th {
		color: white;
	}
</style>


<div class="" style="background-color:#4A235A">
	<div class="row">
		<div class="col-md-12">
			<center><h3 style="color:white"> শিক্ষিত বেকার কেন্দ্রীয় সঞ্চয় ও ঋণদান সমবায় সমিতি লিঃ </h3></center>
			<center><h3 style="color:white">কর্মসংস্থান ও দারিদ্র বিমোচন প্রকল্প</h3></center>
			<center><h5 style="color:white">নিবন্ধন নম্বর -১৩/চাঁদ /১২, তারিখ :০৯/০৭/২০১২ </h5></center>
			<center><h6 style="color:white">ঠিকানা: ঠাকুর বাজার ,শাহারাস্তি , চাঁদপুর। </h6></center>
			<br>

			<div class="" style="background-color:white">
				<div class="div-padding1">
					<div class="form-group">
						<div class="row">
							<div class="col-md-6">

							</div>
							<div class="col-md-6 text-right">
								<h5 style="color:black">কোড নং -১৪২</h5>
							</div>
							<div class="col-md-12 text-center">
								<h2 style="color:black">
									জমা ভাউচার </h2>
							</div>

						</div>
					</div>
				</div>

				<div class="div-padding2">
					<div class="form-group">
						<div class="row">
							<div class="col-md-12">
								<div class="col-md-4">
									<label style="color:black">জমা ভাউচার নং </label>
									<input type="number" class="form-control" name="joma_vowcher_no"
										   placeholder="জমা ভাউচার নং">
								</div>
								<div class="col-md-4">
									<label style="color:black">হিসাব নম্বর </label>
									<input type="number" class="form-control" name="" placeholder="হিসাব নম্বর ">
								</div>
								<div class="col-md-4">
									<label style="color:black">তারিখ </label>
									<input type="date" class="form-control" name="" placeholder="তারিখ">
								</div>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="row">
							<div class="col-md-12">
								<div class="col-md-6">
									<label for="email" style="color:black"> নাম</label>
									<input type="text" class="form-control" id="email"
										   placeholder="সদস্য নাম"
										   name="email">
								</div>
								<div class="col-md-6">
									<label for="email" style="color:black">পিতা /স্বামীর নাম </label>
									<input type="text" class="form-control" id="email" placeholder="পিতা /স্বামীর নাম"
										   name="email">
								</div>


							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="row">
							<div class="col-md-12">
								<div class="col-md-12">
									<label style="color:black">ঠিকানা </label>

									<textarea name="" class="form-control" placeholder="ঠিকানা "></textarea>

								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="div-padding2">
					<div class="form-group">
						<div class="row">
							<div class="col-md-12">

								<div class="table-responsive">
									<table class="table table-bordered">
										<tr style="background:  #dd3333">
											<th class="text-center text_color_th">ক্র নং</th>
											<th class="text-center text_color_th">খাত / বিবরণ
											</th>
											<th class="text-center text_color_th">টাকা
											</th>
										</tr>

										<?php
										$i = '';
										$sum = 0;
										for ($i = 1; $i <= 5; $i++) { ?>

											<tr>
												<td style="color:black"><label><?php echo $i; ?></label></td>
												<td style="color:black"><input type="text" class="form-control"
																			   name=""
																			   value=""></td>
												<td style="color:black">
													<input type="text" class="form-control" name="" value="">
												</td>
											</tr>
											<?php
										} ?>
										<tr>
											<th class=" text_color_th" colspan="2">
												<p style="text-align:right">মোট =</p>
											</th>
											<th class="text_color_th">
												<p></p>
											</th>
										</tr>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>


				<?php echo "<br>"; ?>

			</div>


			<div style="background-color:white;padding:15px;">

				<?php echo "<br>"; ?>
				<div class="form-group">
					<div class="row">

						<div class="col-md-12">


							<div class="col-md-12">


								<?php echo "<br><br><br><br>"; ?>
								<div class="col-md-4">
									<p style="color:black;">
										<input type="text" name="" value="সম্পাদক /ব্যবস্থাপক" class="form-control">
									<hr>
									</p>
									<center><label style="color:black;">
											<b>
												সম্পাদক /ব্যবস্থাপক
											</b>
										</label></center>
								</div>

								<div class="col-md-4">
									<p style="color:black;">
										<input type="text" name="" value="জমাকারী " class="form-control">
									<hr>
									</p>
									<center><label style="color:black;">
											<b>
												জমাকারী
											</b>
										</label></center>

								</div>
								<div class="col-md-4">
									<p style="color:black;">
										<input type="text" name="" value="ক্যাশিয়ার " class="form-control">
									<hr>
									</p>
									<center><label style="color:black;">
											<b>
												কোষাধ্যক্ষ
											</b>
										</label></center>

								</div>

							</div>
						</div>
					</div>
				</div>
				<?php echo "<br>"; ?>
				<div class="form-group">
					<div class="row">
						<div class="col-md-12">
							<center><br><br>
								<button type="submit" style="background: #dd3333" class="btn btn-danger">Submit</button>
							</center>
							<br>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
