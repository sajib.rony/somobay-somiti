<style>
	p {
		font-family: Roboto;
		font-size: medium;
		color: black;
		text-align: justify;
	}

	h4 {
		font-family: Roboto;

		color: black;
	}

	.div-padding1 {

		padding-top: 10px;
		padding-right: 30px;
		padding-bottom: 10px;
		padding-left: 30px;
	}

	.div-padding2 {

		padding-top: 0px;
		padding-right: 30px;
		padding-bottom: 10px;
		padding-left: 30px;
	}

	.input-field1 {
		width: 50%;
		padding: 5px 20px;
		margin: 8px 0;
		display: inline-block;
		border: 1px solid #ccc;
		border-radius: 4px;
		box-sizing: border-box;
	}

	.text_color_th {
		color: white;
	}
</style>

<div class="" style="background-color:#4A235A">
	<div class="row">
		<div class="col-md-12">
			<center><h3 style="color:white"> শিক্ষিত বেকার কেন্দ্রীয় সঞ্চয় ও ঋণদান সমবায় সমিতি লিঃ </h3></center>
			<center><h3 style="color:white">কর্মসংস্থান ও দারিদ্র বিমোচন প্রকল্প</h3></center>
			<center><h5 style="color:white">নিবন্ধন নম্বর -১৩/চাঁদ /১২, তারিখ :০৯/০৭/২০১২ </h5></center>
			<center><h6 style="color:white">ঠিকানা: ঠাকুর বাজার ,শাহারাস্তি , চাঁদপুর। </h6></center>
			<br>

			<div class="" style="background-color:white">
				<div class="div-padding1">
					<div class="form-group">
						<div class="row">
							<div class="col-md-6">

							</div>
							<div class="col-md-6 text-right">
								<h5 style="color:black">কোড - ১৫</h5>
							</div>
							<div class="col-md-12 text-center">
								<h2 style="color:black">
									পণ্য বিক্রয় /বিনিয়োগ খতিয়ান
								</h2>
							</div>

						</div>
					</div>
				</div>
				<div class="div-padding2">
					<div class="form-group">

						<div class="row">
							<div class="col-md-12">
								<?php echo "<br><br>"; ?>
								<div class="col-md-12">
									<label for="email" style="color:black">বিক্রয় /বিনিয়োগের বিবরণ :</label>
									<textarea type="text" class="form-control" id="email"
											  placeholder="বিক্রয় /বিনিয়োগের বিবরণ "
											  name="email"></textarea>
								</div>
								<div class="col-md-4">
									<label for="email" style="color:black">হিসাব নম্বর </label>
									<input type="text" class="form-control" id="email"
										   name="email" value="1020" disabled>
								</div>
								<div class="col-md-4">
									<label for="email" style="color:black">ফরম নং </label>
									<input type="text" class="form-control" id="email"
										   name="email" value="1001 " disabled>
								</div>
								<div class="col-md-4">
									<label for="email" style="color:black">পণ্য বিক্রয় তারিখ </label>
									<input type="" class="form-control" id="email"
										   name="email" value="10/08/2020">
								</div>
								<div class="col-md-4">
									<label for="email" style="color:black">সদস্য নম্বর </label>
									<input type="text" class="form-control" id="email"
										   placeholder="পিমাতার নাম "
										   name="email">
								</div>
								<div class="col-md-4">
									<br> <label for="email" style="color:black">হালনাগাদ - </label>
									<input type="checkbox"><label for="email"
																  style="color:black">হ্যা</label>/<input
											type="checkbox"><label for="email" style="color:black">না</label>


								</div>
							</div>
						</div>
					</div>
					<div class="form-group">

						<div class="row">
							<div class="col-md-12">
								<center><h3 style="color:black"> ঠিকানা </h3></center>
							</div>
							<div class="col-md-12">
								<?php echo "<br><br>"; ?>
								<div class="col-md-4">
									<label for="email" style="color:black">সদস্য নাম</label>
									<input type="text" class="form-control" id="email"
										   placeholder="সদস্য পদ অনুযায়ী নাম"
										   name="email">
								</div>
								<div class="col-md-4">
									<label for="email" style="color:black">পিতা /স্বামীর নাম </label>
									<input type="text" class="form-control" id="email"
										   placeholder="পিতা /স্বামীর নাম"
										   name="email">
								</div>
								<div class="col-md-4">
									<label for="email" style="color:black">মাতার নাম </label>
									<input type="text" class="form-control" id="email" placeholder="পিমাতার নাম "
										   name="email">
								</div>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="row">
							<div class="col-md-12">

								<div class="col-md-4">
									<label for="email" style="color:black">গ্রাম </label>
									<input type="text" class="form-control" id="email"
										   placeholder="গ্রাম /মহল্লা নাম"
										   name="email">
								</div>
								<div class="col-md-4">
									<label for="email" style="color:black">বাড়ি </label>
									<input type="text" class="form-control" id="email" placeholder="বাড়ি "
										   name="email">
								</div>
								<div class="col-md-4">
									<label for="email" style="color:black">পোস্ট </label>
									<input type="text" class="form-control" id="email" placeholder="পোস্ট নাম"
										   name="email">
								</div>
								<div class="col-md-6">
									<label for="email" style="color:black">উপজেলা </label>
									<input type="text" class="form-control" id="email" placeholder="উপজেলা নাম"
										   name="email">
								</div>
								<div class="col-md-6">
									<label for="email" style="color:black">জেলা </label>
									<input type="text" class="form-control" id="email" placeholder="জেলা নাম"
										   name="email">
								</div>

							</div>
						</div>
						<?php echo "<br>"; ?>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="col-md-12">
								<center><h2 style="color:black">
										জমার বিবরণ
									</h2></center>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-md-12">
										<?php echo "<br><br>"; ?>
										<div class="col-md-4">
											<label for="email" style="color:black">পণ্যের ক্রয় মূল্য </label>
											<input type="text" class="form-control" id="email"
												   placeholder="পণ্যের ক্রয় মূল্য "
												   name="email">
										</div>
										<div class="col-md-4">
											<label for="email" style="color:black">বিক্রয় মূল্য</label>
											<input type="text" class="form-control" id="email"
												   placeholder="বিক্রয় মূল্য"
												   name="email">
										</div>
										<div class="col-md-4">
											<label for="email" style="color:black">পরিশোধের মেয়াদ </label>
											<input type="" class="form-control" id="email"
												   placeholder="পরিশোধের মেয়াদ  "
												   name="email" value="10/08/2023">
										</div>
									</div>
								</div>
							</div>


							<div style="background-color:">
								<div class="form-group">
									<div class="row">
										<div class="col-md-12">

											<div class="col-md-4">
												<label for="email" style="color:black">পরিশোধের পদ্ধতি </label>
												<input type="text" class="form-control" id="email"
													   placeholder="পরিশোধের পদ্ধতি "
													   name="email">
											</div>
											<div class="col-md-4">
												<label for="email" style="color:black">কিস্তি সংখ্যা </label>
												<input type="text" class="form-control" id="email"
													   placeholder="কিস্তি সংখ্যা  "
													   name="email">
											</div>
											<div class="col-md-4">
												<label for="email" style="color:black">কিস্তির টাকা </label>
												<input type="text" class="form-control" id="email"
													   placeholder="কিস্তির টাকা "
													   name="email">
											</div>
											<div class="col-md-6">
												<label for="email" style="color:black">কিস্তি পরিশোধের তারিখ </label>
												<input type="text" class="form-control" id="email"
													   placeholder="কিস্তি পরিশোধের তারিখ "
													   name="email">
											</div>
											<div class="col-md-6">
												<label for="email" style="color:black">প্রথম কিস্তির তারিখ </label>
												<input type="text" class="form-control" id="email"
													   placeholder="প্রথম কিস্তির তারিখ "
													   name="email">
											</div>

										</div>
									</div>
									<?php echo "<br>"; ?>
								</div>
							</div>

						</div>
					</div>
				</div>


				<div class="div-padding2">
					<div class="form-group">
						<div class="row">
							<div class="col-md-12">

								<div class="table-responsive">
									<table class="table table-bordered">
										<tbody>

										<tr style="background:  #dd3333">
											<th class="text-center text_color_th" rowspan="2">ক্র. নং</th>
											<th rowspan="2" class="text-center"><span
														style="color: white">কিস্তি নং</span></th>
											<th rowspan="2" class="text-center"><span
														style="color: white">কিস্তির তারিখ</span></th>
											<th colspan="2" class="text-center"><span style="color: white">মূলধন </span>
											</th>

											<th colspan="2" class="text-center"><span style="color: white">লাভ </span>
											</th>
											<th colspan="2" class="text-center"><span style="color: white">মোট </span>
											</th>

											<th colspan="2" class="text-center"><span
														style="color: white">বিলম্ব মান্ডল </span></th>

											<th rowspan="2" class="text-center"><span
														style="color: white">আদায় নং</span></th>
											<th rowspan="2" class="text-center"><span style="color: white">পরিশোধের তারিখ স্বাক্ষর</span>
											</th>

										</tr>

										<tr class='' style="background: #dd3333">

											<th class="text-center"><span style="color: white"> পরিশোধ</span></th>
											<th class="text-center"><span style="color: white"> পাওনা</span></th>
											<th class="text-center"><span style="color: white"> পরিশোধ</span></th>
											<th class="text-center"><span style="color: white"> পাওনা</span></th>
											<th class="text-center"><span style="color: white"> পরিশোধ</span></th>
											<th class="text-center"><span style="color: white"> পাওনা</span></th>
											<th class="text-center"><span style="color: white"> পরিশোধ</span></th>
											<th class="text-center"><span style="color: white"> পাওনা</span></th>


										</tr>


										<?php

										$i = '';
										for ($i = 1;
										$i <= 10;
										$i++) {
										?>
										<tbody>
										<tr class='' style="background: #FFFFFF">
											<th><?php echo $i ?> </th>
											<th><?php echo $i ?> </th>
											<th>10/<?php echo $i ?>/2020</th>
											<th>1000</th>
											<th>20000</th>
											<th>100</th>
											<th>1900</th>
											<th>1100</th>
											<th>22000</th>
											<th>00</th>
											<th>00</th>
											<th>1010</th>
											<th>
												<center><input type="checkbox" checked></center>
											</th>

										</tr>
										</tbody>
										<?php } ?>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">

					<div class="row">
						<div class="col-md-12">

							<center><br><br>


								<button type="submit" style="background: #dd3333" class="btn btn-danger">Submit</button>
							</center>
							<br>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


