<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="Opu">
	<meta name="keyword" content="">
	<link rel="shortcut icon" href="">
	<title>ERP System</title>
	<!-- Bootstrap core CSS -->
	<link href="<?= base_url('assets/admin/css/bootstrap.min.css'); ?>" rel="stylesheet">
	<link href="<?= base_url('assets/admin/css/bootstrap-reset.css'); ?>" rel="stylesheet">

	<!--external css-->
	<link href="<?= base_url('assets/admin/css/font-awesome/css/font-awesome.css'); ?>" rel="stylesheet"/>
	<link href="<?= base_url('assets/admin/css/jquery-easy-pie-chart/jquery.easy-pie-chart.css'); ?>" rel="stylesheet"
		  type="text/css" media="screen"/>
	<link rel="stylesheet" href="<?= base_url('assets/admin/css/owl.carousel.css'); ?>" type="text/css">

	<!--right slidebar-->
	<link href="<?= base_url('assets/admin/css/slidebars.css'); ?>" rel="stylesheet">

	<!-- Custom styles for this template -->
	<link href="<?= base_url('assets/admin/css/style.css'); ?>" rel="stylesheet">
	<link href="<?= base_url('assets/admin/css/style-responsive.css'); ?>" rel="stylesheet"/>

	<!--file Upload -->
	<link rel="stylesheet" type="text/css"
		  href="<?php echo base_url('assets/admin/css/bootstrap-fileupload/bootstrap-fileupload.css'); ?>"/>

	<link rel="stylesheet" type="text/css"
		  href="<?php echo base_url('assets/admin/other/bootstrap-datepicker/css/datepicker.css'); ?>"/>

	<script src="<?= base_url('assets/admin/js/jquery.js'); ?>"></script>
</head>

<body>
<section id="container">
	<!--header start-->
	<header class="header purple-bg">
		<div class="sidebar-toggle-box">
			<i class="fa fa-bars"></i>
		</div>
		<!--logo start-->
		<a href="#" class="logo">ই আর পি সিস্টেম<span></span></a>
		<!--logo end-->


		<div class="nav notify-row" id="top_menu">
		</div>
		<div class="top-nav ">
			<!--search & user info start-->
			<ul class="nav pull-right top-menu">
				<!-- user login dropdown start-->
				<li class="dropdown">
					<a data-toggle="dropdown" class="dropdown-toggle" href="#">
						<!-- <img alt="" src="<?= base_url('assets/admin/img/avatar1_small.jpg'); ?>"> -->
						<span class="username">
							<?php
							$user_type = $this->session->userdata('user');
							if ($user_type == "Operator") {
								echo "Operator";
							} elseif ($user_type == "Accountant") {
								echo "Accountant";
							} elseif ($user_type == "General") {
								echo "General";
							}
							?>

						</span>
						<b class="caret"></b>
					</a>
					<ul class="dropdown-menu extended logout">
						<div class="log-arrow-up"></div>
						<li><a href="<?= base_url('admin'); ?>"><i class="fa fa-key"></i>
								Log Out</a></li>
					</ul>
				</li>
			</ul>
			<!--search & user info end-->
		</div>
	</header>
	<!--header end-->
	<!--sidebar start-->
	<aside>
		<div id="sidebar" class="nav-collapse ">
			<!-- sidebar menu start-->
			<ul class="sidebar-menu" id="nav-accordion">
				<li>
					<a class="active" href="<?= base_url('central-dashboard'); ?>">
						<i class="fa fa-dashboard"></i>
						<span>ড্যাশবোর্ড</span>
					</a>
				</li>
				<li class="sub-menu">
					<a href="javascript:;">
						<i class="fa fa-sitemap"></i>
						<span> মেম্বার </span>
					</a>
					<ul class="sub">
						<li>
							<a href="<?= base_url('c-c-301'); ?>">
								<span> সদস্য ভর্তি  </span>
							</a>
						</li>
						<li>
							<a href="<?= base_url('c-c-302'); ?>">
								<span>সদস্য নীতিমালা</span>
							</a>
						</li>
						<li>
							<a href="<?= base_url('c-c-303'); ?>">
								<span> ক্ষমতাপত্র </span>
							</a>
						</li>
						<li>
							<a href="<?= base_url('c-c-304'); ?>">
								<span> সদস্য পদ প্রত্যাহার </span>
							</a>
						</li>
						<li>
							<a href="<?= base_url('c-c-305'); ?>">
								<span> স্থায়ী আমানত আবেদন ফরম </span>
							</a>
						</li>
						<li>
							<a href="<?= base_url('c-c-306'); ?>">
								<span> অস্থায়ী আমানত আবেদন ফরম </span>
							</a>
						</li>
						<li>
							<a href="<?= base_url('c-c-307'); ?>">
								<span> স্থায়ী/অস্থায়ী আমানত উত্তোলন </span>
							</a>
						</li>
						<li>
							<a href="<?= base_url('c-c-308'); ?>">
								<span>স্থায়ী/অস্থায়ী আমানত ক্ষমতাপত্র</span>

							</a>
						</li>
					</ul>
				</li>
				<li class="sub-menu">
					<a href="javascript:;">
						<i class="fa fa-sitemap"></i>
						<span> একাউন্টস   </span>
					</a>
					<ul class="sub">
						<li>
							<a href="<?= base_url('c-code-142'); ?>">
								<span>জমা ভাউচার</span>
							</a>
						</li>
						<li>
							<a href="<?= base_url('c-code-143'); ?>">
								<span>খরচ ভাউচার</span>
							</a>
						</li>
						<li>
							<a href="<?= base_url('c-c-172'); ?>">
								<span>মাসিক  জমা খরচ হিসাব</span>
							</a>
						</li>
						<li>
							<a href="<?= base_url('c-c-312'); ?>">
								<span>জমা খরচ হিসাব</span>
							</a>
						</li>
						<li>
							<a href="<?= base_url('c-c-173'); ?>">
								<span>মাসিক রাজস্ব হিসাব</span>
							</a>
						</li>
						<li>
							<a href="<?= base_url('c-c-313'); ?>">
								<span>রাজস্ব হিসাব</span>
							</a>
						</li>
						<li>
							<a href="<?= base_url('c-c-176'); ?>">
								<span>মাসিক খাত ভিত্তিক দেনা </span>
							</a>
						</li>
						<li>
							<a href="<?= base_url('c-c-177'); ?>">
								<span>মাসিক খাত ভিত্তিক পাওনা</span>
							</a>
						</li>
						<li>
							<a href="<?= base_url('c-c-314'); ?>">
								<span>লাভ ক্ষতির হিসাব</span>
							</a>
						</li>
						<li>
							<a href="<?= base_url('c-c-315'); ?>">
								<span>মূলধন ও দেনা</span>
							</a>
						</li>
						<li>
							<a href="<?= base_url('c-c-316'); ?>">
								<span>সম্পত্তি ও পাওনা</span>
							</a>
						</li>
						<li>
							<a href="<?= base_url('c-c-77'); ?>">
								<span>রাজস্ব আয়-ব্যয় হিসাব</span>
							</a>
						</li>
						<li>
							<a href="<?= base_url('c-c-175'); ?>">
								<span>হওলাত গ্রহণ এবং প্রদান</span>
							</a>
						</li>

					</ul>
				</li>
				<li class="sub-menu">
					<a href="javascript:;">
						<i class="fa fa-sitemap"></i>
						<span> বিস্তারিত তালিকা </span>
					</a>
					<ul class="sub">
						<li>
							<a href="<?= base_url('c-c-182'); ?>">
								<span>সদস্য পদের ডিটেল লিস্ট</span>
							</a>
						</li>
						<li>
							<a href="<?= base_url('c-c-183'); ?>">
								<span>বিনিয়োগ হতে মূলধন পাওনা </span>
							</a>
						</li>
						<li>
							<a href="<?= base_url('c-c-184'); ?>">
								<span>ভবিষ্য তহবিল জমা</span>
							</a>
						</li>
						<li>
							<a href="<?= base_url('c-c-181'); ?>">
								<span>আসবাবপত্র মজুদ</span>
							</a>
						</li>

					</ul>
				</li>
				<li class="sub-menu">
					<a href="javascript:;">
						<i class="fa fa-sitemap"></i>
						<span> বিনিয়োগ   </span>
					</a>
					<ul class="sub">
						<li>
							<a href="<?= base_url('c-c-309'); ?>">
								<span>বিনিয়োগ /ঋণ পাওয়ার আবেদন</span>
							</a>
						</li>
						<li>
							<a href="<?= base_url('c-c-310'); ?>">
								<span>বিনিয়োগ /ঋণ নীতিমালা </span>
							</a>
						</li>
						<li>
							<a href="<?= base_url('c-code-16'); ?>">
								<span>দৈনিক কিস্তি আদায় প্রতিবেদন</span>
							</a>
						</li>
						<li>
							<a href="<?= base_url('c-code-15'); ?>">
								<span>পণ্য বিক্রয় /বিনিয়োগ খতিয়ান</span>
							</a>
						</li>
					</ul>
				</li>
			</ul>
			<!-- sidebar menu end-->
		</div>
	</aside>

	<section id="main-content">
		<section class="wrapper">
			<?= $content; ?>
		</section>
	</section>

	<footer class="site-footer">
		<div class="text-center">
			2020 &copy; RTSoftbd.
			<a href="#" class="go-top">
				<i class="fa fa-angle-up"></i>
			</a>
		</div>
	</footer>

</section>

<!-- js placed at the end of the document so the pages load faster -->
<script src="<?= base_url('assets/admin/js/jquery.js'); ?>"></script>
<script src="<?= base_url('assets/admin/js/bootstrap.min.js'); ?>"></script>
<script class="include" type="text/javascript"
		src="<?= base_url('assets/admin/js/jquery.dcjqaccordion.2.7.js'); ?>"></script>
<script src="<?= base_url('assets/admin/js/jquery.scrollTo.min.js'); ?>"></script>
<script src="<?= base_url('assets/admin/js/jquery.nicescroll.js'); ?>" type="text/javascript"></script>
<script src="<?= base_url('assets/admin/js/jquery.sparkline.js'); ?>" type="text/javascript"></script>

<script src="<?php echo base_url('assets/admin/css/jquery-easy-pie-chart/jquery.easy-pie-chart.js'); ?>"></script>

<script src="<?= base_url('assets/admin/js/owl.carousel.js'); ?>"></script>
<script src="<?= base_url('assets/admin/js/jquery.customSelect.min.js'); ?>"></script>
<script src="<?= base_url('assets/admin/js/respond.min.js'); ?>"></script>

<!--right slidebar-->
<script src="<?= base_url('assets/admin/js/slidebars.min.js'); ?>"></script>

<!--common script for all pages-->
<script src="<?= base_url('assets/admin/js/common-scripts.js'); ?>"></script>

<!--script for this page-->
<script src="<?= base_url('assets/admin/js/sparkline-chart.js'); ?>"></script>
<script src="<?= base_url('assets/admin/js/easy-pie-chart.js'); ?>"></script>
<script src="<?= base_url('assets/admin/js/count.js'); ?>"></script>

<!--file Upload -->
<script type="text/javascript"
		src="<?php echo base_url('assets/admin/css/bootstrap-fileupload/bootstrap-fileupload.js'); ?>"></script>

<script type="text/javascript"
		src="<?php echo base_url('assets/admin/other/bootstrap-datepicker/js/bootstrap-datepicker.js'); ?>"></script>

<script src="<?php echo base_url('assets/admin/js/advanced-form-components.js'); ?>"></script>
<script>
	//owl carousel

	$(document).ready(function () {
		$("#owl-demo").owlCarousel({
			navigation: true,
			slideSpeed: 300,
			paginationSpeed: 400,
			singleItem: true,
			autoPlay: true

		});
	});

	//custom select box

	$(function () {
		$('select.styled').customSelect();
	});

	$(window).on("resize", function () {
		var owl = $("#owl-demo").data("owlCarousel");
		owl.reinit();
	});

</script>
</body>
</html>
