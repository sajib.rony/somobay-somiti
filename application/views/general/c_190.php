<div class="" style="background-color:#4A235A">

	<div class="row">
		<div class="col-md-12">
			<center><h3 style="color:white">শিক্ষিত বেকার যুব বহুমুখী সমবায় সমিতি লিঃ</h3></center>
			<center><h3 style="color:white">কর্মসংস্থান ও দারিদ্র বিমোচন প্রকল্প</h3></center>
			<center><h5 style="color:white">নিবন্ধন নম্বর -১৩/চাঁদ /১২, তারিখ :০৯/০৭/২০১২ </h5></center>
			<center><h6 style="color:white">ঠিকানা: ঠাকুর বাজার ,শাহারাস্তি , চাঁদপুর। </h6></center>
			<br>


			<div style="background-color:#E59866">
				<div style="background-color:#F7DC6F">
					<div class="row">
						<div class="col-md-12">
							<div class="col-md-10">

							</div>
							<div class="col-md-2">
								<label style=" color:black"><b>সি - ১৯০</b></label>
							</div>
						</div>
					</div>



					<center><h3 style="color:black"><b>সাধারণ বিভাগ</b></h3>
					</center>
					<center><h3 style="color:black"><b>৩০.০৬.২০২০ তারিখে " উদৃত পত্র " (স্থিতি পত্র) </b></h3>
					</center>

					<?php echo "<br>"; ?>


				</div>
				<?php echo "<br>"; ?>
				<div class="form-group">
					<div class="row">
						<div class="col-md-12">
							<div class="col-md-12">
								<div class="col-md-12">
									<div class="table-responsive">
										<table class="table table-bordered">

											<thead>
											<tr style="background: white">
												<th style="color:black" colspan="6">
													<center>মূলধন ও দেনা </center>
												</th>
											</tr>
											<tr style="background: white">
												<th style="color:black">হিসাব কোড</th>
												<th style="color:black">
													<center> বিবরন</center>
												</th>

												<th style="color:black">
													<center> নোট নং</center>
												</th>
												<th style="color:black">
													চলতি বছরের জের (টাকা)
												</th>
												<th style="color:black">
													<center>পূর্ববর্তী বছরের জের (টাকা)</center>
												</th>

												<th style="color:black">
													<center> চলতি বছরে বৃদ্ধি</center>
												</th>
											</tr>
											</thead>
											<tbody>
											<tr>
												<td style="color:black"><label>২০১০১১১</label></td>
												<td style="color:black"><label><p>শেয়ার মূলধন :</p></label>

													<p> গত সনে -</p>
													<p>(+) চলতি সনের আদায় -</p>
													<p>(-) শেয়ারের মূল্য ফেরত - </p>
												</td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
											</tr>

											<tr>
												<td style="color:black"><label>২০৩০১১৫</label></td>
												<td style="color:black"><label><p>সঞ্চয় আমানত দেনা :</p></label>

													<p> গত সনে -</p>
													<p>(+) হাল সনে আদায় -</p>
													<p>(-) হাল সনে ফেরৎ -</p>
												</td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
											</tr>


											<tr>
												<td style="color:black"><label>৩০১০৩০৫</label></td>
												<td style="color:black"><label><p>সঞ্চয়  লাভ দেনা :</p></label>

													<p> গত সনে -</p>
													<p>(+) হাল সনে আদায় -</p>
													<p>(-) হাল সনে ফেরৎ -</p>
												</td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
											</tr>

											<tr>
												<td style="color:black"><label>২০১০২১৫</label></td>
												<td style="color:black"><label><p>সংরক্ষিত তহবিল :</p></label>

													<p> গত সনে -</p>
													<p>(+) হাল সনে আদায় -</p>
													<p>(-) হাল সনে ফেরৎ -</p>
												</td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
											</tr>

											<tr>
												<td style="color:black"><label>২০১০২১৬</label></td>
												<td style="color:black"><label><p>কু-ঋণ তহবিল :</p></label>

													<p> গত সনে -</p>
													<p>(+) হাল সনে আদায় -</p>
													<p>(-) হাল সনে ফেরৎ -</p>
												</td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
											</tr>


											<tr>
												<td style="color:black"><label>২০১০২৩৬ </label></td>
												<td style="color:black"><label><p>দাতব্য তহবিল :</p></label>

													<p> গত সনে -</p>
													<p>(+) হাল সনে আদায় -</p>
													<p>(-) হাল সনে ফেরৎ -</p>
												</td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
											</tr>

											<tr>
												<td style="color:black"><label>২০১০২৩৬ </label></td>
												<td style="color:black"><label><p>উৎসাহ তহবিল  :</p></label>

													<p> গত সনে -</p>
													<p>(+) হাল সনে আদায় -</p>
													<p>(-) হাল সনে ফেরৎ -</p>
												</td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
											</tr>

											<tr>
												<td style="color:black"><label>২০১০২৩২</label></td>
												<td style="color:black"><label><p>অবন্টিত লাভ :</p></label>

													<p> গত সনে -</p>
													<p>(+) হাল সনে আদায় -</p>
													<p>(-) হাল সনে ফেরৎ -</p>
												</td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
											</tr>

											<tr>
												<td style="color:black"><label>২০১০২১৭ </label></td>
												<td style="color:black"><label><p>সমবায় উন্নয়ন তহবিল :</p></label>

													<p> গত সনে -</p>
													<p>(+) হাল সনে আদায় -</p>
													<p>(-) হাল সনে ফেরৎ -</p>
												</td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
											</tr>

											<tr>
												<td style="color:black"><label>২০৩০৫০০ </label></td>
												<td style="color:black"><label><p>হওলাদ গ্রহণ :</p>
													</label>
												</td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
											</tr>

											<tr>
												<td style="color:black"><label>২০১০২৩৫</label></td>
												<td style="color:black"><label><p>নীট লাভ (লাভ ক্ষতি হিসাব হতে আনিত ) :</p>
													</label>
												</td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
											</tr>

											<tr>
												<td style="color:black"><label>২০৩০১৪৮ </label></td>
												<td style="color:black"><label><p>অর্থগ্রহণ সাধারণ বিভাগ হতে
															:</p></label>
												</td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
											</tr>

											<tr>
												<td style="color:black"><label>২০৩০১৪৮ </label></td>
												<td style="color:black"><label><p> অর্থগ্রহণ কেন্দ্রীয় সমিতি হতে
															:</p></label>
												</td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
											</tr>

											<tr>
												<td style="color:black"><label>২০২০১৫০ </label></td>
												<td style="color:black"><label><p>ভবিষ্য তহবিল
															:</p></label>
												</td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
											</tr>



											<tr>
												<td class="text-right" colspan="3" style="color:black"><label>সর্বমোট
														=</label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
											</tr>


											</tbody>
										</table>
									</div>
								</div>


							</div>


						</div>
					</div>
				</div>


				<?php echo "<br>"; ?>

			</div>


			<div style="background-color:#F7DC6F">

				<?php echo "<br>"; ?>
				<div class="form-group">
					<div class="row">

						<div class="col-md-12">


							<?php echo "<br><br><br><br>"; ?>

							<div class="col-md-4">

							</div>

							<div class="col-md-4">


							</div>

							<div class="col-md-4">
								<p style="color:black;">
									<input type="text" name="" value="সম্পাদক /ব্যবস্থাপক" class="form-control">
								<hr>
								</p>
								<center><label style="color:black;">
										<b>
											সম্পাদক /ব্যবস্থাপক
										</b>
									</label></center>
							</div>

						</div>
					</div>
				</div>
				<?php echo "<br>"; ?>

			</div>
		</div>
		<span><br></span>
	</div>
</div>




