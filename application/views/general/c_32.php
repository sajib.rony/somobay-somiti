<div class="" style="background-color:#4A235A">
	<div class="row">
		<div class="col-md-12">
			<center><h3 style="color:white">শিক্ষিত বেকার যুব বহুমুখী সমবায় সমিতি লিঃ</h3></center>
			<center><h3 style="color:white">কর্মসংস্থান ও দারিদ্র বিমোচন প্রকল্প</h3></center>
			<center><h5 style="color:white">নিবন্ধন নম্বর -১৩/চাঁদ /১২, তারিখ :০৯/০৭/২০১২ </h5></center>
			<center><h6 style="color:white">ঠিকানা: ঠাকুর বাজার ,শাহারাস্তি , চাঁদপুর। </h6></center>
			<br>
			<div style="background-color:#E59866">
				<div style="background-color:#F7DC6F">
					<div class="row">
						<div class="col-md-12">
							<div class="col-md-10">

							</div>
							<div class="col-md-2">
								<label style=" color:black"><b>সি -৩২</b></label>
							</div>
						</div>
					</div>

					<center><h3 style="color:black"><b>সাধারণ বিভাগ </b></h3></center>
					<center><h3 style="color:black"><b>খাত ভিত্তিক মাসিক সদস্য প্রতিবেদন </b></h3>
					</center>

					<div class="row">
						<div class="col-md-12">
							<div class="col-md-10">
								<label style=" color:black"><b>মাসের নাম :Aug-2020</b></label>
							</div>
							<div class="col-md-2">
								<label style=" color:black"><b>তারিখ :<?php echo date('d/M/yy') ?></b></label>
							</div>
						</div>
					</div>


				</div>
				<?php echo "<br>"; ?>

				<div class="form-group">
					<div class="row">
						<div class="col-md-12">
							<div class="col-md-12">
								<div class="top10">&nbsp;</div>
								<form action="#" role="form" name="dataform" method="post" accept-charset="utf-8">

									<div class="table-responsive">
										<table class="table table-bordered">
											<thead>
											<tr style="background: #dd3333">
												<th class="sort_asc" style="color: white" colspan="4">
													<center><label>সদস্য সংখ্যা </label></center>
												</th>
												<th class="sort_asc" style="color: white" colspan="5">
													<center><label>মূলধন </label></center>
												</th>
												<th class="sort_asc" style="color: white" colspan="4">
													<center><label>লাভ </label></center>
												</th>

												<th class="" style="color: white">
													<center><label>সর্বমোট</label></center>

												</th>


											</tr>
											</thead>
											<thead>
											<tr style="background: white">
												<th class="sort_asc" style="color: black"><label>সাবেক সংখ্যা </label>
												</th>
												<th class="" style="color: black"><label>চলতি মাসের ভর্তি </label></th>
												<th class="" style="color: black"><label>চলতি মাসের ফেরত</label></th>
												<th class="" style="color: black"><label>স্থিতি সংখ্যা </label></th>
												<th class="" style="color: black"><label>খাতের নাম </label></th>
												<th class="" style="color: black"><label>বিগত মাস পযন্ত স্থিতি
														টাকা </label></th>
												<th class="" style="color: black"><label>মাসের আদায় টাকা </label></th>
												<th class="" style="color: black"><label>মাসের ফেরত টাকা </label></th>
												<th class="" style="color: black"><label>মাসের স্থিতি টাকা </label></th>
												<th class="" style="color: black"><label>বিগত মাস পযন্ত লাভ</label>
												</th>
												<th class="" style="color: black"><label>মাসের লাভ জমা </label></th>
												<th class="" style="color: black"><label>মাসের লাভ ফেরত </label></th>
												<th class="" style="color: black"><label>মাসের লাভ স্থিতি </label></th>
												<th class="" style="color: black"><label>(মূলধন ও লাভ ) </label></th>


											</tr>
											</thead>

											<tbody>
											<tr>
												<?php for ($i = 1;
														   $i <= 14;
														   $i++) { ?>
													<th><p style="color:black; text-align: center">
															<?php echo $i ?></p></th>

												<?php } ?>
											</tr>
											</tbody>
											<tr>
												<td><label style="color:black;"></label></td>
												<td><label style="color:black;"></label></td>
												<td><label style="color:black;"></label></td>
												<td><label style="color:black;"></label></td>
												<td><label style="color:black;">শেয়ার </label></td>
												<td><label style="color:black;"></label></td>
												<td><label style="color:black;"></label></td>
												<td><label style="color:black;"></label></td>
												<td><label style="color:black;"></label></td>
												<td><label style="color:black;"></label></td>
												<td><label style="color:black;"></label></td>
												<td><label style="color:black;"></label></td>
												<td><label style="color:black;"></label></td>
												<td><label style="color:black;"></label></td>
											</tr>
											<tr>
												<td><label style="color:black;"></label></td>
												<td><label style="color:black;"></label></td>
												<td><label style="color:black;"></label></td>
												<td><label style="color:black;"></label></td>
												<td><label style="color:black;">সঞ্চয় </label></td>
												<td><label style="color:black;"></label></td>
												<td><label style="color:black;"></label></td>
												<td><label style="color:black;"></label></td>
												<td><label style="color:black;"></label></td>
												<td><label style="color:black;"></label></td>
												<td><label style="color:black;"></label></td>
												<td><label style="color:black;"></label></td>
												<td><label style="color:black;"></label></td>
												<td><label style="color:black;"></label></td>
											</tr>
											<tr>
												<td colspan="2">
													<label style="color:black">সর্বশেষ সদস্য নম্বর </label>
												</td>
												<td colspan="2"><label style="color:black;">মাসে সঞ্চয় জমাকারীর
														সংখ্যা </label></td>

												<td rowspan="2"><label style="color:black;">মোট =</label></td>
												<td rowspan="2"><label style="color:black;"></label></td>
												<td rowspan="2"><label style="color:black;"></label></td>
												<td rowspan="2"><label style="color:black;"></label></td>
												<td rowspan="2"><label style="color:black;"></label></td>
												<td rowspan="2"><label style="color:black;"></label></td>
												<td rowspan="2"><label style="color:black;"></label></td>
												<td rowspan="2"><label style="color:black;"></label></td>
												<td rowspan="2"><label style="color:black;"></label></td>
												<td rowspan="2"><label style="color:black;"></label></td>
											</tr>
											<tr>
												<td colspan="2">
													<label style="color:black"> </label>
												</td>
												<td colspan="2"><label style="color:black;"></label></td>


											</tr>
										</table>
									</div>
									<div class="row" style="margin-top:5px;">
										<div class="col-md-6"><p class="pageinfo">Page 1 of 1 (Showing 1 to 4 of 4
												records)</p>
										</div>
										<div class="col-md-6">
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

