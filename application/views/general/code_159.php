<style>
	p {
		font-family: Roboto;
		font-size: medium;
		color: black;
		text-align: justify;
	}

	h4 {
		font-family: Roboto;

		color: black;
	}

	.div-padding1 {

		padding-top: 10px;
		padding-right: 30px;
		padding-bottom: 10px;
		padding-left: 30px;
	}

	.div-padding2 {

		padding-top: 10px;
		padding-right: 30px;
		padding-bottom: 10px;
		padding-left: 30px;
	}

	.input-field1 {
		width: 20%;
		padding: 5px 20px;
		margin: 8px 0;
		display: inline-block;
		border: 1px solid #ccc;
		border-radius: 4px;
		box-sizing: border-box;
	}

	.text_color_th {
		color: white;
	}
</style>

<div class="" style="background-color:#4A235A">
	<div class="row">
		<div class="col-md-12">
			<center><h3 style="color:white">শিক্ষিত বেকার সঞ্চয় ও ঋণদান সমবায় সমিতি লিঃ</h3></center>
			<center><h3 style="color:white">কর্মসংস্থান ও দারিদ্র বিমোচন প্রকল্প</h3></center>
			<center><h5 style="color:white">নিবন্ধন নম্বর -১৩/চাঁদ /১২, তারিখ :০৯/০৭/২০১২ </h5></center>
			<center><h6 style="color:white">ঠিকানা: ঠাকুর বাজার ,শাহারাস্তি , চাঁদপুর। </h6></center>
			<br>
		</div>
	</div>
</div>
<div class="" style="background-color:white">
	<div class="div-padding1">
		<div class="form-group">
			<div class="row">
				<div class="col-md-6">
				</div>
				<div class="col-md-6 text-right">
					<h5 style="color:black">কোড -১৫৯</h5>
				</div>

			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-12 text-center">
					<h2 style="color:black">খেলাপী বিনিয়োগ মামলার জন্য তথ্য ফরম </h2>
				</div>

			</div>
		</div>
	</div>
	<div class="div-padding2">
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-12">
						<p>বিনিয়োগ গ্রহীতার নাম</p>
						<input type="text" class="form-control" placeholder="বিনিয়োগ গ্রহীতার নাম">
					</div>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-4">
						<p>বিনিয়োগের পরিমান </p>
						<input type="text" class="form-control" placeholder="বিনিয়োগের পরিমান ">
					</div>
					<div class="col-md-4">
						<p>হিসাব নম্বর </p>
						<input type="text" class="form-control" placeholder="হিসাব নম্বর ">
					</div>
					<div class="col-md-4">
						<p>বিনিয়োগের মেয়াদ </p>
						<input type="text" class="form-control" placeholder="বিনিয়োগের মেয়াদ ">
					</div>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-6">
						<p>বিনিয়োগ গ্রহণের তারিখ </p>
						<input type="text" class="form-control" placeholder="বিনিয়োগ গ্রহণের তারিখ ">
					</div>
					<div class="col-md-6">
						<p>খেলাপীর তারিখ </p>
						<input type="text" class="form-control" placeholder="খেলাপীর তারিখ">
					</div>

				</div>
			</div>
		</div>
	</div>
	<div class="div-padding2">
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<p style="text-align:center">খালিস্থানে লেখা সম্পুন্ন না হলে অন্য কাগজে ক্রমিক নং এর বিপরীতে লেখা
						সংযুক্ত করা যাবে</p>
					<br>
					<p>১। খেলাপিকৃত টাকার হিসাব : মেয়াদ পরবর্তী সময় <input type="text" name="" class="input-field1"
																		   placeholder="তারিখ ">
						তারিখ পযন্ত বছর <input type="text" name="" class="input-field1"
											   placeholder="বছর"> মাস <input type="text" name="" class="input-field1"
																			 placeholder="মাস">
					</p>


					<div class="table-responsive">
						<table class="table-responsive table table-bordered">

							<tr style="background: #dd3333">
								<th class="text-center text_color_th">মূলধন</th>
								<th class="text-center text_color_th">লাভ</th>
								<th class="text-center text_color_th">মোট</th>
								<th class="text-center text_color_th">বিলম্ব মান্ডল</th>
								<th class="text-center text_color_th">দন্ডলাভ</th>
								<th class="text-center text_color_th">অন্যান্য সর্বমোট</th>
							</tr>

							<tr>

								<td><input type="text" name="" class="form-control" placeholder="মূলধন"></td>
								<td><input type="text" name="" class="form-control" placeholder="লাভ"></td>
								<td><input type="text" name="" class="form-control" placeholder="মোট"></td>
								<td><input type="text" name="" class="form-control" placeholder="বিলম্ব মান্ডল"></td>
								<td><input type="text" name="" class="form-control" placeholder="দন্ডলাভ"></td>
								<td><input type="text" name="" class="form-control" placeholder="অন্যান্য সর্বমোট ">
								</td>
							</tr>

						</table>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="div-padding2">
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">

					<p> ২। বিনিয়োগটির প্রদানকালীন সময় অনিয়মগুলো বিস্তারিত তথ্যসহ :</p>
					<textarea type="text" name="" class="form-control"
							  placeholder="বিনিয়োগটির প্রদানকালীন সময় অনিয়মগুলো বিস্তারিত তথ্যসহ"></textarea>

				</div>
			</div>
		</div>
	</div>
	<div class="div-padding2">
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">

					<p>৩। বিনিয়োগটির খেলাপী হওয়ার কারণ (বিস্তারিত লিখতে হবে)</p>
					<textarea type="text" name="" class="form-control"
							  placeholder="বিনিয়োগটির খেলাপী হওয়ার কারণ"></textarea>

				</div>
			</div>
		</div>
	</div>
	<div class="div-padding2">
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">

					<p>৪। বর্তমানে নিন্মোত্তদের আয়ের উৎস , মাসিক আয়ের পরিমান, সম্পদ ও সম্পত্তি এবং বসবাসরত অবস্থান
						ঠিকানাসহ
						:</p>

					<div class="row">
						<div class="col-md-12">
							<p>ক) গ্রহীতার তথ্য </p>
							<textarea type="text" name="" class="form-control"
									  placeholder="গ্রহীতার তথ্য"></textarea><br>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<p>খ) অভিভাবকের তথ্য </p>
							<textarea type="text" name="" class="form-control"
									  placeholder="অভিভাবকের তথ্য"></textarea><br>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<p>গ) জামিনদের তথ্য </p>
							<textarea type="text" name="" class="form-control"
									  placeholder="জামিনদের তথ্য "></textarea><br>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
	<div class="div-padding2">
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">

					<p>৫। কার বিরুদ্ধে মামলা করা হলে,খেলাপীকৃত টাকা আদায় করা যাবে (ব্যাখ্যাসহ লিখতে হবে ) :</p>
					<textarea type="text" name="" class="form-control"
							  placeholder="কার বিরুদ্ধে  মামলা করা হলে,খেলাপীকৃত টাকা আদায় করা যাবে"></textarea>

				</div>
			</div>
		</div>
	</div>
	<div class="div-padding2">
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">

					<p>৬। মামলা ব্যতিত কেন খেলাপী টাকা আদায় করা যাবে না :</p>
					<textarea type="text" name="" class="form-control"
							  placeholder="মামলা ব্যতিত কেন খেলাপী টাকা আদায় করা যাবে না"></textarea>

				</div>
			</div>
		</div>
	</div>
	<div class="div-padding2">
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">

					<p>৭। মামলা করা হলে খেলাপীকৃত টাকা আদায় নিশ্চিত হবে কিনা :</p>
					<textarea type="text" name="" class="form-control"
							  placeholder="মামলা করা হলে খেলাপীকৃত টাকা আদায় নিশ্চিত হবে কিনা"></textarea>

				</div>
			</div>
		</div>
	</div>
	<div class="div-padding2">
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">

					<p>৮। বিনিয়োগ প্রদানের সাথে সংশ্লিষ্টদের বিরুদ্ধে শাস্তিমূলক ব্যবস্তা গ্রহণ যোক্তিক হবে কিনা :</p>
					<textarea type="text" name="" class="form-control"
							  placeholder="বিনিয়োগ প্রদানের সাথে সংশ্লিষ্টদের বিরুদ্ধে শাস্তিমূলক ব্যবস্তা গ্রহণ যোক্তিক হবে কিনা"></textarea>

				</div>
			</div>
		</div>
	</div>
	<div class="div-padding2">
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<p>৯। সদস্য পদের হালনাগাদ জমা স্থিতির বিবরণ </p>

					<div class="table-responsive">
						<table class="table-responsive table table-bordered">

							<tr style="background: #dd3333">
								<th class="text-center text_color_th">বিবরণ</th>
								<th class="text-center text_color_th">সদস্য নম্বর</th>
								<th class="text-center text_color_th">শেয়ার</th>
								<th class="text-center text_color_th">সঞ্চয়</th>
								<th class="text-center text_color_th">আমানত</th>
								<th class="text-center text_color_th">সর্বমোট</th>
							</tr>

							<?php $name = [
									'বিনিয়োগ গ্রহীতার নামে ', 'অভিভাবক এর নামে ', 'জামিনদার এর নামে '
							];
							foreach ($name as $data) {
								?>
								<tr>
									<td><p><?php echo $data ?></p></td>
									<td><input type="text" name="" class="form-control" placeholder="সদস্য নম্বর"></td>
									<td><input type="text" name="" class="form-control" placeholder="শেয়ার"></td>
									<td><input type="text" name="" class="form-control" placeholder="সঞ্চয়"></td>
									<td><input type="text" name="" class="form-control" placeholder="আমানত"></td>
									<td><input type="text" name="" class="form-control" placeholder="সর্বমোট">
									</td>
								</tr>
							<?php } ?>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="div-padding2">
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<p>উল্লেখিত তথ্য ব্যবস্থাপক কর্তৃক পূরণ করে তার নাম , পদবী ,তারিখ ও সীল দিয়ে বিনিয়োগ ফাইলের সাথে ফরম
						প্রেরণ করতে হবে। </p>
				</div>
			</div>
		</div>
	</div>
	<div class="form-group">
		<div class="row">
			<div class="col-md-12">
				<center><br><br>
					<button type="submit" style="background: #dd3333" class="btn btn-danger">Submit</button>
				</center>
				<br>
			</div>
		</div>
	</div>
</div>
</div>
