<style>
	p {
		font-family: Roboto;
		font-size: medium;
		color: black;
		text-align: justify;
	}

	h4 {
		font-family: Roboto;

		color: black;
	}

	.div-padding1 {

		padding-top: 10px;
		padding-right: 30px;
		padding-bottom: 50px;
		padding-left: 30px;
	}

	.div-padding2 {

		padding-top: 20px;
		padding-right: 30px;
		padding-bottom: 50px;
		padding-left: 30px;
	}

	.input-field1 {
		width: 30%;
		padding: 5px 20px;
		margin: 8px 0;
		display: inline-block;
		border: 1px solid #ccc;
		border-radius: 4px;
		box-sizing: border-box;
	}

</style>

<div class="" style="background-color:#4A235A">
	<div class="row">
		<div class="col-md-12">
			<center><h3 style="color:white">শিক্ষিত বেকার সঞ্চয় ও ঋণদান সমবায় সমিতি লিঃ</h3></center>
			<center><h3 style="color:white">কর্মসংস্থান ও দারিদ্র বিমোচন প্রকল্প</h3></center>
			<center><h5 style="color:white">নিবন্ধন নম্বর -১৩/চাঁদ /১২, তারিখ :০৯/০৭/২০১২ </h5></center>
			<center><h6 style="color:white">ঠিকানা: ঠাকুর বাজার ,শাহারাস্তি , চাঁদপুর। </h6></center>
			<br>
		</div>
	</div>
</div>
<div class="" style="background-color:white">
	<div class="div-padding1">
		<div class="form-group">
			<div class="row">
				<div class="col-md-6">

				</div>
				<div class="col-md-6 text-right">
					<h5 style="color:black">কোড -১৯৬</h5>
				</div>

			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-6">
					<h5 style="color:black">স্মারক নং - <?php echo "সা-বি-" . time() ?></h5>
				</div>
				<div class="col-md-6 text-right">
					<h5 style="color:black">তারিখ :<?php echo date('d/m/yy') ?></h5>
				</div>
				<div class="col-md-12 text-center">
					<h2 style="color:black">বিনিয়োগ আবেদন ফরম</h2>
				</div>

			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<p>বরাবর</p>
					<p>সম্পাদক</p>
					<p>ব্যবস্থাপনা কমিটি </p>
					<p>শিক্ষিত বেকার সঞ্চয় ও ঋণদান সমবায় সমিতি লিঃ</p>
					<p>ওয়ারলেছ বাজার , চাঁদপুর সদর, চাঁদপুর। </p><br>
					<h4>বিষয় : বিনিয়োগ পাওয়ার আবেদন। ।</h4><br>
					<p>জনাব,</p>
					<p>আমি নিন্ম স্বাক্ষরকারী অত্র সমিতির একজন সদস্য। আমার বিশেষ প্রয়োজনে সমিতি হতে বিনিয়োগ পাওয়ার জন্য
						আমার ও আমার সদস্য পদের তথ্যাদি নিন্মে পেশ করলাম। </p>
				</div>
			</div>
		</div>
	</div>
	<div class="div-padding2">
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-3">
						<p>নাম</p>
					</div>
					<div class="col-md-9">
						<input type="text" name="" class="form-control" placeholder="নাম">
					</div>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-3">
						<p>পিতা /স্বামীর নাম</p>
					</div>
					<div class="col-md-9">
						<input type="text" name="" class="form-control"
							   placeholder="পিতা /স্বামীর নাম">
					</div>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-3">
						<p>মাতার নাম</p>
					</div>
					<div class="col-md-9">
						<input type="text" name="" class="form-control"
							   placeholder="মাতার নাম">
					</div>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-3">
						<p>মোবাইল /ফোন নম্বর</p>
					</div>
					<div class="col-md-9">
						<input type="text" name="" class="form-control"
							   placeholder="মোবাইল /ফোন নম্বর">
					</div>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-3">
						<p>স্থায়ী ঠিকানা</p>
					</div>
					<div class="col-md-9">
						<textarea type="text" name="" class="form-control"
								  placeholder="স্থায়ী ঠিকানা"></textarea>
					</div>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-3">
						<p>ব্যবসার /কর্মস্থলের ঠিকানা </p>
					</div>
					<div class="col-md-9">
						<textarea type="text" name="" class="form-control"
								  placeholder="ব্যবসার /কর্মস্থলের ঠিকানা "></textarea>
					</div>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-3">
						<p>সদস্য নম্বর ও ভর্তির তারিখ </p>
					</div>
					<div class="col-md-5">
						<input type="text" name="" class="form-control"
							   placeholder="সদস্য নম্বর">
					</div>
					<div class="col-md-4">
						<input type="text" name="" class="form-control"
							   placeholder="ভর্তির তারিখ ">
					</div>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-3">
						<p>সদস্য পদের শেয়ার ও সঞ্চয় স্থিতি </p>
					</div>
					<div class="col-md-3">
						<p>শেয়ার জমা</p>
						<input type="text" name="" class="form-control"
							   placeholder="শেয়ার জমা">
					</div>
					<div class="col-md-3">
						<p>সঞ্চয় জমা </p>
						<input type="text" name="" class="form-control"
							   placeholder="সঞ্চয় জমা ">
					</div>
					<div class="col-md-3">
						<p>সর্বমোট জমা স্থিতি</p>
						<input type="text" name="" class="form-control"
							   placeholder="সর্বমোট জমা স্থিতি">
					</div>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-3">
						<p>অন্যান্য সমিতিতে সদস্য নম্বর </p>
					</div>
					<div class="col-md-9">
						<input type="text" name="" class="form-control"
							   placeholder="অন্যান্য সমিতিতে সদস্য নম্বর ">
					</div>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-3">
						<p>অন্যান্য সমিতিতে শেয়ার ও সঞ্চয় স্থিতি</p>
					</div>
					<div class="col-md-5">
						<p>শেয়ার স্থিতি</p>
						<input type="text" name="" class="form-control"
							   placeholder="শেয়ার স্থিতি">
					</div>
					<div class="col-md-4">
						<p>সঞ্চয় স্থিতি</p>
						<input type="text" name="" class="form-control"
							   placeholder="সঞ্চয় স্থিতি">
					</div>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-3">
						<p>বিনিয়োগের পরিমান</p>
					</div>
					<div class="col-md-5">
						<p>টাকা </p>
						<input type="text" name="" class="form-control"
							   placeholder="টাকা ">
					</div>
					<div class="col-md-4">
						<p> শেয়ার ও সঞ্চয় জমা স্থিতির হার</p>
						<input type="text" name="" class="form-control"
							   placeholder="শেয়ার  ও সঞ্চয় জমা  স্থিতির হার">
					</div>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-3">
						<p>বিনিয়োগ পরিশোধ পদ্ধতি </p>
					</div>
					<div class="col-md-9">
						<input type="text" name="" class="form-control"
							   placeholder="বিনিয়োগ পরিশোধ পদ্ধতি  ">
					</div>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-3">
						<p>বিনিয়োগ গ্রহণের মেয়াদ </p>
					</div>
					<div class="col-md-9">
						<input type="text" name="" class="form-control"
							   placeholder="বিনিয়োগ গ্রহণের মেয়াদ">
					</div>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-3">
						<p>কিস্তি পরিশোধের আয়ের উৎস </p>
					</div>
					<div class="col-md-9">
						<textarea type="text" name="" class="form-control"
								  placeholder="কিস্তি পরিশোধের আয়ের উৎস"></textarea>
					</div>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-3">
						<p>পূর্বে বিনিয়োগ গ্রহণের তথ্য </p>
					</div>
					<div class="col-md-9">
						<textarea type="text" name="" class="form-control"
								  placeholder="পূর্বে বিনিয়োগ গ্রহণের তথ্য "></textarea>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="div-padding2">
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-12">
						<p>বিনিয়োগ গ্রহকৃত টাকা পরিশোধের নিশ্চয়তা : আমি এই মর্মে নিশ্চয়তা প্রদান করছি যে, বিনিয়োগ বা
							ঋণের আবেদনকৃত টাকা মঞ্জুর করা হলে সমবায় সমিতির আইন , সমবায় সমিতির বিধিমালা, সমিতির উপ-আইন,
							সমিতির বিনিয়োগ নীতিমালা ও ঋণের চুক্তিনামা অনুযায়ী পরিশোধ করবো। অন্যথায় সমিতি কর্তৃপক্ষ আমার
							জমাকৃত শেয়ার ও সঞ্চয় হতে বিনিয়োগ এর পাওনা টাকা উত্তোলন করে সমন্বয় করতে পারবে যা আমি সমিতির
							সম্পাদকে শেয়ার ও সঞ্চয় উত্তোলনের ক্ষমতা পত্র প্রদান করলাম এবং অবশিষ্টি টাকা আদায়ে আমার
							বিরুদ্ধে আইনগত ব্যবস্তা গ্রহণ করতে পারবে। আমার অবর্তমানে বা আমার মৃত্যুর পর সমিতির টাকা
							পরিশোধ করার জন্য একজন অভিভাবক ও একজন জামিনদার বিনিয়োগ চুক্তিনামা অনুযায়ী নিযুক্ত করিব।
							<input type="checkbox" name="" class="form-control"></p>
					</div>

				</div>
			</div>
		</div>
	</div>
	<div class="div-padding2">
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-8">
					</div>
					<div class="col-md-4">

						<hr>
						<p>আবেদনকারীর নাম ও স্বাক্ষর </p>
						<p>তারিখ </p>
						<input type="date" name="" class="form-control">
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="div-padding2">
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<p>বিনিয়োগ আবেদনকারীর সকল বিষয় পর্যালোচনা করে দেখা যায় তিনি পূর্বের বিনিয়োগ গ্রহণ করে যথারীতি পরিশোধ
						করেছে। আবেদনকারীর নামে পূর্বে খেলাপী হয়েনি।বিনিয়োগ বাবদ পূর্বের সম্পূর্ণ টাকা পরিশোধ করা হয়েছে,
						বর্তমানে তার নামে কোন বিনিয়োগ নেই এবং বিনিয়োগ বাবদ কোন টাকা পাওনা নেই।
					</p>
				</div>
			</div>
		</div>
	</div>
	<div class="div-padding2">
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<p>
						এমতবস্তায় বিনিয়োগ আবেদনকারীকে <input type="text" name="" class="input-field1"
															 placeholder="টাকা"> টাকা ,<input type="text" name=""
																							  class="input-field1"
																							  placeholder="বছর"> বছর
						<input type="text" name=""
							   class="input-field1"
							   placeholder="মেয়াদ"> মেয়াদে বার্ষিক শতকরা <input type="text" name=""
																				class="input-field1"
																				placeholder="হার"> হারে ,মাসিক
						<input type="text" name=""
							   class="input-field1"
							   placeholder="কিস্তি"> কিস্তিতে বিনিয়োগ প্রদান করার অনুমতি বা অনুমোদন দেওয়া হল।
					</p>
				</div>
			</div>
		</div>
	</div>
	<div class="div-padding2">
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-7">
					</div>
					<div class="col-md-5">

						<hr>
						<p style="text-align:center">সম্পাদক</p>
						<p style="text-align:center">ব্যবস্থাপনা কমিটি</p>
						<p style="text-align:center">শিক্ষিত বেকার সঞ্চয় ও ঋণদান সমবায় সমিতি লিঃ</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="div-padding2">
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<center><br><br>
						<button type="submit" style="background: #dd3333" class="btn btn-danger">Submit</button>
					</center>
					<br>
				</div>
			</div>
		</div>
	</div>
</div>

