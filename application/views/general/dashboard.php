<div class="panel panel-primary">
	<div class="panel-heading">
		<b> শিক্ষিত বেকার যুব বহুমুখী সমবায় সমিতি লিঃ</b>
	</div>
	<div class="panel-body">
		<div class="row state-overview">
			<a href="#">


				<div class="col-lg-3 col-sm-6">
					<section class="panel">
						<div class="symbol red" style="background-color: #186A3B;">
							<i class="fa fa-user user" aria-hidden="true"></i>
						</div>
						<div class="value">
							<h1>
								00
							</h1>
							<p>বৈধ অ্যাকাউন্ট</p>
						</div>
					</section>
				</div>
			</a>
			<a href="#">


				<div class="col-lg-3 col-sm-6">
					<section class="panel">
						<div class="symbol blue" style="background-color: #641E16;">
							<i class="fa fa-user user"></i>
						</div>
						<div class="value">
							<h1>
								00
							</h1>
							<p>মুলতুবি থাকা অ্যাকাউন্ট</p>
						</div>
					</section>
				</div>
			</a>
			<div class="col-lg-3 col-sm-6">
				<section class="panel">
					<div class="symbol blue" style="background-color: #196F3D;">
						<i class="fa fa-user user"></i>
					</div>
					<div class="value">
						<h1>
							00
						</h1>
						<p>অ্যাকাউন্ট প্রত্যাখ্যান</p>
					</div>
				</section>
			</div>
			<div class="col-lg-3 col-sm-6">
				<section class="panel">
					<div class="symbol blue" style="background-color: #641E16;">
						<i class="fa fa-user user"></i>
					</div>
					<div class="value">
						<h1>
							00
						</h1>
						<p>ক্লায়েন্টের মোট সংখ্যা</p>
					</div>
				</section>
			</div>
			<div class="col-lg-3 col-sm-6">
				<section class="panel">
					<div class="symbol blue" style="background-color: #196F3D;">
						<i class="fa fa-user user"></i>
					</div>
					<div class="value">
						<h1>
							00
						</h1>
						<p>বর্তমান বছর বৈধ অ্যাকাউন্ট</p>
					</div>
				</section>
			</div>
			<div class="col-lg-3 col-sm-6">
				<section class="panel">
					<div class="symbol blue" style="background-color: #641E16;">
						<i class="fa fa-user user"></i>
					</div>
					<div class="value">
						<h1>
							00
						</h1>
						<p>বর্তমান বছরের মুলতুবি থাকা অ্যাকাউন্ট</p>
					</div>
				</section>
			</div>
			<div class="col-lg-3 col-sm-6">
				<section class="panel">
					<div class="symbol red" style="background-color: #186A3B;">
						<i class="fa fa-user user" aria-hidden="true"></i>
					</div>
					<div class="value">
						<h1>
							00
						</h1>
						<p>বর্তমান বছরের অ্যাকাউন্ট প্রত্যাখ্যান

						</p>
					</div>
				</section>
			</div><div class="col-lg-3 col-sm-6">
				<section class="panel">
					<div class="symbol blue" style="background-color: #641E16;">
						<i class="fa fa-user user"></i>
					</div>
					<div class="value">
						<h1>
							00
						</h1>
						<p>বর্তমান বছরের ক্লায়েন্টের মোট সংখ্যা </p>
					</div>
				</section>
			</div>


		</div>
	</div>
</div>
