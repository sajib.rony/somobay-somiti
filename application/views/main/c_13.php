<style>
	p {
		font-family: Roboto;
		font-size: medium;
		color: black;
		text-align: justify;
	}

	h4 {
		font-family: Roboto;

		color: black;
	}

	.div-padding1 {

		padding-top: 10px;
		padding-right: 30px;
		padding-bottom: 50px;
		padding-left: 30px;
	}

	.div-padding2 {

		padding-top: 20px;
		padding-right: 30px;
		padding-bottom: 50px;
		padding-left: 30px;
	}

	.input-field1 {
		width: 50%;
		padding: 5px 20px;
		margin: 8px 0;
		display: inline-block;
		border: 1px solid #ccc;
		border-radius: 4px;
		box-sizing: border-box;
	}

</style>


<div class="" style="background-color:#4A235A">

	<div class="row">
		<div class="col-md-12">
			<center><h3 style="color:white">শিক্ষিত বেকার যুব বহুমুখী সমবায় সমিতি লিঃ</h3></center>
			<center><h3 style="color:white">কর্মসংস্থান ও দারিদ্র বিমোচন প্রকল্প</h3></center>
			<center><h5 style="color:white">নিবন্ধন নম্বর -১৩/চাঁদ /১২, তারিখ :০৯/০৭/২০১২ </h5></center>
			<center><h6 style="color:white">ঠিকানা: ঠাকুর বাজার ,শাহারাস্তি , চাঁদপুর। </h6></center>
			<br>
		</div>
	</div>
</div>

<div style="background-color: white">
	<div class="div-padding1">
		<div class="form-group">
			<div class="row">
				<div class="col-md-6">

				</div>
				<div class="col-md-6 text-right">
					<h5 style="color:black">সি - ১৩</h5>
				</div>

			</div>
		</div>

		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<p>বরাবর,</p>
					<p>সভাপতি /সম্পাদক,</p>
					<p> শিক্ষিত বেকার যুব বহুমুখী সমবায় সমিতি লিঃ </p>
					<p>ওয়ারলেছ বাজার , চাঁদপুর সদর, চাঁদপুর। </p><br>
					<h4>বিষয় : ০৩ (তিন) এর অধিক কিস্তি অগ্রিম পরিশোধ করে পুনরায় বিনিয়োগ নেয়ার অনুমতি
						পাওয়া প্রসঙ্গে । </h4><br>
					<p>মহোদয়,</p>
					<p> উল্লেখিত বিষয়ের আলোকে আপনার অবগতির জন্য জানাচ্ছি
						যে, আমি আপনার সমিতির একজন সদস্য। আমার সদস্য নং <input style="width:150px;"
																			  type="text"
																			  name="" placeholder="সদস্য নং"
																			  class="input-field1">। আমার সদস্য পদ
						বর্তমানে
						চালু/বন্ধ রয়েছে। আমি বিগত <input style="width:150px;" type="date" name="" value=""
														 class="input-field1"> তারিখে বিনিয়োগ হিসাব নং <input
								style="width:150px;" type="text" name="" placeholder="হিসাব নং" class="input-field1"> এর
						মাধ্যমে <input style="width:150px;" type="text" name="" placeholder=" টাকা "
									   class="input-field1">
						টাকা <input style="width:150px;" type="text" name="" placeholder=" বছর " class="input-field1">
						বছর
						মেয়াদে বিনিয়োগ গ্রহণ করে নির্ধারিত তারিখে অফিস এসে <input style="width:150px;"
																				  type="text" name=""
																				  value=" "
																				  class="input-field1"> কিস্তি পরিশোধ
						করি।
						বর্তমানে <input style="width:150px;" type="text" name="" value=" " class="input-field1"> নং
						কিস্তি
						চলমান রয়েছে। আমার পেশা <input style="width:150px;" type="text" name=""
													  placeholder=""
													  class="input-field1"> মাসিক আয় <input style="width:150px;"
																							type="text"
																							name="" value=" "
																							class="input-field1">
						টাকা। বর্তমানে আমার <input style="width:150px;" type="text" name="" value=" "
												   class="input-field1">
						কাজে অর্থের বিশেষ প্রয়োজন হওয়ায় পুনরায় <input style="width:150px;" type="text"
																	  name=""
																	  value=" " class="input-field1"> টাকার বিনিয়োগ
						গ্রহণ
						প্রয়োজন। এতে চলমান <input style="width:150px;" type="text" name="" value=" "
												  class="input-field1">
						টি কিস্তি লাভসহ অগ্রীম পরিশোধ করে পুনরায় বিনিয়োগ গ্রহণ করতে চাই। এই ক্ষেত্রে
						ভবিষ্যতে
						আমার কোনো অভিযোগ বা আপত্তি থাকবে না। </p>
					<p>অতএব, উল্লেখিত তথ্য বিবেচনা করে আমাকে অগ্রিম
						কিস্তি পরিশোধ করে পুনরায় বিনিয়োগ নেয়ার জন্য আপনার অনুমতি প্রার্থনা করছি।
					</p>


				</div>
			</div>
		</div>
	</div>

	<div class="div-padding2">
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-6">
						<div class="row" style="border:2px solid black;color:black;padding: 10px 0px;">


							<div class="row" style="padding-bottom: 5px;">
								<div class="col-md-12">
									<div class="col-md-6"><p>সদস্য হওয়ার তারিখ <span>=</span></p></div>
									<div class="col-md-6"><input type="text" class="form-control" name=""
																 placeholder="সদস্য হওয়ার তারিখ">
									</div>
								</div>
							</div>
							<div class="row" style="padding-bottom: 5px;">
								<div class="col-md-12">
									<div class="col-md-6"><p>প্রতিমাসে সঞ্চয় জমার পরিমান <span>=</span></p></div>
									<div class="col-md-6"><input type="text" class="form-control" name=""
																 placeholder="প্রতিমাসে সঞ্চয় জমার পরিমান ">
									</div>
								</div>
							</div>
							<div class="row" style="padding-bottom: 5px;">
								<div class="col-md-12">
									<div class="col-md-6"><p>প্রতিমাসে সঞ্চয় জমার পরিমান <span>=</span></p></div>
									<div class="col-md-6"><input type="text" class="form-control" name=""
																 placeholder="প্রতিমাসে সঞ্চয় জমার পরিমান">
									</div>
								</div>
							</div>
							<div class="row" style="padding-bottom: 5px;">
								<div class="col-md-12">
									<div class="col-md-6"><p>সঞ্চয় জমার সর্বশেষ মাস<span>=</span></p></div>
									<div class="col-md-6"><input type="text" class="form-control" name=""
																 placeholder="সঞ্চয় জমার সর্বশেষ মাস">
									</div>
								</div>
							</div>
							<div class="row" style="padding-bottom: 5px;">
								<div class="col-md-12">
									<div class="col-md-6"><p>শেয়ার স্থিতি<span>=</span></p></div>
									<div class="col-md-6"><input type="text" class="form-control" name=""
																 placeholder="শেয়ার স্থিতি">
									</div>
								</div>
							</div>
							<div class="row" style="padding-bottom: 5px;">
								<div class="col-md-12">
									<div class="col-md-6"><p>সঞ্চয় স্থিতি<span>=</span></p></div>
									<div class="col-md-6"><input type="text" class="form-control" name=""
																 placeholder="সঞ্চয় স্থিতি">
									</div>
								</div>
							</div>
							<div class="row" style="padding-bottom: 5px;">
								<div class="col-md-12">
									<div class="col-md-6"><p>এককালীন আমানত স্থিতি <span>=</span></p></div>
									<div class="col-md-6"><input type="text" class="form-control" name=""
																 placeholder="এককালীন আমানত স্থিতি ">
									</div>
								</div>
							</div>
							<div class="row" style="padding-bottom: 5px;">
								<div class="col-md-12">
									<div class="col-md-6"><p>আমানত গ্রহন স্থিতি<span>=</span></p></div>
									<div class="col-md-6"><input type="text" class="form-control" name=""
																 placeholder="আমানত গ্রহন স্থিতি">
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="row" style="padding-bottom: 10px;">
							<div class="col-md-12">
								<p style="text-align:center">বিনীত নিবেদক</p>
							</div>
						</div>
						<div class="row" style="padding-bottom: 5px;">
							<div class="col-md-12">
								<div class="col-md-6"><p>স্বাক্ষর </p></div>
								<div class="col-md-6"><input type="checkbox" class="form-control" name=""
															 placeholder="">
								</div>
							</div>
						</div>
						<div class="row" style="padding-bottom: 5px;">
							<div class="col-md-12">
								<div class="col-md-6"><p>নাম </p></div>
								<div class="col-md-6"><input type="text" class="form-control" name=""
															 placeholder="নাম">
								</div>
							</div>
						</div>
						<div class="row" style="padding-bottom: 5px;">
							<div class="col-md-12">
								<div class="col-md-6"><p>সদস্য নং</p></div>
								<div class="col-md-6"><input type="text" class="form-control" name=""
															 placeholder="সদস্য নং">
								</div>
							</div>
						</div>
						<div class="row" style="padding-bottom: 5px;">
							<div class="col-md-12">
								<div class="col-md-6"><p>বিনিয়োগ হিসাব নম্বর</p></div>
								<div class="col-md-6"><input type="text" class="form-control" name=""
															 placeholder="বিনিয়োগ হিসাব নম্বর">
								</div>
							</div>
						</div>
						<div class="row" style="padding-bottom: 5px;">
							<div class="col-md-12">
								<div class="col-md-6"><p>আবেদনের তারিখ</p></div>
								<div class="col-md-6"><input type="text" class="form-control" name=""
															 placeholder="আবেদনের তারিখ">
								</div>
							</div>
						</div>
						<div class="row" style="padding-bottom: 5px;">
							<div class="col-md-12">
								<div class="col-md-6"><p>এককালীন আমানত স্থিতি</p></div>
								<div class="col-md-6"><input type="text" class="form-control" name=""
															 placeholder="এককালীন আমানত স্থিতি">
								</div>
							</div>
						</div>
						<div class="row" style="padding-bottom: 5px;">
							<div class="col-md-12">
								<div class="col-md-6"><p>আমানত গ্রহন স্থিতি</p></div>
								<div class="col-md-6"><input type="text" class="form-control" name=""
															 placeholder="আমানত গ্রহন স্থিতি">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="div-padding2">
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<p style="color:black;"> উপরোক্ত তথ্য যাচাই ও নিশ্চিত পূর্বক ব্যবস্থাপকের সুপারিশ
						:....</p>

					<?php echo "<br>"; ?>

					<div class="col-md-6">
						<p style="color:black;"> আবেদন পত্রের সাথে নিম্নোক্ত কাগজাদি প্রেরণ করতে হবে
							:</p>
						<p style="color:black;">১ । সদস্য খতিয়ানের ফটোকপি। </p>
						<p style="color:black;">২ । চলতি বিনিয়োগ হিসাবের লেজারের ফটোকপি ।
						<p>
						<p style="color:black;">৩ । বিনিয়োগের মূল ফাইল ।</p>

					</div>

					<div class="col-md-2"></div>
					<div class="col-md-4">
						<p style="color:black;">
							<input type="text" name="" value="ব্যবস্থাপকের স্বাক্ষর ও সীল "
								   class="form-control">
						<hr>
						</p>
						<p style="text-align: center">
									ব্যবস্থাপকের স্বাক্ষর ও সীল
						</p>
					</div>

				</div>
			</div>
		</div>
	</div>
	<div class="div-padding2">
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<center><br><br>
						<button type="submit" style="background: #dd3333" class="btn btn-danger">Submit</button>
					</center>
					<br>
				</div>
			</div>
		</div>
	</div>
</div>




