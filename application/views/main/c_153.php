<div class="" style="background-color:#4A235A">

	<div class="row">
		<div class="col-md-12">
			<center><h3 style="color:white">শিক্ষিত বেকার যুব বহুমুখী সমবায় সমিতি লিঃ</h3></center>
			<center><h3 style="color:white">কর্মসংস্থান ও দারিদ্র বিমোচন প্রকল্প</h3></center>
			<center><h5 style="color:white">নিবন্ধন নম্বর -১৩/চাঁদ /১২, তারিখ :০৯/০৭/২০১২ </h5></center>
			<center><h6 style="color:white">ঠিকানা: ঠাকুর বাজার ,শাহারাস্তি , চাঁদপুর। </h6></center>
			<br>


			<div style="background-color:#E59866">
				<div style="background-color:#F7DC6F">
					<div class="row">
						<div class="col-md-12">
							<div class="col-md-10">

							</div>
							<div class="col-md-2">
								<label style=" color:black"><b>সি - ১৫৩</b></label>
							</div>
						</div>
					</div>


					<?php echo "<br>"; ?>
					<center><h3 style="color:black"><b>০১.০৭.২০১৯ হতে ৩০.০৬.২০২০ পর্যন্ত লাভ-ক্ষতির হিসাব </b></h3>
					</center>

					<?php echo "<br>"; ?>


				</div>
				<?php echo "<br>"; ?>
				<div class="form-group">
					<div class="row">
						<div class="col-md-12">
							<div class="col-md-12">
								<div class="col-md-12">
									<table class="table table-bordered">

										<thead>
										<tr style="background: white">
											<th colspan="3" class="text-center" style="color:black"> ক্ষতি</th>
											<th colspan="3" class="text-center" style="color:black"> লাভ</th>
										</tr>
										</thead>
										<tbody>

										<tr style="background: white">
											<th style="color:black">ক্র নং</th>
											<th style="color:black">
												<center> বিবরণ</center>
											</th>

											<th style="color:black">
												<center> টাকা</center>
											</th>
											<th style="color:black">ক্র নং</th>
											<th style="color:black">
												<center> বিবরণ</center>
											</th>

											<th style="color:black">
												<center> টাকা</center>
											</th>


										</tr>
										</tbody>
										<tr>
											<td style="color:black"><label>১</label></td>
											<td style="color:black"><label>সাধারণ বিভাগের লাভের ১০% প্রদান </label></td>
											<td style="color:black"><label></label></td>
											<td style="color:black"><label>১</label></td>
											<td style="color:black"><label>বিনিয়োগ হতে লাভ আদায় (কর্মসংস্থান
													প্রকল্প)</label></td>
											<td style="color:black"><label></label></td>
										</tr>
										<tr>
											<td style="color:black"><label>২</label></td>
											<td style="color:black"><label>শেয়ারের উপর লভ্যাংশ প্রদান </label></td>
											<td></td>
											<td style="color:black"><label>২</label></td>
											<td style="color:black"><label>বিনিয়োগ হতে লাভ আদায় (স্বনির্ভর
													প্রকল্প)</label></td>
											<td></td>
										</tr>

										<tr>
											<td style="color:black"><label>৩</label></td>
											<td style="color:black"><label>সঞ্চয় আমানতের লাভ প্রদান </label></td>
											<td></td>
											<td style="color:black"><label>৩</label></td>
											<td style="color:black"><label>এফডিআর হতে লাভ প্রদান </label></td>
											<td></td>
										</tr>

										<tr>
											<td style="color:black"><label>৪</label></td>
											<td style="color:black"><label>এককালীন আমানতের লাভ প্রদান </label></td>
											<td></td>
											<td style="color:black"><label>৪</label></td>
											<td style="color:black"><label>আমানত হতে লাভ প্রাপ্ত </label></td>
											<td></td>
										</tr>

										<tr>
											<td style="color:black"><label>৫</label></td>
											<td style="color:black"><label>আমানত গ্রহণের লাভ প্রদান</label></td>
											<td></td>
											<td style="color:black"><label>৫</label></td>
											<td style="color:black"><label>ব্যাংক হতে লাভ প্রাপ্ত </label></td>
											<td></td>
										</tr>

										<tr>
											<td style="color:black"><label>৬</label></td>
											<td style="color:black"><label>অর্থ গ্রহণের লাভ প্রদান </label></td>
											<td></td>
											<td style="color:black"><label>৬</label></td>
											<td style="color:black"><label>প্রদানকৃত লাভ কর্তন </label></td>
											<td></td>
										</tr>


										<tr>
											<td style="color:black"><label> ৭</label></td>
											<td style="color:black"><label>ভবিষ্য় তহবিলের লাভ প্রদান </label></td>
											<td></td>
											<td style="color:black"><label> ৭</label></td>
											<td style="color:black"><label>আনুষাঙ্গিক আয় </label></td>
											<td></td>
										</tr>


										<tr>
											<td style="color:black"><label> ৮</label></td>
											<td style="color:black"><label>আনুষাঙ্গিক ব্যয় </label></td>
											<td></td>
											<td style="color:black"><label></label></td>
											<td style="color:black"><label></label></td>
											<td></td>
										</tr>

										<tr>
											<td style="color:black"><label>৯</label></td>
											<td style="color:black"><label>বেতন, বোনাস ও বিনোদন ভাতা প্রদান </label>
											</td>
											<td></td>
											<td style="color:black"><label> </label></td>
											<td style="color:black"><label></label></td>
											<td></td>
										</tr>

										<tr>
											<td style="color:black"><label> ১০</label></td>
											<td style="color:black"><label>পেনশন ও উন্নয়ণ তহবিল হতে ব্যয় </label></td>
											<td></td>
											<td style="color:black"><label> </label></td>
											<td style="color:black"><label></label></td>
											<td></td>
										</tr>

										<tr>
											<td style="color:black"><label> ১১</label></td>
											<td style="color:black"><label>ঝুঁকি তহবিল হতে তহবিল হতে ব্যয় </label></td>
											<td></td>
											<td style="color:black"><label> </label></td>
											<td style="color:black"><label></label></td>
											<td></td>
										</tr>

										<tr>
											<td style="color:black"><label> ১২</label></td>
											<td style="color:black"><label>আসবাব পত্রের অপচয় </label></td>
											<td></td>
											<td style="color:black"><label> </label></td>
											<td style="color:black"><label> </label></td>
											<td></td>
										</tr>

										<tr>
											<td style="color:black"><label> </label></td>
											<td style="color:black"><label> </label></td>
											<td></td>
											<td style="color:black"><label> </label></td>
											<td style="color:black"><label> </label></td>
											<td></td>
										</tr>
										<tr>
											<td style="color:black"><label> </label></td>
											<td style="color:black"><label> </label></td>
											<td></td>
											<td style="color:black"><label> </label></td>
											<td style="color:black"><label> </label></td>
											<td></td>
										</tr>
										<tr>
											<td style="color:black"><label> </label></td>
											<td style="color:black"><label> </label></td>
											<td></td>
											<td style="color:black"><label> </label></td>
											<td style="color:black"><label> </label></td>
											<td></td>
										</tr>
										<tr>
											<td style="color:black"><label> </label></td>
											<td style="color:black"><label> </label></td>
											<td></td>
											<td style="color:black"><label> </label></td>
											<td style="color:black"><label> </label></td>
											<td></td>
										</tr>
										<tr>
											<td style="color:black"><label> </label></td>
											<td style="color:black"><label> </label></td>
											<td></td>
											<td style="color:black"><label> </label></td>
											<td style="color:black"><label> </label></td>
											<td></td>
										</tr>
										<tr>
											<td style="color:black"><label> </label></td>
											<td style="color:black"><label> </label></td>
											<td></td>
											<td style="color:black"><label> </label></td>
											<td style="color:black"><label> </label></td>
											<td></td>
										</tr>


										<tr>
											<td colspan="2" class="text-right" style="color:black"><label> মোট =</label>
											</td>
											<td style="color:black"><label> </label></td>
											<td colspan="2" class="text-right" style="color:black"><label> মোট =</label>
											</td>
											<td style="color:black"><label> </label></td>

										</tr>
										<tr>
											<td colspan="2" class="text-right" style="color:black"><label> নীট লাভ
													=</label></td>
											<td style="color:black"><label> </label></td>
											<td colspan="2" class="text-right" style="color:black"><label> নীট ক্ষতি
													=</label></td>
											<td style="color:black"><label> </label></td>

										</tr>
										<tr>
											<td colspan="2" class="text-right" style="color:black"><label> সর্বমোট
													=</label></td>
											<td style="color:black"><label> </label></td>
											<td colspan="2" class="text-right" style="color:black"><label> সর্বমোট
													=</label></td>
											<td style="color:black"><label> </label></td>

										</tr>

										</tbody>
									</table>
								</div>


							</div>


						</div>
					</div>
				</div>


				<?php echo "<br>"; ?>

			</div>


			<div style="background-color:#F7DC6F">

				<?php echo "<br>"; ?>
				<div class="form-group">
					<div class="row">

						<div class="col-md-12">


							<?php echo "<br><br><br><br>"; ?>

							<div class="col-md-4">

							</div>

							<div class="col-md-4">


							</div>

							<div class="col-md-4">
								<p style="color:black;">
									<input type="text" name="" value="সম্পাদক /ব্যবস্থাপক" class="form-control">
								<hr>
								</p>
								<center><label style="color:black;">
										<b>
											সম্পাদক /ব্যবস্থাপক
										</b>
									</label></center>
							</div>

						</div>
					</div>
				</div>
				<?php echo "<br>"; ?>

			</div>
		</div>
		<span><br></span>
	</div>
</div>




