<style>
	p {
		font-family: Roboto;
		font-size: medium;
		color: black;
		text-align: justify;
	}

	h4 {
		font-family: Roboto;

		color: black;
	}

	.div-padding1 {

		padding-top: 10px;
		padding-right: 30px;
		padding-bottom: 10px;
		padding-left: 30px;
	}

	.div-padding2 {

		padding-top: 10px;
		padding-right: 30px;
		padding-bottom: 10px;
		padding-left: 30px;
	}

	.input-field1 {
		width: 25%;
		padding: 5px 20px;
		margin: 8px 0;
		display: inline-block;
		border: 1px solid #ccc;
		border-radius: 4px;
		box-sizing: border-box;
	}

	.text_color_th {
		color: white;
	}
</style>

<div class="" style="background-color:#4A235A">
	<div class="row">
		<div class="col-md-12">
			<center><h3 style="color:white">শিক্ষিত বেকার কেন্দ্রীয় সঞ্চয় ও ঋণদান সমবায় সমিতি লিঃ</h3></center>
			<center><h3 style="color:white">কর্মসংস্থান ও দারিদ্র বিমোচন প্রকল্প</h3></center>
			<center><h5 style="color:white">নিবন্ধন নম্বর -১৩/চাঁদ /১২, তারিখ :০৯/০৭/২০১২ </h5></center>
			<center><h6 style="color:white">ঠিকানা: ঠাকুর বাজার ,শাহারাস্তি , চাঁদপুর। </h6></center>
			<br>
		</div>
	</div>
</div>
<div class="" style="background-color:white">
	<div class="div-padding1">
		<div class="form-group">
			<div class="row">
				<div class="col-md-6">

				</div>
				<div class="col-md-6 text-right">
					<h5 style="color:black"> সি- ১৫৮ </h5>
				</div>

			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-12 text-center">
					<h3 style="color:black"> কর্মকর্তা/কর্মচারীর তথ্য </h3>
				</div>
				<div class="col-md-6">

				</div>
				<div class="col-md-6 text-right">

				</div>
			</div>
		</div>
	</div>
	<div class="div-padding2">
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-12">
						<p><b>ক) বাক্তিগত তথ্য </b></p>
					</div>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-6">
						<p>নাম</p>
						<input type="text" name="" class="form-control" placeholder="নাম">
					</div>
					<div class="col-md-6">
						<p>পিতা /স্বামীর নাম</p>
						<input type="text" name="" class="form-control" placeholder="পিতা /স্বামীর নাম">
					</div>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-4">
						<p>মাতার নাম</p>
						<input type="text" name="" class="form-control" placeholder="মাতার নাম">
					</div>
					<div class="col-md-4">
						<p>গ্রাম</p>
						<input type="text" name="" class="form-control" placeholder="গ্রাম">
					</div>
					<div class="col-md-4">
						<p>বাড়ি</p>
						<input type="text" name="" class="form-control" placeholder="বাড়ি">
					</div>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-4">
						<p>পোস্ট</p>
						<input type="text" name="" class="form-control" placeholder="পোস্ট">
					</div>
					<div class="col-md-4">
						<p>উপজেলা</p>
						<input type="text" name="" class="form-control" placeholder="উপজেলা">
					</div>
					<div class="col-md-4">
						<p>জেলা</p>
						<input type="text" name="" class="form-control" placeholder="জেলা">
					</div>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-3">
						<p>ধর্ম</p>
						<input type="text" name="" class="form-control" placeholder="ধর্ম">
					</div>
					<div class="col-md-3">
						<p>জন্ম তারিখ</p>
						<input type="text" name="" class="form-control" placeholder="জন্ম তারিখ">
					</div>
					<div class="col-md-3">
						<p>বয়স</p>
						<input type="text" name="" class="form-control" placeholder="বয়স">
					</div>
					<div class="col-md-3">
						<p>বৈবাহিক অবস্থা </p>
						<input type="text" name="" class="form-control" placeholder="বৈবাহিক অবস্থা ">
					</div>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-4">
						<p>বিবাহের তারিখ </p>
						<input type="text" name="" class="form-control" placeholder="বিবাহের তারিখ ">
					</div>
					<div class="col-md-4">
						<p>স্ত্রীর নাম </p>
						<input type="text" name="" class="form-control" placeholder="স্ত্রীর নাম ">
					</div>
					<div class="col-md-4">
						<p>স্ত্রীর জন্ম তারিখ</p>
						<input type="text" name="" class="form-control" placeholder=" স্ত্রীর জন্ম তারিখ">
					</div>

				</div>
			</div>
		</div>

		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-3">
						<p>স্ত্রীর বয়স</p>
						<input type="text" name="" class="form-control" placeholder="স্ত্রীর বয়স">
					</div>
					<div class="col-md-3">
						<p>সন্তানের সংখ্যা </p>
						<input type="text" name="" class="form-control" placeholder="সন্তানের সংখ্যা ">
					</div>
					<div class="col-md-3">
						<p>মেয়ের সংখ্যা</p>
						<input type="text" name="" class="form-control" placeholder="মেয়ের সংখ্যা">
					</div>
					<div class="col-md-3">
						<p>ছেলের সংখ্যা </p>
						<input type="text" name="" class="form-control" placeholder="ছেলের সংখ্যা">
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="div-padding2">
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-12">
						<p><b>খ) শিক্ষাগত যোগ্যতা ও অভিজ্ঞতা </b></p>
					</div>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-3">
						<p>সর্বশেষ শিক্ষাগত যোগ্যতা</p>
					</div>
					<div class="col-md-3">
						<p>পরীক্ষার নাম </p>
						<input type="text" name="" class="form-control" placeholder="পরীক্ষার নাম ">
					</div>
					<div class="col-md-3">
						<p>পরীক্ষার সনদ</p>
						<input type="text" name="" class="form-control" placeholder="পরীক্ষার সনদ">
					</div>
					<div class="col-md-3">
						<p>প্রাপ্ত বিভাগ </p>
						<input type="text" name="" class="form-control" placeholder="প্রাপ্ত বিভাগ ">
					</div>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-3">
						<p>চাকরীতে যোগদানকালীন শিক্ষাগত যোগ্যতা</p>
					</div>
					<div class="col-md-3">
						<p>পরীক্ষার নাম </p>
						<input type="text" name="" class="form-control" placeholder="পরীক্ষার নাম ">
					</div>
					<div class="col-md-3">
						<p>পরীক্ষার সনদ</p>
						<input type="text" name="" class="form-control" placeholder="পরীক্ষার সনদ">
					</div>
					<div class="col-md-3">
						<p>প্রাপ্ত বিভাগ </p>
						<input type="text" name="" class="form-control" placeholder="প্রাপ্ত বিভাগ ">
					</div>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-3">
						<p>বিশেষ কোন প্রশিক্ষণ (যদি থাকে)</p>
					</div>
					<div class="col-md-9">

						<textarea type="text" name="" class="form-control" placeholder="বিশেষ কোন প্রশিক্ষণ"></textarea>
					</div>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-3">
						<p>বিশেষ কোন অভিজ্ঞতা (যদি থাকে)</p>
					</div>
					<div class="col-md-9">
						<textarea type="text" name="" class="form-control" placeholder="বিশেষ কোন অভিজ্ঞতা"></textarea>
					</div>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-3">
						<p>বিশেষ কোন কৃতিত্ব (যদি থাকে)</p>
					</div>
					<div class="col-md-9">
						<textarea type="text" name="" class="form-control" placeholder="বিশেষ কোন কৃতিত্ব"></textarea>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="div-padding2">
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-12">
						<p><b>গ) চাকরির বৃত্তান্ত </b></p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="div-padding2">
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<div class="table-responsive">
						<table class="table-responsive table table-bordered">
							<?php for ($i = 1;
									   $i <= 14;
									   $i++) { ?>
								<tr>
									<td><p><?php echo $i; ?></p></td>
									<td><p></p></td>
									<td><p></p></td>

								</tr>
							<?php } ?>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="div-padding2">
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<div class="table-responsive">
						<table class="table-responsive table table-bordered">
							<tr style="background: #dd3333">
								<th class="text-center text_color_th" rowspan="3">নং</th>
								<th class="text-center text_color_th" rowspan="3">নাম পদের যোগদানের তারিখ</th>
								<th class="text-center text_color_th" rowspan="3">পদের ধরণ (স্থায়ী বা অস্থায়ী বা অন্য
									কোনভাবে অর্জিত পদ কিনা )
								</th>
								<th class="text-center text_color_th" rowspan="3">বাস্তব পদের বেতন অঙ্কে ও কথায়</th>
								<th class="text-center text_color_th" rowspan="3">পদের বেতন ব্যতীত অন্যান্য সুবিধা
									প্রাপ্য হয়েছে কিনা?যেসব সুবিধা ভোগ করেছে তা উল্লেখ করুন
								</th>
								<th class="text-center text_color_th" rowspan="3">অতিরিক্ত বেতন থাকলে উল্লেখ করুন</th>
								<th class="text-center text_color_th" rowspan="3">বাস্তব পদের বেতন আদেশ নম্বর ও তারিখ
								</th>
								<th class="text-center text_color_th" rowspan="3">কর্মকর্তা কর্মচারীর স্বাক্ষর</th>
								<th class="text-center text_color_th" rowspan="3">চাকরীকালীন সময় কারণ দর্শনো,
									তিরস্কার শাস্তি থাকলে তা উল্লেখ করুন (বিষয়, স্বাক্ষর ও তারিখ)
								</th>
								<th class="text-center text_color_th" rowspan="3">চাকরীকালীন পুরস্কার,স্বীকৃতি
									ইত্যাদি উল্লেখ করুন স্বাক্ষর ও তারিখ
								</th>
								<th class="text-center text_color_th" rowspan="3">কর্মকর্তা কর্মচারীর স্বাক্ষর</th>
								<th class="text-center text_color_th" colspan="3">ছুটির হিসাব</th>
								<th class="text-center text_color_th" rowspan="3">অফিস প্রধান বা অন্য কোন সত্যায়নকারী
									অফিসারের স্বাক্ষর ও সীল
								</th>
								<th class="text-center text_color_th" rowspan="3">চাকরী হতে অবসানের তারিখ</th>
								<th class="text-center text_color_th" rowspan="3">অবসানের কারণ</th>
								<th class="text-center text_color_th" rowspan="3">কর্মস্থান স্তানান্তর (যেমন বদলি নিয়োগ
									ইত্যাদি)
								</th>
								<th class="text-center text_color_th" rowspan="3">অফিস প্রধান বা কোন সত্যায়নকারী
									অফিসারের স্বাক্ষর ও সীল
								</th>
							</tr>
							<tr style="background: #dd3333">
								<th class="text-center text_color_th" rowspan="2">সাবেক ছুটি গ্রহণের স্থিতি</th>
								<th class="text-center text_color_th" colspan="2">প্রতি ১বছর (বছর ভিত্তিক সার্ভিস বহিঃ
									অনুযায়ী
								</th>
							</tr>
							<tr style="background: #dd3333">
								<th class="text-center text_color_th">সময়কাল</th>
								<th class="text-center text_color_th">প্রাপ্য ছুটি হতে বছরের ভোগকৃত ছুটি )</th>
							</tr>
							<tr style="background: #dd3333">
								<th></th>
								<?php for ($i = 1;
										   $i <= 18;
										   $i++) { ?>
									<th class="text-center text_color_th"><?php echo $i; ?></th>
								<?php } ?>
							</tr>
							<?php for ($i = 1;
									   $i <= 30;
									   $i++) { ?>
								<tr>
									<td><p><?php echo $i; ?></p></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
								</tr>
							<?php } ?>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!--	<div class="div-padding1">-->
	<!--		<div class="form-group">-->
	<!--			<div class="row">-->
	<!--				<div class="col-md-12">-->
	<!--					<div class="col-md-4">-->
	<!--						<hr>-->
	<!--						<p style="text-align: center">প্রস্তুতকারী</p>-->
	<!--					</div>-->
	<!--					<div class="col-md-4">-->
	<!---->
	<!--					</div>-->
	<!--					<div class="col-md-4">-->
	<!--						<hr>-->
	<!--						<p style="text-align: center">ব্যবস্থাপক</p>-->
	<!--					</div>-->
	<!--				</div>-->
	<!--			</div>-->
	<!--		</div>-->
	<!--	</div>-->
	<div class="form-group">
		<div class="row">
			<div class="col-md-12">
				<center><br><br>
					<button type="submit" style="background: #dd3333" class="btn btn-danger">Submit</button>
				</center>
				<br>
			</div>
		</div>
	</div>
</div>

