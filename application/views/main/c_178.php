<div class="" style="background-color:#4A235A">

	<div class="row">
		<div class="col-md-12">
			<center><h3 style="color:white">শিক্ষিত বেকার যুব বহুমুখী সমবায় সমিতি লিঃ</h3></center>
			<center><h3 style="color:white">কর্মসংস্থান ও দারিদ্র বিমোচন প্রকল্প</h3></center>
			<center><h5 style="color:white">নিবন্ধন নম্বর -১৩/চাঁদ /১২, তারিখ :০৯/০৭/২০১২ </h5></center>
			<center><h6 style="color:white">ঠিকানা: ঠাকুর বাজার ,শাহারাস্তি , চাঁদপুর। </h6></center>
			<br>


			<div style="background-color:#E59866">
				<div style="background-color:#F7DC6F">
					<div class="row">
						<div class="col-md-12">
							<div class="col-md-10">

							</div>
							<div class="col-md-2">
								<label style=" color:black"><b>সি - ১৭৮</b></label>
							</div>
						</div>
					</div>


					<?php echo "<br>"; ?>
					<center><h3 style="color:black"><b>৩০.০৬.২০২০ তারিখ পর্যন্ত প্রভিশন হিসাব :  </b></h3>
					</center>

					<?php echo "<br>"; ?>


				</div>
				<?php echo "<br>"; ?>
				<div class="form-group">
					<div class="row">
						<div class="col-md-12">
							<div class="col-md-12">
								<div class="col-md-12">
									<table class="table table-bordered">

										<thead>
										<tr style="background: white">
											<th style="color:black">হিসাব কোড </th>
											<th style="color:black">
												<center> বিবরন </center>
											</th>

											<th style="color:black">
												<center> নোট নং </center>
											</th>
											<th style="color:black">
												চলতি বছরের জের (টাকা)</th>
											<th style="color:black">
												<center>পূর্ববর্তী বছরের জের (টাকা)</center>
											</th>

											<th style="color:black">
												<center> চলতি বছরে বৃদ্ধি </center>
											</th>
										</tr>
										</thead>
										<tbody>
										<tr>
											<td style="color:black"><label>৩০২০০৩৫</label></td>
											<td style="color:black"><label>লাভ আদায় -</label> </td>
											<td style="color:black"><label></label></td>
											<td style="color:black"><label></label></td>
											<td style="color:black"><label></label></td>
											<td style="color:black"><label></label></td>
										</tr>

										<tr>
											<td style="color:black"><label>২০৩০২১৫</label></td>
											<td style="color:black"><label>
													<p>প্রভিশন হিসাব আমানত খাতের ৪০%:</p></label>
												<p>গত সনে -</p>
												<p>(+) হালে আমানতের উপর জমা -</p>
												<p>(+) প্রদানকৃত লাভ কর্তন -</p>
												<p>(-) আমানতের উপর লাভ প্রদান </p>
											</td>
											<td></td>
											<td style="color:black"><label></label></td>
											<td style="color:black"><label></label></td>
											<td></td>
										</tr>

										<tr>
											<td style="color:black"><label>২০৩০২১৫</label></td>
											<td style="color:black">
												<label>
													<p> প্রভিশন হিসাব বেতন খাতের ২৫% :</p>
													গত সনে -</p></label>
												<p> (+) হালে বেতন খাতে জমা -</p>
												<p> (-) বেতন, বোনাস ও বিনোদন ভাতা প্রদান - </p> </td>
											<td></td>
											<td style="color:black"><label></label></td>
											<td style="color:black"><label></label></td>
											<td></td>
										</tr>

										<tr>
											<td style="color:black"><label>২০৩০২১৫</label></td>
											<td style="color:black">
												<label>
													<p>প্রভিশন হিসাব আনুষাঙ্গিক খাতের ১০% :</p></label>
												<p>গত সনে -</p>
												<p>(+) হালে আনুষাঙ্গিক খাতে জমা -</p>
												<p>(+) মূল আনুষাঙ্গিক আদায় -</p>
												<p>(+) মূল আনুষাঙ্গিক ব্যয় -</p>
												<p>(-) আসবাব পত্রের অপচয় - </p> </td>
											<td></td>
											<td style="color:black"><label></label></td>
											<td style="color:black"><label></label></td>
											<td></td>
										</tr>

										<tr>
											<td style="color:black"><label>২০৩০২১৫</label></td>
											<td style="color:black">
												<label>
													<p>সাধারন বিভাগকে প্রদান ১০% :</p></label>

												<p>(+) হাল সনে জমা -</p>

												<p>(-) সাধারন বিভাগকে ফেরত -</p>
											</td>
											<td></td>
											<td style="color:black"><label></label></td>
											<td style="color:black"><label></label></td>
											<td></td>
										</tr>

										<tr>
											<td style="color:black"><label>২০৩০২১৫</label></td>
											<td style="color:black">
												<label>
													<p>প্রভিশন হিসাব পেনশন ও উন্নয়ন খাতের ০৫% :</p></label>

												<p>গত সনে -</p>
												<p>	(+) হাল সনে জমা -
												<p>	(-) হাল সনে প্রদান -</p>
											</td>
											<td></td>
											<td style="color:black"><label></label></td>
											<td style="color:black"><label></label></td>
											<td></td>
										</tr>

										<tr>
											<td style="color:black"><label>২০৩০২১৫</label></td>
											<td style="color:black">
												<label>
													<p>প্রভিশন হিসাব ঝুঁকি তহবিল খাতের ১০% :</p></label>

												<p>গত সনে -</p>
												<p>	(+) হাল সনে জমা -
												<p>	(-) হাল সনে প্রদান -</p>
											</td>
											<td></td>
											<td style="color:black"><label></label></td>
											<td style="color:black"><label></label></td>
											<td></td>
										</tr>

										<tr>
											<td style="color:black"><label>২০৩০২১৫</label></td>
											<td style="color:black">
												<label>
													<p class="text-right">প্রকল্পের প্রভিশনসমূহ =  </p></label>
											</td>
											<td></td>
											<td style="color:black"><label></label></td>
											<td style="color:black"><label></label></td>
											<td></td>
										</tr>

										</tbody>
									</table>
								</div>


							</div>


						</div>
					</div>
				</div>


				<?php echo "<br>"; ?>

			</div>


			<div style="background-color:#F7DC6F">

				<?php echo "<br>"; ?>
				<div class="form-group">
					<div class="row">

						<div class="col-md-12">


							<?php echo "<br><br><br><br>"; ?>

							<div class="col-md-4">

							</div>

							<div class="col-md-4">


							</div>

							<div class="col-md-4">
								<p style="color:black;">
									<input type="text" name="" value="সম্পাদক /ব্যবস্থাপক" class="form-control">
								<hr>
								</p>
								<center><label style="color:black;">
										<b>
											সম্পাদক /ব্যবস্থাপক
										</b>
									</label></center>
							</div>

						</div>
					</div>
				</div>
				<?php echo "<br>"; ?>

			</div>
		</div>
		<span><br></span>
	</div>
</div>




