<div class="" style="background-color:#4A235A">

	<div class="row">
		<div class="col-md-12">
			<center><h3 style="color:white">শিক্ষিত বেকার যুব বহুমুখী সমবায় সমিতি লিঃ</h3></center>
			<center><h3 style="color:white">কর্মসংস্থান ও দারিদ্র বিমোচন প্রকল্প</h3></center>
			<center><h5 style="color:white">নিবন্ধন নম্বর -১৩/চাঁদ /১২, তারিখ :০৯/০৭/২০১২ </h5></center>
			<center><h6 style="color:white">ঠিকানা: ঠাকুর বাজার ,শাহারাস্তি , চাঁদপুর। </h6></center>
			<br>


			<div style="background-color:#E59866">
				<div style="background-color:#F7DC6F">
					<div class="row">
						<div class="col-md-12">
							<div class="col-md-10">

							</div>
							<div class="col-md-2">
								<label style=" color:black"><b>সি - ১৭৯</b></label>
							</div>
						</div>
					</div>


					<?php echo "<br>"; ?>
					<center><h3 style="color:black"><b>৩০.০৬.২০২০ তারিখে " উদৃত পত্র " (স্থিতি পত্র) </b></h3>
					</center>

					<?php echo "<br>"; ?>


				</div>
				<?php echo "<br>"; ?>
				<div class="form-group">
					<div class="row">
						<div class="col-md-12">
							<div class="col-md-12">
								<div class="col-md-12">
									<div class="table-responsive">
										<table class="table table-bordered">

											<thead>
											<tr style="background: white">
												<th style="color:black" colspan="6">
													<center>মূলধন ও দেন</center>
												</th>
											</tr>
											<tr style="background: white">
												<th style="color:black">হিসাব কোড</th>
												<th style="color:black">
													<center> বিবরন</center>
												</th>

												<th style="color:black">
													<center> নোট নং</center>
												</th>
												<th style="color:black">
													চলতি বছরের জের (টাকা)
												</th>
												<th style="color:black">
													<center>পূর্ববর্তী বছরের জের (টাকা)</center>
												</th>

												<th style="color:black">
													<center> চলতি বছরে বৃদ্ধি</center>
												</th>
											</tr>
											</thead>
											<tbody>
											<tr>
												<td style="color:black"><label>২০১০১১১</label></td>
												<td style="color:black"><label><p>শেয়ার মূলধন :</p></label>

													<p> গত সনে -</p>
													<p>(+) চলতি সনের আদায় -</p>
													<p>(-) শেয়ারের মূল্য ফেরত - </p>
												</td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
											</tr>

											<tr>
												<td style="color:black"><label>২০৩০১১৫</label></td>
												<td style="color:black"><label><p>সঞ্চয় আমানত দেনা :</p></label>

													<p> গত সনে -</p>
													<p>(+) হাল সনে আদায় -</p>
													<p>(-) হাল সনে ফেরৎ -</p>
												</td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
											</tr>


											<tr>
												<td style="color:black"><label>৩০১০৩০১</label></td>
												<td style="color:black"><label><p>সঞ্চয় আমানতের লাভ দেনা :</p></label>

													<p> গত সনে -</p>
													<p>(+) হাল সনে আদায় -</p>
													<p>(-) হাল সনে ফেরৎ -</p>
												</td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
											</tr>

											<tr>
												<td style="color:black"><label>২০৩০১৩১</label></td>
												<td style="color:black"><label><p>আমানতের গ্রহণ দেনা :</p></label>

													<p> গত সনে -</p>
													<p>(+) হাল সনে আদায় -</p>
													<p>(-) হাল সনে ফেরৎ -</p>
												</td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
											</tr>

											<tr>
												<td style="color:black"><label>২০৩০১২৭</label></td>
												<td style="color:black"><label><p>এককালীন আমানত দেনা :</p></label>

													<p> গত সনে -</p>
													<p>(+) হাল সনে আদায় -</p>
													<p>(-) হাল সনে ফেরৎ -</p>
												</td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
											</tr>


											<tr>
												<td style="color:black"><label>৩০১০৩০২</label></td>
												<td style="color:black"><label><p>এককালীন আমানতের লাভ দেনা :</p></label>

													<p> গত সনে -</p>
													<p>(+) হাল সনে আদায় -</p>
													<p>(-) হাল সনে ফেরৎ -</p>
												</td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
											</tr>

											<tr>
												<td style="color:black"><label>১০৬০২০০</label></td>
												<td style="color:black"><label><p>হাওলাত গ্রহণ দেনা :</p></label>

													<p> গত সনে -</p>
													<p>(+) হাল সনে আদায় -</p>
													<p>(-) হাল সনে ফেরৎ -</p>
												</td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
											</tr>

											<tr>
												<td style="color:black"><label>২০২০১৫০</label></td>
												<td style="color:black"><label><p>ভবিষ্য় তহবিলের দেনা :</p></label>

													<p> গত সনে -</p>
													<p>(+) হাল সনে আদায় -</p>
													<p>(-) হাল সনে ফেরৎ -</p>
												</td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
											</tr>

											<tr>
												<td style="color:black"><label>৩০১০৩০৫</label></td>
												<td style="color:black"><label><p>ভবিষ্য় তহবিলের লাভ দেনা :</p></label>

													<p> গত সনে -</p>
													<p>(+) হাল সনে আদায় -</p>
													<p>(-) হাল সনে ফেরৎ -</p>
												</td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
											</tr>

											<tr>
												<td style="color:black"><label>২০৩০২১৫</label></td>
												<td style="color:black"><label><p>প্রভিশন হিসাব আমানত খাতের ৪০% :</p>
													</label>
												</td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
											</tr>

											<tr>
												<td style="color:black"><label>২০৩০২১৫</label></td>
												<td style="color:black"><label><p>প্রভিশন হিসাব বেতন খাতের ২৫% :</p>
													</label>
												</td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
											</tr>

											<tr>
												<td style="color:black"><label>২০৩০২১৫</label></td>
												<td style="color:black"><label><p>প্রভিশন হিসাব আনুষাঙ্গিক খাতের ১০%
															:</p></label>
												</td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
											</tr>

											<tr>
												<td style="color:black"><label>২০৩০২১৫</label></td>
												<td style="color:black"><label><p>প্রভিশন হিসাব পেনশন ও উন্নয়ন তহবিল ৫%
															:</p></label>
												</td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
											</tr>

											<tr>
												<td style="color:black"><label>২০৩০২১৫</label></td>
												<td style="color:black"><label><p>প্রভিশন হিসাব ঝুঁকি তহবিল খাতের ১০%
															:</p></label>
												</td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
											</tr>

											<tr>
												<td style="color:black"><label>২০৩০১৪৮</label></td>
												<td style="color:black"><label><p>অর্থ গ্রহণ সাধারন বিভাগ হতে </p>
													</label>
												</td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
											</tr>

											<tr>
												<td style="color:black"><label>২০৩০১৪৮</label></td>
												<td style="color:black"><label><p>অর্থ গ্রহণ কেন্দ্রীয় সমিতি হতে </p>
													</label>
												</td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
											</tr>

											<tr>
												<td style="color:black"><label>২০১০২১৬</label></td>
												<td style="color:black"><label><p>কু-ঋণ তহবিল </p></label>
												</td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
											</tr>

											<tr>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label><p>বাজেয়াপ্ত শেয়ার </p></label>
												</td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
											</tr>

											<tr>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label><p>বাজেয়াপ্ত সঞ্চয় ও আমানত </p></label>
												</td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
											</tr>

											<tr>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label><p>কেন্দ্রীয় সমিতি হতে ঋণ গ্রহণ </p>
													</label>
												</td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
											</tr>

											<tr>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label><p></p></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
											</tr>
											<tr>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label><p></p></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
											</tr>
											<tr>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label><p></p></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
											</tr>
											<tr>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label><p></p></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
											</tr>

											<tr>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label><p></p></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
											</tr>

											<tr>
												<td class="text-right" colspan="3" style="color:black"><label>সর্বমোট
														=</label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
											</tr>


											</tbody>
										</table>
									</div>
								</div>


							</div>


						</div>
					</div>
				</div>


				<?php echo "<br>"; ?>

			</div>


			<div style="background-color:#F7DC6F">

				<?php echo "<br>"; ?>
				<div class="form-group">
					<div class="row">

						<div class="col-md-12">


							<?php echo "<br><br><br><br>"; ?>

							<div class="col-md-4">

							</div>

							<div class="col-md-4">


							</div>

							<div class="col-md-4">
								<p style="color:black;">
									<input type="text" name="" value="সম্পাদক /ব্যবস্থাপক" class="form-control">
								<hr>
								</p>
								<center><label style="color:black;">
										<b>
											সম্পাদক /ব্যবস্থাপক
										</b>
									</label></center>
							</div>

						</div>
					</div>
				</div>
				<?php echo "<br>"; ?>

			</div>
		</div>
		<span><br></span>
	</div>
</div>




