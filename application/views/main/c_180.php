<div class="" style="background-color:#4A235A">

	<div class="row">
		<div class="col-md-12">
			<center><h3 style="color:white">শিক্ষিত বেকার যুব বহুমুখী সমবায় সমিতি লিঃ</h3></center>
			<center><h3 style="color:white">কর্মসংস্থান ও দারিদ্র বিমোচন প্রকল্প</h3></center>
			<center><h5 style="color:white">নিবন্ধন নম্বর -১৩/চাঁদ /১২, তারিখ :০৯/০৭/২০১২ </h5></center>
			<center><h6 style="color:white">ঠিকানা: ঠাকুর বাজার ,শাহারাস্তি , চাঁদপুর। </h6></center>
			<br>


			<div style="background-color:#E59866">
				<div style="background-color:#F7DC6F">
					<div class="row">
						<div class="col-md-12">
							<div class="col-md-10">

							</div>
							<div class="col-md-2">
								<label style=" color:black"><b>সি - ১৮০</b></label>
							</div>
						</div>
					</div>


					<?php echo "<br>"; ?>
					<center><h3 style="color:black"><b>৩০.০৬.২০২০ তারিখে " উদৃত পত্র " (স্থিতি পত্র) </b></h3>
					</center>

					<?php echo "<br>"; ?>


				</div>
				<?php echo "<br>"; ?>
				<div class="form-group">
					<div class="row">
						<div class="col-md-12">
							<div class="col-md-12">
								<div class="col-md-12">
									<div class="table-responsive">
										<table class="table table-bordered">

											<thead>
											<tr style="background: white">
												<th class="text-center" colspan="6" style="color:black">সম্পত্তি ও
													পাওনা
												</th>
											</tr>

											<tr style="background: white">
												<th style="color:black">হিসাব কোড</th>
												<th style="color:black">
													<center> বিবরন</center>
												</th>

												<th style="color:black">
													<center> নোট নং</center>
												</th>
												<th style="color:black">
													চলতি বছরের জের (টাকা)
												</th>
												<th style="color:black">
													<center>পূর্ববর্তী বছরের জের (টাকা)</center>
												</th>

												<th style="color:black">
													<center> চলতি বছরে বৃদ্ধি</center>
												</th>
											</tr>
											</thead>
											<tbody>
											<tr>
												<td style="color:black"><label>১০১০৭০০</label></td>
												<td style="color:black"><label><p>আসবাব পত্র :</p></label>

													<p> গত সনে -</p><br/>
													<p>(+) হালে ক্রয় -</p><br/>
													<p>(-) হালে বিক্রয় - </p><br/>
													<p>(-) হালে অপচয় - </p>
												</td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
											</tr>

											<tr>
												<td style="color:black"><label>১০৫০০০০ </label></td>
												<td style="color:black"><label><p>বিনিয়োগের মূলধন পাওনা :</p></label>

													<p> গত সনে -</p><br/>
													<p>(+) হালে প্রদান -</p><br/>
													<p>(-) হালে আদায় - </p>
												</td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
											</tr>

											<tr>
												<td style="color:black"><label>১০৭০২০০ </label></td>
												<td style="color:black"><label><p>হাওলাতি পাওনা :</p></label>

													<p> গত সনে -</p><br/>
													<p>(+) হালে প্রদান -</p><br/>
													<p>(-) হালে আদায় - </p>
												</td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
											</tr>

											<tr>
												<td style="color:black"><label>১০৭০৫০০ </label></td>
												<td style="color:black"><label><p>অগ্রিম অফিস ভাড়া :</p></label>
													<p>(+) হালে প্রদান -</p><br/>
													<p>(-) হালে প্রদান -</p><br/>
													<p>(-) হালে আদায় - </p>
												</td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
											</tr>

											<tr>
												<td style="color:black"><label>১০৭০৮০০ </label></td>
												<td style="color:black"><label><p>হস্ত মজুদ :</p></label>
												</td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
											</tr>

											<tr>
												<td style="color:black"><label>১০৭০৯০০ </label></td>
												<td style="color:black"><label><p>ব্যাংকে মজুদ :</p></label>
													<p>গত সনে</p><br/>
													<p>(+) হালে জমা -</p><br/>
													<p>(-) হালে উত্তোলন -</p>
												</td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
											</tr>

											<tr>
												<td style="color:black"><label>১০৫০৪১১ </label></td>
												<td style="color:black"><label><p>আমানত গ্রহন হিসাবে জমা (চান্দ্রা
															সমিতি):</p></label>
												</td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
											</tr>

											<tr>
												<td style="color:black"><label>১০৫০৪১১ </label></td>
												<td style="color:black"><label><p>আমানত গ্রহন হিসাবে জমা (কেন্দ্রীয়
															সমিতি):</p></label>
													<p>গত সনে</p><br/>
													<p>(+) হালে জমা -</p><br/>
													<p>(-) হালে উত্তোলন -</p>
												</td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
											</tr>

											<tr>
												<td style="color:black"><label>১০৫০০০০ </label></td>
												<td style="color:black"><label><p>স্বনির্ভর প্রকল্পের বিনিয়োগ মূলধন
															:</p></label>
													<p>গত সনে</p><br/>
													<p>(+) হালে প্রদান -</p><br/>
													<p>(-) হালে আদায় -</p>
												</td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
											</tr>

											<tr>
												<td style="color:black"><label>১০৫০৪১১ </label></td>
												<td style="color:black"><label><p>এফডিআর হিসাবে জমা (কেন্দ্রীয় সমিতি)
															:</p></label>
													<p>গত সনে</p><br/>
													<p>(+) হালে জমা -</p><br/>
													<p>(-) হালে উত্তোলন -</p>
												</td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
											</tr>

											<tr>
												<td style="color:black"><label>১০৫০৪১১ </label></td>
												<td style="color:black"><label><p>স্থায়ী আমানত জমা কেন্দ্রীয় সমিতিতে
															:</p></label>
													<p>গত সনে</p><br/>
													<p>(+) হালে প্রদান -</p><br/>
													<p>(-) হালে আদায় -</p>
												</td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
											</tr>

											<tr>
												<td style="color:black"><label>১০৫০৪১১ </label></td>
												<td style="color:black"><label><p>অস্থায়ী আমানত জমা কেন্দ্রীয় সমিতিতে
															:</p></label>
													<p>গত সনে</p><br/>
													<p>(+) হালে প্রদান -</p><br/>
													<p>(-) হালে আদায় -</p>
												</td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
											</tr>

											<tr>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label><p>যানবাহন বাবদ পাওনা :</p></label>
												</td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
											</tr>

											<tr>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label><p>নগদ মজুদ তহবিল হিসাবে কেন্দ্রীয়
															সমিতিতে জমা :</p></label>
												</td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
											</tr>

											<tr>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label></label>
												</td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
											</tr>

											<tr>
												<td class="text-right" colspan="2" style="color:black"><label>
														সর্বমোট = </label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
											</tr>

											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php echo "<br>"; ?>
			</div>
			<div style="background-color:#F7DC6F">
				<?php echo "<br>"; ?>
				<div class="form-group">
					<div class="row">
						<div class="col-md-12">
							<?php echo "<br><br><br><br>"; ?>
							<div class="col-md-4">
							</div>
							<div class="col-md-4">
							</div>
							<div class="col-md-4">
								<p style="color:black;">
									<input type="text" name="" value="সম্পাদক /ব্যবস্থাপক" class="form-control">
								<hr>
								</p>
								<center><label style="color:black;">
										<b>
											সম্পাদক /ব্যবস্থাপক
										</b>
									</label></center>
							</div>
						</div>
					</div>
				</div>
				<?php echo "<br>"; ?>
			</div>
		</div>
		<span><br></span>
	</div>
</div>




