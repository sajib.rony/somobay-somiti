<style>
	p {
		font-family: Roboto;
		font-size: medium;
		color: black;
		text-align: justify;
	}

	h4 {
		font-family: Roboto;

		color: black;
	}

	.div-padding1 {

		padding-top: 10px;
		padding-right: 30px;
		padding-bottom: 10px;
		padding-left: 30px;
	}

	.div-padding2 {

		padding-top: 10px;
		padding-right: 30px;
		padding-bottom: 10px;
		padding-left: 30px;
	}

	.input-field1 {
		width: 25%;
		padding: 5px 20px;
		margin: 8px 0;
		display: inline-block;
		border: 1px solid #ccc;
		border-radius: 4px;
		box-sizing: border-box;
	}

	.text_color_th {
		color: white;
	}
</style>

<div class="" style="background-color:#4A235A">
	<div class="row">
		<div class="col-md-12">
			<center><h3 style="color:white">শিক্ষিত বেকার কেন্দ্রীয় সঞ্চয় ও ঋণদান সমবায় সমিতি লিঃ</h3></center>
			<center><h3 style="color:white">কর্মসংস্থান ও দারিদ্র বিমোচন প্রকল্প</h3></center>
			<center><h5 style="color:white">নিবন্ধন নম্বর -১৩/চাঁদ /১২, তারিখ :০৯/০৭/২০১২ </h5></center>
			<center><h6 style="color:white">ঠিকানা: ঠাকুর বাজার ,শাহারাস্তি , চাঁদপুর। </h6></center>
			<br>
		</div>
	</div>
</div>
<div class="" style="background-color:white">
	<div class="div-padding1">
		<div class="form-group">
			<div class="row">
				<div class="col-md-6">

				</div>
				<div class="col-md-6 text-right">
					<h5 style="color:black"> সি- ১৮৬ </h5>
				</div>

			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-12 text-center">
					<h3 style="color:black"><input type="date" name="" class="input-field1"> তারিখে কর্মকর্তা/কর্মচারীগনের
						তালিকা ও তথ্যাবলী </h3>
				</div>
				<div class="col-md-6">

				</div>
				<div class="col-md-6 text-right">

				</div>
			</div>
		</div>
	</div>
	<div class="div-padding2">
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<div class="table-responsive">
						<table class="table-responsive table table-bordered">

							<tr style="background: #dd3333">
								<th class="text-center text_color_th" rowspan="2">নং</th>
								<th class="text-center text_color_th" rowspan="2"> কর্মকর্তা কর্মচারীর নাম</th>
								<th class="text-center text_color_th" rowspan="2">চাকরীতে যোগদানের পদবী</th>
								<th class="text-center text_color_th" rowspan="2">চাকরীতে যোগদানের তারিখ</th>
								<th class="text-center text_color_th" rowspan="2">বর্তমান পদবী</th>
								<th class="text-center text_color_th" colspan="6">মাসিক সম্মানী/বেতনভাতা</th>

							</tr>
							<tr style="background: #dd3333">
								<th class="text-center text_color_th">মুলবেতন</th>
								<th class="text-center text_color_th">বাড়ি ভাড়া</th>
								<th class="text-center text_color_th">চিকিসা ভাতা</th>
								<th class="text-center text_color_th">পরিবহন ও যাতায়াত ভাতা</th>
								<th class="text-center text_color_th">টিফিন ও লাঞ্চ ভাতা</th>
								<th class="text-center text_color_th">সর্বমোট</th>
							</tr>
							<tr style="background: #dd3333">
								<?php for ($i = 1;
										   $i <= 11;
										   $i++) { ?>
									<th class="text-center text_color_th"><?php echo $i; ?></th>
								<?php } ?>
							</tr>
							<?php for ($i = 1;
									   $i <= 30;
									   $i++) { ?>
								<tr>
									<td><p><?php echo $i; ?></p></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
								</tr>
							<?php } ?>
							<tr>
								<td colspan="5"><p style="text-align: right">মোট =</p></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="div-padding1">
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-4">
						<hr>
						<p style="text-align: center">প্রস্তুতকারী</p>
					</div>
					<div class="col-md-4">

					</div>
					<div class="col-md-4">
						<hr>
						<p style="text-align: center">ব্যবস্থাপক</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--	<div class="form-group">-->
	<!--		<div class="row">-->
	<!--			<div class="col-md-12">-->
	<!--				<center><br><br>-->
	<!--					<button type="submit" style="background: #dd3333" class="btn btn-danger">Submit</button>-->
	<!--				</center>-->
	<!--				<br>-->
	<!--			</div>-->
	<!--		</div>-->
	<!--	</div>-->
</div>

