<div class="" style="background-color:#4A235A">

	<div class="row">
		<div class="col-md-12">
			<center><h3 style="color:white">শিক্ষিত বেকার যুব বহুমুখী সমবায় সমিতি লিঃ</h3></center>
			<center><h3 style="color:white">কর্মসংস্থান ও দারিদ্র বিমোচন প্রকল্প</h3></center>
			<center><h5 style="color:white">নিবন্ধন নম্বর -১৩/চাঁদ /১২, তারিখ :০৯/০৭/২০১২ </h5></center>
			<center><h6 style="color:white">ঠিকানা: ঠাকুর বাজার ,শাহারাস্তি , চাঁদপুর। </h6></center>
			<br>


			<div style="background-color:#E59866">
				<div style="background-color:#F7DC6F">
					<div class="row">
						<div class="col-md-12">
							<div class="col-md-10">

							</div>
							<div class="col-md-2">
								<label style=" color:black"><b>সি -২২</b></label>
							</div>
						</div>
					</div>


					<?php echo "<br>"; ?>
					<center><h3 style="color:black"><b>সমিতির মাধ্যমে কর্মসংস্তান ও উৎপাদন তথ্য প্রতিবেদন </b></h3>
					</center>
					<?php echo "<br>"; ?>
					<div class="row">
						<div class="col-md-12">
							<div class="col-md-6">
								<label style=" color:black"><b>মাসের নাম
											:<?php echo date('M/yy') ?> &nbsp;&nbsp;&nbsp;</b></label>

							</div>
							<div class="col-md-6">
								<p style="text-align: right"><label style=" color:black"><b>তারিখ
											:<?php echo date('M/yy') ?> &nbsp;&nbsp;&nbsp;</b></label>
								</p>
							</div>


						</div>
					</div>
					<?php echo "<br>"; ?>


				</div>
				<?php echo "<br>"; ?>
				<div class="form-group">
					<div class="row">
						<div class="col-md-12">


							<div class="col-md-12">

								<div class="table-responsive">
									<table class="table table-bordered">

										<tbody>

										<tr style="background: white">
											<th style="color:black" rowspan="2">নং</th>
											<th style="color:black">
												<center> বিনিয়োগ গ্রহণকারীর নাম </center>
											</th>
											<th style="color:black">
												<center> সদস্য নং </center>
											</th>

											<th style="color:black">
												<center>বিনিয়োগ হিসাব নং</center>
											</th>
											<th style="color:black">
												<center>যেভাবে কর্মসংস্তান হয়েছে তার ধরণ </center>
											</th>
											<th style="color:black">
												<center>কর্মসংস্তানের পরিমান </center>
											</th>
											<th style="color:black">
												<center> উৎপাদনের ধরণ </center>
											</th>
											<th style="color:black">
												<center>সর্বশেষ বিনিয়োগ গ্রহণকৃত টাকার পরিমান </center>
											</th>
											<th style="color:black">
												<center>কতবার বিনিয়োগ গ্রহণ করেছে </center>
											</th>
											<th style="color:black">
												<center>১ম বিনিয়োগ গ্রহণকৃত টাকার পরিমান </center>
											</th>
											<th style="color:black">
												<center>সর্বমোট  বিনিয়োগ গ্রহনের পরিমান </center>
											</th>
											<th style="color:black">
												<center>বিনিয়োগ গ্রহীতা স্বাক্ষাৎকার ও ভিডিও ধারনে আগ্রহী কিনা , আগ্রহী হলে তার স্বাক্ষর ও মতামত </center>
											</th>
											<th style="color:black">
												<center>মন্তব্য</center>
											</th>
										</tr>
										</tbody>

										<tbody>

										<tr style="background: white">
											<th style="color:black">
												<center> ১</center>
											</th>
											<th style="color:black">
												<center> ২</center>
											</th>
											<th style="color:black">
												<center>৩</center>
											</th>
											<th style="color:black">
												<center>৪</center>
											</th>
											<th style="color:black">
												<center>৫</center>
											</th>
											<th style="color:black">
												<center>৬</center>
											</th>
											<th style="color:black">
												<center>৭</center>
											</th>
											<th style="color:black">
												<center>৮</center>
											</th>
											<th style="color:black">
												<center>৯</center>
											</th>
											<th style="color:black">
												<center> ১০</center>
											</th>
											<th style="color:black">
												<center> ১১</center>
											</th>
											<th style="color:black">
												<center> ১২</center>
											</th>
											<th style="color:black">
												<center>১৩</center>
											</th>


										</tr>
										</tbody>

										<?php
										$i = '';
										$sum = 0;
										for ($i = 1; $i <= 30; $i++) { ?>
											<tbody>
											<tr>
												<td style="color:black"><label><?php echo $i; ?></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black">
													<label> </label>
												</td>
												<td style="color:black">
													<label> </label>
												</td>
												<td style="color:black">
													<label></label>
												</td>
												<td style="color:black">
													<label></label>
												</td>
												<td style="color:black">
													<label></label>
												</td>
												<td style="color:black">
													<label></label>
												</td>
												<td style="color:black">
													<label></label>
												</td>
												<td style="color:black">
													<label></label>
												</td>
												<td style="color:black">
													<label></label>
												</td>
												<td style="color:black">
													<label></label>
												</td>

												<td style="color:black"><label></label></td>


											</tr>
											</tbody>


											<?php

										} ?>

									</table>
								</div>
							</div>


						</div>
					</div>
				</div>


				<?php echo "<br>"; ?>

			</div>


			<div style="background-color:#F7DC6F">

				<?php echo "<br>"; ?>
				<div class="form-group">
					<div class="row">

						<div class="col-md-12">


							<?php echo "<br><br><br><br>"; ?>

							<div class="col-md-4">


							</div>


							<div class="col-md-4">

							</div>
							<div class="col-md-4">
								<p style="color:black;">
									<input type="text" name="" value="সম্পাদক /ব্যবস্থাপক" class="form-control">
								<hr>
								</p>
								<center><label style="color:black;">
										<b>
											ব্যবস্থাপক
										</b>
									</label></center>

							</div>

						</div>
					</div>
				</div>
				<?php echo "<br>"; ?>

			</div>
		</div>
		<span><br></span>
	</div>
</div>




