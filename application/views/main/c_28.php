<div class="" style="background-color:#4A235A">

	<div class="row">
		<div class="col-md-12">
			<center><h3 style="color:white">শিক্ষিত বেকার যুব বহুমুখী সমবায় সমিতি লিঃ</h3></center>
			<center><h3 style="color:white">কর্মসংস্থান ও দারিদ্র বিমোচন প্রকল্প</h3></center>
			<center><h5 style="color:white">নিবন্ধন নম্বর -১৩/চাঁদ /১২, তারিখ :০৯/০৭/২০১২ </h5></center>
			<center><h6 style="color:white">ঠিকানা: ঠাকুর বাজার ,শাহারাস্তি , চাঁদপুর। </h6></center>
			<br>


			<div style="background-color:#E59866">
				<div style="background-color:#F7DC6F">
					<div class="row">
						<div class="col-md-12">
							<div class="col-md-10">

							</div>
							<div class="col-md-2">
								<label style=" color:black"><b>সি - ২৮</b></label>
							</div>
						</div>
					</div>


					<?php echo "<br>"; ?>
					<center><h3 style="color:black"><b>মাসিক রাজস্ব হিসাব </b></h3>
					</center>

					<?php echo "<br>"; ?>


				</div>
				<?php echo "<br>"; ?>
				<div class="form-group">
					<div class="row">
						<div class="col-md-12">
							<div class="col-md-12">
								<div class="col-md-12">
									<div class="table-responsive">
									<table class="table table-bordered">

										<thead>
										<tr style="background: white">
											<th colspan="4" class="text-center" style="color:black"> আয় </th>
											<th colspan="4" class="text-center"  style="color:black"> ব্যয় </th>
										</tr>
										</thead>
										<tbody>

										<tr style="background: white">
											<th style="color:black"> নং</th>
											<th colspan="2" style="color:black">
												<center> বিবরণ</center>
											</th>

											<th style="color:black">
												<center>মোট টাকা</center>
											</th>
											<th style="color:black"> নং</th>
											<th colspan="2" style="color:black">
												<center> বিবরণ</center>
											</th>

											<th style="color:black">
												<center>মোট টাকা</center>
											</th>


										</tr>

										<tr>
											<td rowspan="8" style="color:black"><label>১</label></td>
											<td class="text-center" colspan="2" style="color:black"><label> লাভ আদায় </label> </td>
											<td style="color:black"><label></label></td>
											<td style="color:black"><label>১</label></td>
											<td colspan="2" style="color:black"><label>আদায় লেভার ১০% সাধারণ বিভাগকে প্রদান</label></td>
											<td style="color:black"><label></label></td>

										</tr>

										<tr>

											<td style="color:black"><label> বিনিয়োগ হতে (কর্মসংস্থান প্রকল্প) </label> </td>
											<td style="color:black"><label></label></td>
											<td style="color:black"><label></label></td>
											<td rowspan="5" style="color:black"><label>২</label></td>
											<td class="text-center" colspan="2" style="color:black"><label>প্রভিশন আমানত খাত</label></td>
											<td style="color:black"><label></label></td>

										</tr>

										<tr>
											<td style="color:black"><label> বিনিয়োগ হতে (স্বনির্ভর প্রকল্প) </label> </td>
											<td style="color:black"><label></label></td>
											<td style="color:black"><label></label></td>

											<td style="color:black"><label>শেয়ারের লাভ্যাংশ ফেরৎ </label></td>
											<td style="color:black"><label></label></td>
											<td style="color:black"><label></label></td>
										</tr>

										<tr>
											<td style="color:black"><label> আমানত হতে  </label> </td>
											<td style="color:black"><label></label></td>
											<td style="color:black"><label></label></td>

											<td style="color:black"><label>আমানতের উপর লাভ প্রদান </label></td>
											<td style="color:black"><label></label></td>
											<td style="color:black"><label></label></td>

										</tr>

										<tr>
											<td style="color:black"><label> এফডিআর হতে   </label> </td>
											<td style="color:black"><label></label></td>
											<td style="color:black"><label></label></td>

											<td style="color:black"><label>অর্থ গ্রহণের লাভ ফেরত </label></td>
											<td style="color:black"><label></label></td>
											<td style="color:black"><label></label></td>

										</tr>

										<tr>
											<td style="color:black"><label> ব্যাংক  হতে    </label> </td>
											<td style="color:black"><label></label></td>
											<td style="color:black"><label></label></td>

											<td style="color:black"><label>আমানত গ্রহণের লাভ ফেরত </label></td>
											<td style="color:black"><label></label></td>
											<td style="color:black"><label></label></td>

										</tr>

										<tr>
											<td style="color:black"><label> অর্থ গ্রহণ হতে  </label> </td>
											<td style="color:black"><label></label></td>
											<td style="color:black"><label></label></td>
											<td rowspan="47" style="color:black"><label>৩</label></td>

											<td class="text-center" colspan="2" style="color:black"><label>প্রভিশন অনুসাঙ্গিক খাত </label></td>
											<td style="color:black"><label></label></td>

										</tr>

										<tr>
											<td style="color:black"><label>   </label> </td>
											<td style="color:black"><label></label></td>
											<td style="color:black"><label></label></td>
											<td style="color:black"><label>মাসিক সভা ব্যায় </label></td>
											<td style="color:black"><label> </label></td>
											<td style="color:black"><label></label></td>

										</tr>

										<tr>
											<td rowspan="5" style="color:black"><label> ২  </label> </td>
											<td style="color:black"><label>প্রভিশন অনুসাঙ্গিক খাত </label></td>
											<td style="color:black"><label></label></td>
											<td style="color:black"><label></label></td>
											<td style="color:black"><label>অতিথি আপ্যায়ন </label></td>
											<td style="color:black"><label></label></td>
											<td style="color:black"><label></label></td>

										</tr>

										<tr>
											<td style="color:black"><label>প্রদানকৃত লাভ কর্তন  </label></td>
											<td  style="color:black"><label></label></td>
											<td  style="color:black"><label></label></td>
											<td style="color:black"><label>সদস্য আপ্যায়ন  </label></td>
											<td style="color:black"><label></label></td>
											<td style="color:black"><label></label></td>

										</tr>

										<tr>
											<td style="color:black"><label>  </label></td>
											<td  style="color:black"><label></label></td>
											<td  style="color:black"><label></label></td>
											<td style="color:black"><label>অফিস ভাড়া প্রদান   </label></td>
											<td style="color:black"><label></label></td>
											<td style="color:black"><label></label></td>

										</tr>

										<tr>
											<td style="color:black"><label>  </label></td>
											<td  style="color:black"><label></label></td>
											<td  style="color:black"><label></label></td>
											<td style="color:black"><label>মনোহরি ক্রয়    </label></td>
											<td style="color:black"><label></label></td>
											<td style="color:black"><label></label></td>

										</tr>

										<tr>
											<td style="color:black"><label>  </label></td>
											<td  style="color:black"><label></label></td>
											<td  style="color:black"><label></label></td>
											<td style="color:black"><label>বিদ্যুৎ, ডিস, পানি ও গ্যাস বিল </label></td>
											<td style="color:black"><label></label></td>
											<td style="color:black"><label></label></td>

										</tr>

										<tr>
											<td rowspan="48" style="color:black"><label> ৩ </label></td>
											<td style="color:black"><label> প্রভিশন আনুষাঙ্গিক খাত  </label></td>
											<td style="color:black"><label></label></td>
											<td style="color:black"><label></label></td>
											<td style="color:black"><label>স্ট্যাম্প ও রেভিনিও ক্রয়  </label></td>
											<td style="color:black"><label></label></td>
											<td style="color:black"><label></label></td>

										</tr>

										<tr>
											<td style="color:black"><label> ভর্তি ফি   </label></td>
											<td  style="color:black"><label></label></td>
											<td  style="color:black"><label></label></td>
											<td style="color:black"><label>মোবাইল ও ফোন বিল   </label></td>
											<td style="color:black"><label></label></td>
											<td style="color:black"><label></label></td>
										</tr>

										<tr>
											<td style="color:black"><label> বিলম্ব মাশুল বিনিয়োগ হতে (কর্মসংস্থান প্র.) </label></td>
											<td style="color:black"><label></label></td>
											<td style="color:black"><label></label></td>
											<td style="color:black"><label>ডাক ও যোগাযোগ ব্যয়  </label></td>
											<td style="color:black"><label></label></td>
											<td style="color:black"><label></label></td>
										</tr>
										<tr>
											<td style="color:black"><label> বিলম্ব মাশুল বিনিয়োগ হতে (স্বনির্ভর প্র.) </label></td>
											<td style="color:black"><label></label></td>
											<td style="color:black"><label></label></td>
											<td style="color:black"><label>ভ্রমণ ও যাতায়াত ব্যয়   </label></td>
											<td style="color:black"><label></label></td>
											<td style="color:black"><label></label></td>
										</tr>

										<tr>
											<td style="color:black"><label> বিনিয়োগ ফর্ম ও স্ট্যাম্প বিক্রয়  </label></td>
											<td  style="color:black"><label></label></td>
											<td  style="color:black"><label></label></td>
											<td style="color:black"><label>সংবাদপত্র  বিল   </label></td>
											<td style="color:black"><label></label></td>
											<td style="color:black"><label></label></td>
										</tr>

										<tr>
											<td style="color:black"><label> বিলম্ব মাশুল সঞ্চয় আমানত হতে   </label></td>
											<td  style="color:black"><label></label></td>
											<td  style="color:black"><label></label></td>
											<td style="color:black"><label>যানবাহনের রক্ষনাবেক্ষন ব্যয়  </label></td>
											<td style="color:black"><label></label></td>
											<td style="color:black"><label></label></td>
										</tr>

										<tr>
											<td style="color:black"><label> সদস্যপত্র প্রত্যাহার ফি (সর্ভিস ফি)  </label></td>
											<td  style="color:black"><label></label></td>
											<td  style="color:black"><label></label></td>
											<td style="color:black"><label>ফটোকপি / ছাপা ব্যয়   </label></td>
											<td style="color:black"><label></label></td>
											<td style="color:black"><label></label></td>
										</tr>

										<tr>
											<td style="color:black"><label> অফিসের নস্ট / অন্য কোনো দ্রব্য বিক্রয় </label></td>
											<td  style="color:black"><label></label></td>
											<td  style="color:black"><label></label></td>
											<td style="color:black"><label>পরিদর্শন কালীন সম্মানী ও আপ্যায়ন ব্যয়   </label></td>
											<td style="color:black"><label></label></td>
											<td style="color:black"><label></label></td>
										</tr>

										<tr>
											<td style="color:black"><label> অপ্রত্যাশিত আয়  </label></td>
											<td  style="color:black"><label></label></td>
											<td  style="color:black"><label></label></td>
											<td style="color:black"><label>ব্যাবস্থাপনা কমিটি ও প্রকল্প কমিটির সম্মানী   </label></td>
											<td style="color:black"><label></label></td>
											<td style="color:black"><label></label></td>
										</tr>

										<tr>
											<td style="color:black"><label> বিনিয়োগ কিস্তিহতে যা: বাবদ আদায়   </label></td>
											<td  style="color:black"><label></label></td>
											<td  style="color:black"><label></label></td>
											<td style="color:black"><label>বিজ্ঞাপন ও প্রচার  </label></td>
											<td style="color:black"><label></label></td>
											<td style="color:black"><label></label></td>
										</tr>

										<tr>
											<td style="color:black"><label>   </label></td>
											<td  style="color:black"><label></label></td>
											<td  style="color:black"><label></label></td>
											<td style="color:black"><label>ক্যালেন্ডার ক্রয়  </label></td>
											<td style="color:black"><label></label></td>
											<td style="color:black"><label></label></td>
										</tr>

										<tr>
											<td style="color:black"><label>   </label></td>
											<td  style="color:black"><label></label></td>
											<td  style="color:black"><label></label></td>
											<td style="color:black"><label>ক্যালেন্ডার ক্রয়  </label></td>
											<td style="color:black"><label></label></td>
											<td style="color:black"><label></label></td>
										</tr>

										<tr>
											<td style="color:black"><label>   </label></td>
											<td  style="color:black"><label></label></td>
											<td  style="color:black"><label></label></td>
											<td style="color:black"><label>অনুদান প্রদান </label></td>
											<td style="color:black"><label></label></td>
											<td style="color:black"><label></label></td>
										</tr>

										<tr>
											<td style="color:black"><label>   </label></td>
											<td  style="color:black"><label></label></td>
											<td  style="color:black"><label></label></td>
											<td style="color:black"><label>কম্পিউটার সার্ভিসিং  </label></td>
											<td style="color:black"><label></label></td>
											<td style="color:black"><label></label></td>
										</tr>

										<tr>
											<td style="color:black"><label>   </label></td>
											<td  style="color:black"><label></label></td>
											<td  style="color:black"><label></label></td>
											<td style="color:black"><label>খেলাপি ও বকেয়া বিনিয়োগ আদায় ব্যয় </label></td>
											<td style="color:black"><label></label></td>
											<td style="color:black"><label></label></td>
										</tr>

										<tr>
											<td style="color:black"><label>   </label></td>
											<td  style="color:black"><label></label></td>
											<td  style="color:black"><label></label></td>
											<td style="color:black"><label>অডিট কালীন ব্যয়  </label></td>
											<td style="color:black"><label></label></td>
											<td style="color:black"><label></label></td>
										</tr>

										<tr>
											<td style="color:black"><label>   </label></td>
											<td  style="color:black"><label></label></td>
											<td  style="color:black"><label></label></td>
											<td style="color:black"><label>রাত্রি কালীন গার্ড   </label></td>
											<td style="color:black"><label></label></td>
											<td style="color:black"><label></label></td>
										</tr>

										<tr>
											<td style="color:black"><label>   </label></td>
											<td  style="color:black"><label></label></td>
											<td  style="color:black"><label></label></td>
											<td style="color:black"><label>সাইনবোর্ড ক্রয় / মেরামত ব্যয়   </label></td>
											<td style="color:black"><label></label></td>
											<td style="color:black"><label></label></td>
										</tr>

										<tr>
											<td colspan="2" style="color:black"><label>   </label></td>
											<td  style="color:black"><label></label></td>
											<td style="color:black"><label>মতবিনিময় সভা ব্যয়  </label></td>
											<td style="color:black"><label></label></td>
											<td style="color:black"><label></label></td>
										</tr>

										<tr>
											<td colspan="2" style="color:black"><label>   </label></td>
											<td  style="color:black"><label></label></td>
											<td style="color:black"><label>ব্যাবস্থাপনা কমিটি আপ্পায়ন ব্যয়   </label></td>
											<td style="color:black"><label></label></td>
											<td style="color:black"><label></label></td>
										</tr>

										<tr>
											<td colspan="2" style="color:black"><label>   </label></td>
											<td  style="color:black"><label></label></td>
											<td style="color:black"><label>বিদ্যুৎ / টিএনটি লাইন সংযোগ ব্যয়    </label></td>
											<td style="color:black"><label></label></td>
											<td style="color:black"><label></label></td>
										</tr>

										<tr>
											<td colspan="2" style="color:black"><label>   </label></td>
											<td  style="color:black"><label></label></td>
											<td style="color:black"><label>ব্যাংক চার্জ কর্তন  </label></td>
											<td style="color:black"><label></label></td>
											<td style="color:black"><label></label></td>
										</tr>

										<tr>
											<td colspan="2" style="color:black"><label>   </label></td>
											<td  style="color:black"><label></label></td>
											<td style="color:black"><label>জাতীয় দিবস / অন্যান্য দিবসে ব্যয় </label></td>
											<td style="color:black"><label></label></td>
											<td style="color:black"><label></label></td>
										</tr>
										<tr>
											<td colspan="2" style="color:black"><label>   </label></td>
											<td  style="color:black"><label></label></td>
											<td style="color:black"><label>জাতীয় দিবস / অন্যান্য দিবসে ব্যয় </label></td>
											<td style="color:black"><label></label></td>
											<td style="color:black"><label></label></td>
										</tr>

										<tr>
											<td colspan="2" style="color:black"><label>   </label></td>
											<td  style="color:black"><label></label></td>
											<td style="color:black"><label>অতিরিক্ত কাজের সম্মানী প্রদান </label></td>
											<td style="color:black"><label></label></td>
											<td style="color:black"><label></label></td>
										</tr>

										<tr>
											<td colspan="2" style="color:black"><label>   </label></td>
											<td  style="color:black"><label></label></td>
											<td style="color:black"><label>বিনিয়োগের বকেয়া আদায়ের মামলা ব্যয় </label></td>
											<td style="color:black"><label></label></td>
											<td style="color:black"><label></label></td>
										</tr>

										<tr>
											<td colspan="2" style="color:black"><label>   </label></td>
											<td  style="color:black"><label></label></td>
											<td style="color:black"><label>রমজান মাসের ইফতারী বা অন্নান্য ব্যয়  </label></td>
											<td style="color:black"><label></label></td>
											<td style="color:black"><label></label></td>
										</tr>

										<tr>
											<td colspan="2" style="color:black"><label>   </label></td>
											<td  style="color:black"><label></label></td>
											<td style="color:black"><label>প্রশিক্ষণ ও অভ্যান্তরীন নিরীক্ষা বাবদ প্রদান   </label></td>
											<td style="color:black"><label></label></td>
											<td style="color:black"><label></label></td>
										</tr>

										<tr>
											<td colspan="2" style="color:black"><label>   </label></td>
											<td  style="color:black"><label></label></td>
											<td style="color:black"><label>সদস্য ভর্তির সম্মানী    </label></td>
											<td style="color:black"><label></label></td>
											<td style="color:black"><label></label></td>
										</tr>

										<tr>
											<td colspan="2" style="color:black"><label>   </label></td>
											<td  style="color:black"><label></label></td>
											<td style="color:black"><label>আনুসাঙ্গিক ভাতা প্রদান  </label></td>
											<td style="color:black"><label></label></td>
											<td style="color:black"><label></label></td>
										</tr>

										<tr>
											<td colspan="2" style="color:black"><label>   </label></td>
											<td  style="color:black"><label></label></td>
											<td style="color:black"><label>বনভোজন ব্যয়  </label></td>
											<td style="color:black"><label></label></td>
											<td style="color:black"><label></label></td>

										<tr>
											<td colspan="2" style="color:black"><label>   </label></td>
											<td  style="color:black"><label></label></td>
											<td style="color:black"><label>নিয়োগ / পদোন্নতি বাবদ ব্যয়   </label></td>
											<td style="color:black"><label></label></td>
											<td style="color:black"><label></label></td>
										</tr>

										<tr>
											<td colspan="2" style="color:black"><label>   </label></td>
											<td  style="color:black"><label></label></td>
											<td style="color:black"><label>বিদেশ ভ্রমণ / প্রশিক্ষণ ব্যয়   </label></td>
											<td style="color:black"><label></label></td>
											<td style="color:black"><label></label></td>
										</tr>

										<tr>
											<td colspan="2" style="color:black"><label>   </label></td>
											<td  style="color:black"><label></label></td>
											<td style="color:black"><label>প্রকল্প পরিচালকের ব্যাক্তিগত তহবিলে প্রদান   </label></td>
											<td style="color:black"><label></label></td>
											<td style="color:black"><label></label></td>
										</tr>

										<tr>
											<td colspan="2" style="color:black"><label>   </label></td>
											<td  style="color:black"><label></label></td>
											<td style="color:black"><label>অতিরিক্ত কাজের আপ্যায়ন ব্যয়   </label></td>
											<td style="color:black"><label></label></td>
											<td style="color:black"><label></label></td>
										</tr>

										<tr>
											<td colspan="2" style="color:black"><label>   </label></td>
											<td  style="color:black"><label></label></td>
											<td style="color:black"><label>অপ্রত্যাশিত ব্যয়  </label></td>
											<td style="color:black"><label></label></td>
											<td style="color:black"><label></label></td>
										</tr>

										<tr>
											<td colspan="2" style="color:black"><label>   </label></td>
											<td  style="color:black"><label></label></td>
											<td style="color:black"><label> </label></td>
											<td style="color:black"><label></label></td>
											<td style="color:black"><label></label></td>
										</tr>

										<tr>
											<td colspan="2" style="color:black"><label>   </label></td>
											<td  style="color:black"><label></label></td>
											<td style="color:black"><label> </label></td>
											<td style="color:black"><label></label></td>
											<td style="color:black"><label></label></td>
										</tr>

										<tr>
											<td colspan="2" style="color:black"><label>   </label></td>
											<td  style="color:black"><label></label></td>
											<td style="color:black"><label> </label></td>
											<td style="color:black"><label></label></td>
											<td style="color:black"><label></label></td>
										</tr>

										<tr>
											<td colspan="2" style="color:black"><label>   </label></td>
											<td  style="color:black"><label></label></td>
											<td style="color:black"><label> </label></td>
											<td style="color:black"><label></label></td>
											<td style="color:black"><label></label></td>
										</tr>

										<tr>
											<td colspan="2" style="color:black"><label>   </label></td>
											<td  style="color:black"><label></label></td>
											<td rowspan="4" style="color:black"><label>৪ </label></td>
											<td class="text-center" colspan="2" style="color:black"><label>প্রভিশন বেতন খাত  </label></td>

											<td style="color:black"><label></label></td>
										</tr>

										<tr>
											<td colspan="2" style="color:black"><label>   </label></td>
											<td  style="color:black"><label></label></td>
											<td style="color:black"><label> কর্মকর্তা কর্মচারীদের বেতন / কার্যভাতা প্রদান</label></td>
											<td style="color:black"><label>   </label></td>

											<td style="color:black"><label></label></td>
										</tr>

										<tr>
											<td colspan="2" style="color:black"><label>   </label></td>
											<td  style="color:black"><label></label></td>
											<td style="color:black"><label> উৎসব ভাতা / বোনাস প্রদান </label></td>
											<td style="color:black"><label>   </label></td>

											<td style="color:black"><label></label></td>
										</tr>

										<tr>
											<td colspan="2" style="color:black"><label>   </label></td>
											<td  style="color:black"><label></label></td>
											<td style="color:black"><label> বিনোদন ভাতা  </label></td>
											<td style="color:black"><label>   </label></td>

											<td style="color:black"><label></label></td>
										</tr>

										<tr>
											<td colspan="2" style="color:black"><label>   </label></td>
											<td  style="color:black"><label></label></td>
											<td style="color:black"><label> ৫  </label></td>
											<td colspan="2" style="color:black"><label> প্রভিশন পেনশন ও উন্নয়ন ব্যয়  </label></td>

											<td style="color:black"><label></label></td>
										</tr>

										<tr>
											<td colspan="2" style="color:black"><label>   </label></td>
											<td  style="color:black"><label></label></td>
											<td style="color:black"><label> ৬  </label></td>
											<td colspan="2" style="color:black"><label> প্রভিশন ঝুঁকি তহবিল হতে প্রদান  </label></td>

											<td style="color:black"><label></label></td>
										</tr>

										<tr>
											<td colspan="2" style="color:black"><label>   </label></td>
											<td  style="color:black"><label></label></td>
											<td style="color:black"><label>   </label></td>
											<td colspan="2" style="color:black"><label>  </label></td>

											<td style="color:black"><label></label></td>
										</tr>

										<tr>
											<td colspan="2" style="color:black"><label>   </label></td>
											<td  style="color:black"><label></label></td>
											<td style="color:black"><label>   </label></td>
											<td colspan="2" style="color:black"><label>   </label></td>

											<td style="color:black"><label></label></td>
										</tr>

										<tr>
											<td colspan="3" class="text-right" style="color:black"><label>মোট =   </label></td>
											<td style="color:black"><label>   </label></td>
											<td colspan="3" class="text-right" style="color:black"><label>মোট =   </label></td>
											<td style="color:black"><label>   </label></td>
										</tr>

										</tbody>
									</table>


									<?php echo "<br/>"; ?>

									<table class="table table-bordered">

										<thead>
										<tr style="background: white">
											<th class="text-center" style="color:black"> অবশিষ্ট (মোট রাজস্ব আয় হতে ব্যয় বিয়োগ হবে) </th>
											<th class="text-center"  style="color:black"> চলতি মাসের সাধারণ বিভাগের ১০%</th>
											<th class="text-center" style="color:black"> চলতি স্থিতি (১-২)</th>
											<th class="text-center"  style="color:black"> পূর্বের মাসের সাধারন বিভাগের ১০%</th>
											<th class="text-center" style="color:black"> চলতি মাসে নীট লাভ /ক্ষতি (৩+৪)</th>
										</tr>
										</thead>
										<tbody>

										<tr>
											<td class="text-center" style="color:black"><label> ১ </label></td>
											<td class="text-center" style="color:black"><label> ২ </label></td>
											<td class="text-center" style="color:black"><label> ৩ </label></td>
											<td class="text-center" style="color:black"><label> ৪ </label></td>
											<td class="text-center" style="color:black"><label> ৫ </label></td>
										</tr>

										<tr>
											<td style="color:black"><label>   </label></td>
											<td style="color:black"><label>   </label></td>
											<td style="color:black"><label>   </label></td>
											<td style="color:black"><label>   </label></td>
											<td style="color:black"><label>   </label></td>
										</tr>

										</tbody>
									</table>

									</div>
								</div>


							</div>


						</div>
					</div>
				</div>


				<?php echo "<br>"; ?>

			</div>


			<div style="background-color:#F7DC6F">

				<?php echo "<br>"; ?>
				<div class="form-group">
					<div class="row">

						<div class="col-md-12">


							<?php echo "<br><br><br><br>"; ?>
							<div class="col-md-4">
								<p style="color:black;">
									<input type="text" name="" value="সম্পাদক /ব্যবস্থাপক" class="form-control">
								<hr>
								</p>
								<center><label style="color:black;">
										<b>
											সম্পাদক /ব্যবস্থাপক
										</b>
									</label></center>
							</div>
							<div class="col-md-4">


							</div>

							<div class="col-md-4">
								<p style="color:black;">
									<input type="text" name="" value="ক্যাশিয়ার " class="form-control">
								<hr>
								</p>
								<center><label style="color:black;">
										<b>
											ক্যাশিয়ার
										</b>
									</label></center>

							</div>

						</div>
					</div>
				</div>
				<?php echo "<br>"; ?>

			</div>
		</div>
		<span><br></span>
	</div>
</div>




