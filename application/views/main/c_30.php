

<div class="" style="background-color:#4A235A ">

	<div class="row ">
		<div class="col-md-12">
			<?php echo "<br>" ?>
			<center><h3 style="color:white">শিক্ষিত বেকার যুব বহুমুখী সমবায় সমিতি লিঃ</h3></center>
			<center><h3 style="color:white">কর্মসংস্থান ও দারিদ্র বিমোচন প্রকল্প</h3></center>
			<center><h5 style="color:white">নিবন্ধন নম্বর -১৩/চাঁদ /১২, তারিখ :০৯/০৭/২০১২ </h5></center>
			<center><h6 style="color:white">ঠিকানা: ঠাকুর বাজার ,শাহারাস্তি , চাঁদপুর। </h6></center>
			<br>


			<div style="background-color:#E59866">
				<div style="background-color:#F7DC6F">
					<div class="row">
						<div class="col-md-12">
							<div class="col-md-10">

							</div>
							<div class="col-md-2">
								<label style=" color:black"><b>সি -৩০</b></label>
							</div>
						</div>
					</div>



					<center><h3 style="color:black"><b>মাসিক খাত ভিত্তিক মূলধন ও দেনা হিসাব </b></h3>
					</center>
					<div class="row">
						<div class="col-md-12">
							<div class="col-md-10">

							</div>
							<div class="col-md-2">
								<label style=" color:black"><b>মাসের নাম :Aug-2020</b></label>
							</div>
						</div>
					</div>

					<?php echo "<br>"; ?>

				</div>

				<div class="form-group">
					<div class="row justify-content-center">
						<div class="col-md-12">
							<div class="table-responsive">

								<table class="table table-bordered ">
									<tbody>

									<tr style="background: white">
										<th style="color:black" colspan="1"></th>
										<th style="color:black">
											<center>শেয়ার মূলধন</center>
										</th>

										<th style="color:black" colspan="2">
											<center>সঞ্চয়</center>
										</th>
										<th style="color:black" colspan="2">
											<center>এককালীন আমানত</center>
										</th>
										<th style="color:black">
											<center>আমানত গ্রহণ</center>
										</th>
										<th style="color:black">
											<center></center>
										</th>
										<th style="color:black">
											<center></center>
										</th>
										<th style="color:black" colspan=""></th>
										<th style="color:black" colspan=""></th>
										<th style="color:black" colspan=""></th>
										<th style="color:black" colspan=""></th>
										<th style="color:black" colspan=""></th>
										<th style="color:black" colspan=""></th>
										<th style="color:black" colspan=""></th>
										<th style="color:black" colspan=""></th>
										<th style="color:black" colspan=""></th>
										<th style="color:black" colspan=""></th>
										<th style="color:black" colspan=""></th>
										<th style="color:black" colspan=""></th>
										<th style="color:black" colspan=""></th>
										<th style="color:black" colspan="2">
											<center>সকল খাতের মোট মূলধন ও দেন</center>
										</th>


									</tr>
									</tbody>
									<tbody>

									<tr style="background: white">
										<th style="color:black">খাত</th>
										<th style="color:black">
											<center>শেয়ার</center>
										</th>

										<th style="color:black">
											<center>মূলধন</center>
										</th>
										<th style="color:black">
											<center>লাভ</center>
										</th>
										<th style="color:black">
											<center>মূলধন</center>
										</th>
										<th style="color:black">
											<center>লাভ</center>
										</th>
										<th style="color:black">
											<center>আমানত</center>
										</th>
										<th style="color:black">
											<center>অর্থ গ্রহণ</center>
											<br>
											<center>(সাধারণ বিভাগ হতে )</center>
										</th>
										<th style="color:black">
											<center>অর্থ গ্রহণ</center>
											<center>(কেন্দ্রীয় সমিতি হতে )</center>
										</th>
										<th style="color:black">
											<center>মূলধন</center>
										</th>
										<th style="color:black">
											<center>লাভ</center>
										</th>
										<th style="color:black">
											<center>হওলাত গ্রহণ</center>
										</th>
										<th style="color:black">
											<center>সাধারণ বিভাগ</center>
											<center>( লাভের ১০% )</center>
										</th>
										<th style="color:black">
											<center>প্রভিশন আমানত খাত</center>
											<center>( লাভের ৪০% )</center>
										</th>
										<th style="color:black">
											<center>প্রভিশন আনুসাঙ্গিক খাত</center>
											<center> (রাজস্ব ও লাভের ১০% )</center>
										</th>
										<th style="color:black">
											<center>প্রভিশন বেতন খাত</center>
											<center>( লাভের ২৫% )</center>
										</th>
										<th style="color:black">
											<center>প্রভিশন পেনশন ও উন্নয়ন খাত</center>
											<center>( লাভের ৫% )</center>
										</th>
										<th style="color:black">
											<center>প্রভিশন ঝুঁকি তহবিল খাত</center>
											<center>( লাভের ১০% )</center>
										</th>
										<th style="color:black">
											<center>কু-ঋণ তহবিল</center>
										</th>
										<th style="color:black">
											<center>বাজেয়াপ্ত শেয়ার</center>
										</th>
										<th style="color:black">
											<center>বাজেয়াপ্ত সঞ্চয় ও আমানত</center>
										</th>
										<th style="color:black">
											<center>কেন্দ্রীয় সমিতি হতে ঋণ গ্রহণ</center>
										</th>
										<th style="color:black">
											<center>মূলধন</center>
										</th>

										<th style="color:black">
											<center>লাভ</center>
										</th>


									</tr>
									</tbody>
									<tbody>

									<tr style="background: white">

										<th style="color:black">
											<center>০১</center>
										</th>

										<th style="color:black">
											<center>০২</center>
										</th>
										<th style="color:black">
											<center>০৩</center>
										</th>
										<th style="color:black">
											<center>০৪</center>
										</th>
										<th style="color:black">
											<center>০৫</center>
										</th>
										<th style="color:black">
											<center>০৬</center>
										</th>
										<th style="color:black">
											<center>০৭</center>
										</th>
										<th style="color:black">
											<center>০৮</center>
										</th>
										<th style="color:black">
											<center>০৯</center>
										</th>
										<th style="color:black">
											<center>১০</center>
										</th>
										<th style="color:black">
											<center>১১</center>
										</th>
										<th style="color:black">
											<center>১২</center>
										</th>
										<th style="color:black">
											<center>১৩</center>
										</th>
										<th style="color:black">
											<center>১৪</center>
										</th>
										<th style="color:black">
											<center>১৫</center>
										</th>
										<th style="color:black">
											<center>১৬</center>
										</th>
										<th style="color:black">
											<center>১৭</center>
										</th>
										<th style="color:black">
											<center>১৮</center>
										</th>
										<th style="color:black">
											<center>১৯</center>
										</th>
										<th style="color:black">
											<center>২০</center>
										</th>
										<th style="color:black">
											<center>২১</center>
										</th>
										<th style="color:black">
											<center>২২</center>
										</th>
										<th style="color:black">
											<center>২৩</center>
										</th>
										<th style="color:black">
											<center>২৪</center>
										</th>


									</tr>
									</tbody>


									<tbody>
									<tr>
										<td style="color:black"><label> জমা</label></td>
										<td style="color:black"><label></label></td>
										<td style="color:black">
											<label> </label>
										</td>
										<td style="color:black">
											<label> </label>
										</td>
										<td style="color:black">
											<label></label>
										</td>
										<td style="color:black">
											<label></label>
										</td>
										<td style="color:black">
											<label></label>
										</td>
										<td style="color:black">
											<label> </label>
										</td>
										<td style="color:black">
											<label></label>
										</td>
										<td style="color:black">
											<label></label>
										</td>
										<td style="color:black">
											<label></label>
										</td>
										<td style="color:black">
											<label></label>
										</td>
										<td style="color:black">
											<label> </label>
										</td>
										<td style="color:black">
											<label></label>
										</td>
										<td style="color:black">
											<label></label>
										</td>
										<td style="color:black">
											<label></label>
										</td>
										<td style="color:black"><label> </label></td>
										<td style="color:black"><label> </label></td>
										<td style="color:black"><label> </label></td>
										<td style="color:black"><label> </label></td>
										<td style="color:black"><label> </label></td>
										<td style="color:black"><label> </label></td>
										<td style="color:black"><label> </label></td>
										<td style="color:black"><label> </label></td>


									</tr>
									</tbody>
									<tbody>
									<tr>
										<td style="color:black"><label> খরচ</label></td>
										<td style="color:black"><label></label></td>
										<td style="color:black">
											<label> </label>
										</td>
										<td style="color:black">
											<label> </label>
										</td>
										<td style="color:black">
											<label></label>
										</td>
										<td style="color:black">
											<label></label>
										</td>
										<td style="color:black">
											<label></label>
										</td>
										<td style="color:black">
											<label> </label>
										</td>
										<td style="color:black">
											<label></label>
										</td>
										<td style="color:black">
											<label></label>
										</td>
										<td style="color:black">
											<label></label>
										</td>
										<td style="color:black">
											<label></label>
										</td>
										<td style="color:black">
											<label> </label>
										</td>
										<td style="color:black">
											<label></label>
										</td>
										<td style="color:black">
											<label></label>
										</td>
										<td style="color:black">
											<label></label>
										</td>
										<td style="color:black"><label> </label></td>
										<td style="color:black"><label> </label></td>
										<td style="color:black"><label> </label></td>
										<td style="color:black"><label> </label></td>
										<td style="color:black"><label> </label></td>
										<td style="color:black"><label> </label></td>
										<td style="color:black"><label> </label></td>
										<td style="color:black"><label> </label></td>

									</tr>
									</tbody>
									<tbody>
									<tr>
										<td style="color:black"><label>অবশিষ্ট </label></td>
										<td style="color:black"><label></label></td>
										<td style="color:black">
											<label> </label>
										</td>
										<td style="color:black">
											<label> </label>
										</td>
										<td style="color:black">
											<label></label>
										</td>
										<td style="color:black">
											<label></label>
										</td>
										<td style="color:black">
											<label></label>
										</td>
										<td style="color:black">
											<label> </label>
										</td>
										<td style="color:black">
											<label></label>
										</td>
										<td style="color:black">
											<label></label>
										</td>
										<td style="color:black">
											<label></label>
										</td>
										<td style="color:black">
											<label></label>
										</td>
										<td style="color:black">
											<label> </label>
										</td>
										<td style="color:black">
											<label></label>
										</td>
										<td style="color:black">
											<label></label>
										</td>
										<td style="color:black">
											<label></label>
										</td>
										<td style="color:black"><label> </label></td>
										<td style="color:black"><label> </label></td>
										<td style="color:black"><label> </label></td>
										<td style="color:black"><label> </label></td>
										<td style="color:black"><label> </label></td>
										<td style="color:black"><label> </label></td>
										<td style="color:black"><label> </label></td>
										<td style="color:black"><label> </label></td>

									</tr>
									</tbody>
									<tbody>
									<tr>
										<td style="color:black"><label>আগত </label></td>
										<td style="color:black"><label></label></td>
										<td style="color:black">
											<label> </label>
										</td>
										<td style="color:black">
											<label> </label>
										</td>
										<td style="color:black">
											<label></label>
										</td>
										<td style="color:black">
											<label></label>
										</td>
										<td style="color:black">
											<label></label>
										</td>
										<td style="color:black">
											<label> </label>
										</td>
										<td style="color:black">
											<label></label>
										</td>
										<td style="color:black">
											<label></label>
										</td>
										<td style="color:black">
											<label></label>
										</td>
										<td style="color:black">
											<label></label>
										</td>
										<td style="color:black">
											<label> </label>
										</td>
										<td style="color:black">
											<label></label>
										</td>
										<td style="color:black">
											<label></label>
										</td>
										<td style="color:black">
											<label></label>
										</td>
										<td style="color:black"><label> </label></td>
										<td style="color:black"><label> </label></td>
										<td style="color:black"><label> </label></td>
										<td style="color:black"><label> </label></td>
										<td style="color:black"><label> </label></td>
										<td style="color:black"><label> </label></td>
										<td style="color:black"><label> </label></td>
										<td style="color:black"><label> </label></td>

									</tr>
									</tbody>
									<tbody>
									<tr>
										<td style="color:black"><label>সর্বমোট </label></td>
										<td style="color:black"><label></label></td>
										<td style="color:black">
											<label> </label>
										</td>
										<td style="color:black">
											<label> </label>
										</td>
										<td style="color:black">
											<label></label>
										</td>
										<td style="color:black">
											<label></label>
										</td>
										<td style="color:black">
											<label></label>
										</td>
										<td style="color:black">
											<label> </label>
										</td>
										<td style="color:black">
											<label></label>
										</td>
										<td style="color:black">
											<label></label>
										</td>
										<td style="color:black">
											<label></label>
										</td>
										<td style="color:black">
											<label></label>
										</td>
										<td style="color:black">
											<label> </label>
										</td>
										<td style="color:black">
											<label></label>
										</td>
										<td style="color:black">
											<label></label>
										</td>
										<td style="color:black">
											<label></label>
										</td>
										<td style="color:black"><label> </label></td>
										<td style="color:black"><label> </label></td>
										<td style="color:black"><label> </label></td>
										<td style="color:black"><label> </label></td>
										<td style="color:black"><label> </label></td>
										<td style="color:black"><label> </label></td>
										<td style="color:black"><label> </label></td>
										<td style="color:black"><label> </label></td>

									</tr>
									</tbody>
									<tbody>
									<tr>
										<td style="color:black"><label>খাতের স্থিতি </label></td>
										<td style="color:black"><label></label></td>
										<td style="color:black">
											<label> </label>
										</td>
										<td style="color:black">
											<label> </label>
										</td>
										<td style="color:black">
											<label></label>
										</td>
										<td style="color:black">
											<label></label>
										</td>
										<td style="color:black">
											<label></label>
										</td>
										<td style="color:black">
											<label> </label>
										</td>
										<td style="color:black">
											<label></label>
										</td>
										<td style="color:black">
											<label></label>
										</td>
										<td style="color:black">
											<label></label>
										</td>
										<td style="color:black">
											<label></label>
										</td>
										<td style="color:black">
											<label> </label>
										</td>
										<td style="color:black">
											<label></label>
										</td>
										<td style="color:black">
											<label></label>
										</td>
										<td style="color:black">
											<label></label>
										</td>
										<td style="color:black"><label> </label></td>
										<td style="color:black"><label> </label></td>
										<td style="color:black"><label> </label></td>
										<td style="color:black"><label> </label></td>
										<td style="color:black"><label> </label></td>
										<td style="color:black"><label> </label></td>
										<td style="color:black"><label> </label></td>
										<td style="color:black"><label> </label></td>

									</tr>
									</tbody>


								</table>

							</div>
						</div>
					</div>
				</div>


				<?php echo "<br>"; ?>

			</div>


			<div style="background-color:#F7DC6F">

				<?php echo "<br>"; ?>
				<div class="form-group">
					<div class="row">

						<div class="col-md-12">


							<div class="col-md-12">


								<?php echo "<br><br><br><br>"; ?>
								<div class="col-md-4">


								</div>
								<div class="col-md-4">


								</div>
								<div class="col-md-4">
									<p style="color:black;">
										<input type="text" name="" value="সম্পাদক /ব্যবস্থাপক" class="form-control">
									<hr>
									</p>
									<center><label style="color:black;">
											<b>
												সম্পাদক /ব্যবস্থাপক
											</b>
										</label></center>
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php echo "<br>"; ?>

			</div>
		</div>
		<span><br></span>
	</div>
</div>





