<style type="text/css">
	.form-control::-webkit-input-placeholder {
		color: #000;
	}

	::placeholder { /* Most modern browsers support this now. */
		color: #000;
	}

	.form-control {
		color: #000 ! important;
	}
</style>

<div class="" style="background-color:#4A235A">

	<div class="row">
		<div class="col-md-12">
			<center><h3 style="color:white">শিক্ষিত বেকার যুব বহুমুখী সমবায় সমিতি লিঃ</h3></center>
			<center><h3 style="color:white">কর্মসংস্থান ও দারিদ্র বিমোচন প্রকল্প</h3></center>
			<center><h5 style="color:white">নিবন্ধন নম্বর -১৩/চাঁদ /১২, তারিখ :০৯/০৭/২০১২ </h5></center>
			<center><h6 style="color:white">ঠিকানা: ঠাকুর বাজার ,শাহারাস্তি , চাঁদপুর। </h6></center>
			<br>


			<div style="background-color:#E59866">
				<div style="background-color:#F7DC6F">
					<div class="row">
						<div class="col-md-12">
							<div class="col-md-10">

							</div>
							<div class="col-md-2">
								<label class="text-right" style=" color:black"><b>সি - ৩৩</b></label>
							</div>
						</div>
					</div>


					<?php echo "<br>"; ?>
					<center><h3 style="color:black"><b>মূলধন, বিনিয়োগ ও বকেয়া সংক্রান্ত মূল্যায়ন প্রতিবেদন  </b></h3>
					</center>

					<?php echo "<br>"; ?>


				</div>

				<div style="height:20px;"></div>

				<?php echo "<br>"; ?>
				<div class="form-group">
					<div class="row">
						<div class="col-md-12">
							<div class="col-md-12"><p style="color:black;float:right;">মাসের নাম
									: <?php echo date('mm/yy') ?></p>
								<div class="col-md-12">

									<div class="table-responsive">
										<table class="table table-bordered ">

											<tr style="background: white">
												<th rowspan="2" class="text-center" style="color:black"> অর্থ বছর</th>
												<th rowspan="2" class="text-center" style="color:black">মাসের সংখ্যা
												</th>
												<th colspan="3" class="text-center" style="color:black"> মূলধন(শেয়ার +
													সঞ্চয় + এককালীন আমানত + আমানত গ্রহণ + এমএসপি)
												</th>
												<th rowspan="2" class="text-center" style="color:black"> অর্থ গ্রহণ
													স্থিতি সাধারণ বিভাগ হতে
												</th>
												<th rowspan="2" class="text-center" style="color:black"> অর্থ গ্রহণ
													স্থিতি কেন্দ্রীয় সমিতি হতে
												</th>

												<th colspan="3" style="color:black">
													<center>বিনিয়োগ(কিস্তিতে + এককালীন)</center>
												</th>

												<th rowspan="2" style="color:black">
													<center>হস্ত মজুদ</center>
												</th>

												<th rowspan="2" style="color:black">
													<center>ব্যাংকে মজুদ</center>
												</th>
												<th colspan="5" class="text-center" style="color:black"> শিক্ষিত বেকার
													কেন্দ্রীয় সমিতিতে জমা
												</th>
												<th rowspan="2" class="text-center" style="color:black"> জুন /১৯ মাসের
													খেলাপী ও বকেয়া মূলধনের পরিমান
												</th>
												<th rowspan="2" class="text-center" style="color:black"> হাল মাসে খেলাপী
													ও বকেয়া মূলধনের পরিমান
												</th>
												<th rowspan="2" class="text-center" style="color:black"> খেলাপী ও বকেয়া
													মূলধনের হ্রাসের পরিমান (১৮-১৯)
												</th>

												<th rowspan="2" style="color:black">
													<center>জুন /১৯ মাসে বকেয়া ও খেলাপী সংখ্যা</center>
												</th>

												<th rowspan="2" style="color:black">
													<center>হাল মাসে বকেয়া ও খেলাপী সংখ্যা</center>
												</th>

												<th rowspan="2" style="color:black">
													<center>বকেয়া ও খেলাপী সংখ্যা হ্রাস (২২-২১)</center>
												</th>
												<th rowspan class="text-center" style="color:black"> জুন/১৯ স্বনির্ভর
													বিনিয়োগ স্থিতি
												</th>
												<th class="text-center" style="color:black"> হাল মাসের বিনিয়োগ মূলধন
													স্থিতি
												</th>
												<th class="text-center" style="color:black"> বিনিয়োগ মূলধন কমতি
													(২৪-২৫)
												</th>

												<th style="color:black">
													<center>জুন/১৯ মাসের প্রভিশনের স্থিতি</center>
												</th>

												<th style="color:black">
													<center>হাল মাসের প্রভিশনের স্থিতি</center>
												</th>

												<th style="color:black">
													<center>প্রভিশন বৃদ্ধির পরিমান (২৪-২৫)</center>
												</th>
												<th style="color:black">
													<center>জুন/১৯ পর্যন্ত সদস্য সংখ্যা স্থিতি</center>
												</th>
												<th style="color:black">
													<center>হাল মাসের সদস্য সংখ্যা স্থিতি</center>
												</th>

												<th style="color:black">
													<center>সদস্য সংখ্যা বৃদ্ধির পরিমান (৩১-৩০)</center>
												</th>

												<th style="color:black">
													<center>সমিতির প্রকল্পের নাম</center>
												</th>

												<th style="color:black">
													<center>জুন /১৯ পর্যন্ত বিনিয়োগকারীর সংখ্যা</center>
												</th>

												<th style="color:black">
													<center>হাল মাস বিনিয়োগকারীর সংখ্যা স্থিতি</center>
												</th>
												<th style="color:black">
													<center> বিনিয়োগকারীর সংখ্যা বৃদ্ধির পরিমান (৩৫-৩৪)</center>
												</th>
												<th style="color:black">
													<center> সন্তোষজনক না হলে মতামত / কারণ</center>
												</th>

											</tr>

											<tr style="background: white">
												<td style="color:black"><label>জুন / ১৯ মাসের মূলধন স্থিতি </label></td>
												<td style="color:black"><label>হাল মাসের মূলধন স্থিতি </label></td>
												<td style="color:black"><label>মূলধন বৃদ্ধির পরিমান (৪-৩) </label></td>
												<td style="color:black"><label>জুন / ১৯ মাসের বিনিয়োগের পাওনা
														স্থিতি </label></td>
												<td style="color:black"><label>হাল মাসের বিনিয়োগের পাওনা স্থিতি </label>
												</td>
												<td style="color:black"><label>বিনিয়োগ বৃদ্ধির পরিমান (৯-৮) </label>
												</td>
												<td style="color:black"><label>এফডিআর স্থিতি </label></td>
												<td style="color:black"><label>স্থায়ী আমানত স্থিতি </label></td>
												<td style="color:black"><label>অস্থায়ী আমানত স্থিতি </label></td>
												<td style="color:black"><label>নগদ মজুদ তহবিল স্থিতি </label></td>
												<td style="color:black"><label>মোট স্থিতি (১৩+১৪+১৫+১৬) </label></td>
												<td rowspan="" style="color:black"><label> </label></td>
												<td rowspan="" style="color:black"><label> </label></td>
												<td rowspan="" style="color:black"><label></label></td>
												<td rowspan="" style="color:black"><label></label></td>
												<td rowspan="" style="color:black"><label></label></td>
												<td rowspan="" style="color:black"><label></label></td>
												<td rowspan="" style="color:black"><label></label></td>
												<td rowspan="" style="color:black"><label> </label></td>
												<td rowspan="" style="color:black"><label></label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td rowspan="" style="color:black"><label></label></td>
											</tr>

											<tr style="background: white">
												<td style="color:black"><label> ১</label></td>
												<td style="color:black"><label>২ </label></td>
												<td style="color:black"><label>৩</label></td>
												<td style="color:black"><label>৪</label></td>
												<td style="color:black"><label>৫</label></td>
												<td style="color:black"><label>৬</label></td>
												<td style="color:black"><label>৭</label></td>
												<td style="color:black"><label>৮ </label></td>
												<td style="color:black"><label>৯</label></td>
												<td style="color:black"><label>১০</label></td>
												<td style="color:black"><label>১১</label></td>
												<td style="color:black"><label>১২</label></td>
												<td style="color:black"><label> ১৩</label></td>
												<td style="color:black"><label>১৪ </label></td>
												<td style="color:black"><label>১৫</label></td>
												<td style="color:black"><label>১৬</label></td>
												<td style="color:black"><label>১৭</label></td>
												<td style="color:black"><label>১৮</label></td>
												<td style="color:black"><label> ১৯</label></td>
												<td style="color:black"><label>২০ </label></td>
												<td style="color:black"><label>২১</label></td>
												<td style="color:black"><label>২২</label></td>
												<td style="color:black"><label>২৩</label></td>
												<td style="color:black"><label>২৪</label></td>
												<td style="color:black"><label>২৫ </label></td>
												<td style="color:black"><label>২৬</label></td>
												<td style="color:black"><label>২৭</label></td>
												<td style="color:black"><label>২৮</label></td>
												<td style="color:black"><label>২৯</label></td>
												<td style="color:black"><label>৩০</label></td>
												<td style="color:black"><label>৩১ </label></td>
												<td style="color:black"><label>৩২</label></td>
												<td style="color:black"><label>৩৩</label></td>
												<td style="color:black"><label>৩৪</label></td>
												<td style="color:black"><label>৩৫</label></td>
												<td style="color:black"><label>৩৬</label></td>
												<td style="color:black"><label>৩৭</label></td>
											</tr>

											<tr>
												<td rowspan="2" style="color:black"><label> ২০১৯-২০২০</label></td>
												<td rowspan="2" style="color:black"><label></label></td>
												<td rowspan="2" style="color:black"><label></label></td>
												<td rowspan="2" style="color:black"><label></label></td>
												<td rowspan="2" style="color:black"><label></label></td>
												<td rowspan="2" style="color:black"><label></label></td>
												<td rowspan="2" style="color:black"><label> </label></td>
												<td rowspan="2" style="color:black"><label></label></td>
												<td rowspan="2" style="color:black"><label></label></td>
												<td rowspan="2" style="color:black"><label></label></td>
												<td rowspan="2" style="color:black"><label></label></td>
												<td rowspan="2" style="color:black"><label></label></td>
												<td rowspan="2" style="color:black"><label> </label></td>
												<td rowspan="2" style="color:black"><label> </label></td>
												<td rowspan="2" style="color:black"><label></label></td>
												<td rowspan="2" style="color:black"><label></label></td>
												<td rowspan="2" style="color:black"><label></label></td>
												<td rowspan="2" style="color:black"><label></label></td>
												<td rowspan="2" style="color:black"><label></label></td>
												<td rowspan="2" style="color:black"><label> </label></td>
												<td rowspan="2" style="color:black"><label></label></td>
												<td rowspan="2" style="color:black"><label></label></td>
												<td rowspan="2" style="color:black"><label></label></td>
												<td rowspan="2" style="color:black"><label> </label></td>
												<td rowspan="2" style="color:black"><label> </label></td>
												<td rowspan="2" style="color:black"><label></label></td>
												<td rowspan="2" style="color:black"><label></label></td>
												<td rowspan="2" style="color:black"><label> </label></td>
												<td rowspan="2" style="color:black"><label></label></td>
												<td rowspan="2" style="color:black"><label></label></td>
												<td rowspan="2" style="color:black"><label></label></td>
												<td rowspan="2" style="color:black"><label> </label></td>
												<td style="color:black"><label>কর্মসংস্থান প্রকল্প </label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td rowspan="2" style="color:black"><label></label></td>
											</tr>

											<tr>
												<td style="color:black"><label>স্বনির্ভর প্রকল্প </label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
											</tr>


										</table>
									</div>

									<br/>
									<p style="color:black;">৩৮ । মতামত/কারণ : <textarea
												style="border:1px solid #999999;width:100%;margin:5px 0;padding:3px;"
												placeholder=" মতামত/কারণ"></textarea></p>
								</div>


							</div>


						</div>


					</div>

				</div>


				<?php echo "<br>"; ?>


				<div class="form-group">
					<div class="row">
						<div class="col-md-12">

							<div class="col-md-12">
								<div class="col-md-6">
									<div class="table-responsive">
										<table class="table table-bordered ">

											<tr style="background: white">
												<th colspan="3" class="text-center" style="color:black">মূলধন</th>
												<th colspan="3" class="text-center" style="color:black">বিনিয়োগ</th>

											</tr>

											<tr style="background: white">
												<td style="color:black"><label>ক্র. নং. </label></td>
												<td style="color:black"><label>খাতের নাম </label></td>
												<td style="color:black"><label>স্থিতি </label></td>
												<td style="color:black"><label>ক্র. নং. </label></td>
												<td style="color:black"><label>খাতের নাম </label></td>
												<td style="color:black"><label>স্থিতি </label></td>
											</tr>

											<tr>
												<td style="color:black"><label>০১ </label></td>
												<td style="color:black"><label>শেয়ার </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label>০১ </label></td>
												<td style="color:black"><label>কিস্তিতে বিনিয়োগ </label></td>
												<td style="color:black"><label> </label></td>
											</tr>

											<tr>
												<td style="color:black"><label>০২ </label></td>
												<td style="color:black"><label>সঞ্চয় </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label>০২ </label></td>
												<td style="color:black"><label>বিশেষ সুবিধা বিনিয়োগ </label></td>
												<td style="color:black"><label> </label></td>
											</tr>

											<tr>
												<td style="color:black"><label>০৩ </label></td>
												<td style="color:black"><label>এককালীন আমানত </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> ক</label></td>
												<td class="text-right" style="color:black"><label>মোট = </label></td>
												<td style="color:black"><label> </label></td>
											</tr>

											<tr>
												<td style="color:black"><label>০৪ </label></td>
												<td style="color:black"><label>আমানত গ্রহণ </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label>০৩ </label></td>
												<td style="color:black"><label>স্বনির্ভর বিনিয়োগ </label></td>
												<td style="color:black"><label> </label></td>
											</tr>

											<tr>
												<td style="color:black"><label>০৫ </label></td>
												<td style="color:black"><label>মোট = </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label>খ </label></td>
												<td style="color:black" class="text-right"><label>মোট = </label></td>
												<td style="color:black"><label> </label></td>
											</tr>

											<tr>
												<td style="color:black"><label>০৬ </label></td>
												<td style="color:black"><label>আমানত খাতের ৪০% </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label>০৪ </label></td>
												<td style="color:black"><label>ব্যাংক এফ.ডি.আর. </label></td>
												<td style="color:black"><label> </label></td>
											</tr>

											<tr>
												<td style="color:black"><label>০৭ </label></td>
												<td style="color:black"><label>বেতন খাতের ২৫% </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label>০৫ </label></td>
												<td style="color:black"><label>আমানত হিসাবে জমা </label></td>
												<td style="color:black"><label> </label></td>
											</tr>

											<tr>
												<td style="color:black"><label>০৮ </label></td>
												<td style="color:black"><label>আনুষাঙ্গিক খাতের ১০% </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label>০৬ </label></td>
												<td style="color:black"><label>কেন্দ্রীয় সমিতিতে এফডিআর </label></td>
												<td style="color:black"><label> </label></td>
											</tr>

											<tr>
												<td style="color:black"><label>০৯ </label></td>
												<td style="color:black"><label>পেনশন ও উন্নয়ন তহবিল ৫% </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label>০৭ </label></td>
												<td style="color:black"><label>কেন্দ্রীয় সমিতিতে স্থায়ী আমানত </label>
												</td>
												<td style="color:black"><label> </label></td>
											</tr>

											<tr>
												<td style="color:black"><label>১০ </label></td>
												<td style="color:black"><label>ঝুঁকি তহবিল ১০% </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label>০৮</label></td>
												<td style="color:black"><label>কেন্দ্রীয় সমিতিতে অস্থায়ী আমানত </label>
												</td>
												<td style="color:black"><label> </label></td>
											</tr>

											<tr>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label>মোট = </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label>গ </label></td>
												<td style="color:black" class="text-right"><label>মোট =</label></td>
												<td style="color:black"><label> </label></td>
											</tr>

											<tr>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label>(৫+১১) সর্বমোট = </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black" class="text-right"><label>(ক+খ+গ) সর্বমোট
														=</label></td>
												<td style="color:black"><label> </label></td>
											</tr>

										</table>
									</div>


								</div>


								<div class="col-md-6">

									<div class="table-responsive">

										<table class="table table-bordered ">

											<tr style="background: white">
												<th colspan="6" class="text-center" style="color:black">২০১৯-২০২০ অর্থ
													বছরের বার্ষিক লক্ষ্যমাত্রা বাস্তবায়ন
												</th>

											</tr>

											<tr style="background: white">
												<td style="color:black"><label>মূল্যায়নের বিষয় </label></td>
												<td style="color:black"><label>মাসিক লক্ষ্যমাত্রা </label></td>
												<td style="color:black"><label>মাসের সংখ্যা </label></td>
												<td style="color:black"><label>লক্ষ্যমাত্রা অনুযায়ী বৃদ্ধি
														(২*৩) </label></td>
												<td style="color:black"><label>বাস্তবায়ন স্থিতি বৃদ্ধি
														(৫,১২,৩০,৩৩) </label></td>
												<td style="color:black"><label>বাড়তি/ঘাটতি (৫-৪) </label></td>
											</tr>

											<tr style="background: white">
												<td style="color:black"><label>১</label></td>
												<td style="color:black"><label>২</label></td>
												<td style="color:black"><label>৩ </label></td>
												<td style="color:black"><label>৪ </label></td>
												<td style="color:black"><label>৫ </label></td>
												<td style="color:black"><label>৬</label></td>
											</tr>

											<tr>
												<td style="color:black"><label>মূলধন বৃদ্ধি</label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label></label></td>
											</tr>

											<tr>
												<td style="color:black"><label>বিনিয়োগ মূলধন বৃদ্ধি </label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label></label></td>
											</tr>

											<tr>
												<td style="color:black"><label>প্রভিশন বৃদ্ধি </label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label></label></td>
											</tr>

											<tr>
												<td style="color:black"><label>সদস্য সংখ্যা বৃদ্ধি </label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label></label></td>
											</tr>

											<tr>
												<td style="color:black"><label>বিনিয়োগের বকেয়া ও খেলাপীর হার </label>
												</td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label></label></td>
											</tr>

										</table>

									</div>
								</div>


							</div>


						</div>
					</div>
				</div>

				<?php echo "<br>"; ?>


			</div>


			<div style="background-color:#F7DC6F">

				<?php echo "<br>"; ?>
				<div class="form-group">
					<div class="row">

						<div class="col-md-12">


							<?php echo "<br><br><br><br>"; ?>
							<div class="col-md-4" style="color:black;">

							</div>
							<div class="col-md-4">

							</div>

							<div class="col-md-4">
								<p style="color:black;">
									<input type="text" name="" value="সম্পাদক / ব্যাবস্থাপক" class="form-control">
								<hr>
								</p>

								<center><label style="color:black;">
										<b>
											সম্পাদক / ব্যাবস্থাপক
										</b>
									</label></center>

							</div>

							<center><label style="color:black;">
									<b>
										<button type="submit" style="background: #dd3333" class="btn btn-danger">
											Submit
										</button>

									</b>
								</label></center>

						</div>
					</div>
				</div>
				<?php echo "<br>"; ?>

			</div>
		</div>
		<span><br></span>
	</div>
</div>




