<div class="" style="background-color:#4A235A">

	<div class="row">
		<div class="col-md-12">
			<center><h3 style="color:white">শিক্ষিত বেকার যুব বহুমুখী সমবায় সমিতি লিঃ</h3></center>
			<center><h3 style="color:white">কর্মসংস্থান ও দারিদ্র বিমোচন প্রকল্প</h3></center>
			<center><h5 style="color:white">নিবন্ধন নম্বর -১৩/চাঁদ /১২, তারিখ :০৯/০৭/২০১২ </h5></center>
			<center><h6 style="color:white">ঠিকানা: ঠাকুর বাজার ,শাহারাস্তি , চাঁদপুর। </h6></center>
			<br>


			<div style="background-color:#E59866">
				<div style="background-color:#F7DC6F">
					<div class="row">
						<div class="col-md-12">
							<div class="col-md-10">

							</div>
							<div class="col-md-2">
								<label class="text-right" style=" color:black"><b>সি - ৩৯</b></label>
							</div>
						</div>
					</div>


					<?php echo "<br>"; ?>
					<center><h3 style="color:black"><b>বিনিয়োগের মাসিক প্রতিবেদন </b></h3>
					</center>

					<?php echo "<br>"; ?>


				</div>
				<?php echo "<br>"; ?>
				<div class="form-group">
					<div class="row">
						<div class="col-md-12">
							<div class="col-md-12">
								<div class="col-md-12">
								<div class="table-responsive">
									<table class="table table-bordered ">

										<tr style="background: white">
											<th rowspan="2" class="text-center" style="color:black"> বিনিয়োগ পদ্ধতি    </th>
											<th colspan="4" class="text-center" style="color:black"> বিনিয়োগ সংখ্যা স্থিতি </th>
											<th colspan="5" class="text-center" style="color:black"> বিনিয়োগের ধরন সংখ্যা  </th>
											<th colspan="4" class="text-center" style="color:black"> বিনিয়োগের মূলধন পাওনা  </th>
											<th colspan="4" class="text-center" style="color:black"> বিনিয়োগ হতে লাভ পাওনা  </th>
											
											<th rowspan="2" style="color:black">
												<center> মূলধন ও লাভসহ মোট পাওনা </center>
											</th>
										</tr>
										

										<tr style="background: white">
											<td style="color:black"><label>বিগত মাসের স্থিতি, সর্বশেষ নং </label></td>
											<td style="color:black"><label>মাসের বিতরণ </label></td>
											<td style="color:black"><label> মাসের পরিশোধ </label></td>
											<td style="color:black"><label>বর্তমান স্থিতি  </label></td>
											<td style="color:black"><label>চাকরিজীবী </label></td>
											<td style="color:black"><label>প্রবাসী </label></td>
											<td style="color:black"><label>ব্যাবসায়ী  </label></td>
											<td style="color:black"><label>অন্যান্য সংখ্যা  </label></td>
											<td style="color:black"><label>মোট   </label></td>
											<td style="color:black"><label>বিগত মাসের মূলধন পাওনা স্থিতি </label></td>
											<td style="color:black"><label>মাসের বিনিয়োগ প্রদান </label></td>
											<td style="color:black"><label>মাসের মূলধন আদায় </label></td>
											<td style="color:black"><label>মাসের মূলধন পাওনা স্থিতি </label></td>
											<td style="color:black"><label>বিগত মাসের লাভ স্থিতি  </label></td>
											<td style="color:black"><label>মাসের বিতরনের লাভ   </label></td>
											<td style="color:black"><label>মাসের আদায় লাভ  </label></td>
											<td style="color:black"><label>পাওনা লাভ </label></td>
										</tr>

										<tr style="background: white">
											<td style="color:black"><label>১ </label></td>
											<td style="color:black"><label>২</label></td>
											<td style="color:black"><label>৩</label></td>
											<td style="color:black"><label>৪</label></td>
											<td style="color:black"><label>৫</label></td>
											<td style="color:black"><label>৬  </label></td>
											<td style="color:black"><label>৭</label></td>
											<td style="color:black"><label>৮ </label></td>
											<td style="color:black"><label>৯</label></td>
											<td style="color:black"><label>১০ </label></td>
											<td style="color:black"><label>১১ </label></td>
											<td style="color:black"><label>১২ </label></td>
											<td style="color:black"><label>১৩</label></td>
											<td style="color:black"><label>১৪</label></td>
											<td style="color:black"><label>১৫</label></td>
											<td style="color:black"><label>১৬</label></td>
											<td style="color:black"><label>১৭</label></td>
											<td style="color:black"><label>১৮</label></td>
											<td style="color:black"><label>১৯</label></td>
										</tr>

										<tr>
											<td rowspan="2" style="color:black"><label>কিস্তিতে</label></td>
											<td style="color:black"><label></label></td>
											<td rowspan="2" style="color:black"><label></label></td>
											<td rowspan="2" style="color:black"><label></label></td>
											<td rowspan="2" style="color:black"><label></label></td>
											<td rowspan="2" style="color:black"><label>  </label></td>
											<td rowspan="2" style="color:black"><label></label></td>
											<td rowspan="2" style="color:black"><label> </label></td>
											<td rowspan="2" style="color:black"><label></label></td>
											<td rowspan="2" style="color:black"><label> </label></td>
											<td rowspan="2" style="color:black"><label> </label></td>
											<td rowspan="2" style="color:black"><label> </label></td>
											<td rowspan="2" style="color:black"><label></label></td>
											<td rowspan="2" style="color:black"><label></label></td>
											<td rowspan="2" style="color:black"><label></label></td>
											<td rowspan="2" style="color:black"><label></label></td>
											<td rowspan="2" style="color:black"><label></label></td>
											<td rowspan="2" style="color:black"><label></label></td>
											<td rowspan="2" style="color:black"><label></label></td>
										</tr>

										<tr>
											<td style="color:black"><label> </label></td>
										</tr>

										<tr>
											<td rowspan="2" style="color:black"><label>বিশেষ সুবিধা বিনিয়োগ </label></td>
											<td style="color:black"><label></label></td>
											<td rowspan="2" style="color:black"><label></label></td>
											<td rowspan="2" style="color:black"><label></label></td>
											<td rowspan="2" style="color:black"><label></label></td>
											<td rowspan="2" style="color:black"><label>  </label></td>
											<td rowspan="2" style="color:black"><label></label></td>
											<td rowspan="2" style="color:black"><label> </label></td>
											<td rowspan="2" style="color:black"><label></label></td>
											<td rowspan="2" style="color:black"><label> </label></td>
											<td rowspan="2" style="color:black"><label> </label></td>
											<td rowspan="2" style="color:black"><label> </label></td>
											<td rowspan="2" style="color:black"><label></label></td>
											<td rowspan="2" style="color:black"><label></label></td>
											<td rowspan="2" style="color:black"><label></label></td>
											<td rowspan="2" style="color:black"><label></label></td>
											<td rowspan="2" style="color:black"><label></label></td>
											<td rowspan="2" style="color:black"><label></label></td>
											<td rowspan="2" style="color:black"><label></label></td>
										</tr>

										<tr>
											<td style="color:black"><label> </label></td>
										</tr>

										<tr>
											<td style="color:black"><label>মোট </label></td>
											<td style="color:black"><label></label></td>
											<td style="color:black"><label></label></td>
											<td style="color:black"><label></label></td>
											<td style="color:black"><label></label></td>
											<td style="color:black"><label>  </label></td>
											<td style="color:black"><label></label></td>
											<td style="color:black"><label> </label></td>
											<td style="color:black"><label></label></td>
											<td style="color:black"><label> </label></td>
											<td style="color:black"><label> </label></td>
											<td style="color:black"><label> </label></td>
											<td style="color:black"><label></label></td>
											<td style="color:black"><label></label></td>
											<td style="color:black"><label></label></td>
											<td style="color:black"><label></label></td>
											<td style="color:black"><label></label></td>
											<td style="color:black"><label></label></td>
											<td style="color:black"><label></label></td>
										</tr>
										
									</table>
								</div>
								</div>


							</div>


						</div>
					</div>
				</div>


				<?php echo "<br>"; ?>

			</div>


			<div style="background-color:#F7DC6F">

				<?php echo "<br>"; ?>
				<div class="form-group">
					<div class="row">

						<div class="col-md-12">


							<?php echo "<br><br><br><br>"; ?>
							<div class="col-md-4">
								
							</div>
							<div class="col-md-4">


							</div>

							<div class="col-md-4">
								<center><label style="color:black;">
										<b>
											মোট পাওনা কথায় : 
										</b>
									</label></center>

								<p style="color:black;">
									<input type="text" name="" value="মোট পাওনা কথায়" class="form-control">
								<hr>
								</p>
								
							</div>

						</div>
					</div>
				</div>
				<?php echo "<br>"; ?>

			</div>
		</div>
		<span><br></span>
	</div>
</div>




