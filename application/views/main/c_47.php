<style type="text/css">

	.form-control::-webkit-input-placeholder {
		color: #000;
	}

	::placeholder { /* Most modern browsers support this now. */
		color: #000;
	}

	.form-control {
		color: #000 ! important;
	}
</style>

<div class="" style="background-color:#4A235A">

	<div class="row">
		<div class="col-md-12">
			<center><h3 style="color:white">শিক্ষিত বেকার যুব বহুমুখী সমবায় সমিতি লিঃ</h3></center>
			<center><h3 style="color:white">কর্মসংস্থান ও দারিদ্র বিমোচন প্রকল্প</h3></center>
			<center><h5 style="color:white">নিবন্ধন নম্বর -১৩/চাঁদ /১২, তারিখ :০৯/০৭/২০১২ </h5></center>
			<center><h6 style="color:white">ঠিকানা: ঠাকুর বাজার ,শাহারাস্তি , চাঁদপুর। </h6></center>
			<br>


			<div style="background-color:#E59866">
				<div style="background-color:#F7DC6F">
					<div class="row">
						<div class="col-md-12">
							<div class="col-md-10">

							</div>
							<div class="col-md-2">
								<label class="text-right" style=" color:black"><b>সি - ৪৭</b></label>
							</div>
						</div>
					</div>


					<?php echo "<br>"; ?>
					<center><h3 style="color:black"><b>বিনিয়োগ বকেয়া ও খেলাপী তালিকা (মাসিক)</b></h3>
					</center>

					<?php echo "<br>"; ?>


				</div>

				<div style="height:20px;"></div>

				<div class="form-group">
					<div class="row">
						<div class="col-md-12">
							<div class="col-md-12">
								<div class="col-md-6">
									<div class="table-responsive">
										<table class="table table-bordered ">

											<tr style="background: white">
												<th rowspan="2" class="text-center" style="color:black">নং</th>
												<th rowspan="2" class="text-center" style="color:black">কিস্তির ধরন</th>
												<th rowspan="2" class="text-center" style="color:black">সংখ্যা</th>
												<th colspan="3" class="text-center" style="color:black">খেলাপী এবং বকেয়া
													হতে মূলধন ও লাভ বাবদ আদায়
												</th>
											</tr>

											<tr style="background: white">
												<td style="color:black"><label>মূলধন </label></td>
												<td style="color:black"><label>লাভ </label></td>
												<td style="color:black"><label>সর্বমোট পাওনা </label></td>
											</tr>

											<tr>
												<td style="color:black"><label> ১ </label></td>
												<td style="color:black"><label> বকেয়া ১ কিস্তি </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> </label></td>
											</tr>

											<tr>
												<td style="color:black"><label> ২ </label></td>
												<td style="color:black"><label>বকেয়া ১ এর অধিক </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> </label></td>
											</tr>

											<tr>
												<td style="color:black"><label> ৩ </label></td>
												<td class="text-right" style="color:black"><label>মোট </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> </label></td>
											</tr>

											<tr>
												<td style="color:black"><label> ৪ </label></td>
												<td style="color:black"><label>খেলাপী কিস্তি </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> </label></td>
											</tr>

											<tr>
												<td style="color:black"><label> ৫ </label></td>
												<td style="color:black"><label> খেলাপী এককালীন </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> </label></td>
											</tr>

											<tr>
												<td style="color:black"><label> ৬ </label></td>
												<td class="text-right" style="color:black"><label>মোট </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> </label></td>
											</tr>

											<tr>
												<td class="text-right" colspan="2" style="color:black"><label> সর্বমোট
														(৩+৬) </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> </label></td>
											</tr>

										</table>
									</div>


								</div>

								<div class="col-md-6">

									<div class="table-responsive">

										<table class="table table-bordered ">

											<tr style="background: white">
												<th class="text-center" style="color:black" colspan="3">
													<h4 style="color:black;margin-top:0px;"><label>মাসের বকেয়া ও বিগত
															মাসের আদায়
															অগ্রগতি </label></h4>

												</th>

											</tr>
											<tr style="background: white">
												<th class="text-center" style="color:black">ধরন</th>
												<th class="text-center" style="color:black">সংখ্যা</th>
												<th class="text-center" style="color:black"> টাকা</th>
											</tr>

											<tr>
												<td style="color:black"><label>কিস্তিতে </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> </label></td>
											</tr>


											<tr>
												<td style="color:black"><label>এককালীন</label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
											</tr>

											<tr>
												<td style="color:black"><label>সর্বমোট</label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
											</tr>

											<tr>
												<td style="color:black"><label>পূর্বের মাসের</label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
											</tr>

											<tr>
												<td style="color:black"><label>কমতি</label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
											</tr>

											<tr>
												<td style="color:black"><label>বৃদ্ধি</label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
											</tr>

										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<?php echo "<br>"; ?>
				<div class="form-group">
					<div class="row">
						<div class="col-md-12">
							<div class="col-md-12">
								<div class="col-md-12">


									<label style="color:black; float:left;">
										<b>
											মাসের নাম : &nbsp;&nbsp;
										</b>
									</label>
									<p style="color:black;float:left;">
										<input style="width:150px;" type="text" name="" placeholder="মাসের নাম"
											   class="form-control">
									</p>
									<p style="color:black;float:right;">
										<input style="width:150px;" type="date" name="" value="" class="form-control">
									</p>
									<label style="color:black;float:right;">
										<b>
											তারিখ : &nbsp;&nbsp;
										</b>
									</label>


									<div class="table-responsive" style="clear: both;">
										<table class="table table-bordered ">

											<tr style="background: white">
												<th rowspan="2" class="text-center" style="color:black"> ক্র. নং</th>
												<th rowspan="2" class="text-center" style="color:black">বিনিয়োগ
													গ্রহণকারীর নাম
												</th>
												<th rowspan="2" class="text-center" style="color:black"> হিসাব নং</th>
												<th rowspan="2" class="text-center" style="color:black"> বিনিয়োগ গ্রহনের
													তারিখ
												</th>
												<th rowspan="2" class="text-center" style="color:black"> বিনিয়োগের টাকার
													পরিমান
												</th>

												<th rowspan="2" style="color:black">
													<center>মেয়াদ</center>
												</th>

												<th rowspan="2" style="color:black">
													<center>প্রতি কিস্তির টাকা</center>
												</th>

												<th rowspan="2" style="color:black">
													<center>বকেয়া কিস্তির সংখ্যা</center>
												</th>

												<th colspan="3" style="color:black">
													<center>বকেয়া বা খেলাপী বাবদ পাওনা</center>
												</th>

												<th rowspan="2" style="color:black">
													<center>মেয়াদ উত্তীর্ণের তারিখ</center>
												</th>
												<th rowspan="2" style="color:black">
													<center>আয়ের উৎস (সরাসরি পেশা)</center>
												</th>
												<th rowspan="2" style="color:black">
													<center>সদস্য নম্বর</center>
												</th>

												<th rowspan="2" style="color:black">
													<center>ভর্তির তারিখ</center>
												</th>

												<th rowspan="2" style="color:black">
													<center>সদস্য পদের জমা স্থিতি</center>
												</th>

												<th colspan="3" style="color:black">
													<center>বিনিয়োগের সাথে সম্পৃক্তকারীর নাম</center>
												</th>
											</tr>

											<tr style="background: white">
												<td style="color:black"><label>মূলধন </label></td>
												<td style="color:black"><label>লাভ </label></td>
												<td style="color:black"><label>মোট </label></td>
												<td style="color:black"><label>অনুমোদনকারী </label></td>
												<td style="color:black"><label>পরিদর্শনকারী </label></td>
												<td style="color:black"><label>সুপারিশকারী </label></td>
											</tr>


											<tr style="background: white">
												<td style="color:black"><label>১ </label></td>
												<td style="color:black"><label>২ </label></td>
												<td style="color:black"><label>৩ </label></td>
												<td style="color:black"><label>৪ </label></td>
												<td style="color:black"><label>৫ </label></td>
												<td style="color:black"><label>৬ </label></td>
												<td style="color:black"><label>৭</label></td>
												<td style="color:black"><label>৮ </label></td>
												<td style="color:black"><label>৯</label></td>
												<td style="color:black"><label>১০ </label></td>
												<td style="color:black"><label>১১ </label></td>
												<td style="color:black"><label>১২ </label></td>
												<td style="color:black"><label>১৩</label></td>
												<td style="color:black"><label>১৪</label></td>
												<td style="color:black"><label>১৫</label></td>
												<td style="color:black"><label>১৬</label></td>
												<td style="color:black"><label>১৭</label></td>
												<td style="color:black"><label>১৮</label></td>
												<td style="color:black"><label>১৯</label></td>
											</tr>

											<tr>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
											</tr>

											<tr>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
											</tr>

											<tr>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
											</tr>

											<tr>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
											</tr>
											<tr>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
											</tr>

											<tr>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
											</tr>

											<tr>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
											</tr>

											<tr>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
											</tr>
											<tr>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
											</tr>

											<tr>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
											</tr>

											<tr>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
											</tr>

											<tr>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
											</tr>

											<tr>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
											</tr>

											<tr>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
											</tr>

											<tr>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
											</tr>

											<tr>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
											</tr>

											<tr>
												<td class="text-right" colspan="4" style="color:black"><label>মোট
														=</label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label> </label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black"><label></label></td>
											</tr>

										</table>
									</div>
								</div>


							</div>


						</div>
					</div>
				</div>


				<?php echo "<br>"; ?>

			</div>


			<div style="background-color:#F7DC6F">

				<?php echo "<br>"; ?>
				<div class="form-group">
					<div class="row">

						<div class="col-md-12">


							<?php echo "<br><br><br><br>"; ?>
							<div class="col-md-4" style="color:black;">
								<p><label> নোট : ১ম । খেলাপীগুলো মাসের ০১-৩০ তারিখ পর্যন্ত ধারাবাহিকভাবে ক্রমিক নং শেষ
										করবে। </label></p>
								<p><label>২য় । একের অধিক বকেয়া কিস্তি ০১-৩০ তারিখ পর্যন্ত ধারাবাহিকভাবে ক্রমিক নং শেষ
										করবে। </label></p>
								<p><label>৩য় । বকেয়া ১ কিস্তি মাসের ০১-৩০ তারিখ পর্যন্ত ধারাবাহিকভাবে ক্রমিক নং শেষ করে
										তালিকা প্রস্তুত করতে বলা হলো। </label></p>
							</div>
							<div class="col-md-4">
								<p style="color:black;">
									<input type="text" name="" value="সম্পাদক / ব্যাবস্থাপক" class="form-control">
								<hr>
								</p>

								<center><label style="color:black;">
										<b>
											সম্পাদক / ব্যাবস্থাপক
										</b>
									</label></center>

							</div>

							<div class="col-md-4">
								<p style="color:black;">
									<input type="text" name="" value="অফিসার" class="form-control">
								<hr>
								</p>

								<center><label style="color:black;">
										<b>
											অফিসার (প্রস্তুতকারীর সুন্দর হাতের লেখা হতে হবে)
										</b>
									</label></center>
							</div>


						</div>

						<center>
							<label style="color:black;">
								<b>
									<button type="submit" style="background: #dd3333" class="btn btn-danger"
											value="সাবমিট">Submit
									</button>
								</b>
							</label>
						</center>

					</div>
				</div>
				<?php echo "<br>"; ?>

			</div>
		</div>
		<span><br></span>
	</div>
</div>




