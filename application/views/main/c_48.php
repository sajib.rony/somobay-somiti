<div class="" style="background-color:#4A235A">

	<div class="row">
		<div class="col-md-12">
			<center><h3 style="color:white">শিক্ষিত বেকার যুব বহুমুখী সমবায় সমিতি লিঃ</h3></center>
			<center><h3 style="color:white">কর্মসংস্থান ও দারিদ্র বিমোচন প্রকল্প</h3></center>
			<center><h5 style="color:white">নিবন্ধন নম্বর -১৩/চাঁদ /১২, তারিখ :০৯/০৭/২০১২ </h5></center>
			<center><h6 style="color:white">ঠিকানা: ঠাকুর বাজার ,শাহারাস্তি , চাঁদপুর। </h6></center>
			<br>


			<div style="background-color:#E59866">
				<div style="background-color:#F7DC6F">
					<div class="row">
						<div class="col-md-12">
							<div class="col-md-10">

							</div>
							<div class="col-md-2">
								<label style=" color:black"><b>সি -৪৮</b></label>
							</div>
						</div>
					</div>


					<?php echo "<br>"; ?>
					<center><h3 style="color:black"><b>মাসিক বকেয়ার হার নির্ধারণ প্রতিবেদন </b></h3>
					</center>
					<?php echo "<br>"; ?>
					<div class="row">
						<div class="col-md-12">
							<div class="col-md-6">


							</div>
							<div class="col-md-6">
								<p style="text-align: right"><label style=" color:black"><b>মাসের নাম
											:<?php echo date('M/yy') ?> &nbsp;&nbsp;&nbsp;</b></label>
								</p>
							</div>


						</div>
					</div>
					<?php echo "<br>"; ?>


				</div>
				<?php echo "<br>"; ?>
				<div class="form-group">
					<div class="row">
						<div class="col-md-12">


							<div class="col-md-12">

								<div class="table-responsive">
									<table class="table table-bordered">

										<tbody>

										<tr style="background: white">
											<th style="color:black" rowspan="2">চলতি মাসে বিনিয়োগ বিতরণ সংখ্যা</th>
											<th style="color:black">
												<center>চলতি মাসে বিনিয়োগ পরিশোধ সংখ্যা</center>
											</th>
											<th style="color:black">
												<center>মাসের বিনিয়োগ সংখ্যা বৃদ্ধি /কমতি</center>
												<center>(১-২)</center>
											</th>

											<th style="color:black">
												<center>পূর্বের মাসের বিনিয়োগ সংখ্যা স্থিতি</center>
											</th>
											<th style="color:black">
												<center>চলতি মাসের বিনিয়োগ সংখ্যা স্থিতি</center>
												<center>(৩+৪)</center>
											</th>
											<th style="color:black">
												<center>মোট বিতরণকৃত বিনিয়োগ মূলধন</center>
											</th>
											<th style="color:black">
												<center>বর্তমান পযন্ত আদায় মূলধন</center>
											</th>
											<th style="color:black">
												<center>বিনিয়োগের মূলধন স্থিতি </center>
												<center>(৬-৭)</center>
											</th>
											<th style="color:black">
												<center>চলতি মাসে বকেয়া ও খেলাপী মূলধন</center>
											</th>
											<th style="color:black">
												<center>বকেয়া ও খেলাপীর হার</center>
											</th>
											<th style="color:black">
												<center>চলতি মাসে খেলাপী মূলধন</center>
											</th>
											<th style="color:black">
												<center>খেলাপীর হার
												</center>
											</th>
											<th style="color:black">
												<center>বকেয়া /খেলাপী বৃদ্ধি হলে তার কারণ</center>
											</th>
										</tr>
										</tbody>

										<tbody>

										<tr style="background: white">
											<th style="color:black">
												<center> ১</center>
											</th>
											<th style="color:black">
												<center> ২</center>
											</th>
											<th style="color:black">
												<center>৩</center>
											</th>
											<th style="color:black">
												<center>৪</center>
											</th>
											<th style="color:black">
												<center>৫</center>
											</th>
											<th style="color:black">
												<center>৬</center>
											</th>
											<th style="color:black">
												<center>৭</center>
											</th>
											<th style="color:black">
												<center>৮</center>
											</th>
											<th style="color:black">
												<center>৯</center>
											</th>
											<th style="color:black">
												<center> ১০</center>
											</th>
											<th style="color:black">
												<center> ১১</center>
											</th>
											<th style="color:black">
												<center> ১২</center>
											</th>
											<th style="color:black">
												<center>১৩</center>
											</th>


										</tr>
										</tbody>

										<?php
										$i = '';
										$sum = 0;
										for ($i = 1; $i <= 30; $i++) { ?>
											<tbody>
											<tr>
												<td style="color:black"><label><?php echo $i; ?></label></td>
												<td style="color:black"><label></label></td>
												<td style="color:black">
													<label> </label>
												</td>
												<td style="color:black">
													<label> </label>
												</td>
												<td style="color:black">
													<label></label>
												</td>
												<td style="color:black">
													<label></label>
												</td>
												<td style="color:black">
													<label></label>
												</td>
												<td style="color:black">
													<label></label>
												</td>
												<td style="color:black">
													<label></label>
												</td>
												<td style="color:black">
													<label></label>
												</td>
												<td style="color:black">
													<label></label>
												</td>
												<td style="color:black">
													<label></label>
												</td>

												<td style="color:black"><label></label></td>


											</tr>
											</tbody>


											<?php

										} ?>

									</table>

									<label style="color:black;"> নোট : বকেয়ার হার বকেয়া মূলধনের উপর নির্ধারিত
										হবে। </label>
								</div>
							</div>


						</div>
					</div>
				</div>


				<?php echo "<br>"; ?>

			</div>


			<div style="background-color:#F7DC6F">

				<?php echo "<br>"; ?>
				<div class="form-group">
					<div class="row">

						<div class="col-md-12">


							<?php echo "<br><br><br><br>"; ?>

							<div class="col-md-4">
								<p style="color:black;">
									<input type="text" name="" value="সম্পাদক /ব্যবস্থাপক" class="form-control">
								<hr>
								</p>
								<center><label style="color:black;">
										<b>
											ব্যবস্থাপক
										</b>
									</label></center>

							</div>
							<div class="col-md-4">
								<p style="color:black;">
									<input type="text" name="" value="পরিদর্শক " class="form-control">
								<hr>
								</p>
								<center><label style="color:black;">
										<b>
											পরিদর্শক
										</b>
									</label></center>

							</div>
							<div class="col-md-4">
								<p style="color:black;">

								</p><br>
								<hr>
								<br>
								<center><label style="color:black;">
										<b>
											বিনিয়োগ অফিসার
										</b>
									</label></center>
								<br>
								<label style="color:black;">
									<b>
										১।
									</b>
								</label><br><br>
								<label style="color:black;">
									<b>
										২।
									</b>
								</label>
								<br><br>
								<label style="color:black;">
									<b>
										৩।
									</b>
								</label>

							</div>

						</div>
					</div>
				</div>
				<?php echo "<br>"; ?>

			</div>
		</div>
		<span><br></span>
	</div>
</div>




