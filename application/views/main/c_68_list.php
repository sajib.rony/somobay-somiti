<div class="" style="background-color:#4A235A">
	<div class="row">
		<div class="col-md-12">
			<center><h3 style="color:white">শিক্ষিত বেকার যুব বহুমুখী সমবায় সমিতি লিঃ</h3></center>
			<center><h3 style="color:white">কর্মসংস্থান ও দারিদ্র বিমোচন প্রকল্প</h3></center>
			<center><h5 style="color:white">নিবন্ধন নম্বর -১৩/চাঁদ /১২, তারিখ :০৯/০৭/২০১২ </h5></center>
			<center><h6 style="color:white">ঠিকানা: ঠাকুর বাজার ,শাহারাস্তি , চাঁদপুর। </h6></center>
			<br>
			<div style="background-color:#E59866">
				<div style="background-color:#F7DC6F">



					<?php echo "<br>"; ?>
					<center><h3 style="color:black"><b>সদস্য পদ প্রত্যাহারের তালিকা</b></h3>
					</center>

					<?php echo "<br>"; ?>


				</div>
				<?php echo "<br>"; ?>

				<div class="form-group">
					<div class="row">
						<div class="col-md-12">


							<div class="col-md-12">

								<div class="top10">&nbsp;</div>


								<form action="#" method="post" accept-charset="utf-8" class="form-inline filter"
									  role="form"><input
										type="hidden"
										name="uri"
										value="http://sbcos.ltd/admin/manage/students">
									<div class="form-group">
										<label for="row" style="color:black;">প্রদর্শনী</label><br>
										<select class="form-control input-xs" name="rows"
												onchange="window.location='http://sbcos.ltd/admin/manage/students/'+this.value+'/0'">
											<option>10</option>
											<option selected="">50</option>
											<option>100</option>
											<option>150</option>
											<option>200</option>
											<option>400</option>
											<option>500</option>
										</select>
									</div>

									<div class="form-group">
										<label for="fullname" style="color:black">নাম</label><br>
										<input type="text" class="form-control input-xs" name="fullname"
											   id="fullname"
											   placeholder="নাম" size="25"
											   value="">
									</div>

									<div class="form-group">
										<label for="email" style="color:black">সদস্য নম্বর</label><br>
										<input type="text" class="form-control input-xs" name="email" id="email"
											   placeholder="সদস্য নম্বর"
											   size="18" value="">
									</div>
									<div class="form-group">
										<label for="email" style="color:black">তারিখ শুরু : </label><br>
										<input type="date" class="form-control input-xs" name="email" id="email"
											   size="18" value="">
									</div>
									<div class="form-group">
										<label for="email" style="color:black">তারিখ শেষ : </label><br>
										<input type="date" class="form-control input-xs" name="email" id="email"
											   size="18" value="">
									</div>
									<div class="form-group">
										<label  style="color:black">স্ট্যাটাস</label><br>
										<select name="status" class="form-control input-xs">
											<option value="">---select---</option>
											<option value="0">Pending</option>
											<option value="1">Approved</option>
										</select>
									</div>
									<div class="form-group">
										<label class="" for="">&nbsp;</label><br>
										<button type="submit" class="btn btn-danger btn-xs"> Filter</button>
									</div>
								</form>
							</div>
							<div class="col-md-12">
								<div class="top10">&nbsp;</div>
								<form action="#" role="form" name="dataform" method="post" accept-charset="utf-8">

									<div class="table-responsive">
										<table class="table table-bordered">
											<thead>
											<tr style="background: white">
												<th class="sort_asc" style="color: black"><label>ক্র. নং </label></th>
												<th class="" style="color: black"><label>সদস্য নম্বর</label></th>
												<th class="" style="color: black"><label>পিতা/স্বামীর নাম</label></th>
												<th class="" style="color: black"><label>ঠিকানা</label></th>
												<th class="" style="color: black"><label>বয়স</label></th>
												<th class="" style="color: black"><label>পেশা</label></th>
												<th class="" style="color: black"><label>শিক্ষাগত যোগ্যতা</label></th>
												<th class="" style="color: black"><label>সদস্য হইবার তারিখ </label></th>
												<th class="" style="color: black"><label>সদস্যের নাম</label></th>
												<th class="" style="color: black"><label>মনোনীত ব্যাক্তির নাম</label>
												</th>
												<th class="" style="color: black"><label>সম্পর্ক</label></th>
												<th class="" style="color: black"><label>সদস্যের স্বাক্ষর</label></th>
												<th class="" style="color: black"><label>সম্পাদক /ব্যবস্থাপক
														স্বাক্ষর</label></th>
												<th class="" style="color: black"><label> ত্যাগের তারিখ </label></th>
												<th class="" style="color: black"><label>ভর্তি ইস্যুকারী
														স্বাক্ষর</label></th>
												<th class="" style="color: black"><label>স্ট্যাটাস</label></th>


											</tr>
											</thead>
											<tbody>
											<tr>
												<td style="color:black"><label>1</label></td>
												<td style="color:black"><label>75534831</label></td>
												<td style="color:black"><label>শুভ্র হাসান</label></td>

												<td style="color:black"><label>চাঁদপুর</label></td>
												<td style="color:black"><label>34 </label></td>
												<td style="color:black"><label>ব্যবসা</label></td>
												<td style="color:black"><label>N/A </label></td>
												<td style="color:black"><label>07/07/2020</label></td>
												<td style="color:black"><label>রাজু আলম</label></td>
												<td style="color:black"><label> আলম</label></td>

												<td style="color:black"><label>ভাই</label></td>
												<td style="color:black"><label><input type="checkbox" checked></label></td>
												<td style="color:black"><label><input type="checkbox" checked></label></td>

												<td style="color:black"><label></label></td>
												<td style="color:black"><label>07/07/2020</label></td>

												<td style="color:black"><label>Approved</label></td>


											</tr>
											<tr>
												<td style="color:black"><label>2</label></td>
												<td style="color:black"><label>64586584</label></td>
												<td style="color:black"><label>শুভ্র হাসান</label></td>
												<td style="color:black"><label>চাঁদপুর</label></td>
												<td style="color:black"><label>50</label></td>
												<td style="color:black"><label>ব্যবসা</label></td>
												<td style="color:black"><label>N/A</label></td>
												<td style="color:black"><label>07/07/2020</label></td>
												<td style="color:black"><label>অপু দত্ত</label></td>
												<td style="color:black"><label> অপু </label></td>

												<td style="color:black"><label>ভাই</label></td>
												<td style="color:black"><label><input type="checkbox" checked></label></td>
												<td style="color:black"><label><input type="checkbox" checked></label></td>

												<td style="color:black"><label></label></td>
												<td style="color:black"><label>07/07/2020</label></td>
												<td style="color:black"><label>Pending</label></td>


											</tr>
											<tr>
												<td style="color:black"><label>3</label></td>
												<td style="color:black"><label>30322869</label></td>
												<td style="color:black"><label>কামাল মোল্লা</label></td>
												<td style="color:black"><label>চাঁদপুর</label></td>
												<td style="color:black"><label>40</label></td>
												<td style="color:black"><label>ব্যবসা</label></td>
												<td style="color:black"><label>N/A</label></td>
												<td style="color:black"><label>07/07/2020</label></td>
												<td style="color:black"><label>কামাল মোল্লা</label></td>
												<td style="color:black"><label> কামাল</label></td>

												<td style="color:black"><label>ভাই</label></td>
												<td style="color:black"><label><input type="checkbox" checked></label></td>
												<td style="color:black"><label><input type="checkbox" checked></label></td>

												<td style="color:black"><label></label></td>
												<td style="color:black"><label>07/07/2020</label></td>
												<td style="color:black"><label>Pending</label></td>


											</tr>
											<tr>
												<td style="color:black"><label>4</label></td>
												<td style="color:black"><label>89611913</label></td>
												<td style="color:black"><label>অপু দত্ত</label></td>
												<td style="color:black"><label>চাঁদপুর</label></td>
												<td style="color:black"><label>60</label></td>
												<td style="color:black"><label>ব্যবসা</label></td>
												<td style="color:black"><label>N/A</label></td>
												<td style="color:black"><label>07/07/2020</label></td>
												<td style="color:black"><label>শুভ্র </label></td>
												<td style="color:black"><label> হাসান</label></td>

												<td style="color:black"><label>ভাই</label></td>
												<td style="color:black"><label><input type="checkbox" checked></label></td>
												<td style="color:black"><label><input type="checkbox" checked></label></td>

												<td style="color:black"><label></label></td>
												<td style="color:black"><label>07/07/2020</label></td>
												<td style="color:black"><label>Approved</label></td>


											</tr>



											</tbody>
										</table>
									</div>
									<div class="row" style="margin-top:5px;">
										<div class="col-md-6"><p class="pageinfo">Page 1 of 1 (Showing 1 to 4 of 4
												records)</p>
										</div>
										<div class="col-md-6">
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

