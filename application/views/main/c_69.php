<style>
	p {
		font-family: Roboto;
		font-size: medium;
		color: black;
		text-align: justify;
	}

	h4 {
		font-family: Roboto;

		color: black;
	}

	.div-padding1 {

		padding-top: 10px;
		padding-right: 30px;
		padding-bottom: 10px;
		padding-left: 30px;
	}

	.div-padding2 {

		padding-top: 10px;
		padding-right: 30px;
		padding-bottom: 10px;
		padding-left: 30px;
	}

	.input-field1 {
		width: 25%;
		padding: 5px 20px;
		margin: 8px 0;
		display: inline-block;
		border: 1px solid #ccc;
		border-radius: 4px;
		box-sizing: border-box;
	}

	.text_color_th {
		color: white;
	}
</style>

<div class="" style="background-color:#4A235A">
	<div class="row">
		<div class="col-md-12">
			<center><h3 style="color:white">শিক্ষিত বেকার কেন্দ্রীয় সঞ্চয় ও ঋণদান সমবায় সমিতি লিঃ</h3></center>
			<center><h3 style="color:white">কর্মসংস্থান ও দারিদ্র বিমোচন প্রকল্প</h3></center>
			<center><h5 style="color:white">নিবন্ধন নম্বর -১৩/চাঁদ /১২, তারিখ :০৯/০৭/২০১২ </h5></center>
			<center><h6 style="color:white">ঠিকানা: ঠাকুর বাজার ,শাহারাস্তি , চাঁদপুর। </h6></center>
			<br>
		</div>
	</div>
</div>
<div class="" style="background-color:white">
	<div class="div-padding1">
		<div class="form-group">
			<div class="row">
				<div class="col-md-6">

				</div>
				<div class="col-md-6 text-right">
					<h5 style="color:black"> সি- ৬৯ </h5>
				</div>

			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-12 text-center">
					<h3 style="color:black">কর্মকর্তা কর্মচারীগনের মাসিক বেতন </h3>
				</div>
				<div class="col-md-6">

				</div>
				<div class="col-md-6 text-right">
					<h5 style="color:black">মাসের নাম- <?php echo date('M/y') ?></h5>
				</div>
			</div>
		</div>
	</div>
	<div class="div-padding2">
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<div class="table-responsive">
						<table class="table-responsive table table-bordered">

							<tr style="background: #dd3333">
								<th class="text-center text_color_th">নং</th>
								<th class="text-center text_color_th">নাম</th>
								<th class="text-center text_color_th">পদবী</th>
								<th class="text-center text_color_th">আইডি নম্বর</th>
								<th class="text-center text_color_th">মূল বেতন</th>
								<th class="text-center text_color_th">উৎসব/উৎসাহ বোনাস</th>
								<th class="text-center text_color_th">বাড়ি ভাড়া ৫০ %</th>
								<th class="text-center text_color_th">চিকিসা ভাতা ১০%</th>
								<th class="text-center text_color_th">পরিবহন ও যাতায়াত ভাতা ২০%</th>
								<th class="text-center text_color_th">টিফিন ও লাঞ্চ ভাতা ২০%</th>
								<th class="text-center text_color_th">ভবিষৎ তহবিল ১০%</th>
								<th class="text-center text_color_th">কর্তন</th>
								<th class="text-center text_color_th">মোট বেতন ও ভাতা (৫+৬+৭+৮+৯+১০)</th>
								<th class="text-center text_color_th">অবশিষ্ট ভাতা (১১+১২)</th>
								<th class="text-center text_color_th">নীট প্রদেয় (১৩-১৪)</th>
								<th class="text-center text_color_th">স্বাক্ষর</th>
								<th class="text-center text_color_th">বেতন কর্তন বা না দেয়ার বিষয় মন্তব্য</th>

							</tr>

							<tr style="background: #dd3333">
								<?php for ($i = 1;
										   $i <= 17;
										   $i++) { ?>
									<th class="text-center text_color_th"><?php echo $i; ?></th>
								<?php } ?>
							</tr>
							<?php for ($i = 1;
									   $i <= 30;
									   $i++) { ?>
								<tr>
									<td><p><?php echo $i; ?></p></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
								</tr>
							<?php } ?>
							<tr>
								<td colspan="3"><p class="align-right">মোট</p></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="div-padding1">
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-4">
						<hr>
						<p style="text-align: center">প্রকল্প পরিচালক </p>
					</div>
					<div class="col-md-4">
						<hr>
						<p style="text-align: center">যাচাইকারী</p>
					</div>
					<div class="col-md-4">
						<hr>
						<p style="text-align: center">ব্যবস্থাপক</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--	<div class="form-group">-->
	<!--		<div class="row">-->
	<!--			<div class="col-md-12">-->
	<!--				<center><br><br>-->
	<!--					<button type="submit" style="background: #dd3333" class="btn btn-danger">Submit</button>-->
	<!--				</center>-->
	<!--				<br>-->
	<!--			</div>-->
	<!--		</div>-->
	<!--	</div>-->
</div>

