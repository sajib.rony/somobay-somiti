<style>
	p {
		font-family: Roboto;
		font-size: medium;
		color: black;
		text-align: justify;
	}

	h4 {
		font-family: Roboto;

		color: black;
	}

	.div-padding1 {

		padding-top: 10px;
		padding-right: 30px;
		padding-bottom: 10px;
		padding-left: 30px;
	}

	.div-padding2 {

		padding-top: 10px;
		padding-right: 30px;
		padding-bottom: 10px;
		padding-left: 30px;
	}

	.input-field1 {
		width: 25%;
		padding: 5px 20px;
		margin: 8px 0;
		display: inline-block;
		border: 1px solid #ccc;
		border-radius: 4px;
		box-sizing: border-box;
	}

	.text_color_th {
		color: white;
	}
</style>

<div class="" style="background-color:#4A235A">
	<div class="row">
		<div class="col-md-12">
			<center><h3 style="color:white">শিক্ষিত বেকার কেন্দ্রীয় সঞ্চয় ও ঋণদান সমবায় সমিতি লিঃ</h3></center>
			<center><h3 style="color:white">কর্মসংস্থান ও দারিদ্র বিমোচন প্রকল্প</h3></center>
			<center><h5 style="color:white">নিবন্ধন নম্বর -১৩/চাঁদ /১২, তারিখ :০৯/০৭/২০১২ </h5></center>
			<center><h6 style="color:white">ঠিকানা: ঠাকুর বাজার ,শাহারাস্তি , চাঁদপুর। </h6></center>
			<br>
		</div>
	</div>
</div>
<div class="" style="background-color:white">
	<div class="div-padding1">
		<div class="form-group">
			<div class="row">
				<div class="col-md-6">

				</div>
				<div class="col-md-6 text-right">
					<h5 style="color:black"> সি- ১৫৮ </h5>
				</div>

			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-12 text-center">
					<h3 style="color:black"> ভবিষৎ তহবিল হিসাব সংরক্ষণ রেজিষ্টের  </h3>
				</div>
				<div class="col-md-6">

				</div>
				<div class="col-md-6 text-right">

				</div>
			</div>
		</div>
	</div>
	<div class="div-padding2">
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-4">
						<p>নাম</p>
						<input type="text" name="" class="form-control" placeholder="নাম">
					</div>
					<div class="col-md-4">
						<p>পদবী </p>
						<input type="text" name="" class="form-control" placeholder="পদবী">
					</div>
					<div class="col-md-4">
						<p>কোড নং</p>
						<input type="text" name="" class="form-control" placeholder="কোড নং">
					</div>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-4">
						<p>চাকরিতে যোগদানের তারিখ </p>
						<input type="text" name="" class="form-control" placeholder="চাকরিতে যোগদানের তারিখ ">
					</div>
					<div class="col-md-4">
						<p>ভবিষৎ তহবিল শুরুর তারিখ </p>
						<input type="text" name="" class="form-control" placeholder="ভবিষৎ তহবিল শুরুর তারিখ ">
					</div>
					<div class="col-md-4">
						<p>আদেশ নং </p>
						<input type="text" name="" class="form-control" placeholder="আদেশ নং">
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="div-padding2">
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<div class="table-responsive">
						<table class="table-responsive table table-bordered">
							<tr style="background: #dd3333">
								<th class="text-center text_color_th">নং</th>
								<th class="text-center text_color_th">তারিখ</th>
								<th class="text-center text_color_th">মাসের নাম /বিবরণ</th>
								<th class="text-center text_color_th">বেতন স্কেল</th>
								<th class="text-center text_color_th">ভবিষৎ তহবিল জমা</th>
								<th class="text-center text_color_th">লাভ জমা</th>
								<th class="text-center text_color_th">উত্তোলন</th>
								<th class="text-center text_color_th">স্থিতি (৪+৫)-৬</th>
								<th class="text-center text_color_th">ঋণ গ্রহণের পরিমান</th>
								<th class="text-center text_color_th">ঋণ এর কিস্তি আদায় টাকা</th>
								<th class="text-center text_color_th">স্থিতি পাওনা (৮-৯)</th>
								<th class="text-center text_color_th">স্বাক্ষর ও শাখা</th>
							</tr>
							<tr style="background: #dd3333">
								<th></th>
								<?php for ($i = 1;
										   $i <= 11;
										   $i++) { ?>
									<th class="text-center text_color_th"><?php echo $i; ?></th>
								<?php } ?>
							</tr>
							<?php for ($i = 1;
									   $i <= 30;
									   $i++) { ?>
								<tr>
									<td><p><?php echo $i; ?></p></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
								</tr>
							<?php } ?>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="form-group">
		<div class="row">
			<div class="col-md-12">
				<center><br><br>
					<button type="submit" style="background: #dd3333" class="btn btn-danger">Submit</button>
				</center>
				<br>
			</div>
		</div>
	</div>
</div>

