<style>
	p {
		font-family: Roboto;
		font-size: medium;
		color: black;
		text-align: justify;
	}

	h4 {
		font-family: Roboto;

		color: black;
	}

	.div-padding1 {

		padding-top: 10px;
		padding-right: 30px;
		padding-bottom: 10px;
		padding-left: 30px;
	}

	.div-padding2 {

		padding-top: 10px;
		padding-right: 30px;
		padding-bottom: 10px;
		padding-left: 30px;
	}

	.input-field1 {
		width: 50%;
		padding: 5px 20px;
		margin: 8px 0;
		display: inline-block;
		border: 1px solid #ccc;
		border-radius: 4px;
		box-sizing: border-box;
	}

	.text_color_th {
		color: white;
	}
</style>

<div class="" style="background-color:#4A235A">
	<div class="row">
		<div class="col-md-12">
			<center><h3 style="color:white">শিক্ষিত বেকার কেন্দ্রীয় সঞ্চয় ও ঋণদান সমবায় সমিতি লিঃ</h3></center>
			<center><h3 style="color:white">কর্মসংস্থান ও দারিদ্র বিমোচন প্রকল্প</h3></center>
			<center><h5 style="color:white">নিবন্ধন নম্বর -১৩/চাঁদ /১২, তারিখ :০৯/০৭/২০১২ </h5></center>
			<center><h6 style="color:white">ঠিকানা: ঠাকুর বাজার ,শাহারাস্তি , চাঁদপুর। </h6></center>
			<br>
		</div>
	</div>
</div>
<div class="" style="background-color:white">
	<div class="div-padding1">
		<div class="form-group">
			<div class="row">
				<div class="col-md-6">

				</div>
				<div class="col-md-6 text-right">
					<h5 style="color:black">কোড নং - ১১৮</h5>
				</div>

			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<h2 style="color:black;font-family: Roboto; text-align: center">মুভমেন্ট রেজিষ্টার </h2>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<p> প্রতিষ্ঠানের নাম <input type="text" name="" class="input-field1"
												placeholder="প্রতিষ্ঠানের নাম ">
						সন: <?php echo date('yy') ?>
					</p>
				</div>
			</div>
		</div>
	</div>
	<div class="div-padding2">
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<div class="table-responsive">
						<table class="table-responsive table table-bordered">
							<tr style="background: #dd3333">
								<th class="text-center text_color_th">ক্রমিক নং</th>
								<th class="text-center text_color_th">তারিখ ও সময়</th>
								<th class="text-center text_color_th">নাম</th>
								<th class="text-center text_color_th">প্রস্থানের কারণ ও গন্তব্য স্থান</th>
								<th class="text-center text_color_th">প্রস্থানকারীর স্বাক্ষর</th>
								<th class="text-center text_color_th">সম্ভাব্য আগমনের তারিখ ও সময়</th>
								<th class="text-center text_color_th">অফিসে এসে উপস্থিতির সময়</th>
								<th class="text-center text_color_th">স্বাক্ষর ও তারিখ</th>
							</tr>
							<?php for ($i = 1;
									   $i <= 30;
									   $i++) { ?>
								<tr>
									<td>
										<p><?php echo $i; ?></p></td>
									<td><input type="text" name="" class="form-control" placeholder="তারিখ ও সময় ">
									<td><input type="text" name="" class="form-control" placeholder="নাম">
									<td><input type="text" name="" class="form-control"
											   placeholder="প্রস্থানের কারণ ও গন্তব্য স্থান">
									<td><input type="checkbox" name="" class="form-control">
									<td><input type="text" name="" class="form-control"
											   placeholder="সম্ভাব্য আগমনের তারিখ ও সময়">
									<td><input type="text" name="" class="form-control"
											   placeholder="অফিসে এসে উপস্থিতির সময়">
									<td><input type="text" name="" class="form-control" placeholder="স্বাক্ষর ও তারিখ">
								</tr>
							<?php } ?>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="form-group">
		<div class="row">
			<div class="col-md-12">
				<center><br><br>
					<button type="submit" style="background: #dd3333" class="btn btn-danger">Submit</button>
				</center>
				<br>
			</div>
		</div>
	</div>
</div>

