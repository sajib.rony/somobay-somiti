<style>
	p {
		font-family: Roboto;
		font-size: medium;
		color: black;
		text-align: justify;
	}

	h4 {
		font-family: Roboto;

		color: black;
	}

	.div-padding1 {

		padding-top: 10px;
		padding-right: 30px;
		padding-bottom: 10px;
		padding-left: 30px;
	}

	.div-padding2 {

		padding-top: 0px;
		padding-right: 30px;
		padding-bottom: 10px;
		padding-left: 30px;
	}

	.input-field1 {
		width: 50%;
		padding: 5px 20px;
		margin: 8px 0;
		display: inline-block;
		border: 1px solid #ccc;
		border-radius: 4px;
		box-sizing: border-box;
	}

	.text_color_th {
		color: white;
	}
</style>

<div class="" style="background-color:#4A235A">
	<div class="row">
		<div class="col-md-12">
			<center><h3 style="color:white">শিক্ষিত বেকার কেন্দ্রীয় সঞ্চয় ও ঋণদান সমবায় সমিতি লিঃ</h3></center>
			<center><h3 style="color:white">কর্মসংস্থান ও দারিদ্র বিমোচন প্রকল্প</h3></center>
			<center><h5 style="color:white">নিবন্ধন নম্বর -১৩/চাঁদ /১২, তারিখ :০৯/০৭/২০১২ </h5></center>
			<center><h6 style="color:white">ঠিকানা: ঠাকুর বাজার ,শাহারাস্তি , চাঁদপুর। </h6></center>
			<br>
		</div>
	</div>
</div>
<div class="" style="background-color:white">
	<div class="div-padding1">
		<div class="form-group">
			<div class="row">
				<div class="col-md-6">

				</div>
				<div class="col-md-6 text-right">
					<h5 style="color:black"> কোড -৯০ </h5>
				</div>

			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-12 text-center">
					<h2 style="color:black">সমিতির সাধারন বিভাগ ও প্রকল্প বিভাগের কর্মকর্তা কর্মচারী</h2>
				</div>
				<div class="col-md-6">
					<h5 style="color:black">মাসের নাম- <?php echo date('M/y') ?></h5>
				</div>
				<div class="col-md-6 text-right">
					<h5 style="color:black">সর্বশেষ তথ্য ও সংশোধিত তারিখ :<?php echo date('d/m/yy') ?></h5>
				</div>
			</div>
		</div>
	</div>
	<div class="div-padding2">
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<div class="table-responsive">
						<table class="table-responsive table table-bordered">

							<tr style="background: #dd3333">
								<th class="text-center text_color_th">নং</th>
								<th class="text-center text_color_th">কর্মকর্তা কর্মচারী নাম</th>
								<th class="text-center text_color_th">গ্রেডের পদবী ,গ্রেডের যোগদান , সর্বমোট বেতন</th>
								<th class="text-center text_color_th">দায়িত্ব পালন পদবী কার্যালয় যোগদান ,গ্রেডের
									ব্যাবধান
								</th>
								<th class="text-center text_color_th">চাকরির ১ম পদবী ,পদের যোগদান, সর্বমোট বেতন</th>
								<th class="text-center text_color_th">সর্বশেষ শিক্ষাগত যোগ্যতা , বয়স ও চাকরির বয়স</th>
								<th class="text-center text_color_th">প্রাপ্ত পদোন্নতি ,ইনক্রিমেন্ট , টাইমস্কেল সংখ্যা ও
									তারিখ
								</th>
								<th class="text-center text_color_th">নিয়োগের ধরণ ও তারিখ ,মোবাইল নম্বর , অন্য কার্যালয়
									কর্মকর্তা হলে কার্যালয়ের নাম ও মন্ত্যব
								</th>
							</tr>
							<?php for ($i = 1;
									   $i <= 30;
									   $i++) { ?>
								<tr>
									<td><p><?php echo $i; ?></p></td>
									<td><input type="text" name="" class="form-control" placeholder=""></td>
									<td><input type="text" name="" class="form-control" placeholder="">
									</td>
									<td><input type="text" name="" class="form-control" placeholder="">
									</td>
									<td><input type="text" name="" class="form-control" placeholder=""></td>
									<td><input type="text" name="" class="form-control" placeholder=""></td>
									<td><input type="text" name="" class="form-control" placeholder="">
									<td><input type="text" name="" class="form-control" placeholder="">
									</td>
								</tr>
							<?php } ?>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
<!--	<div class="div-padding2">-->
<!--		<div class="form-group">-->
<!--			<div class="row">-->
<!--				<div class="col-md-12">-->
<!--					<div class="col-md-7">-->
<!---->
<!--					</div>-->
<!--					<div class="col-md-5">-->
<!--						<hr>-->
<!--						<p style="text-align: center">সভাপতি</p>-->
<!--						<p style="text-align: center">ব্যবস্থাপনা কমিটি</p>-->
<!--						<p style="text-align: center">শিক্ষিত বেকার কেন্দ্রীয় সঞ্চয় ও ঋণদান সমবায় সমিতি লিঃ</p>-->
<!--					</div>-->
<!--				</div>-->
<!--			</div>-->
<!--		</div>-->
<!--	</div>-->
	<div class="form-group">
		<div class="row">
			<div class="col-md-12">
				<center><br><br>
					<button type="submit" style="background: #dd3333" class="btn btn-danger">Submit</button>
				</center>
				<br>
			</div>
		</div>
	</div>
</div>
</div>
