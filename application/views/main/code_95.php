<style>
	p {
		font-family: Roboto;
		font-size: medium;
		color: black;
		text-align: justify;
	}

	h4 {
		font-family: Roboto;

		color: black;
	}

	.div-padding1 {

		padding-top: 10px;
		padding-right: 30px;
		padding-bottom: 10px;
		padding-left: 30px;
	}

	.div-padding2 {

		padding-top: 0px;
		padding-right: 30px;
		padding-bottom: 10px;
		padding-left: 30px;
	}

	.input-field1 {
		width: 50%;
		padding: 5px 20px;
		margin: 8px 0;
		display: inline-block;
		border: 1px solid #ccc;
		border-radius: 4px;
		box-sizing: border-box;
	}

	.text_color_th {
		color: white;
	}
</style>

<div class="" style="background-color:#4A235A">
	<div class="row">
		<div class="col-md-12">
			<center><h3 style="color:white">শিক্ষিত বেকার কেন্দ্রীয় সঞ্চয় ও ঋণদান সমবায় সমিতি লিঃ</h3></center>
			<center><h3 style="color:white">কর্মসংস্থান ও দারিদ্র বিমোচন প্রকল্প</h3></center>
			<center><h5 style="color:white">নিবন্ধন নম্বর -১৩/চাঁদ /১২, তারিখ :০৯/০৭/২০১২ </h5></center>
			<center><h6 style="color:white">ঠিকানা: ঠাকুর বাজার ,শাহারাস্তি , চাঁদপুর। </h6></center>
			<br>
		</div>
	</div>
</div>
<div class="" style="background-color:white">
	<div class="div-padding1">
		<div class="form-group">
			<div class="row">
				<div class="col-md-6">

				</div>
				<div class="col-md-6 text-right">
					<h5 style="color:black"> কোড -৯০ </h5>
				</div>

			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-12 text-center">
					<h2 style="color:black">দৈনিক হাজিরা রেজিস্টার </h2>
				</div>
				<div class="col-md-6">
					<h5 style="color:black">মাসের নাম- <?php echo date('M/y') ?></h5>
				</div>
				<div class="col-md-6 text-right">

				</div>
			</div>
		</div>
	</div>
	<div class="div-padding2">
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<div class="table-responsive">
						<table class="table-responsive table table-bordered">

							<tr style="background: #dd3333">
								<th class="text-center text_color_th">তারিখ</th>
								<th class="text-center text_color_th">নাম</th>
								<th class="text-center text_color_th">স্বাক্ষর</th>
								<th class="text-center text_color_th">আগামন</th>
								<th class="text-center text_color_th">প্রস্তান</th>
							</tr>
							<?php for ($i = 1;
									   $i <= 31;
									   $i++) { ?>
								<tr>
									<td><p><?php echo $i; ?></p></td>
									<td><input type="text" name="" class="form-control" placeholder="নাম"></td>
									<td><input type="checkbox" name="" class="form-control" placeholder=""></td>
									<td><input type="text" name="" class="form-control" placeholder="আগামন"></td>
									<td><input type="text" name="" class="form-control" placeholder="প্রস্তান"></td>

								</tr>
							<?php } ?>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--	<div class="div-padding2">-->
	<!--		<div class="form-group">-->
	<!--			<div class="row">-->
	<!--				<div class="col-md-12">-->
	<!--					<div class="col-md-7">-->
	<!---->
	<!--					</div>-->
	<!--					<div class="col-md-5">-->
	<!--						<hr>-->
	<!--						<p style="text-align: center">সভাপতি</p>-->
	<!--						<p style="text-align: center">ব্যবস্থাপনা কমিটি</p>-->
	<!--						<p style="text-align: center">শিক্ষিত বেকার কেন্দ্রীয় সঞ্চয় ও ঋণদান সমবায় সমিতি লিঃ</p>-->
	<!--					</div>-->
	<!--				</div>-->
	<!--			</div>-->
	<!--		</div>-->
	<!--	</div>-->
	<div class="form-group">
		<div class="row">
			<div class="col-md-12">
				<center><br><br>
					<button type="submit" style="background: #dd3333" class="btn btn-danger">Submit</button>
				</center>
				<br>
			</div>
		</div>
	</div>
</div>
</div>
