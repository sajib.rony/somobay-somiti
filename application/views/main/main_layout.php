<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="Opu">
	<meta name="keyword" content="">
	<link rel="shortcut icon" href="">
	<title>ERP System</title>
	<!-- Bootstrap core CSS -->
	<link href="<?= base_url('assets/admin/css/bootstrap.min.css'); ?>" rel="stylesheet">
	<link href="<?= base_url('assets/admin/css/bootstrap-reset.css'); ?>" rel="stylesheet">

	<!--external css-->
	<link href="<?= base_url('assets/admin/css/font-awesome/css/font-awesome.css'); ?>" rel="stylesheet"/>
	<link href="<?= base_url('assets/admin/css/jquery-easy-pie-chart/jquery.easy-pie-chart.css'); ?>" rel="stylesheet"
		  type="text/css" media="screen"/>
	<link rel="stylesheet" href="<?= base_url('assets/admin/css/owl.carousel.css'); ?>" type="text/css">

	<!--right slidebar-->
	<link href="<?= base_url('assets/admin/css/slidebars.css'); ?>" rel="stylesheet">

	<!-- Custom styles for this template -->
	<link href="<?= base_url('assets/admin/css/style.css'); ?>" rel="stylesheet">
	<link href="<?= base_url('assets/admin/css/style-responsive.css'); ?>" rel="stylesheet"/>

	<!--file Upload -->
	<link rel="stylesheet" type="text/css"
		  href="<?php echo base_url('assets/admin/css/bootstrap-fileupload/bootstrap-fileupload.css'); ?>"/>

	<link rel="stylesheet" type="text/css"
		  href="<?php echo base_url('assets/admin/other/bootstrap-datepicker/css/datepicker.css'); ?>"/>

	<script src="<?= base_url('assets/admin/js/jquery.js'); ?>"></script>
</head>

<body>
<section id="container">
	<!--header start-->
	<header class="header purple-bg">
		<div class="sidebar-toggle-box">
			<i class="fa fa-bars"></i>
		</div>
		<!--logo start-->
		<a href="#" class="logo">ই আর পি সিস্টেম<span></span></a>
		<!--logo end-->


		<div class="nav notify-row" id="top_menu">
		</div>
		<div class="top-nav ">
			<!--search & user info start-->
			<ul class="nav pull-right top-menu">
				<!-- user login dropdown start-->
				<li class="dropdown">
					<a data-toggle="dropdown" class="dropdown-toggle" href="#">
						<!-- <img alt="" src="<?= base_url('assets/admin/img/avatar1_small.jpg'); ?>"> -->
						<span class="username">
							<?php
							$user_type = $this->session->userdata('user');
							if ($user_type == "Operator") {
								echo "Operator";
							} elseif ($user_type == "Accountant") {
								echo "Accountant";
							} elseif ($user_type == "General") {
								echo "General";
							}
							?>

						</span>
						<b class="caret"></b>
					</a>
					<ul class="dropdown-menu extended logout">
						<div class="log-arrow-up"></div>
						<li><a href="<?= base_url('admin'); ?>"><i class="fa fa-key"></i>
								Log Out</a></li>
					</ul>
				</li>
			</ul>
			<!--search & user info end-->
		</div>
	</header>
	<!--header end-->
	<!--sidebar start-->
	<aside>
		<div id="sidebar" class="nav-collapse ">
			<!-- sidebar menu start-->
			<ul class="sidebar-menu" id="nav-accordion">

				<li>
					<a class="active" href="<?= base_url('dashboard'); ?>">
						<i class="fa fa-dashboard"></i>
						<span>ড্যাশবোর্ড</span>
					</a>
				</li>
				<li class="sub-menu">
					<a href="javascript:;">
						<i class="fa fa-sitemap"></i>
						<span> মেম্বার </span>
					</a>
					<ul class="sub">
						<li>
							<a href="<?= base_url('addMember'); ?>">

								<span> সদস্য ভর্তি  </span>
							</a>
						</li>
						<li>
							<a href="<?= base_url('memberList'); ?>">
								<span>সদস্য তথ্য তালিকা  </span>
							</a>
						</li>
						<li>
							<a href="<?= base_url('memberAccounts'); ?>">
								<span>সদস্য  একাউন্টস   </span>
							</a>
						</li>


						<li>
							<a href="<?= base_url('memberListApproved'); ?>">

								<span>বৈধ সদস্য তালিকা  </span>
							</a>
						</li>
						<li>
							<a href="<?php echo base_url('memberListPending') ?>">

								<span> মুলতুবি থাকা সদস্য তালিকা   </span>
							</a>
						</li>
						<li>
							<a href="<?= base_url('sonchoyAmanothUttolon'); ?>">
								<span>সঞ্চয় ও আমানত উত্তোলন  </span>
							</a>
						</li>
						<li>
							<a href="<?= base_url('takaUttolonChahida'); ?>">
								<span>টাকা উত্তোলনের চাহিদা   </span>
							</a>
						</li>
						<li>
							<a href="<?= base_url('c_68'); ?>">
								<span>সদস্য পদ প্রত্যাহার  </span>
							</a>
						</li>
						<li>
							<a href="<?= base_url('c_68_list'); ?>">
								<span>সদস্য পদ প্রত্যাহার তালিকা </span>
							</a>
						</li>
						<li>
							<a href="<?= base_url('c_32'); ?>">
								<span>খাত ভিত্তিক মাসিক প্রতিবেদন </span>
							</a>
						</li>

					</ul>
				</li>

				<li class="sub-menu">
					<a href="javascript:;">
						<i class="fa fa-sitemap"></i>
						<span> আমানত </span>
					</a>
					<ul class="sub">
						<li>
							<a href="<?= base_url('sodorsho_nitimala_add'); ?>">
								<span>সদস্য নীতিমালা  </span>
							</a>
						</li>
						<li>
							<a href="<?= base_url('amanoth_add'); ?>">

								<span>অস্থায়ী আমানত /গ্রহণ  </span>
							</a>
						</li>
						<li>
							<a href="<?= base_url('amanoth_list'); ?>">

								<span>অস্থায়ী আমানত তালিকা </span>
							</a>
						</li>
						<li>
							<a href="<?= base_url('mashikAmanoth_add'); ?>">
								<span> মাসিক সঞ্চয় আমানত  </span>
							</a>
						</li>
						<li>
							<a href="<?= base_url('mashikAmanoth_list'); ?>">
								<span> মাসিক সঞ্চয় তালিকা </span>
							</a>
						</li>
						<li>
							<a href="<?= base_url('akalinAmanoth_add'); ?>">
								<span>স্থায়ী আমানত  </span>
							</a>
						</li>
						<li>
							<a href="<?= base_url('akalinAmanoth_list'); ?>">
								<span>স্থায়ী আমানত তালিকা </span>
							</a>
						</li>
					</ul>
				</li>


				<li class="sub-menu">
					<a href="javascript:;">
						<i class="fa fa-sitemap"></i>
						<span> একাউন্টস  </span>
					</a>
					<ul class="sub">

						<li>
							<a href="<?= base_url('code_8'); ?>">

								<span>জমা ভাউচার  </span>
							</a>
						</li>
						<li>
							<a href="<?= base_url('code_9'); ?>">

								<span>খরচ ভাউচার  </span>
							</a>
						</li>
						<li>
							<a href="<?= base_url('code_10'); ?>">

								<span>সঞ্চয় জমা রশিদ </span>
							</a>
						</li>
						<li>
							<a href="<?= base_url('c_27'); ?>">

								<span>মাসিক জমা খরচ</span>
							</a>
						</li>
						<li>
							<a href="<?= base_url('c_28'); ?>">

								<span>মাসিক রাজস্ব হিসাব </span>
							</a>
						</li>
						<li>
							<a href="<?= base_url('c_29'); ?>">

								<span> লভ্যাংশ আদায় বিতরণ</span>
							</a>
						</li>
						<li>
							<a href="<?= base_url('c_30'); ?>">

								<span>মূলধন ও দেনা </span>
							</a>
						</li>
						<li>
							<a href="<?= base_url('c_31'); ?>">

								<span> সম্পত্তি ও পাওনা</span>
							</a>
						</li>
						<li>
							<a href="<?= base_url('c_33'); ?>">

								<span> মূলধন, বিনিয়োগ ও বকেয়া</span>
							</a>
						</li>
						<li>
							<a href="<?= base_url('c_153'); ?>">

								<span>লাভ-ক্ষতির হিসাব</span>
							</a>
						</li>
						<li>
							<a href="<?= base_url('c_178'); ?>">

								<span>প্রভিশন হিসাব </span>
							</a>
						</li>
						<li>
							<a href="<?= base_url('c_179'); ?>">

								<span>মূলধন ও দেন</span>
							</a>
						</li>
						<li>
							<a href="<?= base_url('c_180'); ?>">

								<span>সম্পত্তি ও পাওনা </span>
							</a>
						</li>

						<li>
							<a href="<?= base_url('c_79'); ?>">

								<span> রাজস্ব  আয়/ ব্যয় </span>
							</a>
						</li>
						<li>
							<a href="<?= base_url('c_42'); ?>">

								<span> হওলাত গ্রহণ  এবং প্রদান </span>
							</a>
						</li>
					</ul>
				</li>
				<li class="sub-menu">
					<a href="javascript:;">
						<i class="fa fa-sitemap"></i>
						<span> বিস্তারিত তালিকা </span>
					</a>
					<ul class="sub">
						<li>
							<a href="<?= base_url('c_182'); ?>">
								<span>সদস্য পদের ডিটেল লিস্ট</span>
							</a>
						</li>
						<li>
							<a href="<?= base_url('c_183'); ?>">
								<span>মূলধন পাওনা স্থিতির</span>
							</a>
						</li>
						<li>
							<a href="<?= base_url('c_184'); ?>">
								<span>ভবিষ্য তহবিল জমা</span>
							</a>
						</li>
						<li>
							<a href="<?= base_url('c_185'); ?>">
								<span>যানবাহন হতে কিস্তি পাওনার</span>
							</a>
						</li>
						<li>
							<a href="<?= base_url('c_181'); ?>">
								<span>আসবাবপত্র মজুদ</span>
							</a>
						</li>

					</ul>
				</li>
				<li class="sub-menu">
					<a href="javascript:;">
						<i class="fa fa-sitemap"></i>
						<span> লাভ বিতরণ তালিকা </span>
					</a>
					<ul class="sub">
						<li>
							<a href="<?= base_url('c_80'); ?>">
								<span>সঞ্চয় খাতে লাভ </span>
							</a>
						</li>
						<li>
							<a href="<?= base_url('c_81'); ?>">
								<span>এককালীন আমানত খাতে লাভ </span>
							</a>
						</li>

					</ul>
				</li>


				<li class="sub-menu">
					<a href="javascript:;">
						<i class="fa fa-sitemap"></i>
						<span> বিনিয়োগ   </span>
					</a>
					<ul class="sub">
						<li>
							<a href="<?= base_url('beniyog_add'); ?>">

								<span>পণ্য ক্রয় বা বিনিয়োগ আবেদন </span>
							</a>
						</li>
						<li>
							<a href="<?= base_url('beniyog_list'); ?>">

								<span>পণ্য ক্রয় বা বিনিয়োগ তালিকা </span>
							</a>
						</li>
						<li>
							<a href="<?= base_url('besses_beniyog_add'); ?>">
								<span>বিশেষ বিনিয়োগ আবেদন </span>
							</a>
						</li>
						<li>
							<a href="<?= base_url('besses_beniyog_list'); ?>">
								<span> বিশেষ বিনিয়োগ তালিকা </span>
							</a>
						</li>
						<li>
							<a href="<?= base_url('biniyog_kotiyan'); ?>">
								<span>পণ্য বিক্রয় /বিনিয়োগ খতিয়ান  </span>
							</a>
						</li>
						<li>
							<a href="<?= base_url('biniyog_paper_return'); ?>">
								<span>জমাকৃত কাজগপত্র ফেরত </span>
							</a>
						</li>
						<li>
							<a href="<?= base_url('biniyog_paper_return_list'); ?>">
								<span>কাজগপত্র ফেরত তালিকা</span>
							</a>
						</li>
						<li>
							<a href="<?= base_url('biniyog_grohonkarir_chahida'); ?>">
								<span>বিনিয়োগ গ্রহণকারীর চাহিদা    </span>
							</a>
						</li>
						<li>
							<a href="<?= base_url('biniyog_grohonkarir_chahida_list'); ?>">
								<span>বিনিয়োগ চাহিদা  তালিকা  </span>
							</a>
						</li>
						<li>
							<a href="<?= base_url('ponno_kroy_bikry_chukti'); ?>">
								<span> পণ্য ক্রয়  / বিক্রয় চুক্তিনামা   </span>
							</a>
						</li>
						<li>
							<a href="<?= base_url('ponno_kroy_bikry_chukti_list'); ?>">
								<span>  ক্রয়  / বিক্রয় চুক্তিনামা  তালিকা </span>
							</a>
						</li>
						<li>
							<a href="<?= base_url('sodorsho_protahar'); ?>">
								<span> সদস্য প্রত্যাহার  </span>
							</a>
						</li>
						<li>
							<a href="<?= base_url('sodorsho_protahar_list'); ?>">
								<span> সদস্য প্রত্যাহার তালিকা </span>
							</a>
						</li>
						<li>
							<a href="<?= base_url('code_61'); ?>">
								<span>বিনিয়োগ ফাইলে সম্পৃক্ত তথ্য</span>
							</a>
						</li>


						<li>
							<a href="<?= base_url('c_38'); ?>">
								<span>পণ্য বিক্রি ও বিনিয়োগ প্রতিবেদন</span>
							</a>
						</li>
						<li>
							<a href="<?= base_url('c_39'); ?>">
								<span>বিনিয়োগের মাসিক প্রতিবেদন</span>
							</a>
						</li>
						<li>
							<a href="<?= base_url('c_46'); ?>">
								<span>পণ্য বিক্রয় /বিনিয়োগ পরিশোধ </span>
							</a>
						</li>
						<li>
							<a href="<?= base_url('code_16'); ?>">
								<span>দৈনিক কিস্তি আদায় </span>
							</a>
						</li>


						<li>
							<a href="<?= base_url('c_147'); ?>">
								<span>দৈনিক বকেয়া কিস্তির তালিকা</span>
							</a>
						</li>
						<li>
							<a href="<?= base_url('c_146'); ?>">
								<span>দৈনিক কিস্তির পরিমান তালিকা</span>
							</a>
						</li>
						<li>
							<a href="<?= base_url('c_145'); ?>">
								<span>দৈনিক কিস্তির রেজিস্টার </span>
							</a>
						</li>


						<li>
							<a href="<?= base_url('c_22'); ?>">
								<span>কর্মসংস্তান ও উৎপাদন তথ্য </span>
							</a>
						</li>


						<li>
							<a href="<?= base_url('c_47'); ?>">
								<span>বিনিয়োগ বকেয়া ও খেলাপী </span>
							</a>
						</li>
						<li>
							<a href="<?= base_url('c_48'); ?>">
								<span>মাসিক বকেয়ার হার</span>
							</a>
						</li>
						<li>
							<a href="<?= base_url('code_49'); ?>">
								<span>দৈনিক বিনিয়োগের কিস্তি</span>
							</a>
						</li>


						<li>
							<a href="<?= base_url('c_13'); ?>">
								<span>পুনরায় বিনিয়োগ নেয়ার অনুমতি</span>
							</a>
						</li>
						<li>
							<a href="<?= base_url('c_33'); ?>">
								<span>মূল্যায়ন প্রতিবেদন </span>
							</a>
						</li>
						<li>
							<a href="<?= base_url('c_33'); ?>">
								<span>মূল্যায়ন প্রতিবেদন </span>
							</a>
						</li>


					</ul>
				</li>

				<li class="sub-menu">
					<a href="javascript:">
						<i class="fa fa-sitemap"></i>
						<span> Service </span>
					</a>
					<ul class="sub">
						<li>
							<a href="<?= base_url('code_192'); ?>">
								<span>code_192 </span>
							</a>
						</li>
						<li>
							<a href="<?= base_url('code_90'); ?>">
								<span>code_90 </span>
							</a>
						</li>
						<li>
							<a href="<?= base_url('code_95'); ?>">
								<span>code_95 </span>
							</a>
						</li>
						<li>
							<a href="<?= base_url('c_151'); ?>">
								<span>c_151 </span>
							</a>
						</li>
						<li>
							<a href="<?= base_url('c_69'); ?>">
								<span>c_69 </span>
							</a>
						</li>
						<li>
							<a href="<?= base_url('c_186'); ?>">
								<span>c_186 </span>
							</a>
						</li>
						<li>
							<a href="<?= base_url('c_158'); ?>">
								<span>c_158 </span>
							</a>
						</li>
						<li>
							<a href="<?= base_url('c_76'); ?>">
								<span>c_76</span>
							</a>
						</li>
						<li>
							<a href="<?= base_url('c_162'); ?>">
								<span>c_162</span>
							</a>
						</li>
						<li>
							<a href="<?= base_url('code_63'); ?>">
								<span>code_63</span>
							</a>
						</li>

					</ul>
				</li>

				<li class="sub-menu">
					<a href="javascript:;">
						<i class="fa fa-sitemap"></i>
						<span> ইন্সপেকশন </span>
					</a>
					<ul class="sub">
						<li>
							<a href="<?= base_url('code_23'); ?>">
								<span>অধীনস্থকারীর ভুল/ত্রূটি</span>
							</a>
						</li>
						<li>
							<a href="<?= base_url('c_144'); ?>">
								<span>সমিতি পরিদর্শন </span>
							</a>
						</li>

					</ul>
				</li>
				<li class="sub-menu">
					<a href="javascript:;">
						<i class="fa fa-sitemap"></i>
						<span> Others </span>
					</a>
					<ul class="sub">
						<li>
							<a href="<?= base_url('c_136'); ?>">
								<span>ভ্রমণভাতা </span>
							</a>
						</li>


					</ul>
				</li>
				<li class="sub-menu">
					<a href="javascript:;">
						<i class="fa fa-sitemap"></i>
						<span> Audit </span>
					</a>
					<ul class="sub">
						<li>
							<a href="#">
								<span>Internal audit </span>
							</a>
						</li>
						<li>
							<a href="#">
								<span>Co-operative/Govt. Audit  </span>
							</a>
						</li>


					</ul>
				</li>
				<!--				<li>-->
				<!--					<a href="#">-->
				<!--						<i class="fa fa-sitemap"></i>-->
				<!--						<span> সদস্য  উইথড্রও  লিস্ট </span>-->
				<!--					</a>-->
				<!--				</li>-->


				<!--				<li class="sub-menu">-->
				<!--					<a href="javascript:;">-->
				<!--						<i class="fa fa-sitemap"></i>-->
				<!--						<span> সদস্য ডিপোজিট </span>-->
				<!--					</a>-->
				<!--					<ul class="sub">-->
				<!--						<li>-->
				<!--							<a href="#">-->
				<!--								<i class="fa fa-sitemap"></i>-->
				<!--								<span> এককালীন আমানতের নীতিমালা  </span>-->
				<!--							</a>-->
				<!--						</li>-->
				<!--						<li>-->
				<!--							<a href="#">-->
				<!--								<span>আমানত গ্রহণের নীতিমালা </span>-->
				<!--							</a>-->
				<!--						</li>-->
				<!--						<li>-->
				<!--							<a href="#">-->
				<!--								<span>সদস্য নীতিমালা</span>-->
				<!--							</a>-->
				<!--						</li>-->
				<!--						<li>-->
				<!--							<a href="#">-->
				<!--								<span>মাসিক সঞ্চয় আমানতের নীতিমালা </span>-->
				<!--							</a>-->
				<!--						</li>-->
				<!---->
				<!---->
				<!--					</ul>-->
				<!--				</li>-->


				<!--				<li>-->
				<!---->
				<!--					<a href="#">-->
				<!--						<i class="fa fa-sitemap"></i>-->
				<!--						<span> সদস্য ডিপোজিট উইথড্রও </span>-->
				<!--					</a>-->
				<!--				</li>-->
				<!--				<li>-->
				<!---->
				<!--					<a href="--><? //= base_url('trade/DataAccess/sonchy_form'); ?><!--">-->
				<!--						<i class="fa fa-sitemap"></i>-->
				<!--						<span> খাত ভিত্তিক মাসিক সদস্য প্রতিবেদন  </span>-->
				<!--					</a>-->
				<!--				</li>-->
				<!--				<li>-->
				<!---->
				<!--					<a href="#">-->
				<!--						<i class="fa fa-sitemap"></i>-->
				<!--						<span> সদস্য পদের জমাকৃত টাকা উত্তলোনের চাহিদা  </span>-->
				<!--					</a>-->
				<!--				</li>-->


			</ul>
			<!-- sidebar menu end-->
		</div>
	</aside>

	<section id="main-content">
		<section class="wrapper">
			<?= $content; ?>
		</section>
	</section>

	<footer class="site-footer">
		<div class="text-center">
			2020 &copy; RTSoftbd.
			<a href="#" class="go-top">
				<i class="fa fa-angle-up"></i>
			</a>
		</div>
	</footer>

</section>

<!-- js placed at the end of the document so the pages load faster -->
<script src="<?= base_url('assets/admin/js/jquery.js'); ?>"></script>
<script src="<?= base_url('assets/admin/js/bootstrap.min.js'); ?>"></script>
<script class="include" type="text/javascript"
		src="<?= base_url('assets/admin/js/jquery.dcjqaccordion.2.7.js'); ?>"></script>
<script src="<?= base_url('assets/admin/js/jquery.scrollTo.min.js'); ?>"></script>
<script src="<?= base_url('assets/admin/js/jquery.nicescroll.js'); ?>" type="text/javascript"></script>
<script src="<?= base_url('assets/admin/js/jquery.sparkline.js'); ?>" type="text/javascript"></script>

<script src="<?php echo base_url('assets/admin/css/jquery-easy-pie-chart/jquery.easy-pie-chart.js'); ?>"></script>

<script src="<?= base_url('assets/admin/js/owl.carousel.js'); ?>"></script>
<script src="<?= base_url('assets/admin/js/jquery.customSelect.min.js'); ?>"></script>
<script src="<?= base_url('assets/admin/js/respond.min.js'); ?>"></script>

<!--right slidebar-->
<script src="<?= base_url('assets/admin/js/slidebars.min.js'); ?>"></script>

<!--common script for all pages-->
<script src="<?= base_url('assets/admin/js/common-scripts.js'); ?>"></script>

<!--script for this page-->
<script src="<?= base_url('assets/admin/js/sparkline-chart.js'); ?>"></script>
<script src="<?= base_url('assets/admin/js/easy-pie-chart.js'); ?>"></script>
<script src="<?= base_url('assets/admin/js/count.js'); ?>"></script>

<!--file Upload -->
<script type="text/javascript"
		src="<?php echo base_url('assets/admin/css/bootstrap-fileupload/bootstrap-fileupload.js'); ?>"></script>

<script type="text/javascript"
		src="<?php echo base_url('assets/admin/other/bootstrap-datepicker/js/bootstrap-datepicker.js'); ?>"></script>

<script src="<?php echo base_url('assets/admin/js/advanced-form-components.js'); ?>"></script>
<script>
	//owl carousel

	$(document).ready(function () {
		$("#owl-demo").owlCarousel({
			navigation: true,
			slideSpeed: 300,
			paginationSpeed: 400,
			singleItem: true,
			autoPlay: true

		});
	});

	//custom select box

	$(function () {
		$('select.styled').customSelect();
	});

	$(window).on("resize", function () {
		var owl = $("#owl-demo").data("owlCarousel");
		owl.reinit();
	});

</script>
</body>
</html>
