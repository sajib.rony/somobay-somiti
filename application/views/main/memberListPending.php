<div class="col-md-12">
	<h1 class="page-header"><i class="fa fa-arrow-circle-o-right"></i> মুলতুবি থাকা সদস্য লিস্ট
		<!--		<div class="btn-group pull-right">-->
		<!--			<a href="-->
		<?php //echo base_url('') ?><!--/addMember" class="btn btn-primary btn-sm"> সদস্য ভর্তি </a>-->
		<!--		</div>-->
	</h1>

	<div class="top10">&nbsp;</div>


	<form action="#" method="post" accept-charset="utf-8" class="form-inline filter" role="form"><input type="hidden"
																										name="uri"
																										value="http://sbcos.ltd/admin/manage/students">
		<div class="form-group">
			<label for="row">প্রদর্শনী</label><br>
			<select class="form-control input-xs" name="rows"
					onchange="window.location='http://sbcos.ltd/admin/manage/students/'+this.value+'/0'">
				<option>10</option>
				<option selected="">50</option>
				<option>100</option>
				<option>150</option>
				<option>200</option>
				<option>400</option>
				<option>500</option>
			</select>
		</div>

		<div class="form-group">
			<label for="fullname">নাম</label><br>
			<input type="text" class="form-control input-xs" name="fullname" id="fullname" placeholder="নাম" size="25"
				   value="">
		</div>
		<div class="form-group">
			<label for="mobile">মোবাইল </label><br>
			<input type="text" class="form-control input-xs" name="mobile" id="mobile" placeholder="মোবাইল"
				   size="18" value="" onkeypress="return event.charCode >= 48 &amp;&amp; event.charCode <= 57">
		</div>
		<div class="form-group">
			<label for="email">সদস্য নম্বর</label><br>
			<input type="text" class="form-control input-xs" name="email" id="email" placeholder="সদস্য নম্বর"
				   size="18" value="">
		</div>
		<div class="form-group">
			<label for="email">তারিখ শুরু : </label><br>
			<input type="date" class="form-control input-xs" name="email" id="email" size="18" value="">
		</div>
		<div class="form-group">
			<label for="email">তারিখ শেষ : </label><br>
			<input type="date" class="form-control input-xs" name="email" id="email" size="18" value="">
		</div>
		<div class="form-group">
			<label>স্ট্যাটাস</label><br>
			<select name="status" class="form-control input-xs">
				<option value="">---select---</option>
				<option value="0">Pending</option>
				<option value="1">Approved</option>
			</select>
		</div>
		<div class="form-group">
			<label class="" for="">&nbsp;</label><br>
			<button type="submit" class="btn btn-danger btn-xs"> Filter</button>
		</div>
	</form>
	<div class="top10">&nbsp;</div>
	<form action="#" role="form" name="dataform" method="post" accept-charset="utf-8">
		<div class="table-responsive">


			<table class="table table-striped table-hover">
				<thead>
				<tr>
					<th class="sort_asc"><a href="http://sbcos.ltd/admin/manage/students/50/0/id/desc">সদস্য নম্বর</a>
					</th>

					<th><a href="http://sbcos.ltd/admin/manage/students/50/0/fullname/asc">পুরো নাম</a></th>
					<th><a href="http://sbcos.ltd/admin/manage/students/50/0/present_address/asc">ঠিকানা</a></th>
					<th><a href="http://sbcos.ltd/admin/manage/students/50/0/mobile/asc">মোবাইল </a></th>
					<th><a href="http://sbcos.ltd/admin/manage/students/50/0/email/asc">পেশা</a></th>
					<th><a href="http://sbcos.ltd/admin/manage/students/50/0/email/asc">তারিখ</a></th>
					<th><a href="http://sbcos.ltd/admin/manage/students/50/0/email/asc">স্ট্যাটাস </a></th>
					<!--				<th><a href="http://sbcos.ltd/admin/manage/students/50/0/status/asc">Status</a></th>-->
					<th width="80">Action</th>
				</tr>
				</thead>
				<tbody>

				<tr>
					<td>64586584</td>
					<!--<td>1</td>-->

					<td><a href="#">শুভ্র হাসান </a></td>

					<td>চাঁদপুর</td>
					<td>1683731580</td>
					<td>ব্যবসা</td>
					<td>07/08/2020</td>
					<td>Pending</td>
					<td>
						<a href="<?php echo base_url('memberListView') ?>">
							<button type="button" class="btn btn-primary btn-xs"> View</button>
						</a>
					</td>
				</tr>
				<tr>
					<td>30322869</td>
					<td><a href="#">কামাল মোল্লা</a>
					</td>

					<td>চাঁদপুর</td>
					<td>1683731580</td>
					<td>ব্যবসা</td>
					<td>07/08/2020</td>
					<td>Pending</td>
					<td>
						<a href="<?php echo base_url('memberListView') ?>">
							<button type="button" class="btn btn-primary btn-xs"> View</button>
						</a>
					</td>
				</tr>


				</tbody>
			</table>
		</div>
		<div class="row" style="margin-top:5px;">
			<div class="col-md-6"><p class="pageinfo">Page 1 of 1 (Showing 1 to 4 of 4 records)</p></div>
			<div class="col-md-6">
			</div>
		</div>
	</form>
</div>
