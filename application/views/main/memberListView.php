<html>
<body>
<div style="background-color:#4A235A">
	<div class="row">
		<div class="col-md-12">
			<div class="col-md-4"></div>
			<div class="col-md-4">
				<center><h2 style="color: white"><b>সদস্য সকল তথ্য </b></h2></center>
			</div>
			<div class="col-md-4"></div>
		</div>
	</div>
</div>
<div style="background-color:#F1C40F">
	<div class="row">
		<div class="col-md-12">
			<?php echo "<br>";?>
		</div>
	</div>
</div>
<div style="background-color:#D1F2EB">

	<?php echo "<br>"; ?>
	<div class="row">
		<div class="col-md-12">
			<div class="col-md-6">
				<div class="row">
					<div class="col-md-12">
						<span><br></span>
						<div class="col-md-3"></div>
						<div class="col-md-3">
							<h4 style="color:black">সদস্য নম্বর</h4>
						</div>
						<div class="col-md-6">
							<h4 style="color:black">: &nbsp 75534831</h4>
						</div>

					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<span><br></span>
						<div class="col-md-3"></div>
						<div class="col-md-3">
							<h4 style="color:black"> সদস্য নাম </h4>
						</div>
						<div class="col-md-6">
							<h4 style="color:black">: &nbsp রাজু আলম </h4>
						</div>

					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="row">
					<div class="col-md-12">
						<span><br></span>

						<div class="col-md-4">
							<h4 style="color:black">পিতা /স্বামীর নাম</h4>
						</div>
						<div class="col-md-8">
							<h4 style="color:black">: &nbsp শুভ্র হাসান </h4>
						</div>

					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<span><br></span>
						<div class="col-md-4">
							<h4 style="color:black"> মাতার নাম </h4>
						</div>
						<div class="col-md-8">
							<h4 style="color:black">: &nbsp সুফিয়া বেগম </h4>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>


	<div class="row">
		<div class="col-md-12">
			<div class="col-md-6 col-md-push-5">

				<br><br><h4 style="color:black"><b> স্থায়ী ঠিকানা </b></h4></div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="col-md-6">
				<div class="row">
					<div class="col-md-12">
						<span><br></span>
						<div class="col-md-3"></div>
						<div class="col-md-3">
							<h4 style="color:black"> গ্রাম </h4>
						</div>
						<div class="col-md-6">
							<h4 style="color:black">:&nbsp চাঁদপুর</h4>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<span><br></span>
						<div class="col-md-3"></div>
						<div class="col-md-3">
							<h4 style="color:black"> বাড়ি </h4>
						</div>
						<div class="col-md-6">
							<h4 style="color:black">:&nbsp চাঁদপুর</h4>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<span><br></span>
						<div class="col-md-3"></div>
						<div class="col-md-3">
							<h4 style="color:black"> জেলা </h4>
						</div>
						<div class="col-md-6">
							<h4 style="color:black">:&nbsp চাঁদপুর</h4>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<span><br></span>
						<div class="col-md-3"></div>
						<div class="col-md-3">
							<h4 style="color:black"> বর্তমান ঠিকানা </h4>
						</div>
						<div class="col-md-5">
							<h4 style="color:black">:&nbsp চাঁদপুর</h4>
						</div>
					</div>
				</div>

			</div>
			<div class="col-md-6">
				<div class="row">
					<div class="col-md-12">
						<span><br></span>

						<div class="col-md-4">
							<h4 style="color:black"> পোস্ট </h4>
						</div>
						<div class="col-md-8">
							<h4 style="color:black">:&nbsp চাঁদপুর</h4>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<span><br></span>

						<div class="col-md-4">
							<h4 style="color:black"> উপজেলা </h4>
						</div>
						<div class="col-md-8">
							<h4 style="color:black">:&nbsp চাঁদপুর</h4>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<span><br></span>

						<div class="col-md-4">
							<h4 style="color:black"><span> &nbsp</span></h4>
						</div>
						<div class="col-md-8">
							<h4 style="color:black"><span></span></h4>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<span><br></span>

						<div class="col-md-4">
							<h4 style="color:black"> যোগাযোগ </h4>
						</div>
						<div class="col-md-8">
							<h4 style="color:black">:&nbsp 0192827734</h4>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="col-md-6 col-md-push-5">

				<br><br><h4 style="color:black"><b>অন্যান্য তথ্য</b></h4></div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="col-md-6">
				<div class="row">
					<div class="col-md-12">
						<span><br></span>
						<div class="col-md-3"></div>
						<div class="col-md-3">
							<h4 style="color:black"> বয়স </h4>
						</div>
						<div class="col-md-6">
							<h4 style="color:black">:&nbsp ৫০</h4>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-12">
						<span><br></span>
						<div class="col-md-3"></div>
						<div class="col-md-3">
							<h4 style="color:black"> ধর্ম </h4>
						</div>
						<div class="col-md-6">
							<h4 style="color:black">:&nbsp ইসলাম</h4>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="row">
					<div class="col-md-12">
						<span><br></span>
						<div class="col-md-4">
							<h4 style="color:black"> পেশা </h4>
						</div>
						<div class="col-md-8">
							<h4 style="color:black">:&nbsp ব্যবসা</h4>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


	<div class="row">
		<div class="col-md-12">


			<center><br><br><h4 style="color:black"><b> বিশেষ নির্দেশ:আমার মৃত্যুর পর শুধুমাত্র তারাই অত্র হিসাবের
						গচ্ছিত
						টাকার মালিক হইবে। </b></h4>

			</center>
		</div>

	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="col-md-6">
				<div class="row">
					<div class="col-md-12">
						<span><br></span>
						<div class="col-md-3"></div>
						<div class="col-md-3">
							<h4 style="color:black"> পূর্ণ নাম </h4>
						</div>
						<div class="col-md-6">
							<h4 style="color:black">:&nbsp আলম</h4>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-12">
						<span><br></span>
						<div class="col-md-3"></div>
						<div class="col-md-3">
							<h4 style="color:black"> মাতার নাম </h4>
						</div>
						<div class="col-md-6">
							<h4 style="color:black">:&nbsp সুফিয়া বেগম </h4>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<span><br></span>
						<div class="col-md-3"></div>
						<div class="col-md-3">
							<h4 style="color:black"> সদস্যের সাথে সম্পর্ক </h4>
						</div>
						<div class="col-md-6">
							<h4 style="color:black">:&nbsp ভাই </h4>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="row">
					<div class="col-md-12">
						<span><br></span>
						<div class="col-md-4">
							<h4 style="color:black"> পিতা /স্বামীর নাম </h4>
						</div>
						<div class="col-md-8">
							<h4 style="color:black">:&nbsp শুভ্র হাসান </h4>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<span><br></span>
						<div class="col-md-4">
							<h4 style="color:black">বর্তমান ঠিকানা </h4>
						</div>
						<div class="col-md-8">
							<h4 style="color:black">:&nbsp চাঁদপুর </h4>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<span><br></span>
						<div class="col-md-4">
							<h4 style="color:black">মোবাইল/ফোন</h4>
						</div>
						<div class="col-md-8">
							<h4 style="color:black">:&nbsp 0112984142 </h4>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<span><br></span>

</div>

<div style="background-color:#4A235A">
	<div class="row">
		<div class="col-md-12">

			<h6 style="color: white"><b>আমি এই মর্মে অঙ্গীকার করছি যে , উপরে উল্লেখিত সকল তথ্যাবলী সত্য ও
					সঠিক। সমিতির হিসাবে আমার নাম অন্তুর্ভুক্ত করা হলে সমিতির সকল নিয়ম ও কর্তৃপক্ষের যে কোন
					সিদ্ধান্ত মেনে নিব।</b></h6>
		</div>

	</div>
</div>
</div>
</body>
</html>
