<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<title>ERP System</title>
	<!--	<link rel="shortcut icon" href="#" type="image/x-icon">-->
	<!-- Bootstrap -->
	<link href="<?php echo base_url('assets/landing_page/css/bootstrap.min.css') ?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/landing_page/css/font-awesome.min.css') ?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/landing_page/css/owl.carousel.min.css') ?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/landing_page/css/owl.theme.default.css') ?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/landing_page/css/style.css') ?>" rel="stylesheet">
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

	<!--
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/login.css'); ?>">
    -->

	<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/login/css/util.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/login/css/main.css">


	<style type="text/css">
		.hvr-grow {
			display: inline-block;
			vertical-align: middle;
			-webkit-transform: perspective(1px) translateZ(0);
			transform: perspective(1px) translateZ(0);
			box-shadow: 0 0 1px transparent;
			-webkit-transition-duration: 0.3s;
			transition-duration: 0.3s;
			-webkit-transition-property: transform;
			transition-property: transform;
		}

		.hvr-grow:hover, .hvr-grow:focus, .hvr-grow:active {
			-webkit-transform: scale(1.05);
			transform: scale(1.05);
		}

		.panel-body h4 ul li {
			margin-left: 30px;
		}


	</style>

	<script>
		window.onload = function () {

			var chart = new CanvasJS.Chart("chartContainer", {
				animationEnabled: true,
				theme: "light1", // "light1", "light2", "dark1", "dark2"
				title: {
					text: "চার্ট"
				},
				axisY: {
					title: "চারট ডেটা %",
					suffix: "%",
					includeZero: false
				},
				axisX: {
					title: "Countries"
				},
				data: [{
					type: "column",
					yValueFormatString: "#,##0.0#\"%\"",
					dataPoints: [
						{label: "আমানতগঞ্জ", y: 6.70},
						{label: "়া হাটখোলা ", y: 3.50},
						{label: "লাকুটিয়া", y: 2.30},
						{label: " কাশীপুর", y: 4.60},
						{label: "কুয়াকাটা", y: 1.60}
					]
				}]
			});
			chart.render();

		}
	</script>

	<style>
		.box {
			background-color: white;
			width: 600px;
			height: 600px;
			border: 15px solid #784212;
			padding: 50px;
			margin: 20px;
		}
	</style>

</head>


<body>


<div class="container">
	<div class="" style="background-color:#4A235A">
		<div class="row">
			<div class="col-md-12">
				<?php echo "<br>";?>
				<center><h3 style="color:white">শিক্ষিত বেকার যুব বহুমুখী সমবায় সমিতি লিঃ</h3></center>
				<center><h3 style="color:white">কর্মসংস্থান ও দারিদ্র বিমোচন প্রকল্প</h3></center>
				<center><h5 style="color:white">নিবন্ধন নম্বর -১৩/চাঁদ /১২, তারিখ :০৯/০৭/২০১২ </h5></center>
				<center><h6 style="color:white">ঠিকানা: ঠাকুর বাজার ,শাহারাস্তি , চাঁদপুর। </h6></center>
				<br>
			</div>
		</div>
	</div>


	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<div class="login100-form-title"
					 style="background-image: url(<?php echo base_url() ?>assets/login/images/bg-01.jpg);">
					<span class="login100-form-title-1">
						Sign In
					</span>
				</div>
				<form class="login100-form validate-form" method="post"
					  action="<?php echo base_url('main/Main_controller/admin_login_check') ?>"
					  name="login_form" onSubmit="return validateForm()">

					<div class="wrap-input100 validate-input m-b-26" data-validate="Username is required">
						<span class="label-input100">Username</span>
						<input class="input100" type="text" name="username" placeholder="Enter username">
						<span class="focus-input100"></span>
					</div>

					<div class="wrap-input100 validate-input m-b-18" data-validate="Password is required">
						<span class="label-input100">Password</span>
						<input class="input100" type="password" name="pass" placeholder="Enter password">
						<span class="focus-input100"></span>
					</div>
					<div class="wrap-input100 validate-input m-b-18" >
						<span class="label-input100">Select Type</span>
						<select class="form-control" name="SystemAdminListID">


							<option value="Operator">Operator</option>
							<option value="Accountant">Accountant</option>
							<option value="General">General</option>

						</select>
					</div>

					<div class="flex-sb-m w-full p-b-30">
						<div class="contact100-form-checkbox">
							<input class="input-checkbox100" id="ckb1" type="checkbox" name="remember-me">
							<label class="label-checkbox100" for="ckb1">
								Remember me
							</label>
						</div>

						<div>
							<a href="#" class="txt1">
								Forgot Password?
							</a>
						</div>
					</div>

					<div class="container-login100-form-btn">
						<button class="login100-form-btn">
							Login
						</button>
<!--						style="background: #dd3333"-->
					</div>
				</form>
			</div>
		</div>
	</div>


<!--	<div class="" style="background-color:#fffff5">-->
<!--		<div class="center">-->
<!---->
<!--			<div class="row">-->
<!--				<div class="col-md-4"></div>-->
<!---->
<!--				<div class="col-md-4">-->
<!---->
<!--					<div class="right-panel ">-->
<!--						<h2 class="text-center ">Login Form </h2>-->
<!--						<br>-->
<!--						<form class="login-form" method="post"-->
<!--							  action="--><?php //echo base_url('main/Main_controller/admin_login_check') ?><!--"-->
<!--							  name="login_form" onSubmit="return validateForm()">-->
<!--							<div class="main-content">-->
<!--								<div class="input-group ">-->
<!--									<span class="input-group-addon">User Name</span>-->
<!--									<input id="email" type="text" class="form-control" name="UserName"-->
<!--										   placeholder="Enter your Username">-->
<!--									<b id="NameErrMgs" style="color: red;"></b>-->
<!--								</div>-->
<!--								<br>-->
<!--								<div class="input-group">-->
<!--                                                <span class="input-group-addon"-->
<!--													  style="padding-right: 24px;"> Password </span>-->
<!--									<input id="password" type="password" class="form-control"-->
<!--										   name="Password" placeholder="Enter your Password">-->
<!--									<b id="PwErrMgs" style="color: red;"></b>-->
<!--								</div>-->
<!--								<br>-->
<!--								<div class="input-group">-->
<!--									<span class="input-group-addon">Select Type</span>-->
<!--									<select class="form-control" name="SystemAdminListID">-->
<!---->
<!--										<option value="Operator">Operator</option>-->
<!--										<option value="Accountant">Accountant</option>-->
<!--										<option value="General">General</option>-->
<!---->
<!--									</select>-->
<!--								</div>-->
<!--								<br>-->
<!--								<div class="form-group ">-->
<!--									<button class="btn btn-info btn-lg btn-block login-button"-->
<!--											type="submit" style="background: #dd3333">Login-->
<!--									</button>-->
<!--								</div>-->
<!---->
<!--							</div>-->
<!--						</form>-->
<!--					</div>-->
<!---->
<!--				</div>-->
<!--			</div>-->
<!--		</div>-->
<!--		--><?php //echo "<br><br><br><br><br><br>" ?>
<!--	</div>-->


	<div class="footer-area">
		<div class="row">
			<div class="col-md-6">
				Copyright &copy; 2020 - Design & Developed By <a href="">RTSOFTBD</a>
			</div>
			<div class="col-md-6">
				<div class="right">
				</div>
			</div>
		</div>
	</div>
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="<?php echo base_url('assets/landing_page/js/bootstrap.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/landing_page/js/owl.carousel.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/landing_page/js/active.js'); ?>"></script>

<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
</body>
</html>
