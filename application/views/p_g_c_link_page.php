<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<title>ERP System</title>
	<link rel="shortcut icon" href="#" type="image/x-icon">
	<link href='https://fonts.googleapis.com/css?family=Yellowtail' rel='stylesheet'>
	<!-- Bootstrap -->
	<link href="<?php echo base_url('assets/landing_page/css/bootstrap.min.css') ?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/landing_page/css/font-awesome.min.css') ?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/landing_page/css/owl.carousel.min.css') ?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/landing_page/css/owl.theme.default.css') ?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/landing_page/css/style.css') ?>" rel="stylesheet">
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

	<!--
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/login.css'); ?>">
    -->
	<style type="text/css">
		.hvr-grow {
			display: inline-block;
			vertical-align: middle;
			-webkit-transform: perspective(1px) translateZ(0);
			transform: perspective(1px) translateZ(0);
			box-shadow: 0 0 1px transparent;
			-webkit-transition-duration: 0.3s;
			transition-duration: 0.3s;
			-webkit-transition-property: transform;
			transition-property: transform;
		}

		.hvr-grow:hover, .hvr-grow:focus, .hvr-grow:active {
			-webkit-transform: scale(1.05);
			transform: scale(1.05);
		}

		.panel-body h4 ul li {
			margin-left: 30px;
		}

		.box {
			background-color: white;
			width: 300px;
			height: 300px;
			border: 15px solid #784212;
			padding: 50px;
			margin: 20px;
		}

		.text-style1 {
			color: #d04764;
			font-family: 'Lobster', cursive;

			font-size: 36px;
			font-weight: normal;
			line-height: 48px;
			margin: 0 0 18px;
			text-shadow: 1px 0 0 #fff;

		}

		a {
			color: white;
			font-family: 'Yellowtail';
			font-size: 5vw;
		}


		a:hover {
			text-decoration: underline
		}

		.box-background_1 {
			background-image: url("assets/images/bg_1.jpg");
			width: auto;
			height: auto;
		}

		.box-background_2 {
			background-image: url("assets/images/bg_2.jpg");
			/*background-color: #cccccc;*/
			width: auto;
			height: auto;
		}

		.box-background_3 {
			background-image: url("assets/images/bg_3.jpg");
			/*background-color: #cccccc;*/
			width: auto;
			height: auto;
		}


	</style>

	<script>
		window.onload = function () {

			var chart = new CanvasJS.Chart("chartContainer", {
				animationEnabled: true,
				theme: "light1", // "light1", "light2", "dark1", "dark2"
				title: {
					text: "চার্ট"
				},
				axisY: {
					title: "চারট ডেটা %",
					suffix: "%",
					includeZero: false
				},
				axisX: {
					title: "Countries"
				},
				data: [{
					type: "column",
					yValueFormatString: "#,##0.0#\"%\"",
					dataPoints: [
						{label: "আমানতগঞ্জ", y: 6.70},
						{label: "়া হাটখোলা ", y: 3.50},
						{label: "লাকুটিয়া", y: 2.30},
						{label: " কাশীপুর", y: 4.60},
						{label: "কুয়াকাটা", y: 1.60}
					]
				}]
			});
			chart.render();

		}
	</script>
</head>


<body>
<div class="container">
	<div class="" style="background-color:#4A235A">
		<div class="row">
			<div class="col-md-12">
				<center><h3 style="color:white">শিক্ষিত বেকার যুব বহুমুখী সমবায় সমিতি লিঃ</h3></center>
				<center><h3 style="color:white">কর্মসংস্থান ও দারিদ্র বিমোচন প্রকল্প</h3></center>
				<center><h5 style="color:white">নিবন্ধন নম্বর -১৩/চাঁদ /১২, তারিখ :০৯/০৭/২০১২ </h5></center>
				<center><h6 style="color:white">ঠিকানা: ঠাকুর বাজার ,শাহারাস্তি , চাঁদপুর। </h6></center>
				<br>

				<div style="background-color:#FEF5E7">
					<?php echo "<br><br><br>" ?>
					<div class="row">
						<div class="col-md-12">
							<div class="col-md-6">

								<div class="box box-background_1 ">
									<br><br>
									<h2 style="text-align:center;" class="text-style1"><a
												href="<?php echo base_url('dashboard') ?>">Project</a>
									</h2>
								</div>

							</div>
							<div class="col-md-6">
								<div class="box box-background_2">
									<br><br>
									<h2 style="text-align:center" class="text-style1">
										<a href="<?php echo base_url('general-dashboard') ?>">General</a>
									</h2>
								</div>
							</div>
<!--							<div class="col-md-4">-->
<!--								<div class="box box-background_3">-->
<!--									<br><br>-->
<!--									<h2 style="text-align:center" class="text-style1"><a-->
<!--												href="--><?php //echo base_url('central-dashboard') ?><!--">Central</a>-->
<!--									</h2>-->
<!---->
<!--								</div>-->
<!--							</div>-->


						</div>
					</div>


					<?php echo "<br><br><br><br><br><br><br><br><br><br><br><br><br>" ?>
				</div>

				<div class="footer-area">
					<div class="row">
						<div class="col-md-6">
							Copyright &copy; 2020 - Design & Developed By <span>RTSOFTBD</span>
						</div>
						<div class="col-md-6">
							<div class="right">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="<?php echo base_url('assets/landing_page/js/bootstrap.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/landing_page/js/owl.carousel.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/landing_page/js/active.js'); ?>"></script>

<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
</body>
</html>
