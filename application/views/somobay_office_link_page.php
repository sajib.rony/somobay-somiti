<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<title>ERP System</title>
	<link rel="shortcut icon" href="#" type="image/x-icon">
	<link href='https://fonts.googleapis.com/css?family=Yellowtail' rel='stylesheet'>
	<!-- Bootstrap -->
	<link href="<?php echo base_url('assets/landing_page/css/bootstrap.min.css') ?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/landing_page/css/font-awesome.min.css') ?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/landing_page/css/owl.carousel.min.css') ?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/landing_page/css/owl.theme.default.css') ?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/landing_page/css/style.css') ?>" rel="stylesheet">
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	<!--button css link start-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/login/css/util.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/login/css/main.css">
	<!--button css link end-->
	<style type="text/css">
		.hvr-grow {
			display: inline-block;
			vertical-align: middle;
			-webkit-transform: perspective(1px) translateZ(0);
			transform: perspective(1px) translateZ(0);
			box-shadow: 0 0 1px transparent;
			-webkit-transition-duration: 0.3s;
			transition-duration: 0.3s;
			-webkit-transition-property: transform;
			transition-property: transform;
		}

		.hvr-grow:hover, .hvr-grow:focus, .hvr-grow:active {
			-webkit-transform: scale(1.05);
			transform: scale(1.05);
		}

		.panel-body h4 ul li {
			margin-left: 30px;
		}

		.box {
			background-color: white;
			/*background-image: url("assets/images/bg_o_1.jpg");*/
			width: auto;
			height: auto;
			border: 15px solid #784212;
			padding: 30px;
			margin: 20px;
		}

		.text-style1 {
			color: darkred;
			font-family: 'Lobster', Roboto;

			font-size: 20px;
			font-weight: normal;
			line-height: normal;
			margin: 0 0 18px;
			text-shadow: 1px 0 0 #fff;

		}

		.link:hover {
			color: #333333;
		}

		.link1:hover {
			background-color: #333333;
		}

		a {
			color: darkred;
			font-family: Roboto
			font-size: 2vw;
		}


		/*a:hover {*/
		/*	text-decoration: underline*/
		/*}*/

		.button-bg-color {
			background: #dd3333;
		}

		.button-bg-color1 {
			background: #3E8ABE;
		}

		.button-bg-color2 {

			background: #8CC600;
		}

		.text-align_1 {
			text-align: center;
		}

		.modal-dialog {
			max-width: 750px;
		}

		.modal-header {
			background-color: #337AB7;
			padding: 16px 16px;
			color: #FFF;
			border-bottom: 10px dashed #337AB7;
		}

		.close {
			color: #FFFFFF;
			opacity: 1;
		}
	</style>

	<script>
		window.onload = function () {

			var chart = new CanvasJS.Chart("chartContainer", {
				animationEnabled: true,
				theme: "light1", // "light1", "light2", "dark1", "dark2"
				title: {
					text: "চার্ট"
				},
				axisY: {
					title: "চারট ডেটা %",
					suffix: "%",
					includeZero: false
				},
				axisX: {
					title: "Countries"
				},
				data: [{
					type: "column",
					yValueFormatString: "#,##0.0#\"%\"",
					dataPoints: [
						{label: "আমানতগঞ্জ", y: 6.70},
						{label: "়া হাটখোলা ", y: 3.50},
						{label: "লাকুটিয়া", y: 2.30},
						{label: " কাশীপুর", y: 4.60},
						{label: "কুয়াকাটা", y: 1.60}
					]
				}]
			});
			chart.render();

		}
	</script>
</head>


<body>
<div class="container">
	<div class="" style="background-color:#4A235A">
		<div class="row">
			<div class="col-md-12">
				<center><h3 style="color:white">শিক্ষিত বেকার যুব বহুমুখী সমবায় সমিতি লিঃ</h3></center>
				<center><h3 style="color:white">কর্মসংস্থান ও দারিদ্র বিমোচন প্রকল্প</h3></center>
				<center><h5 style="color:white">নিবন্ধন নম্বর -১৩/চাঁদ /১২, তারিখ :০৯/০৭/২০১২ </h5></center>
				<center><h6 style="color:white">ঠিকানা: ঠাকুর বাজার ,শাহারাস্তি , চাঁদপুর। </h6></center>
				<br>

				<div style="background-color:white">
					<div class="col-md-12">
						<div class="col-md-2"></div>
						<div class="col-md-2">
							<p><br></p>
							<form action="<?php echo base_url('central-dashboard') ?>" method="post">
								<div class="container-login100-form-btn">
									<button class="login100-form-btn button-bg-color link1">
										Central Panel
									</button>
								</div>
							</form>
						</div>
						<div class="col-md-2">
							<p><br></p>
							<form action="<?php echo base_url('general-dashboard') ?>" method="post">
								<div class="container-login100-form-btn">
									<button class="login100-form-btn link1">
										General Panel
									</button>
								</div>
							</form>
						</div>
						<div class="col-md-2">
							<p><br></p>
							<form action="<?php echo base_url('dashboard') ?>" method="post">
								<div class="container-login100-form-btn">
									<button class="login100-form-btn button-bg-color2 link1">
										Project Panel
									</button>
								</div>
							</form>
						</div>

						<div class="col-md-2">
							<p><br></p>
							<div class="container-login100-form-btn">
								<button type="button" class="login100-form-btn button-bg-color1" data-toggle="modal"
										data-target="#branch_create">
									Branch Create
								</button>
							</div>
						</div>

						<div class="col-md-2">
							<p><br></p>
							<form action="<?php echo base_url('admin') ?>" method="post">
								<div class="container-login100-form-btn">
									<button class="login100-form-btn button-bg-color link1">
										Logout
									</button>
								</div>
							</form>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<?php


							$data['office_branch'] = [
									'ব্রাঞ্চ নম্বর ০১', 'ব্রাঞ্চ নম্বর ০২', 'ব্রাঞ্চ নম্বর ০৩', 'ব্রাঞ্চ নম্বর ০৪',
									'ব্রাঞ্চ নম্বর ০৫', 'ব্রাঞ্চ নম্বর ০৬', 'ব্রাঞ্চ নম্বর ০৭', 'ব্রাঞ্চ নম্বর ০৮',
									'ব্রাঞ্চ নম্বর ০৯', 'ব্রাঞ্চ নম্বর ১০', 'ব্রাঞ্চ নম্বর ১১', 'ব্রাঞ্চ নম্বর ১২',
									'ব্রাঞ্চ নম্বর ১৩', 'ব্রাঞ্চ নম্বর ১৪', 'ব্রাঞ্চ নম্বর ১৫', 'ব্রাঞ্চ নম্বর ১৬',
									'ব্রাঞ্চ নম্বর ১৭',

							];
							foreach ($data['office_branch'] as $value) {
								?>
								<div class="col-md-6">
									<div class="box">
										<a
												href="<?php echo base_url('link_page') ?>">
											<h2 style="text-align:center;" class="text-style1 link">শিক্ষিত বেকার
												যুব
												বহুমুখী
												সমবায় সমিতি লিঃ</h2>
											<h4 style="text-align:center; font-family: Roboto" class="link">কর্মসংস্থান
												ও দারিদ্র
												বিমোচন প্রকল্প</h4>
											<h6 style="text-align:center; font-family: Roboto" class="link">
												নিবন্ধন নম্বর -১৩/চাঁদ /১২, তারিখ :০৯/০৭/২০১২ </h6>
											<h6 style="text-align:center; font-family: Roboto" class="link">
												ঠিকানা: ঠাকুর বাজার ,শাহারাস্তি , চাঁদপুর।
											</h6>
											<br>
											<p style="text-align:center;"
											   class="text-style1 link text-align_1"><?php echo $value; ?></p>
										</a>
									</div>
								</div>
							<?php } ?>
						</div>
					</div>
					<?php echo "<br><br><br><br><br><br><br><br><br><br><br><br><br>" ?>
				</div>
				<div class="footer-area">
					<div class="row">
						<div class="col-md-6">
							Copyright &copy; 2020 - Design & Developed By <span>RTSOFTBD</span>
						</div>
						<div class="col-md-6">
							<div class="right">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
	<div class="modal" tabindex="-1" id="branch_create" role="dialog">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h2 class="modal-title text-align_1" style="font-family: Yellowtail;">New Branch Create</h2>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form action="" method="post">
						<div class="row" style="padding-bottom: 10px;">
							<div class="col-md-12">
								<p class="text-style1">ব্রাঞ্চ নাম</p>
								<textarea type="text" name="" class="form-control" placeholder="ব্রাঞ্চ নাম"></textarea>
							</div>
						</div>
						<div class="row" style="padding-bottom: 10px;">
							<div class="col-md-12">
								<p class="text-style1">প্রকল্প নাম </p>
								<textarea type="text" name="" class="form-control" placeholder="প্রকল্প নাম"></textarea>
							</div>
						</div>
						<div class="row" style="padding-bottom: 10px;">
							<div class="col-md-12">
								<p class="text-style1">নিবন্ধন নম্বর </p>
								<textarea type="text" name="" class="form-control"
										  placeholder="নিবন্ধন নম্বর "></textarea>
							</div>
						</div>
						<div class="row" style="padding-bottom: 10px;">
							<div class="col-md-12">
								<p class="text-style1">ঠিকানা</p>
								<textarea type="text" name="" class="form-control"
										  placeholder="ঠিকানা"></textarea>
							</div>
						</div>
						<div class="row" style="padding-bottom: 10px;">
							<div class="col-md-12">
								<p class="text-style1">ইমেইল</p>
								<input type="email" name="" class="form-control"
									   placeholder="demo@gmail.com">
							</div>
						</div>
						<div class="row" style="padding-bottom: 10px;">
							<div class="col-md-12">
								<p class="text-style1">Username</p>
								<input type="text" name="" class="form-control"
									   placeholder="username create">
							</div>
						</div>
						<div class="row" style="padding-bottom: 10px;">
							<div class="col-md-12">
								<p class="text-style1">Password</p>
								<input type="password" name="" class="form-control"
									   placeholder="********">
							</div>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<div class="row">
						<div class="col-md-12">
							<div class="col-md-4"></div>
							<div class="col-md-4">
								<div class="container-login100-form-btn">
									<button type="button" class="login100-form-btn">Add Branch</button>
								</div>
							</div>
							<div class="col-md-4">
								<div class="container-login100-form-btn">
									<button type="button" class="login100-form-btn button-bg-color link1"
											data-dismiss="modal">Close
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="<?php echo base_url('assets/landing_page/js/bootstrap.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/landing_page/js/owl.carousel.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/landing_page/js/active.js'); ?>"></script>

<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
</body>
</html>
